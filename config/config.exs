# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

# By default, the umbrella project as well as each child
# application will require this configuration file, ensuring
# they all use the same configuration. While one could
# configure all applications here, we prefer to delegate
# back to each application for organization purposes.
import_config "../apps/*/config/config.exs"

config :kernel,
  sync_nodes_optional: [
    :"knowledge-dev-nodeapp1@10.202.0.52",
    :"knowledge-dev-nodeapp2@10.202.0.53"
  ],
  sync_nodes_timeout: 10000

if Mix.env() == :prod do
  config :logger,
    level: :info,
    backends: [{LoggerLogstashBackend, :elastic_log}, LoggerJSON]
end

# Sample configuration (overrides the imported configuration above):
#
#     config :logger, :console,
#       level: :info,
#       format: "$date $time [$level] $metadata$message\n",
#       metadata: [:user_id]
