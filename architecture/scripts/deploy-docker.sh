#!/bin/bash

set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ../..

# If NO_TTY environment variable is unset then use tty
[ -z ${NO_TTY} ] && {
    TTY=t
}

docker run --rm -v $(pwd)/:/data/current \
  -v ~/.ssh:/data/.ssh \
  --net=host \
  --entrypoint "/data/current/architecture/scripts/deploy-docker-entrypoint.sh" \
  -i${TTY} registry.smile-magento.com/docker/ansistrano $@

