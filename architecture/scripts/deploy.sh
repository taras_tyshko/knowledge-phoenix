#!/bin/bash

set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

USAGE_ADDITIONAL_PARAMETER="[version]"

source scripts/_environment-without-lxc.sh
source scripts/_ansible.sh

if [ -z $1 ]  
  then usage
fi

EXTRA_VARS="deploy_version=$2"

# Run paybooks
ansible-galaxy install -r provisioning/requirements-deploy.yml -p provisioning/roles -n -f
ansible-playbook --ssh-extra-args="${ANSIBLE_SSH_ARGS}" provisioning/deploy.yml -i provisioning/inventory/$inventory -e "${EXTRA_VARS}"
