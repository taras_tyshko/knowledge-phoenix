#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

source scripts/_environment-without-lxc.sh
source scripts/_ansible.sh

if [ -z $1 ]
  then usage
fi

# Run paybooks
ansible-galaxy install -r provisioning/requirements.yml -p provisioning/roles -n -f
ansible-playbook --ssh-extra-args="${ANSIBLE_SSH_ARGS}" provisioning/rollback.yml -i provisioning/inventory/$inventory -e "${EXTRA_VARS}"
