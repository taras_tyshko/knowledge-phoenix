#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ../..

# If NO_TTY environment variable is unset then use tty
[ -z ${NO_TTY} ] && {
    TTY=t
}

docker run --rm -v $(pwd)/:/data/current \
  -e MIX_ENV=prod \
  --net=host \
  --entrypoint "/data/current/architecture/scripts/build.sh" \
  -i${TTY} registry.smile-magento.com/docker/phoenix:ubuntu1604 $@
