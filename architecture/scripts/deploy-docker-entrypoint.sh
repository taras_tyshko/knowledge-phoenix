#!/bin/bash
# Entrypoint script for deploy
set -e

eval `ssh-agent`
ssh-add /data/.ssh/id_rsa
cp -R /data/.ssh /root/.ssh
cd /data/current

architecture/scripts/deploy.sh $@

exit $?