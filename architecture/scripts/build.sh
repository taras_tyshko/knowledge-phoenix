#!/bin/bash
set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"
cd ..

source scripts/_environment-without-lxc.sh

if [ -z $1 ]  
  then usage
fi

cd ..

if [ -z $MIX_ENV ]
	then export MIX_ENV=dev
fi

echo Current MIX_ENV is $MIX_ENV

mix local.hex --force
mix local.rebar --force
mix deps.get --force
mix release

