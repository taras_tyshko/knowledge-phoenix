#!/bin/bash

ANSIBLE_VERSION=$(ansible --version | grep "ansible" | cut -d " " -f 2);

if [ "${ANSIBLE_VERSION}" \< "2.1.0.0" ]; then
    echo "ERROR:"
    echo "  You must upgrade your Ansible version"
    echo "  This skeleton is only compatible with Ansible 2.1.x.x"
    echo "  Your actual version is [${ANSIBLE_VERSION}]"
    exit 1
fi

if [ "${ANSIBLE_VERSION}" \> "2.1.9.9" ]; then
    echo "ERROR:"
    echo "  You must downgrade your Ansible version"
    echo "  This skeleton is only compatible with Ansible 2.1.x.x"
    echo "  Your actual version is [${ANSIBLE_VERSION}]"
    exit 1
fi

ANSIBLE_SSH_ARGS="-o UserKnownHostsFile=./known_hosts"
if [ "${inventory}" == "lxc" ]; then
    ANSIBLE_SSH_ARGS=""
fi
