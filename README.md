# Phoenix

## Prerequirements !!!VERY IMPORTANT!!!  
    
   [SSH client configured](https://wiki.smile.fr/view/Agences/Kiev/ShSshConfig)  
   [Ansible 2.1](https://wiki.smile.fr/view/Systeme/AnsibleIntro)    
   [SmileLXC](https://wiki.smile.fr/view/Dirtech/LxcForDevs)  
     
     
## Create local development environment

``` bash
# cd into project folder
cd knowledge_phoenix

# Create empty LXC container
sudo cdeploy

# Provide container with tools (this should take a long time)
architecture/scripts/provision.sh lxc

# Go into container
ssh smile@elixir.lxc

# cd into project folder
cd knowledge_phoenix/

# Install deps
mix deps.get

# Run migrations
mix ecto.migrate

# Run server
mix phx.server

```

*Local services URLs:*

  Your application: http://elixir.lxc:4000

  Elasticsearch: http://elixir.lxc:9200

  Kibana: http://elixir.lxc:5601

  PostgreSQL (not accessible outside of container):
  ``` bash
  # Connect to LXC and runs psql shell
  ssh -t smile@elixir.lxc psql -h localhost -U phoenix -W phoenix
  ```

  Account credentials you can find under architecture/provisioning/inventory/group_vars/ folder


## Manual Deployment

```  bash
# Build package
./architecture/scripts/build.sh

# Deploy package
./architecture/scripts/deploy.sh [env]

```

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
