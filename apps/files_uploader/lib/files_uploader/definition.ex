defmodule FilesUploader.Definition do
  defmacro __using__(_opts) do
    quote do
      use Arc.Definition

      Module.put_attribute(
        __MODULE__,
        :versions,
        Application.get_env(:files_uploader, __MODULE__)[:versions]
      )

      Module.put_attribute(
        __MODULE__,
        :extension_whitelist,
        Application.get_env(:files_uploader, __MODULE__)[:extension_whitelist]
      )

      def get_sources(data), do: [data]

      def prepare_to_store(source), do: source

      def set_sources(data, source, new_source), do: data

      defp get_source_dir(module \\ __MODULE__) do
        module
        |> Module.split()
        |> List.last()
        |> String.downcase()
      end

      defoverridable(get_sources: 1, prepare_to_store: 1, set_sources: 3)
    end
  end
end
