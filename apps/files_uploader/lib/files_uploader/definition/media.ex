defmodule FilesUploader.Definition.Media do
  @moduledoc """
  The module contains the relevant configuration to store and
  retrieve media files from the description of application models.

  This definition module contains relevant functions to determine:

    - Optional transformations of the uploaded file
    - Where to put your files (the storage directory)
    - What to name your files
    - How to secure your files (private? Or publicly accessible?)
    - Default placeholders

  This module use the `Arc` library for flexible file upload.
  For more information see `https://github.com/stavro/arc`
  """

  alias FilesUploader.File

  use FilesUploader.Definition

  @sources_format ~r/<img\s+src="(data:image\/[a-z]{2,5};base64,([^"]{1,}))"\s+alt="(?:(?:\d+\/)+)?([^"]{1,})[^<>]{1,}>/

  def __storage, do: Arc.Storage.Local

  def get_sources(data) do
    @sources_format
    |> Regex.scan(data)
    |> Enum.uniq_by(&Enum.fetch!(&1, 1))
  end

  def prepare_to_store(source) do
    Map.new()
    |> Map.put(:filename, Enum.fetch!(source, 3))
    |> Map.put(
      :binary,
      source
      |> Enum.fetch!(2)
      |> Base.decode64!()
    )
  end

  def set_sources(data, source, new_source) do
    String.replace(data, Enum.fetch!(source, 1), File.encode(new_source))
  end

  def validate({file, _}) do
    file_extension =
      file.file_name
      |> Path.extname()
      |> String.downcase()

    @extension_whitelist
    |> Enum.member?(file_extension)
  end

  def filename(_version, {%{file_name: name}, _scope}) do
    Base.url_encode64(name, padding: false)
  end

  def storage_dir(version, {_file, scope}) do
    Application.get_env(:files_uploader, :path) <>
      "/uploads/#{get_source_dir()}/#{version}/#{scope.type}/#{scope.uuid}"
  end

  def transform(:desktop, _) do
    {:convert, "-resize 85%"}
  end

  def transform(:tablet, _) do
    {:convert, "-resize 65%"}
  end

  def transform(:mobile, _) do
    {:convert, "-resize 45%"}
  end
end
