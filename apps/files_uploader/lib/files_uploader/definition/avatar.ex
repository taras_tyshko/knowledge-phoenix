defmodule FilesUploader.Definition.Avatar do
  @moduledoc """
  The module contains the relevant configuration to store and retrieve users avatars.

  This definition module contains relevant functions to determine:

    - Optional transformations of the uploaded file
    - Where to put your files (the storage directory)
    - What to name your files
    - How to secure your files (private? Or publicly accessible?)
    - Default placeholders

  This module use the `Arc` library for flexible file upload.
  For more information see `https://github.com/stavro/arc`
  """

  alias FilesUploader.File
  alias FilesUploader.Definition.Media

  use FilesUploader.Definition

  def __storage, do: Arc.Storage.Local

  def set_sources(data, source, new_source) do
    String.replace(data, source, File.encode(new_source))
  end

  def filename(_version, {%{file_name: name}, _scope}) do
    Base.url_encode64(name, padding: false)
  end

  def storage_dir(version, {_file, scope}) do
    Application.get_env(:files_uploader, :path) <>
      "/uploads/#{get_source_dir(Media)}/#{version}/#{scope.type}/#{scope.uuid}/"
  end

  def transform(:thumb, _) do
    {:convert, "-strip -thumbnail 100x100^ -gravity center -extent 100x100"}
  end
end
