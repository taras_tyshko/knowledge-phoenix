defmodule FilesUploader.Definition.Sources do
  @moduledoc """
  The module contains the relevant configuration to store and
  retrieve media files from the description of application models.

  This definition module contains relevant functions to determine:

    - Optional transformations of the uploaded file
    - Where to put your files (the storage directory)
    - What to name your files
    - How to secure your files (private? Or publicly accessible?)
    - Default placeholders

  This module use the `Arc` library for flexible file upload.
  For more information see `https://github.com/stavro/arc`
  """

  use FilesUploader.Definition

  def __storage, do: Arc.Storage.Local

  def set_sources(_data, _source, new_source), do: new_source

  def filename(_version, {%{file_name: name}, _scope}) do
    Base.url_encode64(name, padding: false)
  end

  def storage_dir(_version, {_file, scope}) do
    Application.get_env(:files_uploader, :path) <>
      "/uploads/#{get_source_dir()}/#{scope.type}/#{scope.uuid}"
  end
end
