defmodule FilesUploader.Ecto.Schema do
  defmacro __using__(_) do
    quote do
      import FilesUploader.Ecto.Schema
    end
  end

  defmacro storage_cast(changeset_or_data, params, fields, options) do
    quote bind_quoted: [
            changeset_or_data: changeset_or_data,
            params: params,
            fields: fields,
            options: options
          ] do
      model =
        case changeset_or_data do
          %Ecto.Changeset{} -> Ecto.Changeset.apply_changes(changeset_or_data)
          %{__meta__: _} -> changeset_or_data
        end

      uploader_params =
        [fields]
        |> List.flatten()
        |> Enum.reduce([], fn field, acc ->
          with scope <- FilesUploader.Scope.get!(model),
               data <- get_in(params, [field]),
               true <- not is_nil(data) do
            data =
              [node() | Node.list()]
              |> Enum.map(fn node ->
                :rpc.call(node, FilesUploader, :store, [options[:type], data, scope])
              end)
              |> List.first()

            [{:"#{field}", data}]
          else
            _ -> []
          end
        end)
        |> Enum.into(%{})

      cast(changeset_or_data, uploader_params, fields)
    end
  end
end
