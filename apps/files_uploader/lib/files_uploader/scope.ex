defmodule FilesUploader.Scope do
  @scope_fields ~r/@([a-z_]{1,})/

  def get!(model) do
    with source <- model.__struct__.__schema__(:source),
         scope <- Application.fetch_env!(:files_uploader, :scope)[:"#{source}"],
         true <- not is_nil(scope) do
      Regex.replace(@scope_fields, scope, fn _, field ->
        "#{Map.fetch!(model, :"#{field}")}"
      end)
      |> List.wrap()
      |> Map.new(&{:type, &1})
      |> put_in([:uuid], get_in(model, [Access.key(:uuid)]))
    else
      _ -> raise "Scope config was not found"
    end
  end
end
