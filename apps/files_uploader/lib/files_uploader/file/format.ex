defmodule FilesUploader.File.Format do
  @type t :: %__MODULE__{
          path: String.t(),
          updated_at: integer()
        }

  @derive [Jason.Encoder]
  defstruct [:path, :updated_at]

  use ExConstructor
end
