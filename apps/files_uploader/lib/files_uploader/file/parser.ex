defmodule FilesUploader.File.Parser do
  def parse!(string) do
    case parse(string) do
      {:ok, file} -> file
      {:error, :invalid} -> raise("Invalid format")
    end
  end

  def parse("ELIXIR.FILESUPLOADER" <> json), do: Jason.decode(json)

  def parse(_), do: {:error, :invalid}
end
