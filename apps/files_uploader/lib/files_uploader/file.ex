defmodule FilesUploader.File do
  alias FilesUploader.File.{Format, Parser}

  def new(path) when is_binary(path) do
    Format.new(%{
      path: path,
      updated_at: set_time_point()
    })
  end

  def find(data) when is_binary(data) do
    ~r/#{mark()}{[^{}]{1,}}/
    |> Regex.scan(data)
    |> List.flatten()
  end

  def to_url(file, url) when is_map(file) do
    file["path"]
    |> String.replace(Application.fetch_env!(:files_uploader, :path), url)
    |> Kernel.<>("?v=#{file["updated_at"]}")
  end

  def to_url(data, url) when is_binary(data) do
    data
    |> decode()
    |> to_url(url)
  end

  def set_time_point do
    NaiveDateTime.utc_now()
    |> NaiveDateTime.to_erl()
    |> :calendar.datetime_to_gregorian_seconds()
  end

  def decode(data) do
    Parser.parse!(data)
  end

  def encode(path) do
    path
    |> new()
    |> Jason.encode!()
    |> compose()
  end

  def compose(string) do
    Kernel.<>(mark(), string)
  end

  defp mark do
    FilesUploader
    |> Atom.to_string()
    |> String.upcase()
  end
end
