defmodule FilesUploader do
  alias FilesUploader.File

  def store(definition, data, scope) when is_map(scope) do
    data
    |> definition.get_sources()
    |> Enum.reduce(data, fn source, acc ->
      {definition.prepare_to_store(source), scope}
      |> definition.store()
      |> case do
        {:ok, new_source} ->
          definition.set_sources(acc, source, definition.url({new_source, scope}))

        {:error, _reason} ->
          acc
      end
    end)
  end

  def store(_definition, src, _scope), do: src

  def url(data, static_url) when is_binary(data) do
    File.find(data)
    |> Enum.reduce(data, fn file, acc ->
      String.replace(acc, file, File.to_url(file, static_url))
    end)
  end

  def url(data, _static_url), do: data

  def delete(definition, filename, scope) when is_map(scope) do
    definition.delete({filename, scope})
  end

  def delete(_definition, _filename, _scope), do: :error
end
