defmodule FilesUploader.MixProject do
  use Mix.Project

  def project do
    [
      app: :files_uploader,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        :arc,
        :logger
      ],
      env: [path: "./uploads", scope: [], host: "http://elixir.lxc:4000/uploads"]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:arc, "~> 0.11.0"},
      {:plug, "~> 1.7.1"}
    ]
  end
end
