use Mix.Config

# We'll use the Bamboo.TestAdapter for our tests.  There are other adapters for
# all manner of things, including Mandrill, Sendgrid, Mailgun, and SparkPost, as
# well as one for storing them in memory locally for development purposes.
config :email_worker, EmailWorker.Mailer, adapter: Bamboo.TestAdapter
