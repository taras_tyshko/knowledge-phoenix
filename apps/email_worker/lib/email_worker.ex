defmodule EmailWorker do
  @moduledoc """
    This module implements the fintion for sending mail.
  """
  use Bamboo.Phoenix, view: EmailWorker.EmailView
  import Bamboo.Email
  alias EmailWorker.Mailer

  @doc """
      This function sends template: `"sign_in.html.eex"`.
  """
  @spec sign_in_email(struct) :: Bamboo.Email.t()
  def sign_in_email(person) do
    base_email()
    |> to("#{person.email}")
    |> subject("Welcome to Smile!")
    |> html_body("<strong>Welcome</strong>")
    |> assign(:person, person)
    |> render("sign_in.html")
    |> Mailer.deliver_later()
  end

  @doc """
      This function sends template: `"recover_password.html.eex"`.
  """
  @spec recover_password_in_email(String.t(), struct) :: Bamboo.Email.t()
  def recover_password_in_email(email, data) do
    base_email()
    |> to("#{email}")
    |> subject("Welcome to Knowledge!")
    |> html_body("<strong>Knowledge recover password</strong>")
    |> assign(:data, data)
    |> render("recover_password.html")
    |> Mailer.deliver_later()
  end

  @doc """
      This function sends template: `"email_verification.html.eex"`.
  """
  @spec email_verification(String.t(), struct) :: Bamboo.Email.t()
  def email_verification(email, data) do
    base_email()
    |> to("#{email}")
    |> subject("Welcome to Smile!")
    |> html_body("<strong>Welcome</strong>")
    |> assign(:data, data)
    |> render("email_verification.html")
    |> Mailer.deliver_later()
  end

  defp base_email do
    new_email()
    |> from({"Knowledge Phoenix", "knowledge_phoenix@smile-magento.com"})
    |> put_header("Reply-To", "smile@support.com")
  end
end
