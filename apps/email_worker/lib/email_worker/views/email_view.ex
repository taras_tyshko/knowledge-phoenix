defmodule EmailWorker.EmailView do
  @moduledoc false

  use Phoenix.View, root: "lib/email_worker/templates"
end
