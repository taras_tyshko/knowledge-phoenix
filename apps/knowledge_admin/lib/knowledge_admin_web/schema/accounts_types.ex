defmodule KnowledgeAdminWeb.Schema.AccountsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgeAdminWeb.Resolvers
  alias KnowledgeAdminWeb.Middlewares
  alias SmileDB.{Users, Achievements, Permissions}

  object :account_queries do
    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field :account, :account do
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "Check hash from change password to log in Admin Panel."
    field(:check_hash, :boolean) do
      arg(:hash, non_null(:string),
        description: "Hash from confirm password to check password in Admin panel."
      )

      resolve(&Resolvers.Accounts.check_hash/3)
    end

    @desc "Get a list of user on defined, with the cursor of a pagination."
    field :find_users, :general_paginator do
      middleware(Middlewares.Authentication)
      arg(:after, :string, description: "Fetch the records after this cursor.")

      arg(:input, non_null(:string),
        description: "Search user by word. Where word is user nickname."
      )

      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Accounts.find_users/3)
    end

    @desc "Get a list of experts on defined or search experts, with the cursor of a pagination."
    field :find_experts, :general_paginator do
      middleware(Middlewares.Authentication)
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:input, :string, description: "Search user by word. Where word is user nickname.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Accounts.find_experts/3)
    end
  end

  @desc "The given object implements the receipt of data pertaining to the user's account."
  object :account do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn parent, _, _ ->
        {:ok, parent.id}
      end)
    end

    @desc "Get user Data."
    field :user, :user do
      resolve(&Resolvers.Accounts.find_user/3)
    end

    ## TODO Delete if added in role object :list_permissions from role!
    @desc "Gets a list permission in user by role."
    field(:list_permissions, list_of(:string)) do
      resolve(fn parent, _, _ ->
        Permissions.list_permissions(parent.role)
      end)
    end

    @desc "List user of Achieves."
    field(:achieves, list_of(:achieve)) do
      resolve(fn parent, _args, _resolution ->
        {:ok, Achievements.list_achieves(parent.id)}
      end)
    end

    field(:list_expert_request_tags, list_of(:string)) do
      resolve(fn
        %{list_expert_requests: list_expert_requests}, _, _resolution ->
          {:ok, list_expert_requests}

        parent, _, _resolution ->
          {:ok, Users.list_expert_requests(parent.id)}
      end)
    end
  end
end
