defmodule KnowledgeAdminWeb.Schema.PaginatorsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :general_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:user), description: "List of ExpertRequest.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :achieve_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:achieve), description: "List of Achieves.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :permission_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:permission), description: "List of Achieves.")
  end

  @desc "Implement the structure to obtain additional data for the cursor pagination such as."
  object :metadata do
    field(:after, :string, description: "Fetch the records after this cursor.")
    field(:limit, :integer, description: "Number of results to return.")
    field(:total_count, :integer, description: "Number of all results.")
  end
end
