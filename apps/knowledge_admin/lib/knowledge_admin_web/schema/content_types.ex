defmodule KnowledgeAdminWeb.Schema.ContentTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgeAdminWeb.Resolvers
  alias KnowledgeAdminWeb.Middlewares

  object :content_queries do
    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field(:list_expert_requests, :general_paginator) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search experts requests by users.")
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Content.list_expert_requests/3)
    end

    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field(:list_achieves, :achieve_paginator) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search experts requests by users.")
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Content.list_achieves/3)
    end

    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field(:list_permissions, :permission_access) do
      middleware(Middlewares.Authentication)

      resolve(&Resolvers.Content.list_permissions/3)
    end

    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field(:list_user_achieves, :general_paginator) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search experts requests by users.")
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Content.list_user_achieves/3)
    end

    @desc "Implements search tags according to specific arguments."
    field :tags_autocomplete, list_of(:string) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search Tag by word.")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.Content.tags_autocomplete/3)
    end

    @desc "Implements search achieves according to specific arguments."
    field :achieves_autocomplete, list_of(:achieve) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search Achieve by word.")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.Content.achieves/3)
    end
  end
end
