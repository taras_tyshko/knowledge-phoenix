defmodule KnowledgeAdminWeb.Schema.UserTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgeAdminWeb.Resolvers
  alias KnowledgeAdminWeb.Middlewares

  object :user_queries do
    @desc "Get a list of users on search user, with the cursor of a pagination"
    field :banned_and_hidden_users, :general_paginator do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search user by word. Where word is user nickname")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:after, :string, description: "Fetch the records after this cursor.")

      resolve(&Resolvers.User.banned_and_hidden_users/3)
    end

    @desc "Get a list of users on search user, with the cursor of a pagination"
    field :not_banned_and_hidden_users, list_of(:user) do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search user by word. Where word is user nickname")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.User.users_not_ban/3)
    end

    @desc "Get a list of users on search user who not have achieves, with the cursor of a pagination"
    field :users_not_achieves, :general_paginator do
      middleware(Middlewares.Authentication)
      arg(:input, :string, description: "Search user by word. Where word is user nickname")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:after, :string, description: "Fetch the records after this cursor.")

      resolve(&Resolvers.User.find_users_not_achieves/3)
    end
  end
end
