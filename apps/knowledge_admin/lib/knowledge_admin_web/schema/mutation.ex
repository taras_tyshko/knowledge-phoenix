defmodule KnowledgeAdminWeb.Schema.Mutation do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgeAdminWeb.Middlewares
  alias KnowledgeAdminWeb.Resolvers

  object :mutations do
    @desc "There are mutations in this object pertaining to accounts."
    field :account, :accounts do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "There are mutations in this object pertaining to Content."
    field :content, :user_content do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "There are mutations in this object pertaining to user Permissions."
    field :permission, :permissions do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "There are mutations in this object pertaining to user Achieves."
    field :achieves, :achieve_data do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "There are mutations in this object pertaining to Expert request data."
    field :expert_requests, :expert_request_data do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "User Authorisation Admin Panel."
    field(:authorization, :string) do
      arg(:email, non_null(:string), description: "Name-User ID to log in to Admin panel.")
      arg(:password, non_null(:string), description: "User password to log in to Admin panel.")

      resolve(&Resolvers.Mutation.callback/3)
    end

    @desc "This object contains mutations related to the authorization."
    field :related_contents, :related_content do
      middleware(Middlewares.Authentication)
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "Add user role from Admin Panel."
    field :set_user_role, :user do
      middleware(Middlewares.Authentication)
      arg(:user_id, non_null(:integer), description: "Unique User ID.")
      arg(:role, non_null(:string), description: "Name Role.")

      resolve(&Resolvers.Mutation.set_user_role/3)
    end

    @desc "Add user role from Admin Panel."
    field :create_user_access, :user do
      middleware(Middlewares.Authentication)
      arg(:user_id, non_null(:integer), description: "Unique User ID.")
      arg(:password, non_null(:string), description: "Name Role.")

      resolve(&Resolvers.Mutation.create_user_access/3)
    end

    @desc "Add user role from Admin Panel."
    field :create_role, :role do
      middleware(Middlewares.Authentication)
      arg(:role, non_null(:string), description: "Unique role name.")

      resolve(&Resolvers.Mutation.create_role/3)
    end

    @desc "Forgot user password in Admin Panel."
    field(:forgot_password, :boolean) do
      arg(:email, non_null(:string), description: "User email.")

      resolve(&Resolvers.Mutation.forgot_password/3)
    end

    @desc "Cancel change password for log in in Admin Panel."
    field(:cancel_change_password, :boolean) do
      arg(:hash, non_null(:string), description: "Hash to confirm do.")

      resolve(&Resolvers.Mutation.cancel_change_password/3)
    end

    @desc "Chenge user password in Admin Panel."
    field(:change_password, :string) do
      arg(:hash, :string, description: "Hash to confirm do.")
      arg(:password, non_null(:string), description: "User password to log in to Admin panel.")

      resolve(&Resolvers.Mutation.change_password/3)
    end
  end

  object :related_content do
    @desc "Create related Content."
    field :create_related_content, :related_contents do
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      arg(:related_content_id, non_null(:integer),
        description: "Unique identifier of the Content."
      )

      resolve(&Resolvers.Mutation.create_related_contents/3)
    end

    @desc "Delete related contents."
    field :delete_related_content, :related_contents do
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      arg(:related_content_id, non_null(:integer),
        description: "Unique identifier of the Content."
      )

      resolve(&Resolvers.Mutation.delete_related_content/3)
    end
  end

  object :accounts do
    @desc "Create user from admin."
    field :admin_create_user, :user do
      arg(:status, :string, description: "User status.")
      arg(:password, :string, description: "User password.")
      arg(:email, non_null(:string), description: "E-mail address.")
      arg(:last_name, non_null(:string), description: "Last name user.")
      arg(:first_name, non_null(:string), description: "First name user.")
      arg(:role, non_null(:string), description: "Unique identifier of the User.")

      resolve(&Resolvers.Mutation.create_user/3)
    end

    @desc "Update user info by admin."
    field :update_user_info, :user do
      arg(:user_id, non_null(:integer), description: "Unique User ID.")
      arg(:status, :string, description: "User status.")
      arg(:email, :string, description: "E-mail address.")
      arg(:expert, list_of(:string), description: "Tags that users is expert.")
      arg(:role, :string, description: "Unique identifier of the User.")

      resolve(&Resolvers.Mutation.update_user_info/3)
    end

    @desc "Ban user."
    field :ban_account, :user do
      arg(:user_id, non_null(:integer), description: "Unique User ID.")
      arg(:time, :integer, description: "Time in millisecond.")

      resolve(&Resolvers.Mutation.ban_account/3)
    end

    @desc "Unban user."
    field :unban_account, :user do
      arg(:user_id, non_null(:integer), description: "Unique User ID.")

      resolve(&Resolvers.Mutation.unban_account/3)
    end

    @desc "Hidden all content from user."
    field :hidden_account_content, :user do
      arg(:user_id, non_null(:integer), description: "Unique User ID.")

      resolve(&Resolvers.Mutation.hidden_account_content/3)
    end

    @desc "Un hidden all content from user."
    field :un_hidden_account_content, :user do
      arg(:user_id, non_null(:integer), description: "Unique User ID.")

      resolve(&Resolvers.Mutation.unhidden_account_content/3)
    end
  end

  object :user_content do
    @desc "Update user Content."
    field :update_user_content, :contents_union do
      arg(:title, :string, description: "Title Content.")
      arg(:status, :string, description: "Status content: open, closed, other.")
      arg(:description, :string, description: "Content description.")
      arg(:tags, list_of(:string), description: "List content tags.")
      arg(:shadow, :boolean, description: "Unique identifier of the Content.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(&Resolvers.Mutation.update_user_content/3)
    end

    @desc "Create user answer."
    field :create_user_answer, :answer do
      arg(:description, :string, description: "Content description.")
      arg(:tags, list_of(:string), description: "List content tags.")
      arg(:shadow, :boolean, description: "Unique identifier of the Answer.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(&Resolvers.Mutation.create_user_answer/3)
    end

    @desc "Update user answer."
    field :update_user_answer, :answer do
      arg(:description, :string, description: "Content description.")
      arg(:tags, list_of(:string), description: "List content tags.")
      arg(:shadow, :boolean, description: "Unique identifier of the Answer.")
      arg(:answer_id, non_null(:integer), description: "Unique identifier of the Answer.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")

      resolve(&Resolvers.Mutation.update_user_answer/3)
    end

    @desc "Update user comment."
    field :update_user_comment, :comment do
      arg(:description, :string, description: "Content description.")
      arg(:shadow, :boolean, description: "Unique identifier of the Comment.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
      arg(:comment_id, non_null(:integer), description: "Unique identifier of the Comment.")

      resolve(&Resolvers.Mutation.update_user_comment/3)
    end

    @desc "Hidden comment level."
    field :hidden_comment_level, list_of(:comment) do
      arg(:shadow, :boolean, description: "Unique identifier of the Comment.")
      arg(:comment_id, non_null(:integer), description: "Unique identifier of the Comment.")

      resolve(&Resolvers.Mutation.hidden_comment_level/3)
    end

    @desc "Delete user Content."
    field :delete_user_content, :contents_union do
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(&Resolvers.Mutation.delete_user_content/3)
    end

    @desc "Delete user answer."
    field :delete_user_answer, :answer do
      arg(:answer_id, non_null(:integer), description: "Unique identifier of the Answer.")

      resolve(&Resolvers.Mutation.delete_user_answer/3)
    end

    @desc "Delete user comment."
    field :delete_user_comment, :comment do
      arg(:comment_id, non_null(:integer), description: "Unique identifier of the Comment.")

      resolve(&Resolvers.Mutation.delete_user_comment/3)
    end
  end

  object :expert_request_data do
    @desc "Create exspert request."
    field :put_expert_to_user, :user do
      arg(:user_id, non_null(:integer), description: "Unique identifier of the User.")
      arg(:tags, list_of(:string), description: "Tag which is submitted for confirmation.")

      resolve(&Resolvers.Mutation.put_expert_to_user/3)
    end

    @desc "Deleted exspert request."
    field :delete_expert_request, :user do
      arg(:user_id, non_null(:integer), description: "Unique identifier of the User.")
      arg(:tags, list_of(:string), description: "Tag which is submitted for confirmation.")

      resolve(&Resolvers.Mutation.delete_expert_request/3)
    end

    @desc "Confirm exspert request."
    field :confirm_expert_request, :user do
      arg(:user_id, non_null(:integer), description: "Unique identifier of the User.")
      arg(:tags, list_of(:string), description: "Tag which is submitted for confirmation.")

      resolve(&Resolvers.Mutation.confirm_expert_request/3)
    end
  end

  object :permissions do
    @desc "Create user permission."
    field :create_permission, :permission do
      arg(:name, non_null(:string), description: "Name of the Permission.")

      resolve(&Resolvers.Mutation.create_permission/3)
    end

    @desc "Update user permission."
    field :update_permission, :permission do
      arg(:name, non_null(:string), description: "Name of the Permission.")
      arg(:permission_id, non_null(:integer), description: "Unique identifier of the Permission.")

      resolve(&Resolvers.Mutation.update_permission/3)
    end

    @desc "Delete user permission."
    field :delete_permission, :permission do
      arg(:permission_id, non_null(:integer), description: "Unique identifier of the Permission.")

      resolve(&Resolvers.Mutation.delete_permission/3)
    end

    @desc "Create/Delete user permission."
    field :assign_role_permissions, :boolean do
      arg(:access_data, :string, description: "Name of the Permission.")

      resolve(&Resolvers.Mutation.assign_role_permissions/3)
    end
  end

  object :achieve_data do
    @desc "Created Achieve."
    field :create_achieve, :achieve do
      arg(:title, non_null(:string), description: "Achieve name of the Achives.")
      arg(:avatar, non_null(:string), description: "Avatar of the Achives.")

      resolve(&Resolvers.Mutation.create_achieve/3)
    end

    @desc "Updated Achieve."
    field :update_achieve, :achieve do
      arg(:achieve_id, non_null(:integer), description: "Unique identifier of the Achieve.")
      arg(:title, non_null(:string), description: "Achieve name of the Achives.")
      arg(:avatar, non_null(:string), description: "Avatar of the Achives.")

      resolve(&Resolvers.Mutation.update_achieve/3)
    end

    @desc "Deleted Achieve."
    field :delete_achieve, :achieve do
      arg(:achieve_id, non_null(:integer), description: "Unique identifier of the Achieve.")

      resolve(&Resolvers.Mutation.delete_achieve/3)
    end

    @desc "Assign User Achieve."
    field :assign_user_achieves, :user do
      arg(:user_id, non_null(:integer), description: "Unique identifier of the User.")
      arg(:achieves_id, list_of(:integer), description: "Unique identifier of the Achieve.")

      resolve(&Resolvers.Mutation.set_user_achieves/3)
    end
  end
end
