defmodule KnowledgeAdminWeb.Schema do
  @moduledoc false
  use SmileDB.Graphql

  import_types(KnowledgeAdminWeb.Schema.Mutation)
  import_types(KnowledgeAdminWeb.Schema.UserTypes)
  import_types(KnowledgeAdminWeb.Schema.ContentTypes)
  import_types(KnowledgeAdminWeb.Schema.AccountsTypes)
  import_types(KnowledgeAdminWeb.Schema.PaginatorsTypes)

  query do
    import_fields(:user_queries)
    import_fields(:account_queries)
    import_fields(:content_queries)
  end

  mutation do
    import_fields(:mutations)
  end

  @spec middleware(list, map(), map()) :: list
  def middleware(middleware, field, object) do
    super(middleware, field, object)
  end
end
