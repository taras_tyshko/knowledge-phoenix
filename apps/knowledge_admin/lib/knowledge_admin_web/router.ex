defmodule KnowledgeAdminWeb.Router do
  @moduledoc false
  use KnowledgeAdminWeb, :router

  scope "/" do
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: KnowledgeAdminWeb.Schema)

    forward("/", Absinthe.Plug, schema: KnowledgeAdminWeb.Schema)
  end
end
