defmodule KnowledgeAdminWeb.Resolvers.Content do
  @moduledoc """
    A module for the release of resolvers Content which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.{Accounts, Users, Tags, Achievements, Permissions}
  alias ElasticSearch.Paginator

  @doc """
    This feature retrieves list [`ExpertRequest`](SmileDB.Users.Expert.html) data from pagination.

  ## Examples

      def list_expert_requests(_parent, %{limit: limit, after: after_cursor}, _resolution) do
        %{entries: entries, metadata: metadata} =
              Users.Expert
              |> order_by(desc: :inserted_at, desc: :id)
              |> Repo.paginate(
                after: after_cursor,
                cursor_fields: [:inserted_at, :id],
                limit: limit
              )
      end
  """
  @spec list_expert_requests(
          map(),
          map(),
          Absinthe.Resolution.t()
        ) :: {:ok, Paginator.t()}
  def list_expert_requests(
        _parent,
        %{input: input, limit: limit, after: after_cursor},
        _resolution
      ) do
    ids =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.id)

    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Users.Expert).index) do
        query do
          bool do
            must do
              terms("user_id", ids)
            end

            filter do
              exists("tags")
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )
      |> get_and_update_in(
        [:entries, Access.all()],
        &{&1,
         &1.user_id
         |> Accounts.get_user!()
         |> Map.put(:list_expert_requests, &1.tags)}
      )
      |> elem(1)

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_expert_requests(_parent, %{limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Users.Expert).index) do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("tags")
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )
      |> get_and_update_in(
        [:entries, Access.all()],
        &{&1,
         &1.user_id
         |> Accounts.get_user!()
         |> Map.put(:list_expert_requests, &1.tags)}
      )
      |> elem(1)

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    This feature retrieves list [`Achieve`](SmileDB.Achievements.Achieve.html) data from pagination.

  ## Examples

      def list_achieves(_parent, %{limit: limit, after: after_cursor, input: input}, _resolution) do
        %{entries: entries, metadata: metadata} =
          search(index: ElasticSearch.get_meta!(Achievements.Achieve).index) do
            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "title_search^3"
                    ],
                    type: "phrase_prefix"
                  )
                end
              end
            end

            sort do
              [inserted_at: :desc, id: :desc]
            end
          end
          |> ElasticSearch.Repo.paginate(
            limit: limit,
            cursor_fields: [:inserted_at, :id],
            after: after_cursor
          )

        {:ok,
        %{
          entries: entries,
          metadata: metadata
        }}
      end
  """
  @spec list_achieves(map(), map(), Absinthe.Resolution.t()) :: {:ok, Paginator.t()}
  def list_achieves(_parent, %{limit: limit, after: after_cursor, input: input}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Achievements.Achieve).index) do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "title_search^3"
                ],
                type: "phrase_prefix"
              )
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_achieves(_parent, %{limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Achievements.Achieve).index) do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    This feature retrieves list [`User`](SmileDB.Accounts.User.html) data from pagination.

  ## Examples

      def list_user_achieves(_parent, %{limit: limit, after: after_cursor}, _resolution) do
        %{entries: entries, metadata: metadata} =
          search(index: ElasticSearch.get_meta!(Achievements.UserAchieve).index) do
            query do
              bool do
                must do
                  match_all(boost: 1.0)
                end
              end
            end

            sort do
              [inserted_at: :desc, id: :desc]
            end
          end
          |> ElasticSearch.Repo.paginate(
            limit: limit,
            cursor_fields: [:inserted_at, :id],
            after: after_cursor
          )

        {:ok,
        %{
          entries: Enum.map(entries, &ElasticSearch.Repo.get(Accounts.User, &1.user_id)),
          metadata: metadata
        }}
      end
  """
  @spec list_user_achieves(map(), map(), Absinthe.Resolution.t()) :: {:ok, Paginator.t()}
  def list_user_achieves(_parent, %{limit: limit, after: after_cursor, input: input}, _resolution) do
    ids =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end
          end
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.id)

    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Achievements.UserAchieve).index) do
        query do
          bool do
            must do
              terms("user_id", ids)
            end

            filter do
              exists("achieves")
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: Enum.map(entries, &ElasticSearch.Repo.get(Accounts.User, &1.user_id)),
       metadata: metadata
     }}
  end

  def list_user_achieves(_parent, %{limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search(index: ElasticSearch.get_meta!(Achievements.UserAchieve).index) do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end
          end
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: Enum.map(entries, &ElasticSearch.Repo.get(Accounts.User, &1.user_id)),
       metadata: metadata
     }}
  end

  @doc """
    This feature retrieves list permissions data.

  ## Examples

      def list_permissions(_parent, _args, _resolution) do
        permissions =
          search(index: ElasticSearch.get_meta!(Permissions.Permission).index) do
            query do
              bool do
                must do
                  match_all(boost: 1.0)
                end
              end
            end

            sort do
              [id: :desc]
            end
          end
          |> ElasticSearch.Repo.all()

        role_access =
          search(index: ElasticSearch.get_meta!(Permissions.RoleAccess).index) do
            query do
              bool do
                must do
                  match_all(boost: 1.0)
                end
              end
            end

            sort do
              [inserted_at: :desc, id: :desc]
            end
          end
          |> ElasticSearch.Repo.all()

        {:ok, %{permissions: permissions, roles: roles}}
      end
  """
  @spec list_permissions(map(), map(), map()) :: {:ok, list(Permissions.RoleAccess.t())}
  def list_permissions(_parent, _args, _resolution) do
    permissions =
      search(index: ElasticSearch.get_meta!(Permissions.Permission).index) do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end
          end
        end

        sort do
          [id: :asc]
        end
      end
      |> ElasticSearch.Repo.all()

    {:ok, %{permissions: permissions, roles: Accounts.list_roles()}}
  end

  @doc """
  Returns a list of tags title for autocomplete.

  ## Examples

      def tags_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        query =
          Tirexs.Search.search [index: "tags"] do
            size(limit)

            query do
              match(
                "title",
                input,
                fuzziness: "AUTO",
                operator: "and"
              )
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            titles = Enum.map(result.hits.hits, & &1.title)

            {:ok, Enum.uniq(titles)}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """
  @spec tags_autocomplete(map(), %{limit: Integer.t(), input: String.t()}, map()) ::
          {:ok, list(String.t())}
  def tags_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Tags.Tag).index do
        suggest do
          tag_suggest do
            prefix(input, [])

            completion do
              field("title_autocomplete", [])
              fuzzy(:fuzziness, "AUTO")
            end
          end
        end

        sort do
          [count: :desc]
        end
      end
      |> put_in(
        [:search, :suggest, :tag_suggest, :completion, :fuzzy, :prefix_length],
        String.length(input)
      )
      |> ElasticSearch.Repo.all(limit)
      |> Keyword.get(:tag_suggest)
      |> Enum.filter(&(&1.count > 0)) || []

    {:ok,
     suggest
     |> Enum.map(& &1.title)
     |> Enum.uniq()}
  end

  @doc """
  Returns a list of achieves title for autocomplete.

  ## Examples

      def achieves(_parent, %{limit: limit, input: input}, _resolution) do
        suggest =
          search index: ElasticSearch.get_meta!(Achievements.Achieve).index do
            suggest do
              achieve_suggest do
                prefix(input, [])

                completion do
                  field("title_autocomplete", [])
                  fuzzy(:fuzziness, "AUTO")
                end
              end
            end
          end
          |> put_in(
            [:search, :suggest, :achieve_suggest, :completion, :fuzzy, :prefix_length],
            String.length(input)
          )
          |> ElasticSearch.Repo.all(limit)
          |> Keyword.get(:achieve_suggest)

        {:ok,
        suggest
        |> Enum.map(& &1.title)
        |> Enum.uniq()}
      end 
  """
  @spec achieves(map(), %{limit: Integer.t(), input: String.t()}, map()) ::
          list(String.t()) | list()
  def achieves(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Achievements.Achieve).index do
        query do
          match_phrase_prefix("title_search", input)
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, suggest}
  end
end
