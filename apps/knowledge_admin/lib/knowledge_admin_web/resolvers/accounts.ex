defmodule KnowledgeAdminWeb.Resolvers.Accounts do
  @moduledoc """
    A module for the release of resolvers Accounts which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.{Accounts, Users}

  @doc """
    Functions to process query get user data through the nickname .

  ## Examples

      def find_profile_account(_parent, args, _resolution) do
        Accounts.get_user_by_nickname!(args.nickname)
      end
  """
  @spec find_profile_account(map(), %{nickname: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()}
  def find_profile_account(_parent, %{nickname: nickname}, _resolution) do
    {:ok, Accounts.get_user_by_nickname!(nickname)}
  end

  @doc """
    Functions to process query get user data through the token.

  ## Examples

      def find_profile_account(_parent, _args, %{context: %{current_user: user}}) do
        user
      end
  """
  @spec find_profile_account(map(), map(), %{context: %{current_user: map()}}) ::
          {:ok, Accounts.User.t()}
  def find_profile_account(_parent, _args, %{context: %{current_user: user}}) do
    {:ok, user}
  end

  @doc """
    Functions to process query get user data through the parent.

  ## Examples

      def find_user(parent, _args, _resolution) do
        parent
      end
  """
  @spec find_user(Accounts.User.t(), map(), Absinthe.Resolution.t()) :: {:ok, Accounts.User.t()}
  def find_user(parent, _args, _resolution) do
    {:ok, parent}
  end

  @doc """
    This feature checks the hash whether it is present in the database to recover the user's use and sign in to the Web panel.

  ## Examples

      iex>  Accounts.check_hash(%{hash: "asdad123123adad1231asd"})
      {:ok, true}

      iex>  Accounts.check_hash(%{hash: "asdad123123adad1asdasd231asd"})
      {:ok, false}
  """
  @spec check_hash(map(), %{hash: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, true} | {:ok, false}
  def check_hash(_parent, %{hash: hash}, _resolution) do
    if not is_nil(Users.get_change_password_by_hash(hash)), do: {:ok, true}, else: {:ok, false}
  end

  def hide_user_account(_parent, %{user_id: user_id}, _resolution) do
    user_id
    |> Accounts.get_user!()
    |> Accounts.delete_account()
  end

  def delete_user_account(_parent, %{user_id: user_id}, _resolution) do
    case user_id
         |> Accounts.get_user!()
         |> Accounts.delete_user() do
      {:ok, user} ->
        # FilesUploader.delete(FilesUploader.Definition.Avatar, user)
        # FilesUploader.delete(FilesUploader.Definition.Media, user)
        {:ok, user}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    Returns a list of users.

    Provides search the structure of users from `ElasticSearch`.

  ## Examples

      iex>  Accounts.find_users(%{input: "test", after: null, limit: 5})
      {:ok, %{entries: [%User{} ...], metadata: %{limit: 5, after: null, totalCount: 4}}

      iex>  Accounts.find_users(%{input: "test", after: null, limit: 5})
      {:ok, %{entries: [], metadata: %{limit: 5, after: null, totalCount: 0}}
  """
  @spec find_users(
          map(),
          %{input: String.t(), after: String.t(), limit: Integer.t()},
          Absinthe.Resolution.t()
        ) :: ElasticSearch.Paginator.t()
  def find_users(_parent, %{input: input, after: after_cursor, limit: limit}, _resolution) do
    query =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            must_not do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        query,
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
  Returns a list of experts.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def find_experts(_parent, %{after: after_cursor, limit: limit}, _resolution) do
        %{entries: experts, metadata: metadata} =
          Accounts.User
          |> where([u], fragment("array_length(?, 1) > 0", u.expert))
          |> order_by(desc: :contents_count)
          |> Repo.paginate(
            after: after_cursor,
            cursor_fields: [:contents_count, :id],
            limit: limit
          )
      end
  """
  @spec find_experts(map(), %{limit: Integer.t(), input: String.t(), after: String.t()}, map()) ::
          {:ok, list(String.t())}
  def find_experts(_parent, %{input: input, after: after_cursor, limit: limit}, _resolution) do
    query =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2",
                  "tags^1"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        query,
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def find_experts(_parent, %{after: after_cursor, limit: limit}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end
end
