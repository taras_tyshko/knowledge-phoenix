defmodule KnowledgeAdminWeb.Resolvers.User do
  @moduledoc """
    The module allows to work with users entity. Provides renders user by id,
    returns a list of experts, them filtering by tags, search and autocomplete.

    In addition, the module provides api for the user profile, such as:
    updates information about user, uploads and imports user avatar.

    Search and autocomplete implemented by using `ElasticSearch`. We use `Tirexs`
    module that provides many features for work with `ElasticSearch`. See the
    [`Tirexs`](https://hexdocs.pm/tirexs/readme.html) docs for more details.
  """

  import Tirexs.Search
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.{Accounts, Achievements}

  @doc """
  Returns a list of users who not have achieves.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def find_users_not_achieves(_parent, %{input: input, limit: limit, after: after_cursor}, _resolution) do
        user_ids =
          search(index: ElasticSearch.get_meta!(Achievements.UserAchieve).index) do
            query do
              bool do
                must do
                end
              end
            end
          end
          |> ElasticSearch.Repo.all()
          |> Enum.map(& &1.user_id)

        %{entries: entries, metadata: metadata} =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2"
                    ],
                    type: "phrase_prefix"
                  )
                end

                must_not do
                  terms("id", user_ids)
                end
              end
            end

            sort do
              [contents_count: :desc, id: :desc]
            end
          end
          |> ElasticSearch.Repo.paginate(
            limit: limit,
            cursor_fields: [:contents_count, :id],
            after: after_cursor
          )

        {:ok, %{entries: entries, metadata: metadata}}
      end
  """
  @spec find_users_not_achieves(
          map(),
          %{input: String.t(), limit: Integer.t(), after: String.t()},
          map()
        ) :: {:ok, list(Accounts.User.t())}
  def find_users_not_achieves(
        _parent,
        %{input: input, limit: limit, after: after_cursor},
        _resolution
      ) do
    user_ids =
      search(index: ElasticSearch.get_meta!(Achievements.UserAchieve).index) do
        query do
          bool do
            must do
            end

            filter do
              exists("achieves")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.user_id)

    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            must_not do
              terms("id", user_ids)
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok, %{entries: entries, metadata: metadata}}
  end

  @doc """
  Returns a list of users who are banned and hidden users.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def banned_and_hidden_users(_parent, %{input: input, after: after_cursor, limit: limit}, _resolution) do
        query =
          search index: "users" do
            query do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            sort do
              [contents_count: :desc, id: :desc]
            end
          end

        %{entries: entries, metadata: metadata} =
          ElasticSearch.Repo.paginate(
            query,
            limit: limit,
            cursor_fields: [:contents_count, :id],
            after: after_cursor
          )

        users = Enum.map(entries, &Accounts.get_user!(&1.id))

        {:ok, %{
          entries: users,
          metadata: metadata
        }}
      end
  """
  @spec banned_and_hidden_users(
          map(),
          %{input: String.t(), limit: Integer.t(), after: String.t()},
          map()
        ) :: {:ok, list(Accounts.User.t())}
  def banned_and_hidden_users(
        _parent,
        %{input: input, limit: limit, after: after_cursor},
        _resolution
      ) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            must_not do
              terms("status", ["active", "deleted"])
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok, %{entries: entries, metadata: metadata}}
  end

  def banned_and_hidden_users(_parent, %{limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            must_not do
              terms("status", ["active", "deleted"])
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok, %{entries: entries, metadata: metadata}}
  end

  @doc """
  Returns a list of users who is not have `status: "active"`.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def users_not_ban(_parent, %{limit: limit, input: input}, _resolution) do
        entries =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2"
                    ],
                    type: "phrase_prefix"
                  ) 
                end

                must_not do
                  terms("status", ["banned", "deleted", "hidden"])
                end
              end
            end

            sort do
              [contents_count: :desc, id: :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        {:ok, entries}
      end
  """
  @spec users_not_ban(map(), %{limit: Integer.t(), input: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, list(Accounts.User.t())}
  def users_not_ban(_parent, %{limit: limit, input: input}, _resolution) do
    entries =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            must_not do
              terms("status", ["banned", "deleted", "hidden"])
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, entries}
  end

  @doc """
  Checking the anonumous of user .

  ## Examples

      def check_anonymous(parent, args, resolution) do
        parent =
          parent.anonymous
          |> if(do: Map.put(parent, :user_id, nil), else: parent)

        dataloader(Accounts).(parent, args, resolution)
      end
  """
  @spec check_anonymous(map(), map(), map()) :: {:ok, Accounts.User.t()}
  def check_anonymous(parent, args, resolution) do
    parent = get_parent(parent, resolution.context)
    dataloader(Accounts).(parent, args, resolution)
  end

  defp get_parent(parent, %{current_user: user}) do
    (!parent.anonymous || (parent.anonymous && user && user.id == parent.user_id))
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end

  defp get_parent(parent, %{}) do
    (!parent.anonymous || parent.anonymous)
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end
end
