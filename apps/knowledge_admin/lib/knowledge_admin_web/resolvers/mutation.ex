defmodule KnowledgeAdminWeb.Resolvers.Mutation do
  @moduledoc """
    A module for the release of resolvers [`Mutation`](KnowledgeAdminWeb.Resolvers.Mutation.html) which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.Auth.Guardian

  alias SmileDB.{
    Tags,
    Users,
    Source,
    Accounts,
    Contents,
    Permissions,
    Achievements,
    Subscriptions,
    Notifications
  }

  # generate password length.
  @password_length 12

  @doc """
    Functions to process query callback user auth token.

  ## Examples

      def callback(_parent, %{username: username, password: password}, _resolution) do
        {:ok, token} = Auth.callback_auth(username, password)
      end
  """
  @spec callback(map(), %{username: String.t(), password: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, String.t()}
  def callback(_parent, %{email: email, password: password}, _resolution) do
    KnowledgeAdmin.Auth.callback_auth(email, password)
  end

  @doc """
    This function implements the use of a particular role.

  ## Examples

      def set_user_role(_parent, %{username: username, password: password}, _resolution) do
        {:ok, role} = Accounts.set_user_role(user_id, role)
      end
  """
  @spec set_user_role(map(), %{user_id: Integer.t(), role: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, User.t()} | {:error, Ecto.ChangeError.t()}
  def set_user_role(_parent, %{user_id: user_id, role: role}, _resolution) do
    user_id
    |> Accounts.get_user!()
    |> Accounts.update_user(%{role: role})
  end

  @doc """
    This function implements the use of access by which it can be authorized.

  ## Examples

      def create_user_access(_parent, %{user_id: user_id, password: password}, _resolution) do
        user_id
        |> Accounts.get_user!()
        |> Accounts.update_user(%{password: password})
      end
  """
  @spec create_user_access(
          map(),
          %{user_id: Integer.t(), username: String.t(), password: String.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError.t()}
  def create_user_access(
        _parent,
        %{user_id: user_id, password: password},
        _resolution
      ) do
    user_id
    |> Accounts.get_user!()
    |> Accounts.update_user(%{password: password})
  end

  @doc """
    This feature implements a user's creation of a role for the web panel.

  ## Examples

      iex>  Mutation.create_user(%{email: email, status: status, last_name: last_name, first_name: first_name, role: role})
      {:ok, %User{}}

      # Not all fields will be transferred.
      iex>  Mutation.create_user(%{email: email})
      {:error, %Ecto.ChangeError{}}
  """
  @spec create_user(
          map(),
          %{
            email: String.t(),
            status: String.t(),
            last_name: String.t(),
            first_name: String.t(),
            password: String.t(),
            role: String.t()
          },
          Absinthe.Resolution.t()
        ) :: {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError.t()}
  def create_user(_parent, args, _resolution) do
    args =
      if is_nil(args.password) do
        Map.put(
          args,
          :password,
          :crypto.strong_rand_bytes(@password_length)
          |> Base.encode64()
          |> binary_part(0, @password_length)
        )
      else
        args
      end

    {:ok, user} =
      args
      |> Map.put(:avatar, Accounts.default_avatar(args.email))
      |> Accounts.create_user()

    EmailWorker.sign_in_email(user)

    {:ok, user}
  end

  @doc """
    This feature implements the user's password change to login to the Web admin panel.

  ## Examples

      iex>  Mutation.change_password(%{password: password})
      {:ok, "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9"}

      # Field does not matter.
      iex>  Mutation.change_password(%{password: ""})
      {:error, %Ecto.ChangeError{}}
  """
  @spec change_password(Accounts.User.t(), %{password: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, String.t()} | {:error, Ecto.ChangeError.t()}
  def change_password(_parent, %{password: password, hash: hash}, _resolution)
      when not is_nil(password) and not is_nil(hash) do
    with change_hash <- Users.get_change_password_by_hash(hash),
         true <- !is_nil(change_hash) do
      case change_hash.user_id
           |> Accounts.get_user!()
           |> Accounts.update_user(%{password: password}) do
        {:ok, user} ->
          user.id
          |> Users.get_change_password_by_user()
          |> Enum.map(&Users.delete_change_password(&1))

          {:ok, true}

        {:error, changeset} ->
          {:error, changeset}
      end
    else
      _ ->
        {:error, "Hash is not valid!"}
    end
  end

  def change_password(parent, %{password: password}, _resolution) do
    case parent.id
         |> Accounts.get_user!()
         |> Accounts.update_user(%{password: password}) do
      {:ok, user} ->
        {:ok, jwt, _claims} = Guardian.encode_and_sign(user)
        {:ok, jwt}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    This function implements the creation of a check by mail or user in the database.

    If the user exists then creates a record for him in the table ChangePassword and sends a letter to the mail for a search or reject the request.

  ## Examples

      iex>  Mutation.forgot_password(%{email: "test@example.com"})
      {:ok, true}

      # User with current email doesn`t exist in db.
      iex>  Mutation.forgot_password(%{email: ""})
      {:error, "User with such mail does not exist!"}
  """
  @spec forgot_password(Accounts.User.t(), %{email: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, String.t()} | {:error, Ecto.ChangeError.t()}
  def forgot_password(_parent, %{email: email}, %{context: %{pubsub: pubsub}}) do
    with user <- Accounts.get_user_by_email(email),
         true <- !is_nil(user) do
      {:ok, change_password} =
        Users.create_change_password(%{
          hash: Phoenix.Token.sign(pubsub, "knowledge_smile_admin", user.id),
          user_id: user.id
        })

      EmailWorker.recover_password_in_email(email, change_password)
      {:ok, true}
    else
      _ ->
        {:error, "User with such mail does not exist!"}
    end
  end

  @doc """
    This feature deletes a record in a table [`ChangePassword`](SmileDB.Users.ChangePassword.html).

  ## Examples

      iex>  Mutation.cancel_change_password(%{hash: "asdad123123adad1231asd"})
      {:ok, true}

      iex>  Mutation.cancel_change_password(%{hash: "asdad123123adad1asdasd231asd"})
      {:ok, false}
  """
  @spec cancel_change_password(map(), %{hash: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Users.ChangePassword.t()} | {:error, Ecto.ChangeError.t()}
  def cancel_change_password(_parent, %{hash: hash}, _resolution) do
    with change_password <- Users.get_change_password_by_hash(hash),
         true <- !is_nil(change_password) do
      Users.delete_change_password(change_password)
      {:ok, true}
    else
      _ ->
        {:ok, false}
    end
  end

  @doc """
    This function implements the creation of related content.

  ## Example

      iex> Mutation.create_related_contents(_parent, %{related_content_id: 2, content_id: 1}, _resolution)
      {:ok, %Contents.RelatedContent{}}

      # related_content_id: 123  - value doesn`t exist in db.
      iex> Mutation.create_related_contents(_parent, %{related_content_id: 123, content_id: 1}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec create_related_contents(
          map(),
          %{related_content_id: Integer.t(), content_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Contents.RelatedContent.t()} | {:error, Ecto.ChangeError.t()}
  def create_related_contents(_parent, args, _resolution) do
    Contents.create_related_content(args)
  end

  @doc """
    This function implements the delete of related content.

  ## Example

      iex> Mutation.delete_related_content(_parent, %{related_content_id: 2, content_id: 1}, _resolution)
      {:ok, %Contents.RelatedContent{}}

      # related_content_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_related_content(_parent, %{related_content_id: 123, content_id: 1}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_related_content(
          map(),
          %{related_content_id: Integer.t(), content_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Contents.RelatedContent.t()} | {:error, Ecto.ChangeError.t()}
  def delete_related_content(_parent, args, _resolution) do
    args
    |> Contents.get_related_content()
    |> Contents.delete_related_content()
  end

  @doc """
    Created hiden [`Content`](SmileDB.Source.Content.html).

  ## Example

      iex> Mutation.update_user_content(_parent, %{content_id: 1, shadow: true}, _resolution)
      {:ok, %Content{}}

      iex> Mutation.update_user_content(_parent, %{content_id: 1, shadow: false}, _resolution)
      {:ok, %Content{}}

      # content_id: 123  - value doesn`t exist in db.
      iex> Mutation.update_user_content(_parent, %{content_id: 123, shadow: false}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_user_content(
          Accounts.User.t(),
          %{
            content_id: Integer.t(),
            shadow: boolean,
            title: String.t(),
            status: String.t(),
            tags: list(String.t()),
            description: String.t(),
            anonymous: boolean
          },
          Absinthe.Resolution.t()
        ) :: {:ok, Source.Content.t()} | {:error, Ecto.ChangeError.t()}
  def update_user_content(_parent, args, _resolution) do
    args.content_id
    |> Source.get_content!()
    |> Source.update_content(args)
  end

  @doc """
    Created hiden [`Answer`](SmileDB.Source.Answer.html).

  ## Example

      iex> Mutation.create_user_answer(_parent, %{answer_id: 1, shadow: true, tags: ["test"], description: "test description", anonymous: true}, _resolution)
      {:ok, %Answer{}}

      # answer_id: 123  - value doesn`t exist in db.
      iex> Mutation.create_user_answer(_parent, %{answer_id: 123, shadow: false}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec create_user_answer(map(), map(), Absinthe.Resolution.t()) ::
          {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError.t()}
  def create_user_answer(_parent, args, %{context: %{current_user: user}}) do
    case args
         |> Map.put(:user_id, user.id)
         |> Map.put(:author, user.nickname)
         |> Source.create_answer() do
      {:ok, answer} ->
        ((answer.content_id
          |> Subscriptions.list_users_id_from_contents_subscription()
          |> Enum.uniq()) -- [user.id])
        |> Enum.map(
          &%{
            user_id: &1,
            answer_id: answer.id
          }
        )
        |> Notifications.create_answer_notifications()
        |> Enum.map(
          &Map.merge(&1, %{
            shadow: answer.shadow,
            anonymous: answer.anonymous
          })
        )
        |> Enum.map(&Notifications.put_notifications(&1))

        Source.put_feed(answer)
        {:ok, answer}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    Created hiden [`Answer`](SmileDB.Source.Answer.html).

  ## Example

      iex> Mutation.update_user_answer(_parent, %{answer_id: 1, shadow: true}, _resolution)
      {:ok, %Answer{}}

      iex> Mutation.update_user_answer(_parent, %{answer_id: 1, shadow: false}, _resolution)
      {:ok, %Answer{}}

      # answer_id: 123  - value doesn`t exist in db.
      iex> Mutation.update_user_answer(_parent, %{answer_id: 123, shadow: false}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_user_answer(
          Accounts.User.t(),
          %{
            answer_id: Integer.t(),
            shadow: boolean,
            tags: list(String.t()),
            description: String.t(),
            anonymous: boolean
          },
          Absinthe.Resolution.t()
        ) :: {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError.t()}
  def update_user_answer(_parent, args, _resolution) do
    args.answer_id
    |> Source.get_answer!()
    |> Source.update_answer(args)
  end

  @doc """
    Created hidden [`Comment`](SmileDB.Source.Comment.html).

  ## Example

      iex> Mutation.update_user_comment(_parent, %{comment_id: 1, shadow: true}, _resolution)
      {:ok, %Comment{}}

      iex> Mutation.update_user_comment(_parent, %{comment_id: 1, shadow: false}, _resolution)
      {:ok, %Comment{}}

      # comment_id: 123  - value doesn`t exist in db.
      iex> Mutation.update_user_comment(_parent, %{comment_id: 123, shadow: false}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_user_comment(
          Accounts.User.t(),
          %{
            comment_id: Integer.t(),
            shadow: boolean,
            description: String.t(),
            anonymous: boolean
          },
          Absinthe.Resolution.t()
        ) :: {:ok, Source.Comment.t()} | {:error, Ecto.ChangeError.t()}
  def update_user_comment(_parent, args, _resolution) do
    args.comment_id
    |> Source.get_comment!()
    |> Source.update_comment(args)
  end

  @doc """
    Hidden all [`Comment`](SmileDB.Source.Comment.html) level.

  ## Example

      iex> Mutation.hidden_comment_level(_parent, %{comment_id: 1, shadow: true}, _resolution)
      {:ok, %Comment{}}

      iex> Mutation.hidden_comment_level(_parent, %{comment_id: 1, shadow: false}, _resolution)
      {:ok, %Comment{}}

      # comment_id: 123  - value doesn`t exist in db.
      iex> Mutation.hidden_comment_level(_parent, %{comment_id: 123, shadow: false}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec hidden_comment_level(
          map(),
          %{comment_id: Integer.t(), shadow: boolean},
          Absinthe.Resolution.t()
        ) :: {:ok, list(Source.Comment.t())} | {:error, Ecto.ChangeError.t()}
  def hidden_comment_level(_parent, %{comment_id: comment_id, shadow: shadow}, _resolution) do
    {:ok,
     search index: ElasticSearch.get_meta!(Source.Comment).index do
       query do
         term("path", comment_id)
       end
     end
     |> ElasticSearch.Repo.all()
     |> Enum.map(&Source.update_comment(&1, %{shadow: shadow}))
     |> Enum.map(&elem(&1, 1))}
  end

  @doc """
    Deleted [`Content`](SmileDB.Source.Content.html).

  ## Example

      iex> Mutation.delete_user_content(_parent, %{content_id: 1}, _resolution)
      {:ok, %Content{}}

      # content_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_user_content(_parent, %{content_id: 123}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_user_content(
          Accounts.User.t(),
          %{content_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Source.Content.t()} | {:error, Ecto.ChangeError.t()}
  def delete_user_content(_parent, %{content_id: content_id}, _resolution) do
    content_id
    |> Source.get_content!()
    |> Source.delete_content()
  end

  @doc """
    Deleted [`Answer`](SmileDB.Source.Answer.html).

  ## Example

      iex> Mutation.delete_user_answer(_parent, %{answer_id: 1}, _resolution)
      {:ok, %Answer{}}

      # answer_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_user_answer(_parent, %{answer_id: 123}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_user_answer(Accounts.User.t(), %{answer_id: Integer.t()}, Absinthe.Resolution.t()) ::
          {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError.t()}
  def delete_user_answer(_parent, %{answer_id: answer_id}, _resolution) do
    answer_id
    |> Source.get_answer!()
    |> Source.delete_answer()
  end

  @doc """
    Deleted [`Comment`](SmileDB.Source.Comment.html).

  ## Example

      iex> Mutation.delete_user_comment(_parent, %{comment_id: 1}, _resolution)
      {:ok, %Comment{}}

      # comment_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_user_comment(_parent, %{comment_id: 123}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_user_comment(
          Accounts.User.t(),
          %{comment_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Source.Comment.t()} | {:error, Ecto.ChangeError.t()}
  def delete_user_comment(_parent, %{comment_id: comment_id}, _resolution) do
    comment_id
    |> Source.get_comment!()
    |> Source.delete_comment()
  end

  @doc """
    Created  [`ExpertRequest`](SmileDB.Users.Expert.html).

  ## Example

      iex> Mutation.put_expert_to_user(_parent, %{tags: ["test"], user_id: 1}, _resolution)
      {:ok, %User{}}

      iex> Mutation.put_expert_to_user(_parent, %{tags: [""], user_id: 1}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec put_expert_to_user(
          map(),
          %{tags: list(String.t()), user_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Users.Expert.t()} | {:error, Ecto.ChangeError.t()}
  def put_expert_to_user(_parent, %{tags: tags, user_id: user_id}, _resolution) do
    tags
    |> Enum.uniq()
    |> Enum.map(&Tags.get_or_insert_tag(Macro.underscore(&1)))

    with current_user <- Accounts.get_user!(user_id),
         data <-
           %{
             expert:
               current_user.expert
               |> List.flatten(tags)
               |> Enum.uniq()
           },
         data <-
           if(
             Enum.empty?(current_user.expert),
             do: Map.put(data, :start_expert, DateTime.utc_now()),
             else: data
           ),
         {:ok, user} <- Accounts.update_user(current_user, data) do
      tags
      |> Users.get_experts_by_user(user_id)
      |> Enum.map(&Users.delete_expert(&1))

      {:ok, user}
    end
  end

  @doc """
    Deleted  [`ExpertRequest`](SmileDB.Users.Expert.html).

  ## Example

      iex> Mutation.delete_expert_request(_parent, %{expert_request_id: 1}, _resolution)
      {:ok, %Expert{}}

      # expert_request_id: 1  - value doesn`t exist in db.
      iex> Mutation.delete_expert_request(_parent, %{expert_request_id: 1}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_expert_request(
          Accounts.User.t(),
          %{expert_request_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Users.Expert.t()} | {:error, Ecto.ChangeError.t()}
  def delete_expert_request(_parent, %{user_id: user_id, tags: tags}, _resolution) do
    tags
    |> Users.get_experts_by_user(user_id)
    |> Enum.map(&Users.delete_expert(&1))

    {:ok, Accounts.get_user!(user_id)}
  end

  @doc """
    Confirm [`ExpertRequest`](SmileDB.Users.Expert.html).

  ## Example

      iex> Mutation.confirm_expert_request(_parent, %{expert_request_id: 1}, _resolution)
      {:ok, %Expert{}}

      # expert_request_id: 123  - value doesn`t exist in db.
      iex> Mutation.confirm_expert_request(_parent, %{expert_request_id: 123}, _resolution)
      ** (Ecto.NoResultsError)

  """
  @spec confirm_expert_request(
          Accounts.User.t(),
          %{expert_request_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Users.Expert.t()} | {:error, Ecto.ChangeError.t()}
  def confirm_expert_request(_parent, %{user_id: user_id, tags: tags}, _resolution) do
    with list_of_experts_requests <- Users.get_experts_by_user(user_id),
         user <- Accounts.get_user!(user_id),
         {:ok, user} <-
           Accounts.update_user(user, %{
             expert:
               user.expert
               |> List.flatten(tags)
               |> Enum.uniq()
           }) do
      list_of_experts_requests
      |> Enum.map(&Users.delete_expert(&1))

      {:ok, user}
    end
  end

  @doc """
    Created [`Permission`](SmileDB.Permissions.Permission.html).

  ## Example

      iex> Mutation.create_user_permission(_parent, %{name: "create new role"}, _resolution)
      {:ok, %Permission{}}

      iex> Mutation.create_user_permission(_parent, %{name: ""}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec create_permission(map(), %{name: String}, Absinthe.Resolution.t()) ::
          {:ok, Permissions.Permission.t()} | {:error, Ecto.ChangeError.t()}
  def create_permission(_parent, args, _resolution) do
    case Permissions.create_permission(args) do
      {:ok, permissiion} ->
        roles = Accounts.list_roles()

        Enum.map(roles, fn role ->
          Permissions.create_role_access(%{permission_id: permissiion.id, role: role.role})
        end)

        Task.async(fn -> Permissions.assign_permission_by_role(roles) end)

        {:ok, permissiion}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    Updated [`Permission`](SmileDB.Permissions.Permission.html).

  ## Example

      iex> Mutation.update_permission(_parent, %{permission_id: 1, name: "create new role"}, _resolution)
      {:ok, %Permission{}}

      iex> Mutation.update_permission(_parent, %{permission_id: 1, name: ""}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_permission(
          map(),
          %{permission_id: Integer.t(), name: String.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Permissions.Permission.t()} | {:error, Ecto.ChangeError.t()}
  def update_permission(_parent, args, _resolution) do
    args.permission_id
    |> Permissions.get_permission!()
    |> Permissions.update_permission(args)
  end

  @doc """
    Deleted [`Permission`](SmileDB.Permissions.Permission.html).

  ## Example

      iex> Mutation.delete_permission(_parent, %{permission_id: 1}, _resolution)
      {:ok, %Permission{}}

      # expert_request_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_permission(_parent, %{permission_id: 1}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_permission(
          map(),
          %{permission_id: Integer.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Permissions.Permission.t()} | {:error, Ecto.ChangeError.t()}
  def delete_permission(_parent, %{permission_id: permission_id}, _resolution) do
    case permission_id
         |> Permissions.get_permission!()
         |> Permissions.delete_permission() do
      {:ok, permission} ->
        roles = Accounts.list_roles()

        Task.async(fn -> Permissions.assign_permission_by_role(roles) end)

        {:ok, permission}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    Created [`Achieve`](SmileDB.Achievements.Achieve.html).

  ## Example

      iex> Mutation.create_achieve(_parent, %{title: "test", avatar: "//avatar.png"}, _resolution)
      {:ok, %Achieve{}}

      iex> Mutation.create_achieve(_parent, %{title: "", avatar: ""}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec create_achieve(
          map(),
          %{title: String.t(), avatar: String.t()},
          Absinthe.Resolution.t()
        ) :: {:ok, Achievements.Achieve.t()} | {:error, Ecto.ChangeError.t()}
  def create_achieve(_parent, args, _resolution) do
    Achievements.create_achieve(args)
  end

  @doc """
    Updated [`Achieve`](SmileDB.Achievements.Achieve.html).

  ## Example

      iex> Mutation.update_achieve(_parent, %{achieve_id: 1, title: "test", avatar: "//avatar.png"}, _resolution)
      {:ok, %Achieve{}}

      iex> Mutation.update_achieve(_parent, %{achieve_id: 1, title: "", avatar: ""}, _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_achieve(
          map(),
          %{
            achieve_id: Integer.t(),
            title: String.t(),
            avatar: String.t()
          },
          Absinthe.Resolution.t()
        ) :: {:ok, Achievements.Achieve.t()} | {:error, Ecto.ChangeError.t()}
  def update_achieve(_parent, args, _resolution) do
    args.achieve_id
    |> Achievements.get_achieve!()
    |> Achievements.update_achieve(args)
  end

  @doc """
    Deleted [`Achieve`](SmileDB.Achievements.Achieve.html).

  ## Example

      iex> Mutation.delete_achieve(_parent, %{achieve_id: 1}, _resolution)
      {:ok, %Achieve{}}

      # achieve_id: 123  - value doesn`t exist in db.
      iex> Mutation.delete_achieve(_parent, %{achieve_id: 1), _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec delete_achieve(map(), %{achieve_id: Integer.t()}, Absinthe.Resolution.t()) ::
          {:ok, Achievements.Achieve.t()} | {:error, Ecto.ChangeError.t()}
  def delete_achieve(_parent, args, _resolution) do
    args.achieve_id
    |> Achievements.get_achieve!()
    |> Achievements.delete_achieve()
  end

  @doc """
     Set user [`Achieve`](SmileDB.Achievements.Achieve.html).

  ## Example

      iex> Mutation.set_user_achieves(_parent, %{achieve_id: [1], user_id: 1}, _resolution)
      {:ok, %User{}}

      # achieve_id: 1  - value doesn`t exist in db.
      iex> Mutation.set_user_achieves(_parent, %{achieve_id: [1], user_id: 1), _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec set_user_achieves(
          map(),
          %{user_id: Integet.t(), achieves_id: list(Integet.t())},
          Absinthe.Resolution.t()
        ) :: {:ok, Achievements.UserAchieve.t() | {:error, Ecto.ChangeError.t()}}
  def set_user_achieves(_parent, %{user_id: user_id, achieves_id: achieves_id}, _resolution) do
    achieves =
      Achievements.UserAchieve
      |> where([ua], ua.user_id == ^user_id)
      |> SmileDB.Repo.all()

    achieves
    |> Enum.filter(&(not (&1.achieve_id in achieves_id)))
    |> Enum.map(&Achievements.delete_user_achieve(&1))

    (achieves_id -- Enum.map(achieves, & &1.achieve_id))
    |> Enum.map(&Achievements.create_user_achieve(%{achieve_id: &1, user_id: user_id}))

    {:ok, ElasticSearch.Repo.get(Accounts.User, user_id)}
  end

  @doc """
    Update info by [`User`](SmileDB.Accounts.User.html).

  ## Examples

      iex> Mutation.update_user_info(_parent, %{user_id: 123, expert: ["test"]}, _resolution)
      {:ok, %User{}}

      # user_id: 123  - value doesn`t exist in db.
      iex> Mutation.update_user_info(_parent, %{user_id: 123, expert: ["test"]), _resolution)
      {:error, %Ecto.ChangeError{}}

  """
  @spec update_user_info(map(), map(), map()) :: {:ok, User.t()} | {:error, Ecto.ChangeError.t()}
  def update_user_info(_parent, args, _resolution) do
    args.user_id
    |> Accounts.get_user!()
    |> Accounts.update_user(args)
  end

  @doc """
  Hidden all content from [`User`](SmileDB.Accounts.User.html).

  ## Examples

      iex> Mutation.unhidden_account_content(_parent, %{user_id: 123}, _resolution)
      :ok

      iex> Mutation.unhidden_account_content(_parent, %{user_id: 123}, _resolution)
      :ok
  """
  @spec unhidden_account_content(map(), %{user_id: Integer.t()}, map()) :: atom
  def unhidden_account_content(_parent, %{user_id: user_id}, _resolution) do
    with user <- Accounts.get_user(user_id),
         {:ok, user} <- Accounts.update_user(user, %{status: "active"}),
         :ok <- Accounts.Hidden.visible(user.id, false) do
      {:ok, user}
    end
  end

  @doc """
  Hidden all content from [`User`](SmileDB.Accounts.User.html).

  ## Examples

      iex> Mutation.hidden_account_content(_parent, %{user_id: 123, time: 123123123123123}, _resolution)
      :ok

      iex> Mutation.hidden_account_content(_parent, %{user_id: 123), _resolution)
      :ok
  """
  @spec hidden_account_content(map(), %{user_id: Integer.t()}, map()) :: atom
  def hidden_account_content(_parent, %{user_id: user_id}, _resolution) do
    with user <- Accounts.get_user(user_id),
         {:ok, user} <- Accounts.update_user(user, %{status: "hidden"}),
         :ok <- Accounts.Hidden.visible(user.id, true) do
      {:ok, user}
    end
  end

  @doc """
  Ban access from [`User`](SmileDB.Accounts.User.html).

  ## Examples

      iex> Mutation.ban_account(_parent, %{user_id: 123, time: 123123123123123}, _resolution)
      :ok

      iex> Mutation.ban_account(_parent, %{user_id: 123), _resolution)
      :ok
  """
  @spec ban_account(map(), %{user_id: Integer.t(), time: Integer.t()}, map()) ::
          {:ok, Accounts.User.t()}
  def ban_account(_parent, %{user_id: user_id, time: time}, _resolution) do
    with {:ok, user} <- Accounts.ban_account(user_id, time),
         :ok <- Accounts.Block.ban(user.id, time) do
      {:ok, user}
    end
  end

  def ban_account(_parent, %{user_id: user_id}, _resolution) do
    Accounts.ban_account(user_id)
  end

  @doc """
  Unban access from [`User`](SmileDB.Accounts.User.html).

  ## Examples

      iex> Mutation.unban_account(_parent, %{user_id: 123), _resolution)
      :ok
  """
  @spec unban_account(map(), %{user_id: Integer.t(), time: Integer.t()}, map()) ::
          {:ok, Accounts.User.t()}
  def unban_account(_parent, %{user_id: user_id}, _resolution) do
    with {:ok, user} <- Accounts.unban_account(user_id),
         :ok <- Accounts.Block.unban(user.id) do
      {:ok, user}
    end
  end

  @doc """
    Assingn a [`Role`](Accounts.Role.html) permissions.

    This feature implements [`Role`](Accounts.Role.html)-specialisation updates.

  ## Examples

      iex>  Mutation.assign_role_permissions(_parent, %{access_data: 123456789}, _resolution)
      {:ok, true}

      iex>  Mutation.assign_role_permissions(_parent, %User{access_data: "asd", _resolution)
      {:error, %Ecto.ChangeError{}}
  """
  @spec assign_role_permissions(map(), %{access_data: String.t()}, map()) :: :ok
  def assign_role_permissions(_parent, %{access_data: data}, _resolution) do
    Enum.map(Jason.decode!(~s(#{data})), fn {key, values} ->
      states =
        values
        |> Integer.to_charlist(2)
        |> to_string()
        |> String.codepoints()

      role_access =
        search(index: ElasticSearch.get_meta!(Permissions.RoleAccess).index) do
          query do
            term("role.keyword", key)
          end

          sort do
            [permission_id: :asc]
          end
        end
        |> ElasticSearch.Repo.all()

      Enum.map(0..(Enum.count(role_access) - 1), fn index ->
        Permissions.update_role_access(Enum.at(role_access, index), %{
          state: Enum.at(states, index)
        })
      end)

      Accounts.get_role!(key)
      |> Accounts.update_role(%{access_code: values})
    end)

    {:ok, true}
  end

  @doc """
    Create a user [`Role`](Accounts.Role.html).

  ## Examples

      iex>  Accounts.create_role(_parent, %{role: "test"}, _)
      {:ok, %Accounts.Role{}}

      iex>  Accounts.create_role(%User{role: "test")
      {:error, %Ecto.ChangeError{}}
  """
  @spec create_role(map(), %{role: String.t()}, map()) ::
          {:ok, Accounts.Role.t()} | {:error, Ecto.ChangeError.t()}
  def create_role(_parent, %{role: role}, _resolution) do
    case Accounts.create_role(%{role: role, access_code: 0}) do
      {:ok, role} ->
        search index: ElasticSearch.get_meta!(Permissions.Permission).index do
          query do
            bool do
              must do
                match_all(boost: 1.0)
              end
            end
          end
        end
        |> ElasticSearch.Repo.all()
        |> Enum.map(&Permissions.create_role_access(%{permission_id: &1.id, role: role.role}))

        {:ok, role}

      {:error, changeset} ->
        {:error, changeset}
    end
  end
end
