defmodule KnowledgeAdmin.Auth do
  @moduledoc false
  import Comeonin.Pbkdf2, only: [checkpw: 2]
  alias SmileDB.Accounts
  alias SmileDB.Auth.Guardian

  @spec callback_auth(String.t(), String.t()) :: {:ok, Accounts.User.t()} | {:error, String.t()}
  def callback_auth(email, password) do
    case authenticate_user(email, password) do
      {:ok, user} ->
        {:ok, jwt, _claims} = Guardian.encode_and_sign(user)
        {:ok, jwt}

      {:error, _reason} ->
        {:error, "Invalid username/password combination"}
    end
  end

  @spec authenticate_user(String.t(), String.t()) ::
          {:ok, Accounts.User.t()} | {:error, String.t()}
  def authenticate_user(email, given_password) do
    email
    |> Accounts.get_user_by_email()
    |> check_password(given_password)
  end

  @spec check_password(String.t(), String.t()) :: {:ok, Accounts.User.t()} | {:error, String.t()}
  defp check_password(nil, _), do: {:error, "Incorrect username or password"}

  defp check_password(user, given_password) do
    case checkpw(given_password, user.password_hash) do
      true -> {:ok, user}
      false -> {:error, "Incorrect username or password"}
    end
  end
end
