defmodule KnowledgeAdmin.Mixfile do
  use Mix.Project

  def project do
    [
      app: :knowledge_admin,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {KnowledgeAdmin.Application, []},
      extra_applications: [:smile_db, :runtime_tools],
      env: []
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:smile_db, in_umbrella: true},
      {:guardian, "~> 1.1.1"},
      {:phoenix, "~> 1.3.4"},
      {:absinthe, "~> 1.4.13"},
      {:absinthe_plug, "~> 1.4.5"},
      {:absinthe_phoenix, "~> 1.4.3"},
      {:dataloader, "~> 1.0.4"}
    ]
  end
end
