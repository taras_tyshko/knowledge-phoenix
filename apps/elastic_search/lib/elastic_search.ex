defmodule ElasticSearch do
  @moduledoc """
    The module implements the function with which you can create a structure for [`Elasticsearch`](ElasticSearch.html) and get metadata relative to the specified model.

    To work with this module, use the following `alias`:

      alias ElasticSearch.Mapping
      alias ElasticSearch.Entity.Fields
      alias ElasticSearch.Entity.TriggerUpdate

    Also need to use:

      use ExConstructor
  """
  alias ElasticSearch.Mapping
  alias ElasticSearch.Entity.Fields
  alias ElasticSearch.Entity.TriggerUpdate

  @type t :: %ElasticSearch{
          index: atom(),
          type: atom(),
          primary_key: list(atom()),
          fields: Fields.t(),
          trigger_update: TriggerUpdate.t(),
          cascade_delete: list(atom()),
          reindex_function: Function.information()
        }
  defstruct(
    index: nil,
    type: nil,
    primary_key: [],
    fields: %Fields{},
    trigger_update: %TriggerUpdate{},
    cascade_delete: [],
    cascade_decrement: [],
    reindex_function: &ElasticSearch.Ecto.put/1
  )

  use ExConstructor

  @doc """
    This furction implements a task for the mix which does the launch of mapping tables in [`Elasticsearch`](ElasticSearch.html)

  ## Exapmle

      iex> ElasticSearch.setup
      :ok
  """
  @spec setup() :: atom
  def setup() do
    Application.fetch_env!(:elastic_search, :indexes)
    |> Enum.each(fn model ->
      model
      |> get_meta!()
      |> Mapping.create()
    end)
  end

  @doc """
    This function implements the receipt metadata of the object model in the table.

  ## Examples

      iex> ElasticSearch.get_meta!(SmileDB.Source.Content)
      %ElasticSearch{}

      iex> ElasticSearch.get_meta!(%SmileDB.Source.Content{})
      %ElasticSearch{}
  """
  @spec get_meta!(struct()) :: ElasticSearch.t()
  def get_meta!(struct) do
    with meta <- get_meta(struct),
         true <- !is_nil(meta) do
      meta
    else
      _ -> raise "no such metadata"
    end
  end

  @doc """
    This function implements the receipt metadata of the object model in the table.

  ## Examples

      iex> ElasticSearch.get_meta(SmileDB.Source.Content)
      %ElasticSearch{}

      iex> ElasticSearch.get_meta(%SmileDB.Source.Content{})
      %ElasticSearch{}
  """
  @spec get_meta(struct()) :: ElasticSearch.t()
  def get_meta(model) when is_map(model), do: get_meta(model.__struct__) 

  def get_meta(struct) when is_atom(struct) do
    struct
    |> apply(:__schema__, [:elastic_search])
    |> Map.put(:primary_key, struct.__schema__(:primary_key))
  end
end
