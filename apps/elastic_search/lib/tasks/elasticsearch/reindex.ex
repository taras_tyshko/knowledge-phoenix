defmodule Mix.Tasks.Elasticsearch.Reindex do
  @moduledoc false

  use Mix.Task

  def run(args) do
    Application.ensure_all_started(:smile_db)
    {options, models} = OptionParser.parse!(args)

    indexes = Application.fetch_env!(:elastic_search, :indexes)

    (if(!Enum.empty?(models), do: models) || indexes)
    |> Enum.map(fn model ->
      case model do
        atom when is_atom(atom) ->
          atom

        string when is_binary(string) ->
          String.to_existing_atom("Elixir.#{string}")
      end
    end)
    |> Enum.each(fn model ->
      options[:repo]
      |> repo()
      |> ElasticSearch.Ecto.reindex(model)
    end)
  end

  defp repo("Elixir." <> repo), do: :"Elixir.#{repo}"
  defp repo(repo), do: :"Elixir.#{repo}"
end
