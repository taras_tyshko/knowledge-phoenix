defmodule Mix.Tasks.Elasticsearch.Setup do
  @moduledoc """
    This module is a rally-calling function which does launch function to create the desired structure in [`Elasticsearch`](ElasticSearch.html).
    
    To operate the module, use: `use Mix.Task`
  """

  use Mix.Task

  @doc """
    This furction implements a task for the mix which does the launch of mapping tables in [`Elasticsearch`](ElasticSearch.html).

  ## Example

      use Mix.Task

      iex> Mix.Tasks.Elasticsearch.Setup.run()
      :ok
  """
  @spec run(String.t()) :: atom
  def run(_) do
    ElasticSearch.setup()
  end
end
