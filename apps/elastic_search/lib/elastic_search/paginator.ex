defmodule ElasticSearch.Paginator do
  @moduledoc """
    This function implements the declared type for the [`Pagination`](ElasticSearch.Paginator.html).

    Also used is a call module [`ExConstructor`](https://hexdocs.pm/exconstructor/ExConstructor.html).

    Add `use ExConstructor` after a [`defstruct`](https://hexdocs.pm/exconstructor/ExConstructor.html#__using__/1) statement to inject a constructor function into the module.
    The generated constructor, called new by default, handles map-vs-keyword-list, string-vs-atom-keys, and camelCase-vs-under_score input data issues automatically, DRYing up your code and letting you move on to the interesting parts of your program.

  ## Fields available in the default.

    * entries - a list entries contained in this page.
    * metadata - metadata attached to this page.
  """

  @type t :: %__MODULE__{
          entries: [any()] | [],
          metadata: ElasticSearch.Paginator.Metadata.t()
        }

  defstruct [:metadata, :entries]

  use ExConstructor
end
