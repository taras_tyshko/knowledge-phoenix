defmodule ElasticSearch.Repo do
  import Tirexs.Search

  @doc """
  Gets a content data by `ID`.

  Raises `** (RuntimeError) not found` if the `struct` does not exist.

  ## Examples

      iex> ElasticSearch.Repo.get!(SmileDB.Source.Content, 123)
      %Content{}

      iex> ElasticSearch.Repo.get!(SmileDB.Source.Content, 456)
      ** (RuntimeError) not found

  """
  @spec get!(struct, Integer.t()) :: struct | RuntimeError.t()
  def get!(model, id) do
    result = get(model, id)
    if result, do: result, else: throw(Ecto.NoResultsError)
  end

  @doc """
  Gets a content data by `ID`.

  Raises `nil` if the `struct` does not exist.

  ## Examples

      iex> ElasticSearch.Repo.get(SmileDB.Source.Answer, 123)
      %Answer{}

      iex> ElasticSearch.Repo.get(SmileDB.Source.Answer, 456)
      nil

  """
  @spec get(struct, Integer.t()) :: struct | nil
  def get(model, id) do
    with doc <- ElasticSearch.get_meta!(model),
         result <- Tirexs.HTTP.get("/#{doc.index}/#{doc.type}/#{id}"),
         {:ok, model} <- Tuple.delete_at(result, 1) do
      deserialize(model)
    else
      _ -> nil
    end
  end

  @doc """
  Gets a content data by `key: value`.

  Raises `** (throw) Ecto.NoResultsError` if the `struct` does not exist.

  ## Examples

      iex> ElasticSearch.Repo.get_by!(SmileDB.Source.Comment, answer_id: 123)
      %Comment{}

      iex> ElasticSearch.Repo.get_by!(SmileDB.Source.Comment, answer_id: 456)
      ** (throw) Ecto.NoResultsError

  """
  @spec get_by!(struct, list()) :: struct | Ecto.NoResultsError.t()
  def get_by!(model, params_pairs) when is_list(params_pairs) do
    case get_by(model, params_pairs) do
      nil -> throw(Ecto.NoResultsError)
      result -> result
    end
  end

  @doc """
  Gets a content data by `key: value`.

  Raises `** (throw) Ecto.MultipleResultsError` if the `struct` does not exist.

  ## Examples

      iex> ElasticSearch.Repo.get_by!(SmileDB.Accounts.User, nickname: "nickname")
      %User{}

      iex> ElasticSearch.Repo.get_by!(SmileDB.Accounts.User, nickname: "sometest")
      ** (throw) Ecto.MultipleResultsError

      iex> ElasticSearch.Repo.get_by!(SmileDB.Accounts.User, nickname: 456)
      nil

  """
  @spec get_by(struct, list()) :: struct | Ecto.MultipleResultsError.t() | nil
  def get_by(model, params_pairs) when is_list(params_pairs) do
    search_result =
      search index: ElasticSearch.get_meta!(model).index do
        query do
          bool do
            must do
            end
          end
        end
      end
      |> put_in(
        [:search, :query, :bool, :must],
        Enum.map(params_pairs, fn {key, value} -> [match: ["#{key}": value]] end)
      )
      |> all()

    cond do
      search_result == [] -> nil
      Enum.count(search_result) == 1 -> List.first(search_result)
      Enum.count(search_result) > 1 -> throw(Ecto.MultipleResultsError)
    end
  end

  @doc """
  Put a content data to [`ElasticSearch`](ElasticSearch.html).

  ## Examples

      iex> ElasticSearch.Repo.put(%SmileDB.Accounts.User{})
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.put({:ok, %SmileDB.Accounts.User{}}, [])
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.put({:error, %Ecto.Changeset{}}, [])
      {:error, %Ecto.Changeset{}}

  """
  @spec put(struct, list()) :: {:ok, struct}
  def put(model, options \\ [])
  def put({:ok, model}, options), do: {:ok, put(model, options)}

  @spec put({:error, map()}, list()) :: {:error, map()}
  def put({:error, details}, _options), do: {:error, details}

  @spec put(struct, map()) :: {:ok, struct} | :error
  def put(model, options) when is_map(model),
    do: put(model, ElasticSearch.get_meta!(model.__struct__), options)

  @doc """
  Put and Update a content data to [`ElasticSearch`](ElasticSearch.html).

  ## Examples

      iex> ElasticSearch.Repo.put(%SmileDB.Accounts.User{}, %{website: "some website"}, [])
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.put(%SmileDB.Accounts.User{}, %{test: "some test"}, [])
      :error

  """
  @spec put(map(), map(), list()) :: {:ok, struct} | :error
  def put(model, doc, _options) when is_map(model) do
    data = serialize(model)

    case insert(doc, data) do
      {:ok, _code, _metadata} ->
        update(model, doc, nested: data)

      {:error, _code, _metadata} ->
        :error
    end

    doc.trigger_update.counter
    |> Enum.each(fn %{
                      related_model: related_model,
                      update_field: update_field,
                      condition: condition
                    } ->
      related_doc = ElasticSearch.get_meta!(related_model)

      case condition.(data) do
        nil -> nil
        id -> increment(related_doc, id, update_field)
      end
    end)

    model
  end

  defp insert(%{fields: %{array: array}} = doc, data) when length(array) > 0 do
    Enum.map(array, fn %{id: id, field: field, value: value} ->
      case array_increment(doc, data[id], field, data[value]) do
        {:error, _, _} ->
          data =
            data
            |> Keyword.delete(value)
            |> Keyword.put(:id, data[id])
            |> Keyword.put(:"#{field}", ["#{data[value]}"])

          doc
          |> put_in([Access.key!(:fields), Access.key!(:array)], [])
          |> insert(data)

        data ->
          data
      end
    end)
    |> List.first()
  end

  defp insert(doc, data) do
    insert(doc, data, Keyword.get(data, :id))
  end

  defp insert(doc, data, nil) do
    Tirexs.HTTP.put("/#{doc.index}/#{doc.type}?refresh", data)
  end

  defp insert(doc, data, id) do
    Tirexs.HTTP.put("/#{doc.index}/#{doc.type}/#{id}?refresh", data)
  end

  @doc """
  Update a content data to [`ElasticSearch`](ElasticSearch.html).

  ## Examples

      iex> ElasticSearch.Repo.update(%SmileDB.Accounts.User{})
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.update({:ok, %SmileDB.Accounts.User{}}, [])
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.update({:error, %Ecto.Changeset{}}, [])
      {:error, %Ecto.Changeset{}}

  """
  @spec update(struct, list()) :: {:ok, struct}
  def update(model, options \\ [])
  def update({:ok, model}, options), do: {:ok, update(model, options)}

  @spec update({:error, map()}, list()) :: {:error, map()}
  def update({:error, details}, _options), do: {:error, details}

  @spec put(struct, map()) :: {:ok, struct} | :error
  def update(model, options) when is_map(model),
    do: update(model, ElasticSearch.get_meta!(model.__struct__), options)

  @doc """
  Update a content data to [`ElasticSearch`](ElasticSearch.html).

  ## Examples

      iex> ElasticSearch.Repo.update(%SmileDB.Accounts.User{}, %{website: "some website"}, [])
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.update(%SmileDB.Accounts.User{}, %{test: "some test"}, [])
      :error

  """
  @spec update(map(), map(), list()) :: {:ok, struct} | :error
  def update(model, doc, options) when is_map(model) do
    case Keyword.fetch(options, :nested) do
      :error ->
        data = serialize(model)
        upgrade(doc, data, data[:id])

      {:ok, data} ->
        doc.fields.nested
        |> Enum.each(fn %{
                          model: nested_model,
                          model_fetch_field: m_field,
                          field: field,
                          current_fetch_field: c_field
                        } ->
          nested_doc = ElasticSearch.get_meta!(nested_model)

          search index: nested_doc.index do
            query do
            end
          end
          |> put_in([:search, :query, :term], "#{m_field}": Map.fetch!(model, c_field))
          |> all()
          |> Enum.each(fn obj ->
            upgrade(nested_doc, ["#{field}": data], obj.id)
          end)
        end)
    end

    model
  end

  defp upgrade(%{index: index, type: type}, data, id) do
    (Tirexs.Resources.APIs._update([index], "#{type}", id) <> "?refresh")
    |> Tirexs.HTTP.post(doc: data)
  end

  def delete(model, options \\ [])
  def delete({:ok, model}, options), do: {:ok, delete(model, options)}
  def delete({:error, details}, _options), do: {:error, details}

  def delete(model, options) when is_map(model),
    do: delete(model, ElasticSearch.get_meta!(model.__struct__), options)

  @doc """
  Delete a content data with [`ElasticSearch`](ElasticSearch.html).

  ## Examples

      iex> ElasticSearch.Repo.delete(%SmileDB.Accounts.User{}, %{website: "some website"}, [])
      {:ok, %SmileDB.Accounts.User{}}

      iex> ElasticSearch.Repo.delete(%SmileDB.Accounts.User{}, %{test: "some test"}, [])
      :error

  """
  @spec delete(map(), map(), list()) :: {:ok, struct} | :error
  def delete(model, doc, options) when is_map(model) do
    data = serialize(model)

    case remove(doc, data) do
      {:ok, _code, _metadata} ->
        update(model, doc, nested: nil)

      {:error, _code, _metadata} ->
        :error
    end

    doc.trigger_update.counter
    |> Enum.each(fn %{
                      related_model: related_model,
                      update_field: update_field,
                      condition: condition
                    } ->
      related_doc = ElasticSearch.get_meta!(related_model)

      case condition.(data) do
        nil -> nil
        id -> decrement(related_doc, id, update_field)
      end
    end)

    (Keyword.get(options, :cascade_delete) || doc.cascade_delete)
    |> Enum.each(fn cascaded ->
      query =
        search index: ElasticSearch.get_meta!(cascaded).index do
          query do
          end
        end

      put_in(query[:search][:query][:term], "#{doc.type}_id": data[:id])
      |> all()
      |> Enum.each(&delete(&1))
    end)

    doc.cascade_decrement
    |> Enum.each(fn {cascaded, field_value} ->
      %{
        index: index,
        fields: %{
          array: array
        }
      } = ElasticSearch.get_meta!(cascaded)

      Enum.each(array, fn %{field: field, value: value} ->
        query =
          search index: index do
            query do
            end
          end

        put_in(query[:search][:query][:term], "#{field}": data[field_value])
        |> all()
        |> Enum.map(&Map.put(&1, value, data[field_value]))
        |> Enum.each(&delete(&1, field: field))
      end)
    end)

    model
  end

  defp remove(%{fields: %{array: array}} = doc, data) when length(array) > 0 do
    Enum.map(array, fn %{id: id, field: field, value: value} ->
      array_decrement(doc, data[id], field, data[value])
    end)
    |> List.first()
  end

  defp remove(doc, data) do
    Tirexs.HTTP.delete("/#{doc.index}/#{doc.type}/#{data[:id]}?refresh")
  end

  def clean_up() do
    Application.fetch_env!(:elastic_search, :indexes)
    |> Enum.each(&clean_up(&1))
  end

  def clean_up(model) do
    doc = ElasticSearch.get_meta!(model)

    Tirexs.HTTP.post(
      "/#{doc.index}/#{doc.type}/_delete_by_query?conflicts=proceed&refresh",
      query: [
        match_all: [
          boost: 1.0
        ]
      ]
    )
  end

  def increment(doc, id, field) do
    (Tirexs.Resources.APIs._update([doc.index], "#{doc.type}", id) <> "?refresh")
    |> Tirexs.HTTP.post(script: "ctx._source.#{field}+=1")
  end

  def decrement(doc, id, field) do
    (Tirexs.Resources.APIs._update([doc.index], "#{doc.type}", id) <> "?refresh")
    |> Tirexs.HTTP.post(script: "ctx._source.#{field}-=1")
  end

  def array_increment(doc, id, field, value) do
    (Tirexs.Resources.APIs._update([doc.index], "#{doc.type}", id) <> "?refresh")
    |> Tirexs.HTTP.post(script: "ctx._source.#{field}.add('#{value}')")
  end

  def array_decrement(doc, id, field, value) do
    (Tirexs.Resources.APIs._update([doc.index], "#{doc.type}", id) <> "?refresh")
    |> Tirexs.HTTP.post(
      script: "ctx._source.#{field}.remove(ctx._source.#{field}.indexOf('#{value}'))"
    )
  end

  def serialize(model) when is_map(model) do
    model.__struct__
    |> apply(:es_encode, [model])
    |> additional_fields()
  end

  def serialize(data), do: data

  def deserialize(data) when is_list(data) and length(data) !== 0 do
    Enum.map(data, &deserialize(&1))
  end

  def deserialize(data) when is_map(data) do
    map = Map.get(data, :_source, data)

    map.__struct__
    |> String.to_existing_atom()
    |> apply(:es_decode, [map])
  end

  def deserialize(data), do: data

  defp additional_fields(model) when is_map(model) do
    model
    |> Map.to_list()
    |> additional_fields()
  end

  defp additional_fields(model) when is_list(model) do
    with %{fields: %{additional: additional}} <- ElasticSearch.get_meta!(model[:__struct__]),
         keys <- Keyword.keys(additional) do
      Enum.reduce(keys, model, fn key, acc ->
        Enum.reduce(additional[key], acc, fn
          {field, _}, tmp_acc ->
            Keyword.put(tmp_acc, :"#{field}_#{key}", acc[field])

          field, tmp_acc ->
            Keyword.put(tmp_acc, :"#{field}_#{key}", acc[field])
        end)
      end)
    end
  end

  @doc """
  Defines a paginator.

  Fetches all the results matching the query within the cursors.

  ## Options

    :after - Fetch the records after this cursor.
    :cursor_fields - The fields used to determine the cursor. In most cases, this should be the same fields as the ones used for sorting in the query.
    :limit - Limits the number of records returned per page.

  ## Examples

      query =
        Tirexs.Search.search [index: "contents"] do
          query do
            match_all
          end
          sort do
            [inserted_at: :desc, _id: :desc]
          end
        end

      %{entries: entries, metadata: metadata} =
        Elastic.paginate(query, limit: 50, cursor_fields: [:inserted_at, :_id])
  """
  def paginate(query, params) do
    query =
      with limit <- params[:limit],
           limit <- if(limit <= 0, do: 0, else: limit + 1),
           query <- put_in(query[:search][:size], limit) do
        if not is_nil(params[:after]) do
          base64_after =
            params[:after]
            |> Base.decode64!()
            |> Jason.decode!()

          search_after =
            Enum.map(params[:cursor_fields], fn field ->
              Map.fetch!(base64_after, Atom.to_string(field))
            end)

          put_in(query[:search][:search_after], search_after)
        else
          query
        end
      end

    case Tirexs.Query.create_resource(query) do
      {:ok, _code, result} ->
        data =
          if Enum.count(result.hits.hits) > params[:limit] do
            List.delete_at(result.hits.hits, -1)
          else
            result.hits.hits
          end

        cursor_after =
          if Enum.count(result.hits.hits) > params[:limit] do
            last = List.last(data)

            if not is_nil(last) do
              [params[:cursor_fields], last.sort]
              |> List.zip()
              |> Enum.into(%{})
              |> Jason.encode!()
              |> Base.encode64()
            end
          end

        %{}
        |> Map.put_new(:metadata, %{
          total_count: result.hits.total,
          limit: params[:limit],
          after: cursor_after
        })
        |> Map.put_new(:entries, deserialize(data))

      {:error, _code, _status} ->
        %{}
        |> Map.put_new(:metadata, %{
          total_count: 0,
          limit: params[:limit],
          after: nil
        })
        |> Map.put_new(:entries, [])
    end
  end

  def all(query, size \\ 10000) do
    result =
      query[:search][:size]
      |> put_in(size)
      |> Tirexs.Query.create_resource()
      |> Tuple.delete_at(1)

    case result do
      {:ok, result} -> parse_es_result(result)
      {:error, _reason} -> []
    end
    |> deserialize()
  end

  defp parse_es_result(result) do
    cond do
      Map.has_key?(result, :suggest) ->
        Map.keys(result.suggest)
        |> Enum.map(fn suggest_name ->
          suggestions =
            result.suggest
            |> Map.fetch!(suggest_name)
            |> Enum.map(& &1.options)
            |> List.flatten()
            |> deserialize()

          {suggest_name, suggestions}
        end)

      true ->
        result.hits.hits
    end
  end
end
