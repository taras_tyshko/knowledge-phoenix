defmodule ElasticSearch.Mapping do
  @moduledoc """
    The module releases functions that make the necessary structure in ElasticSearch. For implementation of the [`Tirexs`](https://hexdocs.pm/tirexs/api-reference.html).
  """

  @doc """
    The function implements the creation of mapping in the ElasticSearch.
  ## Example 

      iex> ElasticSearch.Mapping.create(
        %{index: :contents,
          type: :content, 
          fields: %{
              additional: [
                search: [:title],
                new_autocomplete: [:title]
              ]
            }
          }
        )
      {:ok, 200, %{acknowledged: true}}
  """
  @spec create(%{index: Atom.t(), type: Atom.t(), fields: %{additional: list()}}) ::
          {:ok, Integer.t(), %{acknowledged: boolean}}
  def create(%{index: index, type: type, fields: %{additional: additional}}) do
    Tirexs.Mapping.create_resource(index: "#{index}", type: "#{type}")

    Tirexs.HTTP.post("/#{index}/_close")

    Tirexs.Resources.APIs._settings([index])
    |> Tirexs.HTTP.put(
      analysis: [
        filter: [
          nGram_filter: [
            type: "nGram",
            min_gram: 1,
            max_gram: 20,
            token_chars: ["letter", "digit", "punctuation", "symbol"]
          ]
        ],
        analyzer: [
          autocomplete_analyzer: [
            type: "custom",
            tokenizer: "whitespace",
            filter: ["lowercase", "asciifolding", "nGram_filter"]
          ],
          whitespace_analyzer: [
            type: "custom",
            tokenizer: "whitespace",
            filter: ["lowercase", "asciifolding"]
          ]
        ]
      ]
    )

    Tirexs.HTTP.post("/#{index}/_open")

    Tirexs.Resources.APIs._mapping(["#{index}"], "#{type}")
    |> Tirexs.HTTP.put(
      properties:
        Enum.map(additional, fn {key, fields} ->
          Enum.map(fields, fn field ->
            apply(ElasticSearch.Mapping.Field, key, [field])
          end)
        end)
        |> List.flatten()
    )
  end
end
