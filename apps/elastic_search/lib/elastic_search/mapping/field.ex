defmodule ElasticSearch.Mapping.Field do
  @moduledoc """
    The module has a set of filations that have a preset structure to retrieve data from [`Elasticsearch`](ElasticSearch.html).
  """

  @doc """
    This function implements the return of the structure for `autocomplete`.
    
    This function has pattern maching with the following examples of the structure for `Elasticsearch`.

  ## Examples 1

      def autocomplete({field, context_field}) do
        {:"\#{field}_autocomplete",
          [
            type: "completion",
            analyzer: "standard",
            contexts: [
              [
                name: context_field,
                type: "category",
                path: context_field
              ]
            ]
          ]}
      end

  ## Examples 2

      def autocomplete(field) do
        {:"\#{field}_autocomplete",
          [
            type: "completion",
            analyzer: "standard"
          ]}
      end
  """
  @spec autocomplete(map()) :: map()
  def autocomplete({field, context_field}) do
    {:"#{field}_autocomplete",
     [
       type: "completion",
       analyzer: "standard",
       contexts: [
         [
           name: context_field,
           type: "category",
           path: context_field
         ]
       ]
     ]}
  end

  @spec autocomplete(String.t()) :: map()
  def autocomplete(field) do
    {:"#{field}_autocomplete",
     [
       type: "completion",
       analyzer: "standard"
     ]}
  end

  @doc """
    This function implements the return of the structure for `new_autocomplete`.
    
    This function has pattern maching with the following examples of the structure for `Elasticsearch`.

  ## Examples 1

      def new_autocomplete(field) do
        {:"\#{field}_new_autocomplete",
          [
            type: "text",
            analyzer: "autocomplete_analyzer",
            search_analyzer: "whitespace_analyzer"
          ]}
      end
  """
  @spec new_autocomplete(String.t()) :: map()
  def new_autocomplete(field) do
    {:"#{field}_new_autocomplete",
     [
       type: "text",
       analyzer: "autocomplete_analyzer",
       search_analyzer: "whitespace_analyzer"
     ]}
  end

  @doc """
    This function implements the return of the structure for `search`.
    
    This function has pattern maching with the following examples of the structure for `Elasticsearch`.

  ## Examples 1

      def search(field) do
        {:"\#{field}_search",
        [
          type: "text",
          analyzer: "whitespace_analyzer"
        ]}
      end
  """
  @spec search(String.t()) :: map()
  def search(field) do
    {:"#{field}_search",
     [
       type: "text",
       analyzer: "whitespace_analyzer"
     ]}
  end
end
