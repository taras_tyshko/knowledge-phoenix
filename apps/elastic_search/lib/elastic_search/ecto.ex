defmodule ElasticSearch.Ecto do
  import Ecto.Query

  def restore(repo, model, table) do
    reindex(repo, table, model)
    |> Enum.map(fn data ->
      table
      |> ElasticSearch.get_meta!()
      |> Map.fetch!(:cascade_delete)
      |> Enum.map(&restore(repo, data, &1))
    end)
  end

  def reindex(repo, %Ecto.Query{} = query) do
    repo.transaction(fn ->
      query
      |> repo.stream(max_rows: Application.get_env(:elastic_search, :reindex_max_rows))
      |> Enum.to_list()
    end)
    |> elem(1)
  end

  def reindex(repo, model) do
    with functions <- model.__info__(:functions),
         true <- Keyword.has_key?(functions, :source_query),
         doc <- ElasticSearch.get_meta!(model.__struct__) do
      Enum.map(model.source_query, &reindex(repo, &1))
      |> List.flatten()
      |> Enum.map(doc.reindex_function)
    end
  end

  def reindex(repo, model, data) do
    with functions <- model.__info__(:functions),
         true <- Keyword.has_key?(functions, :source_query),
         doc <- ElasticSearch.get_meta!(model.__struct__),
         data_doc <- ElasticSearch.get_meta!(data) do
      Enum.map(model.source_query, fn query ->
        try do
          query = from(subquery(query), where: ^["#{data_doc.type}_id": data.id])
          reindex(repo, query)
        rescue
          _ -> []
        end
      end)
      |> List.flatten()
      |> Enum.map(doc.reindex_function)
    end
  end

  def put(model) when is_map(model) do
    doc =
      model.__struct__
      |> ElasticSearch.get_meta!()
      |> Map.put(:trigger_update, ElasticSearch.Entity.TriggerUpdate.new(%{}))

    ElasticSearch.Repo.put(model, doc, [])
  end
end
