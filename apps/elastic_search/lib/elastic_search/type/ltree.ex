defmodule ElasticSearch.Type.LTree do
  @behaviour ElasticSearch.Type

  def cast(value) when not is_nil(value), do: Enum.map(value.labels, &String.to_integer(&1))
  def cast(value), do: value

  def load(value) when not is_nil(value) do
    {:ok, result} =
      value
      |> Enum.join(".")
      |> EctoLtree.LabelTree.cast()
    result
  end
  def load(value), do: value
end
