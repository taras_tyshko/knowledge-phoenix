defmodule ElasticSearch.Type.DateTime do
  @behaviour ElasticSearch.Type

  def cast(value) when not is_nil(value) do
    DateTime.to_iso8601(value)
  end
  def cast(value), do: value

  def load(value) when not is_nil(value) do
    {:ok, result, _} = DateTime.from_iso8601(value)
    result
  end
  def load(value), do: value
end
