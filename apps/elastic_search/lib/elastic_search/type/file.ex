defmodule ElasticSearch.Type.File do
  @behaviour ElasticSearch.Type

  def cast(value) when is_list(value) do
    Enum.map(value, &cast(&1))
  end

  def cast(value) when not is_nil(value) do
    ElasticSearch.Repo.serialize(value)
  end

  def cast(value), do: value

  def load(value) when is_list(value) do
    Enum.map(value, &load(&1))
  end

  def load(value) when not is_nil(value) do
    ElasticSearch.Repo.deserialize(value)
  end

  def load(value), do: value
end
