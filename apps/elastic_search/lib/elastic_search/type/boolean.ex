defmodule ElasticSearch.Type.Boolean do
  @behaviour ElasticSearch.Type

  def cast(value) when not is_nil(value) do
    "#{value}"
  end
  def cast(value), do: value

  def load(value) when not is_nil(value) do
    String.to_atom(value)
  end
  def load(value), do: value
end
