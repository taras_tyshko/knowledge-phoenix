defmodule ElasticSearch.Entity.Fields.Array do
  @type t :: %__MODULE__{
                id: atom(),
                value: atom(),
                field: atom()
             }
  defstruct([:id, :value, :field])

  use ExConstructor
end
