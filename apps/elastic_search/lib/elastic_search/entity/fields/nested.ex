defmodule ElasticSearch.Entity.Fields.Nested do
  @type t :: %__MODULE__{
                model: atom(),
                model_fetch_field: atom(),
                field: atom(),
                current_fetch_field: atom()
             }
  defstruct([:model,:model_fetch_field, :field, :current_fetch_field])

  use ExConstructor
end
