defmodule ElasticSearch.Entity.TriggerUpdate.Counter do
  @type t :: %__MODULE__{
          related_model: atom(),
          update_field: atom(),
          condition: function()
        }
  defstruct([:related_model, :update_field, :condition])

  use ExConstructor
end
