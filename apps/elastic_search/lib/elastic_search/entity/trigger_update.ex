defmodule ElasticSearch.Entity.TriggerUpdate do
  @type t :: %__MODULE__{
               counter: list(ElasticSearch.Entity.TriggerUpdate.Counter.t()),
               fields: list(tuple())
             }
  defstruct(
    counter: [],
    fields: []
  )

  use ExConstructor
end
