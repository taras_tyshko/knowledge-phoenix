defmodule ElasticSearch.Entity.Fields do
  @type t :: %__MODULE__{
               additional: list(tuple()),
               convert: list(tuple()),
               nested: list(ElasticSearch.Entity.Fields.Nested.t()),
               array: list(ElasticSearch.Entity.Fields.Array.t())
             }
  defstruct(
    additional: [],
    convert: [],
    nested: [],
    array: []
  )

  use ExConstructor
end
