defmodule ElasticSearch.Paginator.Metadata do
  @moduledoc """
    This function implements the declared type for the [`Metadata`](ElasticSearch.Paginator.Metadata.html).

    Also used is a call module [`ExConstructor`](https://hexdocs.pm/exconstructor/ExConstructor.html).

    Add `use ExConstructor` after a [`defstruct`](https://hexdocs.pm/exconstructor/ExConstructor.html#__using__/1) statement to inject a constructor function into the module.
    The generated constructor, called new by default, handles map-vs-keyword-list, string-vs-atom-keys, and camelCase-vs-under_score input data issues automatically, DRYing up your code and letting you move on to the interesting parts of your program.

  ## Fields available in the default.

    * after - an opaque cursor representing the last row of the current page.
    * limit - the maximum number of entries that can be contained in this page.
    * total_count - the total number of entries matching the query.
  """

  @type opaque_cursor :: String.t()

  @type t :: %__MODULE__{
          after: opaque_cursor(),
          limit: integer(),
          total_count: integer()
        }

  defstruct [:after, :limit, :total_count]

  use ExConstructor
end
