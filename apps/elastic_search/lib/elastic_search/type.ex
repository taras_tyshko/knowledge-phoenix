defmodule ElasticSearch.Type do
  @callback cast(any) :: String.t
  @callback load(String.t) :: {:ok, any}

  def cast(model) when is_map(model) do
    with %{fields: %{convert: types}} <- ElasticSearch.get_meta!(model.__struct__) do
      Enum.reduce(Keyword.keys(types), model, fn field, acc ->
        data = Map.fetch!(model, field)
        Map.put(acc, field, apply(types[field], :cast, [data]))
      end)
    end
  end

  def load(model) when is_map(model) do
    with %{fields: %{convert: types}} <- ElasticSearch.get_meta!(model) do
      Enum.reduce(Keyword.keys(types), model, fn field, acc ->
        data = Map.fetch!(model, field)
        Map.put(acc, field, apply(types[field], :load, [data]))
      end)
    end
  end
end
