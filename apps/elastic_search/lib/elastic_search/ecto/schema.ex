defmodule ElasticSearch.Ecto.Schema do
  defmacro __using__(_) do
    quote do
      import Ecto.Query
      import ElasticSearch.Ecto.Schema

      @spec __schema__(Atom.t()) :: nil
      def __schema__(:elastic_search), do: nil

      @spec es_encode(struct) :: struct
      def es_encode(model) do
        model
        |> ElasticSearch.Type.cast()
        |> Map.to_list()
        |> Enum.filter(&(!is_map(elem(&1, 1))))
      end

      @spec es_decode(struct) :: struct
      def es_decode(data) do
        struct(__MODULE__, data)
        |> ElasticSearch.Type.load()
        |> put_in([Access.key(:__meta__, %{}), Access.key(:state, %{})], :loaded)
      end

      @doc """
      This function implements the receipt query from current `__MODULE__`.

      ## Example
          def source_query do
            [from(m in __MODULE__, select: m)]
          end
      """
      @spec source_query() :: list()
      def source_query, do: [from(m in __MODULE__, select: m)]

      defoverridable es_encode: 1, es_decode: 1, __schema__: 1, source_query: 0
    end
  end

  @spec trigger_update(Ecto.Changeset.t()) :: struct
  def trigger_update(changeset) do
    %Ecto.Changeset{data: model, changes: changes} = changeset
    doc = ElasticSearch.get_meta!(model.__struct__)

    Enum.each(doc.trigger_update.fields, fn {field, value} ->
      with {:ok, new_field_value} <- Map.fetch(changes, field),
           {:ok, old_field_value} <- Map.fetch(model, field) do
        Enum.each(value, fn {related_model, mfield} ->
          with related_doc <- ElasticSearch.get_meta!(related_model) do
            Tirexs.HTTP.post(
              "/#{related_doc.index}/#{related_doc.type}/_update_by_query?conflicts=proceed",
              script: [
                source: "ctx._source['#{mfield}'] = params.new_value",
                lang: "painless",
                params: [
                  new_value: new_field_value
                ]
              ],
              query: [
                match: [
                  "#{mfield}": old_field_value
                ]
              ]
            )
          end
        end)
      end
    end)

    changeset
  end
end
