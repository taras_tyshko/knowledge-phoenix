defmodule ElasticSearch.MixProject do
  use Mix.Project

  def project do
    [
      app: :elastic_search,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [
        :logger,
        :tirexs,
        :exconstructor
      ],
      env: [indexes: [], reindex_max_rows: 500]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:tirexs, "~> 0.8.15"},
      {:exconstructor, "~> 1.1.0"}
    ]
  end
end
