defmodule Messenger.Subscription.MessageTest do
  use MessengerWeb.SubscriptionCase
  alias Messenger.QueryHelpers

  describe "message" do
    test "messageAdd/1 add new message and check result subscription", %{
      socket: socket,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              messageAdd(topic: $topic) {
                  #{QueryHelpers.message()}
              }
          }
      """

      mutation = """
          mutation ($roomId: Int!, $description: String) {
              addMessages(description: $description, roomId: $roomId) {
                  #{QueryHelpers.message()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{"roomId" => room.id, "description" => "some description"}
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addMessages" => addMessages}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"messageAdd" => addMessages}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "messageEdit/1 edit message and check result subscription", %{
      socket: socket,
      room: room,
      message: message
    } do
      subscription = """
          subscription ($topic: String!) {
              messageEdit(topic: $topic) {
                  #{QueryHelpers.message()}
              }
          }
      """

      mutation = """
          mutation ($messageId: Int!, $description: String) {
              editMessages(description: $description, messageId: $messageId) {
                  #{QueryHelpers.message()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{"messageId" => message.id, "description" => "some description"}
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"editMessages" => editMessages}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"messageEdit" => editMessages}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "messageDelete/1 delete message and check result subscription", %{
      socket: socket,
      room: room,
      message: message
    } do
      subscription = """
          subscription ($topic: String!) {
              messageDelete(topic: $topic) {
                  #{QueryHelpers.message()}
              }
          }
      """

      mutation = """
          mutation ($messageId: Int!, $roomId: Int!) {
              deleteMessages(roomId: $roomId, messageId: $messageId) {
                  #{QueryHelpers.message()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"messageId" => message.id, "roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"deleteMessages" => deleteMessages}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"messageDelete" => deleteMessages}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "channelMessagesClear/1 clear messages for channel and check result subscription", %{
      socket: socket,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              channelMessagesClear(topic: $topic) {
                  #{QueryHelpers.room()}
              }
          }
      """

      mutation = """
          mutation ($roomId: Int!) {
              clearMessagesChannel(roomId: $roomId) {
                  #{QueryHelpers.room()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"clearMessagesChannel" => clearMessagesChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelMessagesClear" => clearMessagesChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end
end
