defmodule Messenger.Subscription.RoomTest do
  use MessengerWeb.SubscriptionCase

  alias Messenger.QueryHelpers

  describe "rooms" do
    test "channelJoin/1 Check what channel it exist and return result about subscription.", %{
      socket: socket,
      user: user,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              channelJoin(topic: $topic) {
                  #{QueryHelpers.room()}
              }
          }
      """

      mutation = """
          mutation ($userId: Int!) {
              joinChannel(userId: $userId) {
                  #{QueryHelpers.room()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"userId" => user.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"joinChannel" => joinChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelJoin" => joinChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "deletePeopleFromChannel/1 Delete People in channel and return result about subscription.",
         %{socket: socket, user: user, room: room} do
      subscription = """
          subscription ($topic: String!) {
              channelDeletePeople(topic: $topic) {
                  id
                  users {
                    id
                  }
              }
          }
      """

      mutation = """
          mutation ($userId: [Int]!, $roomId: Int!) {
              deletePeopleFromChannel(userId: $userId, roomId: $roomId) {
                  id
                  users {
                    id
                  }   
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"userId" => [user.id], "roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"deletePeopleFromChannel" => deletePeopleFromChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelDeletePeople" => deletePeopleFromChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "channelAddPeople/1 Delete People in channel and return result about subscription.", %{
      socket: socket,
      room: room,
      user_one: user_one
    } do
      subscription = """
          subscription ($topic: String!) {
              channelAddPeople(topic: $topic) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($userId: [Int]!, $roomId: Int!) {
              addPeopleInChannel(userId: $userId, roomId: $roomId) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }   
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"userId" => [user_one.id], "roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addPeopleInChannel" => addPeopleInChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelAddPeople" => addPeopleInChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "channelLeave/1 when user leave channel and return result about subscription.", %{
      socket: socket,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              channelLeave(topic: $topic) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($roomId: Int!) {
              leaveChannel(roomId: $roomId) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"leaveChannel" => leaveChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelLeave" => leaveChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "channelDelete/1 delete channel return result about subscription.", %{
      socket: socket,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              channelDelete(topic: $topic) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($roomId: Int!) {
              deleteChannel(roomId: $roomId) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"deleteChannel" => deleteChannel}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelDelete" => deleteChannel}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "channelRenameName/1 Rename channel name and return result about subscription.", %{
      socket: socket,
      room: room
    } do
      subscription = """
          subscription ($topic: String!) {
              channelRenameName(topic: $topic) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($roomId: Int!, $name: String!) {
              renameChannelName(roomId: $roomId, name: $name) {
                  id
                  users {
                    #{QueryHelpers.user()}
                  }    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:#{room.id}"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{"roomId" => room.id, "name" => "some update name"}
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"renameChannelName" => renameChannelName}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"channelRenameName" => renameChannelName}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end

  describe "userTyping" do
    test "typingUser/1 Check who of the users channel prints the text.", %{
      socket: socket,
      user: user,
      room: room,
      user_one: user_one
    } do
      mutation = """
          mutation ($userId: [Int]!, $roomId: Int!) {
              addPeopleInChannel(userId: $userId, roomId: $roomId) {
                  id
              }
          }
      """

      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"userId" => [user_one.id], "roomId" => room.id})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addPeopleInChannel" => addPeopleInChannel}} = reply
      assert addPeopleInChannel["id"] == Integer.to_string(room.id)

        subscription = """
            subscription ($topic: String!) {
                typingUser(topic: $topic) {
                  roomId
                  status
                  user {
                      id
                  }
                }
            }
        """

        mutation = """
            mutation ($roomId: Int!, $status: Boolean!) {
                userTyping(roomId: $roomId, status: $status)
            }
        """

        # setup a subscription
        ref = push_doc(socket, subscription, variables: %{"topic" => "user_typing:#{user_one.nickname}"})
        assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
        # Rest of test case

        # run a mutation to trigger the subscription
        ############ User typing status: true. ##########
        ref =
          push_doc(socket, mutation, variables: %{"roomId" => room.id, "status" => true})

        assert_reply(ref, :ok, reply, 1_000)
        assert %{data: %{"userTyping" => _userTyping}} = reply

        # check to see if we got subscription data
        expected = %{
          result: %{data: %{"typingUser" => %{"roomId" => room.id, "status" => true, "user" => %{"id" => Integer.to_string(user.id)}}}},
          subscriptionId: subscription_id
        }

        assert_push("subscription:data", push, 1_000)
        assert expected == push
        
        ############ User untyping status: false. ##########
        ref =
          push_doc(socket, mutation, variables: %{"roomId" => room.id, "status" => false})

        assert_reply(ref, :ok, reply, 1_000)
        assert %{data: %{"userTyping" => _userTyping}} = reply

        # check to see if we got subscription data.
        expected = %{
          result: %{data: %{"typingUser" => %{"roomId" => room.id, "status" => false, "user" => %{"id" => Integer.to_string(user.id)}}}},
          subscriptionId: subscription_id
        }

        assert_push("subscription:data", push, 1_000)
        assert expected == push
    end
  end
end
