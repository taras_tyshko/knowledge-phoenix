defmodule Messenger.Mutation.RoomTest do
  use MessengerWeb.ConnCase
  use ExUnit.Case, async: true

  alias Messenger.QueryHelpers
  alias SmileDB.ContentHelpers
  alias Messenger.AbsintheHelpers

  describe "rooms" do
    test "add_people_in_channel/1 returns the list user in room", %{
      conn: conn,
      room: room,
      user_one: user_one
    } do
      query = """
        mutation {
          addPeopleInChannel(roomId: #{room.id}, userId: [#{user_one.id}]) {
            id
            users {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["addPeopleInChannel"]

      assert Integer.to_string(room.id) == data["id"]
      assert [] == data["users"]
    end

    test "delete_people_from_channel/1 returns the list user in room", %{
      conn: conn,
      room: room,
      user_one: user_one
    } do
      query = """
        mutation {
          addPeopleInChannel(roomId: #{room.id}, userId: [#{user_one.id}]) {
            id
            users {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["addPeopleInChannel"]

      assert Integer.to_string(room.id) == data["id"]
      assert [] == data["users"]

      query_two = """
        mutation {
          deletePeopleFromChannel(roomId: #{room.id}, userId: [#{user_one.id}]) {
            id
            users {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query_two))

      data_two = json_response(res, 200)["data"]["deletePeopleFromChannel"]

      assert Integer.to_string(room.id) == data_two["id"]
      assert [] == data_two["users"]
    end

    test "join_channel/1 returns the list user in room", %{conn: conn, user_one: user_one} do
      query = """
        mutation {
          joinChannel(userId: #{user_one.id}) {
            id
            users {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["joinChannel"]

      user_fetch_data =
        user_one
        |> ContentHelpers.fetch_data_with_model(Map.keys(List.first(data["users"])))
        |> ContentHelpers.conver_datatimes_and_id()

      assert [user_fetch_data] == data["users"]
    end

    test "leave_channel/1 returns the list user in room", %{conn: conn, room: room} do
      query = """
        mutation {
          leaveChannel(roomId: #{room.id}) {
            id
            users {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["leaveChannel"]

      assert Integer.to_string(room.id) == data["id"]
      assert [] == data["users"]
    end

    test "rename_channel_name/1 returns the updated room name", %{conn: conn, room: room} do
      variables = %{name: "some updated name"}

      query = """
        mutation {
          renameChannelName(roomId: #{room.id}, name: "#{variables.name}") {
            id
            name
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["renameChannelName"]

      assert Integer.to_string(room.id) == data["id"]
      assert variables.name == data["name"]
    end

    test "create_channel/1 returns the new channel", %{conn: conn, user: user} do
      variables = %{name: "some name"}

      query = """
        mutation {
          createChannel(userId: [#{user.id}], name: "#{variables.name}") {
            #{QueryHelpers.room()}
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["createChannel"]

      user_fetch_data =
        user
        |> ContentHelpers.fetch_data_with_model(Map.keys(List.first(data["owners"])))
        |> ContentHelpers.conver_datatimes_and_id()

      assert true == data["type"]
      assert [] == data["users"]
      assert variables.name == data["name"]
      assert [user_fetch_data] == data["owners"]
      assert user_fetch_data == data["message"]["user"]
      assert "createChannel" == data["message"]["eventName"]

      assert %{"desc" => "user:#{user.id}", "users" => [user_fetch_data]} ==
               data["message"]["description"]
    end

    test "delete_dhannel/1 returns the delete channel", %{conn: conn, room: room} do
      query = """
        mutation {
          deleteChannel(roomId: #{room.id}) {
            id
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      assert Integer.to_string(room.id) == json_response(res, 200)["data"]["deleteChannel"]["id"]
    end
  end

  describe "user_typing" do
    test "userTyping/1 returns the true if user is typing.", %{conn: conn, room: room} do
      query = """
        mutation {
          userTyping(roomId: #{room.id}, status: true)
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      assert true == json_response(res, 200)["data"]["userTyping"]
    end

    test "userTyping/1 returns the fasle if user is not typing.", %{conn: conn, room: room} do
      query = """
        mutation {
          userTyping(roomId: #{room.id}, status: false)
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      assert false == json_response(res, 200)["data"]["userTyping"]
    end
  end
end
