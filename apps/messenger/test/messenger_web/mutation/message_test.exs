defmodule Messenger.Mutation.MessageTest do
  use MessengerWeb.ConnCase
  use ExUnit.Case, async: true

  alias Messenger.QueryHelpers
  alias SmileDB.ContentHelpers
  alias Messenger.AbsintheHelpers

  describe "messages" do
    test "add_messages/1 returns the new message", %{
      conn: conn,
      room: room,
      user: user
    } do
      query = """
        mutation {
          addMessages(roomId: #{room.id}, description: "@#{user.nickname} some description") {
            description {
              #{QueryHelpers.description()}
            }
            eventName
            room {
              id
            }
            user {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["addMessages"]

      user_fetch_data =
        user
        |> ContentHelpers.model_fetch_user_data(data)

      assert nil == data["eventName"]
      assert user_fetch_data == data["user"]
      assert Integer.to_string(room.id) == data["room"]["id"]

      assert %{"desc" => "@#{user.nickname} some description", "users" => [user_fetch_data]} ==
               data["description"]
    end

    test "delete_messages/1 returns the delete message", %{
      conn: conn,
      room: room,
      message: message
    } do
      query = """
        mutation {
          deleteMessages(roomId: #{room.id}, messageId: #{message.id}) {
            id
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      assert Integer.to_string(message.id) ==
               json_response(res, 200)["data"]["deleteMessages"]["id"]
    end

    test "edit_messages/1 returns the updated message", %{
      conn: conn,
      message: message
    } do
      query = """
        mutation {
          editMessages(description: "some updated description", messageId: #{message.id}) {
            id
            description {
              #{QueryHelpers.description()}
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["editMessages"]

      assert Integer.to_string(message.id) == data["id"]
      assert %{"desc" => "some updated description", "users" => []} == data["description"]
    end

    test "clear_messages_channel/1 returns the cleared messages channel", %{
      conn: conn,
      room: room
    } do
      query = """
        mutation {
          clearMessagesChannel(roomId: #{room.id}) {
            id
            message {
              description {
                #{QueryHelpers.description()}
              }
              eventName
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["clearMessagesChannel"]

      assert Integer.to_string(room.id) == data["id"]
      assert "cleanChannel" == data["message"]["eventName"]
      assert %{"desc" => "cleanChannel", "users" => []} == data["message"]["description"]
    end
  end
end
