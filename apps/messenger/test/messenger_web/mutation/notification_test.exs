defmodule Messenger.Mutation.NotificationTest do
  use MessengerWeb.ConnCase
  use ExUnit.Case, async: true
  alias Messenger.AbsintheHelpers

  describe "notifications" do
    test "delete_user_messages_notifications/1 returns the delete user messages notifications", %{
      conn: conn,
      user: user,
      message: message
    } do
      query = """
        mutation {
          deleteUserMessagesNotifications(messages: [#{message.id}]) {
            id
            messagesNotifications {
                roomId
            }
          }
        }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteUserMessagesNotifications"]

      assert [] == data["messagesNotifications"]
      assert user.id == data["id"]
    end
  end
end
