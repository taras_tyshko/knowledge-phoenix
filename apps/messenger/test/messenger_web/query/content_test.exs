defmodule Messenger.Query.ContentTest do
  use MessengerWeb.ConnCase
  use ExUnit.Case, async: true

  alias Messenger.QueryHelpers
  alias SmileDB.ContentHelpers
  alias Messenger.AbsintheHelpers

  describe "content" do
    test "getChannel/1 returns the channel", %{
      conn: conn,
      room: room,
      message: message,
      user: user
    } do
      query = """
      {
        getChannel(roomId: #{room.id}) {
          #{QueryHelpers.room()}
        }
      }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "query"))

      data = json_response(res, 200)["data"]["getChannel"]

      message_data =
        message
        |> ContentHelpers.fetch_data_with_model(Map.keys(data["message"]))
        |> Map.put("user", ContentHelpers.model_fetch_user_data(user, data["message"]))
        |> Map.put("room", %{"id" => Integer.to_string(room.id)})
        |> Map.put("description", %{"desc" => message.description, "users" => []})
        |> ContentHelpers.conver_datatimes_and_id()

      user_data =
        user
        |> ContentHelpers.fetch_data_with_model(
          data["owners"]
          |> List.first()
          |> Map.keys()
        )
        |> ContentHelpers.conver_datatimes_and_id() 

      room_data =
        room
        |> Map.put(:type, false)
        |> Map.put(:message, message_data)
        |> Map.put(:owners, [user_data])
        |> ContentHelpers.fetch_data_with_model(Map.keys(data))
        |> Map.put("users", [])
        |> ContentHelpers.conver_datatimes_and_id()

      assert room_data == data
    end

    test "checkChannel/1 returns (true: channel it or false: not ", %{
      conn: conn,
      user_one: user_one,
    } do
      query = """
      {
        checkChannel(userId: #{user_one.id})
      }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "query"))

      assert false == json_response(res, 200)["data"]["checkChannel"]

      mutation = """
        mutation {
          joinChannel(userId: #{user_one.id}) {
              users {
                #{QueryHelpers.user()}
              }
          }
        }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(mutation))

      data_mutation = json_response(res, 200)["data"]["joinChannel"]

      user_data =
        user_one
        |> ContentHelpers.fetch_data_with_model(
          data_mutation["users"]
          |> List.first()
          |> Map.keys()
        )
        |> ContentHelpers.conver_datatimes_and_id()

      assert [user_data] == data_mutation["users"]

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "query"))

      assert true == json_response(res, 200)["data"]["checkChannel"]
    end

    test "listUserRooms/1 returns the list user channels", %{
      conn: conn,
      room: room,
      message: message,
      user: user
    } do
      query = """
      {
        listUserRooms(after: null, limit: 1) {
          entries {
            #{QueryHelpers.room()}
          }
          metadata {
            after
            limit
            totalCount  
          }
        }
      }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "query"))

      data = json_response(res, 200)["data"]["listUserRooms"]["entries"] |> List.first()

      message_data =
        message
        |> ContentHelpers.fetch_data_with_model(Map.keys(data["message"]))
        |> Map.put("user", ContentHelpers.model_fetch_user_data(user, data["message"]))
        |> Map.put("room", %{"id" => Integer.to_string(room.id)})
        |> Map.put("description", %{"desc" => message.description, "users" => []})
        |> ContentHelpers.conver_datatimes_and_id()

      user_data =
        user
        |> ContentHelpers.fetch_data_with_model(
          data["owners"]
          |> List.first()
          |> Map.keys()
        )
        |> ContentHelpers.conver_datatimes_and_id() 

      room_data =
        room
        |> Map.put(:type, false)
        |> Map.put(:message, message_data)
        |> Map.put(:owners, [user_data])
        |> ContentHelpers.fetch_data_with_model(Map.keys(data))
        |> Map.put("users", [])
        |> ContentHelpers.conver_datatimes_and_id()

      assert room_data == data 
      assert 1 == json_response(res, 200)["data"]["listUserRooms"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["listUserRooms"]["metadata"]["after"]
      assert 1 == json_response(res, 200)["data"]["listUserRooms"]["metadata"]["totalCount"]
    end

    test "messages/1 returns the list messages", %{
      conn: conn,
      room: room,
      message: message,
      user: user
    } do
      query = """
      {
        messages(after: null, limit: 1, roomId: #{room.id}) {
          entries {
            #{QueryHelpers.message()}
          }
          metadata {
            after
            limit
            totalCount  
          }
        }
      }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "query"))

      data = json_response(res, 200)["data"]["messages"]["entries"] |> List.first()

      message_data =
        message
        |> ContentHelpers.fetch_data_with_model(Map.keys(data["room"]["message"]))
        |> Map.put("user", ContentHelpers.model_fetch_user_data(user, data["room"]["message"]))
        |> Map.put("room", %{"id" => Integer.to_string(room.id)})
        |> Map.put("description", %{"desc" => message.description, "users" => []})
        |> ContentHelpers.conver_datatimes_and_id()

      user_data =
        user
        |> ContentHelpers.fetch_data_with_model(
          data["room"]["owners"]
          |> List.first()
          |> Map.keys()
        )
        |> ContentHelpers.conver_datatimes_and_id() 

      room_data =
        room
        |> Map.put(:type, false)
        |> Map.put(:message, message_data)
        |> Map.put(:owners, [user_data])
        |> ContentHelpers.fetch_data_with_model(Map.keys(data["room"]))
        |> Map.put("users", [])
        |> ContentHelpers.conver_datatimes_and_id()

      result_data =
        message
        |> ContentHelpers.fetch_data_with_model(Map.keys(data))
        |> Map.put("user", ContentHelpers.model_fetch_user_data(user, data))
        |> Map.put("room", room_data)
        |> Map.put("description", %{"desc" => message.description, "users" => []})
        |> ContentHelpers.conver_datatimes_and_id()

      assert result_data == data
      assert 1 == json_response(res, 200)["data"]["messages"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["messages"]["metadata"]["after"]
      assert 1 == json_response(res, 200)["data"]["messages"]["metadata"]["totalCount"]
    end
  end
end
