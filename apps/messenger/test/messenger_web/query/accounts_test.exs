defmodule Messenger.Query.AccountsTest do
  use MessengerWeb.ConnCase
  use ExUnit.Case, async: true

  alias Messenger.QueryHelpers
  alias SmileDB.ContentHelpers
  alias Messenger.AbsintheHelpers

  describe "accounts" do
    test "get_account_user/1 returns the user", context do
      query = """
      {
        account {
          user {
            #{QueryHelpers.user()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user = json_response(res, 200)["data"]["account"]["user"]

      fetch_data =
        context.user
        |> ContentHelpers.fetch_data_with_model(Map.keys(user))
        |> ContentHelpers.conver_datatimes_and_id()

      assert fetch_data == user
    end

    test "list_of_user_room_id/1 returns for user listOfUserRoomId", context do
      query = """
      {
        account {
          listOfUserRoomId
        }
      }
      """

      res =
        context.conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      assert [context.room.id] == json_response(res, 200)["data"]["account"]["listOfUserRoomId"]
    end

    test "messages_notifications/1 returns for user list messages_notifications", %{
      conn: conn,
      room: room,
      message: message,
      user: user
    } do
      query = """
      {
        account {
          messagesNotifications {
            roomId
            message {
                id
                description {
                    #{QueryHelpers.description()}
                }
                insertedAt
                eventName
                user {
                    #{QueryHelpers.user()}
                }
                room {
                  id
                }
                updatedAt
            }
          }
        }
      }
      """

      res =
        conn
        |> post("/messenger/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      data = json_response(res, 200)["data"]["account"]["messagesNotifications"] |> List.first()

      message_data =
        message
        |> ContentHelpers.fetch_data_with_model(Map.keys(data["message"]))
        |> ContentHelpers.conver_datatimes_and_id()
        |> Map.put("room", %{"id" => Integer.to_string(room.id)})
        |> Map.put("user", ContentHelpers.model_fetch_user_data(user, data["message"]))
        |> Map.put("description", %{"desc" => message.description, "users" => []})

      assert room.id == data["roomId"]
      assert message_data == data["message"]
    end
  end
end
