defmodule Messenger.QueryHelpers do
  @moduledoc """
      In this module, the modules describe the functions of objects for unification of samples of data from `GraphQl`.

      What would use these functions for samples should be set `alias` or `import` [`Messenger.QueryHelpers]`(Messenger.QueryHelpers.html) of this module and insert the required function through the interpolation as data as the returned function is a string.
  """

  alias Messenger.QueryHelpers

  @spec user() :: String.t()
  def user do
    "
                id
                answersCount
                authProvider
                avatar
                biography
                birthday
                commentsCount
                email
                expert
                status
                website
                lastName
                firstName
                insertedAt
                likesCount
                contentsCount
                gender
                googleUid
                facebookUid
                nickname
                updatedAt
            "
  end

  @spec description() :: String.t()
  def description do
    "
                desc
                users {
                    #{QueryHelpers.user()}
                    }   
            "
  end
  
  @spec room() :: String.t()
  def room do
    "
                id
                message {
                    id
                    description {
                        #{QueryHelpers.description()}
                    }
                    insertedAt
                    eventName
                    user {
                        #{QueryHelpers.user()}
                    }
                    room {
                        id
                    }
                updatedAt
                }
                name
                type
                insertedAt
                owners {
                    #{QueryHelpers.user()}
                }
                users {
                    #{QueryHelpers.user()}
                }
                updatedAt
            "
  end

  @spec message() :: String.t()
  def message do
    "
                id
                description {
                    #{QueryHelpers.description()}
                }
                insertedAt
                eventName
                user {
                    #{QueryHelpers.user()}
                }
                room {
                    #{QueryHelpers.room()}
                }
                updatedAt
            "
  end

  @spec metadata() :: String.t()
  def metadata do
    "
                after
                limit
                totalCount
            "
  end
end
