defmodule MessengerWeb.SubscriptionCase do
  @moduledoc """
  This module defines the test case to be used by
  subscription tests
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with channels
      use MessengerWeb.ChannelCase

      use Absinthe.Phoenix.SubscriptionTest,
        schema: MessengerWeb.Schema
      
      # The default endpoint for testing
      @endpoint KnowledgePhoenixWeb.Endpoint

      setup do
        {:ok, %{user: user, room: room, message: message, user_room: user_room, user_one: user_one}} =
          SmileDB.ContentHelpers.create_content([])

        {:ok, jwt, _claims} = SmileDB.Auth.Guardian.encode_and_sign(user)

        {:ok, socket} =
          Phoenix.ChannelTest.connect(MessengerWeb.UserSocket, %{
            "Authorization" => "Bearer " <> jwt
          })

        {:ok, socket} = Absinthe.Phoenix.SubscriptionTest.join_absinthe(socket)

        ## Added to socket and auth token 
        {:ok, %{socket: socket, user: user, room: room, message: message, user_room: user_room, user_one: user_one}}
      end
    end
  end
end
