defmodule MessengerWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest
      import MessengerWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint KnowledgePhoenixWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(SmileDB.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(SmileDB.Repo, {:shared, self()})
    end

    {:ok, %{user: user, room: room, message: message, user_room: user_room, user_one: user_one}} =
      SmileDB.ContentHelpers.create_content([]) 

    {:ok, jwt, _claims} = SmileDB.Auth.Guardian.encode_and_sign(user)

    conn =
      Phoenix.ConnTest.build_conn()
      |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
      |> Plug.Conn.put_req_header("content-type", "application/json")
      |> Absinthe.Plug.put_options(context: %{current_user: user})

    {:ok,
     %{
       conn: conn,
       user: user,
       room: room,
       message: message,
       user_room: user_room,
       user_one: user_one
     }}
  end
end
