defmodule MessengerWeb.Schema do
  @moduledoc false
  use SmileDB.Graphql

  import_types(MessengerWeb.Schema.Mutation)
  import_types(MessengerWeb.Schema.UserTypes)
  import_types(MessengerWeb.Schema.RoomTypes)
  import_types(MessengerWeb.Schema.Subscription)
  import_types(MessengerWeb.Schema.ChannelTypes)
  import_types(MessengerWeb.Schema.MessageTypes)
  import_types(MessengerWeb.Schema.AccountsTypes)
  import_types(MessengerWeb.Schema.PaginatorsTypes)

  alias MessengerWeb.Middlewares

  @desc "In this Root Query we display all API routes with which you can work. To work with the API, the router must be required to transmit the 'Token' to the user.
    The work with the router is sure to see what parameters you need to transmit and what type of data it takes, because the types of data are strictly asked."
  query do
    import_fields(:user_queries)

    import_fields(:rooms_queries)

    import_fields(:messages_queries)
  end

  mutation do
    import_fields(:mutations)
  end

  subscription do
    import_fields(:subscriptions)
  end

  def middleware(middleware, field, object) do
    [Middlewares.HandleUserStatus | super(middleware, field, object)]
  end
end
