defmodule MessengerWeb.Router do
  @moduledoc false
  use MessengerWeb, :router

  pipeline :require_auth do
    plug(SmileDB.Auth.Guardian.Pipeline.AuthPipeline)
  end

  scope "/" do
    pipe_through([:require_auth])
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: MessengerWeb.Schema)

    forward("/", Absinthe.Plug, schema: MessengerWeb.Schema)
  end
end
