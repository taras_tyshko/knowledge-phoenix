defmodule MessengerWeb.Middlewares.Notifications do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias Messenger.RoomConnections
  alias SmileDB.{Messages, Notifications}

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(resolution, async: true) do
    Task.async(__MODULE__, :call, [resolution, async: false])
    resolution
  end

  def call(
        %{
          context: %{
            pubsub: pubsub,
            current_user: %{id: id}
          },
          value: %Messages.Message{} = message
        } = resolution,
        _config
      ) do
    message
    |> fetch_list_users()
    |> List.delete(id)
    |> Enum.map(
      &%{
        user_id: &1,
        room_id: message.room_id,
        message_id: message.id
      }
    )
    |> Notifications.create_message_notifications()
    |> Enum.map(&publish_account_notification(&1, pubsub))

    resolution
  end

  def call(
        %{
          context: %{
            pubsub: pubsub,
            current_user: %{id: id}
          },
          value:
            %{
              system_message: message
            } = value
        } = resolution,
        _config
      ) do
    value
    |> fetch_list_users()
    |> List.delete(id)
    |> Enum.map(
      &%{
        user_id: &1,
        room_id: message.room_id,
        message_id: message.id
      }
    )
    |> Notifications.create_message_notifications()
    |> Enum.map(&publish_account_notification(&1, pubsub))

    resolution
  end

  def call(resolution, _config), do: resolution

  defp fetch_list_users(%{send_to_users: list_users}), do: list_users

  defp fetch_list_users(model) do
    case model do
      %{room_id: id} ->
        id

      %Messages.Room{id: id} ->
        id
    end
    |> RoomConnections.get_connections()
  end

  defp publish_account_notification(notification, pubsub) do
    Absinthe.Subscription.publish(
      pubsub,
      notification,
      messages_notifications: "notifications:#{notification.user_id}"
    )
  end
end
