defmodule MessengerWeb.Middlewares.AccountNotifications do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias Messenger.RoomConnections
  alias SmileDB.Messages

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(resolution, async: true) do
    Task.async(__MODULE__, :call, [resolution, async: false])
    resolution
  end

  def call(
        %{
          context: %{
            pubsub: pubsub,
            current_user: %{id: id}
          },
          value: value
        } = resolution,
        _config
      )
      when not is_nil(value) do
    value
    |> fetch_list_users()
    |> List.delete(id)
    |> Enum.map(&publish_account_notification_status(&1, pubsub))

    resolution
  end

  def call(resolution, _config), do: resolution

  defp fetch_list_users(%{send_to_users: list_users}), do: list_users

  defp fetch_list_users(model) do
    case model do
      %{room_id: id} ->
        id

      %Messages.Room{id: id} ->
        id
    end
    |> RoomConnections.get_connections()
  end

  defp publish_account_notification_status(id, pubsub) do
    Absinthe.Subscription.publish(
      pubsub,
      %{id: id, message_notifications: nil},
      account_info: "account:#{id}"
    )
  end
end
