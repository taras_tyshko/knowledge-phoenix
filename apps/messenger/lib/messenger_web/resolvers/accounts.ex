defmodule MessengerWeb.Resolvers.Accounts do
  @moduledoc """
      A module for the release of resolvers Accounts which are washed out in the schema.
  """

  alias SmileDB.{Repo, Accounts, Notifications}
  import Ecto.Query

  @doc """
    Functions to process query get messages notifications user data through the parent.

  ## Examples

      def get_user_messages_notifications(parent, _args, _resolution) do
        Notifications.get_message_notification(parent.id)
      end
  """
  @spec get_user_messages_notifications(Accounts.User.t(), map(), map()) ::
          {:ok, list(%{room_id: Integer.t(), message_id: Integer.t()})}
  def get_user_messages_notifications(parent, _args, _resolution) do
    {:ok, Notifications.get_message_notification(parent.id)}
  end

  @doc """
    Functions to process query delete user notifications by id.

  ## Examples

      def delete_account_notifications(_parent, %{id: ids}, _resolution) do
        Enum.map(ids, &Notifications.delete_notification/1)
      end
  """
  @spec delete_account_notifications(
          map(),
          %{id: list(String.t())},
          Absinthe.Resolution.t()
        ) :: {:ok, Accounts.User.t()}
  def delete_account_notifications(_parent, %{room_id: room_id}, %{
        context: %{current_user: user}
      }) do
    list_notifications =
      Notifications.MessageNotification
      |> where([n], n.user_id == ^user.id)
      |> where([n], n.room_id == ^room_id)
      |> select([n], n.id)
      |> Repo.all()
      |> Notifications.delete_message_notifications()

    {:ok,
     user
     |> Map.put(:delete_notifications, list_notifications)
     |> Map.put(:message_notifications, nil)}
  end
end
