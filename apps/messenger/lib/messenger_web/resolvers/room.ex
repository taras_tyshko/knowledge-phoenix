defmodule MessengerWeb.Resolvers.Room do
  @moduledoc """
      A module for the release of resolvers Room which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias Messenger.SystemMessages
  alias Messenger.RoomConnections
  alias SmileDB.{Repo, Accounts, Messages}

  @doc """
    Functions to process query get room data through the parent.

  ## Examples

      def find_room(parent, _args, _resolution) do
        parent
      end
  """
  @spec find_room(Messages.User.t(), map(), map()) :: {:ok, Messages.User.t()}
  def find_room(parent, _args, _resolution) do
    {:ok, parent}
  end

  @doc """
  Functions to process mutation `join_channel`.

  ## Examples

      def join_channel(_parent, %{user_id: user_id}, %{context: %{current_user: user, pubsub: pubsub}}) do
        user_room_id =
          Messages.UserRoom
          |> where([u], u.user_id == ^user_id)
          |> where([u], u.type == "privat")
          |> select([u], u.room_id)
          |> Repo.all()

        room =
          Messages.UserRoom
          |> where([u], u.user_id == ^user.id)
          |> where([u], u.type == "privat")
          |> where([u], u.room_id in ^user_room_id)
          |> select([u], u.room_id)
          |> Repo.all()

        if Enum.empty?(room) do
          case Messages.create_room(%{user_id: user.id}) do
            {:ok, room} ->
              Repo.transaction(fn ->
                Messages.create_user_room(%{user_id: user_id, type: "privat", room_id: room.id})
                Messages.create_user_room(%{user_id: user.id, type: "privat", room_id: room.id})
              end)

              {:ok, room}

            {:error, changeset} ->
              {:error, changeset.errors}
          end
        else
          {:ok, Messages.get_room!(List.first(room))}
        end
      end
  """
  @spec join_channel(map(), %{user_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Room.t()} | {:error, map()}
  def join_channel(_parent, %{user_id: user_id}, %{
        context: %{current_user: user}
      }) do
    room_ids =
      search index: ElasticSearch.get_meta!(Messages.UserRoom).index do
        query do
          terms("user_id", [user_id, user.id])
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.room_id)
      |> Enum.group_by(& &1)
      |> Enum.filter(&(Enum.count(elem(&1, 1)) >= 2))
      |> Enum.map(&elem(&1, 0))

    room =
      Messages.Room
      |> where([u], u.type == "privat")
      |> where([u], u.id in ^room_ids)
      |> Repo.all()

    if Enum.empty?(room) do
      case Messages.create_room(%{type: "privat"}) do
        {:ok, room} ->
          Repo.transaction(fn ->
            [user_id, user.id]
            |> Enum.map(fn user_id ->
              Messages.create_user_room(%{
                user_id: user_id,
                room_id: room.id,
                role: "owner"
              })
            end)
          end)

          {:ok, room}

        {:error, changeset} ->
          {:error, changeset}
      end
    else
      {:ok, List.first(room)}
    end
  end

  @doc """
  Functions to process mutation `create_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def create_channel(_parent, args, _resolution) do
          Messages.create_room(args)
      end
  """
  @spec create_channel(map(), %{name: String.t(), user_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Room.t()}
          | {:error, map()}
          | {:error, String.t()}
  def create_channel(_parent, %{name: name, user_id: user_id}, %{
        context: %{current_user: user}
      })
      when length(user_id) > 0 do
    case Messages.create_room(%{name: name, type: "public"}) do
      {:ok, room} ->
        user_ids =
          (user_id ++ [user.id])
          |> Enum.uniq()
          |> Enum.map(fn user_id ->
            %{
              user_id: user_id,
              room_id: room.id,
              role: if(user_id == user.id, do: "owner", else: "customer")
            }
          end)
          |> Messages.create_user_rooms()
          |> Enum.map(& &1.user_id)

        {:ok,
         room
         |> Map.put(:mutation, elem(__ENV__.function, 0))
         |> Map.put(
           :system_message,
           %{
             info_source: RoomConnections.connect(room.id, user_ids),
             current_user: user
           }
         )
         |> SystemMessages.generate()}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  def create_channel(_parent, _, _) do
    {:error, "List user_id is empty"}
  end

  @doc """
  Functions to process mutation `rename_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def rename_channel(_parent, args, _resolution) do
          args.room_id
          |> Messages.get_room!()
          |> Messages.update_room(args)
      end
  """
  @spec rename_channel(map(), map(), %{context: map()}) ::
          {:ok, Messages.Room.t()} | {:error, map()}
  def rename_channel(_parent, args, %{context: %{current_user: user}}) do
    with {:ok, room} <-
           args.room_id
           |> Messages.get_room!()
           |> Messages.update_room(args) do
      {:ok,
       room
       |> Map.put(:mutation, elem(__ENV__.function, 0))
       |> Map.put(
         :system_message,
         %{
           info_source: room,
           current_user: user
         }
       )
       |> SystemMessages.generate()}
    end
  end

  @doc """
  Functions to process query `get_channel` get room data.

  ## Examples

      def get_channel(_parent, args, _resolution) do
        {:ok, Messages.get_room!(args.room_id)}
      end
  """
  @spec get_channel(map(), %{room_id: Integer.t()}, map()) ::
          {:ok, Messages.Room.t()} | {:error, map()}
  def get_channel(_parent, %{room_id: room_id}, %{context: %{current_user: user}}) do
    room =
      case Repo.get_by(Messages.UserRoom, user_id: user.id, room_id: room_id) do
        nil -> nil
        _ -> Messages.get_room!(room_id)
      end

    {:ok, room}
  end

  def get_channel(_parent, %{user_id: user_id}, %{context: %{current_user: user}}) do
    room_ids =
      search index: ElasticSearch.get_meta!(Messages.UserRoom).index do
        query do
          terms("user_id", [user_id, user.id])
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.room_id)
      |> Enum.group_by(& &1)
      |> Enum.filter(&(Enum.count(elem(&1, 1)) >= 2))
      |> Enum.map(&elem(&1, 0))

    room =
      Messages.Room
      |> where([u], u.type == "privat")
      |> where([u], u.id in ^room_ids)
      |> Repo.all()
      |> List.first()

    {:ok, room}
  end

  @doc """
  Functions to process mutation `delete_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_channel(_parent, args, _resolution) do
          args.room_id
          |> Messages.get_room!()
          |> Messages.delete_room()
      end
  """
  @spec delete_channel(map(), %{room_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Room.t()} | {:error, String.t()}
  def delete_channel(_parent, %{room_id: room_id}, _resolution) do
    list_users = RoomConnections.get_connections(room_id)

    case room_id
         |> Messages.get_room!()
         |> Messages.delete_room() do
      {:ok, room} ->
        {:ok,
         room
         |> Map.put(:send_to_users, RoomConnections.disconnect(room_id, list_users))
         |> Map.put(:mutation, elem(__ENV__.function, 0))}

      other ->
        other
    end
  end

  @doc """
  Functions to process mutation `clear_messages_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def clear_messages_channel(_parent, args, _resolution) do
        Messages.Message
        |> where([m], m.room_id == ^args.room_id)
        |> Repo.delete_all()
      end
  """
  @spec clear_messages_channel(map(), %{room_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Room.t()} | {:error, String.t()}
  def clear_messages_channel(_parent, %{room_id: room_id}, %{
        context: %{current_user: user}
      }) do
    Messages.Message
    |> where([m], m.room_id == ^room_id)
    |> Messages.delete_messages()

    {:ok,
     Messages.Room
     |> struct(Map.new())
     |> Map.put(:id, room_id)
     |> Map.put(:mutation, elem(__ENV__.function, 0))
     |> Map.put(
       :system_message,
       %{
         info_source: "cleanChannel",
         current_user: user
       }
     )
     |> SystemMessages.generate()}
  end

  @doc """
  Functions to process mutation `add_people_in_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_people_in_channel(_parent, args, _resolution) do
          Messages.create_user_room(args)
      end
  """
  @spec add_people_in_channel(map(), %{user_id: Integer.t(), room_id: Integer.t()}, %{
          context: map()
        }) :: {:ok, Messages.Room.t()}
  def add_people_in_channel(_parent, %{user_id: user_id, room_id: room_id}, %{
        context: %{current_user: user}
      }) do
    user_ids =
      user_id
      |> Enum.map(fn user_id ->
        %{
          user_id: user_id,
          room_id: room_id,
          role: "customer"
        }
      end)
      |> Messages.create_user_rooms()
      |> Enum.map(& &1.user_id)

    {:ok,
     Messages.Room
     |> struct(Map.new())
     |> Map.put(:id, room_id)
     |> Map.put(:mutation, elem(__ENV__.function, 0))
     |> Map.put(
       :system_message,
       %{
         info_source: RoomConnections.connect(room_id, user_ids),
         current_user: user
       }
     )
     |> SystemMessages.generate()}
  end

  @doc """
  Functions to process mutation `delete_people_since_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_people_since_channel(_parent, args, _resolution) do
          args.id
          |> Messages.get_user_rooms!()
          |> Messages.delete_user_room()
      end
  """
  @spec delete_people_from_channel(map(), %{user_id: Integer.t(), room_id: Integer.t()}, %{
          context: map()
        }) :: {:ok, Messages.Room.t()} | {:error, String.t()}
  def delete_people_from_channel(_parent, %{user_id: user_id, room_id: room_id}, %{
        context: %{current_user: user}
      }) do
    list_users = RoomConnections.get_connections(room_id)

    user_ids =
      Messages.UserRoom
      |> where([ur], ur.user_id in ^user_id)
      |> where([ur], ur.room_id == ^room_id)
      |> Messages.delete_user_rooms()
      |> Enum.map(& &1.user_id)

    {:ok,
     Messages.Room
     |> struct(Map.new())
     |> Map.put(:id, room_id)
     |> Map.put(:send_to_users, list_users)
     |> Map.put(:mutation, elem(__ENV__.function, 0))
     |> Map.put(
       :system_message,
       %{
         info_source: RoomConnections.disconnect(room_id, user_ids),
         current_user: user
       }
     )
     |> SystemMessages.generate()}
  end

  @doc """
  Functions to process mutation `delete_people_since_channel` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_people_since_channel(_parent, args, _resolution) do
        args.id
        |> Messages.get_user_rooms!()
        |> Messages.delete_user_room()
      end
  """
  @spec leave_channel(map(), %{room_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Room.t()}
  def leave_channel(_parent, %{room_id: room_id}, %{
        context: %{current_user: user}
      }) do
    list_users = RoomConnections.get_connections(room_id)

    user_ids =
      Messages.UserRoom
      |> where([ur], ur.user_id == ^user.id)
      |> where([ur], ur.room_id == ^room_id)
      |> Messages.delete_user_rooms()
      |> Enum.map(& &1.user_id)

    {:ok,
     Messages.Room
     |> struct(Map.new())
     |> Map.put(:id, room_id)
     |> Map.put(:send_to_users, list_users)
     |> Map.put(:mutation, elem(__ENV__.function, 0))
     |> Map.put(
       :system_message,
       %{
         info_source: RoomConnections.disconnect(room_id, user_ids),
         current_user: user
       }
     )
     |> SystemMessages.generate()}
  end

  @doc """
  Functions to process query `list_user_rooms` and pagination.

  ## Examples

      def list_user_rooms(_parent, args}, %{context: %{total_count: total_count}}) do
          %{entries: entries, metadata: metadata} =
              Messages.UserRoom
              |> where([u], u.user_id == ^args.user_id)
              |> order_by([desc: :inserted_at])
              |> select([m], %{room_id: m.room_id, inserted_at: m.inserted_at, id: m.id})
              |> Repo.paginate(
                  after: args.after_cursor,
                  cursor_fields: [:inserted_at, :id],
                  limit: args.limit,
                  include_total_count: total_count
              )

          {:ok, %{entries: Enum.map(entries, &Messages.get_room!(&1.room_id)), metadata: metadata}}
      end
  """
  @spec list_user_rooms(map(), %{limit: Integer.t(), after: String.t()}, %{context: map()}) ::
          {:ok, %{entries: list(Messages.Room.t()), metadata: map()}}
  def list_user_rooms(_parent, %{limit: limit, after: after_cursor}, %{
        context: %{current_user: user}
      }) do
    room_ids =
      search index: ElasticSearch.get_meta!(Messages.UserRoom).index do
        query do
          term("user_id", user.id)
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.room_id)

    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Messages.Room).index do
        query do
          terms("id", room_ids)
        end

        sort do
          ["last_message.inserted_at": :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:"last_message.inserted_at", :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query autocomplete rooms.

  ## Examples

      def rooms_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        suggest =
          search index: ElasticSearch.get_meta!(Messages.Room).index do
            suggest do
              room_suggest do
                prefix(input, [])

                completion do
                  field("name_autocomplete", [])
                  fuzzy(:fuzziness, "AUTO")
                end
              end
            end
          end
          |> ElasticSearch.Repo.all(limit)

        {:ok,
          suggest.room_suggest
          |> List.first()
          |> Map.fetch!(:options)
          |> Enum.map(&Map.fetch!(&1._source, :name))
          |> Enum.uniq()}
      end
  """
  @spec rooms_autocomplete(map(), %{limit: Integer.t(), input: String.t()}, map()) ::
          list(Messages.Room.t())
  def rooms_autocomplete(_parent, %{limit: limit, input: input}, %{context: %{current_user: user}}) do
    rooms_ids =
      search(index: ElasticSearch.get_meta!(Messages.UserRoom).index) do
        query do
          term("user_id", user.id)
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.room_id)

    rooms =
      search(index: ElasticSearch.get_meta!(Messages.Room).index) do
        query do
          bool do
            must do
              match_phrase_prefix("name_search", input)
            end

            must_not do
              term("type", "privat")
            end

            filter do
              terms("id", rooms_ids)
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, rooms}
  end

  @doc """
    Functions to process query get user data through the parent.

  ## Examples

      def get_user_rooms(parent, _args, _resolution) do
        Messages.list_of_user_rooms(parent.id)
      end
  """
  @spec get_user_rooms(Accounts.User.t(), map(), %{context: map()}) ::
          {:ok, list(Accounts.User.t())} | {:error, map()}
  def get_user_rooms(%{id: id}, _args, _resolution) do
    {:ok, Messages.list_user_rooms(room_id: id)}
  end

  @doc """
    This mutation implements the functional, shows who the users are printing the text in the channel.
    Also this feature makes a newsletter for users who are in this room that the user is printing the message.

    ## Examples

      def user_typing(_parent, %{status: status}, %{
            context: %{current_user: user, pubsub: pubsub}
          }) do
        user_rooms = Messages.list_user_rooms(room_id)

        Enum.map(user_rooms -- [user.id], fn user_id ->
          user_data = Accounts.get_user!(user_id)
          # push subscription when event name: channel_info: "user_typing:\#{user.nickname}".
          push_subscription_user_typing(user_data, pubsub, %{
            room_id: room_id,
            status: status,
            user: user
          })
        end)

        {:ok, status}
      end
  """
  @spec user_typing(map(), %{room_id: Integer.t(), status: boolean}, %{context: map()}) ::
          {:ok, boolean}
  def user_typing(_parent, %{room_id: room_id, status: status}, %{
        context: %{current_user: user}
      }) do
    {:ok,
     %{
       room_id: room_id,
       status: status,
       user: user
     }}
  end
end
