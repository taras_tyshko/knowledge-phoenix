defmodule MessengerWeb.Resolvers.Message do
  @moduledoc """
      A module for the release of resolvers Accounts which are washed out in the schema.
  """

  import Tirexs.Search

  alias SmileDB.Messages

  @doc """
  Functions to process mutation `add_messages` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_messages(_parent, args, _resolution) do
          case create_message(args) do
              {:ok, message} ->
                  {:ok, message}

              {:error, changeset} ->
                  {:error, changeset}
          end
      end
  """
  @spec add_messages(map(), map(), %{context: map()}) ::
          {:ok, Messages.Message.t()} | {:error, map()}
  def add_messages(_parent, args, %{context: %{current_user: user}}) do
    with {:ok, message} <-
           args
           |> Map.put(:user_id, user.id)
           |> Messages.create_message() do
      {:ok, Map.put(message, :mutation, elem(__ENV__.function, 0))}
    end
  end

  @doc """
  Functions to process mutation ` edit_messages` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def edit_messages(_parent, args, _resolution) do
          case Messages.get_message!(args.message_id) do
              {:ok, message} ->
                  Messages.update_message(message, args)

              {:error, changeset} ->
                  {:error, changeset}
          end
      end
  """
  @spec edit_messages(map(), map(), map()) ::
          {:ok, Messages.Message.t()} | {:error, map()}
  def edit_messages(_parent, args, _resolution) do
    with {:ok, message} <-
           args.message_id
           |> Messages.get_message!()
           |> Messages.update_message(args) do
      {:ok, Map.put(message, :mutation, elem(__ENV__.function, 0))}
    end
  end

  @doc """
  Functions to process mutation `delete_messages` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_messages(_parent, args, _resolution) do
          case Messages.get_message!(args.message_id) do
              {:ok, message} ->
                  Messages.delete_message(message)

              {:error, changeset} ->
                  {:error, changeset}
          end
      end
  """
  @spec delete_messages(map(), %{message_id: Integer.t(), room_id: Integer.t()}, %{context: map()}) ::
          {:ok, Messages.Message.t()} | {:error, map()}
  def delete_messages(_parent, args, _resolution) do
    with {:ok, message} <-
           args.message_id
           |> Messages.get_message!()
           |> Messages.delete_message() do
      {:ok, Map.put(message, :mutation, elem(__ENV__.function, 0))}
    end
  end

  @doc """
  Gets a list messages for room with pagination.

  ## Examples

      def list_messages(_parent, args, %{context: %{total_count: total_count}}) do
          {:ok,
              %{entries: entries, metadata: metadata} =
                  Message
                  |> where([m], m.room_id == ^args.room_id)
                  |> order_by(desc: :inserted_at)
                  |> Repo.paginate(
                      after: args.after_cursor,
                      cursor_fields: [:inserted_at, :id],
                      limit: args.limit,
                      include_total_count: total_count
                  )
              }
  end

  """
  @spec list_messages(map(), %{room_id: Integer.t(), limit: Integer.t(), after: String.t()}, %{
          context: map()
        }) ::
          {:ok, %{entries: list(Messages.Message.t()), metadata: map()}}
          | {:error, map()}
  def list_messages(_parent, %{room_id: room_id, limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Messages.Message).index do
        query do
          term("room_id", room_id)
        end

        sort do
          [inserted_at: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: Enum.reverse(entries),
       metadata: metadata
     }}
  end

  @doc """
  Gets a last message.

  Raises `Ecto.NoResultsError` if the Message does not exist.

  ## Examples

      iex> get_last_message!(123)
      Message |> last |> Repo.one

      iex> get_last_message!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_last_message(Messages.Message.t(), map(), map()) ::
          {:ok, Messages.Message.t()} | {:error, map()}
  def get_last_message(parent, _args, _resolution) do
    {:ok, Messages.get_last_message(parent.id)}
  end
end
