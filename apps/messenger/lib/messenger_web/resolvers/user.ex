defmodule MessengerWeb.Resolvers.User do
  @moduledoc """
    The module allows to work with users entity. Provides renders user by id,
    returns a list of experts, them filtering by tags, search and autocomplete.

    In addition, the module provides api for the user profile, such as:
    updates information about user, uploads and imports user avatar.

    Search and autocomplete implemented by using `ElasticSearch`. We use `Tirexs`
    module that provides many features for work with `ElasticSearch`. See the
    [`Tirexs`](https://hexdocs.pm/tirexs/readme.html) docs for more details.
  """

  import Tirexs.Search

  alias SmileDB.Accounts
  alias SmileDB.Messages
  alias SmileDB.Subscriptions

  def find_people(_parent, %{limit: limit, input: input}, %{context: %{current_user: user}}) do
    chat_users_id =
      [user_id: user.id]
      |> Messages.list_user_rooms()
      |> Enum.map(fn user_room ->
        [room_id: user_room.room_id]
        |> Messages.list_user_rooms()
        |> Enum.map(& &1.user_id)
      end)
      |> List.flatten()
      |> Enum.filter(&(&1 != user.id))

    chat_users =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              terms("id", chat_users_id)
            end
          end
        end

        sort do
          [contents_count: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    data =
      with limit <- limit - Enum.count(chat_users),
           true <- limit > 0 do
        followers_id = Subscriptions.list_users_subscription(user.id)

        followers =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2"
                    ],
                    type: "phrase_prefix"
                  )
                end

                must_not do
                  terms("id", [user.id | chat_users_id])
                end

                filter do
                  terms("id", followers_id)
                end
              end
            end

            sort do
              [contents_count: :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        data =
          with limit <- limit - Enum.count(followers),
               true <- limit > 0 do
            experts =
              search index: ElasticSearch.get_meta!(Accounts.User).index do
                query do
                  bool do
                    must do
                      multi_match(
                        input,
                        [
                          "name_search^3",
                          "nickname_search^2"
                        ],
                        type: "phrase_prefix"
                      )
                    end

                    must_not do
                      terms(
                        "id",
                        Enum.concat(
                          [user.id | followers_id],
                          chat_users_id
                        )
                      )
                    end

                    filter do
                      exists("expert")
                    end
                  end
                end

                sort do
                  [contents_count: :desc]
                end
              end
              |> ElasticSearch.Repo.all(limit)

            data =
              with limit <- limit - Enum.count(experts),
                   true <- limit > 0 do
                data =
                  search index: ElasticSearch.get_meta!(Accounts.User).index do
                    query do
                      bool do
                        must do
                          bool do
                            must do
                              multi_match(
                                input,
                                [
                                  "name_search^3",
                                  "nickname_search^2"
                                ],
                                type: "phrase_prefix"
                              )
                            end

                            must_not do
                              terms(
                                "id",
                                Enum.concat(
                                  Enum.concat(
                                    [user.id | Enum.map(experts, & &1.id)],
                                    followers_id
                                  ),
                                  chat_users_id
                                )
                              )
                            end
                          end
                        end

                        must_not do
                          exists("expert")
                        end
                      end
                    end

                    sort do
                      [contents_count: :desc]
                    end
                  end
                  |> ElasticSearch.Repo.all(limit)

                List.flatten(experts, data)
              else
                false -> experts
              end

            List.flatten(followers, data)
          else
            false -> followers
          end

        List.flatten(chat_users, data)
      else
        false -> chat_users
      end

    {:ok, data}
  end

  def find_users(_parent, args, %{context: %{current_user: user}}) do
    with input <- Map.fetch!(args, :input),
         limit <- Map.get(args, :limit, 10000),
         room_id <- Map.get(args, :room_id) do
      {:ok, users_search(input, limit, user.id, room_id)}
    end
  end

  defp users_search(input, limit, user, nil) do
    users_search(input, limit, user, [])
    |> Map.put(:channel_members, nil)
  end

  defp users_search(input, limit, user, room_id) when is_integer(room_id) do
    user_ids =
      with %{id: id, type: type} <- Messages.get_room!(room_id),
           "public" <- type do
        search index: ElasticSearch.get_meta!(Messages.UserRoom).index do
          query do
            term("room_id", id)
          end
        end
        |> ElasticSearch.Repo.all()
        |> Enum.map(& &1.user_id)
      else
        "privat" -> nil
      end

    users_search(input, limit, user, user_ids)
  end

  defp users_search(input, limit, user, users_in_room) when is_list(users_in_room) do
    in_channel_users =
      input
      |> in_channel_query(users_in_room, user)
      |> ElasticSearch.Repo.all(limit)

    in_channel_users_ids = Enum.map(in_channel_users, & &1.id)

    experts =
      input
      |> experts_query(in_channel_users_ids, user)
      |> ElasticSearch.Repo.all(limit)

    experts_ids = Enum.map(experts, & &1.id)

    not_in_channel_users =
      input
      |> not_in_channel_query(
        Enum.concat(
          in_channel_users_ids,
          experts_ids
        ),
        user
      )
      |> ElasticSearch.Repo.all(limit)

    %{
      channel_members: in_channel_users,
      experts: experts,
      not_in_channel: not_in_channel_users
    }
  end

  defp in_channel_query(input, user_ids, current_user_id) do
    search index: ElasticSearch.get_meta!(Accounts.User).index do
      query do
        bool do
          must do
            multi_match(
              input,
              [
                "name_search^3",
                "nickname_search^2"
              ],
              type: "phrase_prefix"
            )
          end

          must_not do
            term("id", current_user_id)
          end

          filter do
            terms("id", user_ids)
          end
        end
      end
    end
  end

  defp experts_query(input, user_ids, current_user_id) do
    search index: ElasticSearch.get_meta!(Accounts.User).index do
      query do
        bool do
          must do
            bool do
              must do
                multi_match(
                  input,
                  [
                    "name_search^3",
                    "nickname_search^2"
                  ],
                  type: "phrase_prefix"
                )
              end

              must_not do
                term("id", current_user_id)
              end
            end
          end

          must_not do
            terms("id", user_ids)
          end

          filter do
            exists("expert")
          end
        end
      end
    end
  end

  defp not_in_channel_query(input, user_ids, current_user_id) do
    search index: ElasticSearch.get_meta!(Accounts.User).index do
      query do
        bool do
          must do
            bool do
              must do
                bool do
                  must do
                    multi_match(
                      input,
                      [
                        "name_search^3",
                        "nickname_search^2"
                      ],
                      type: "phrase_prefix"
                    )
                  end

                  must_not do
                    term("id", current_user_id)
                  end
                end
              end

              must_not do
                terms("id", user_ids)
              end
            end

            must_not do
              exists("expert")
            end
          end
        end
      end
    end
  end
end
