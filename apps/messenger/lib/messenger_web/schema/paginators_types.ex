defmodule MessengerWeb.Schema.PaginatorsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :room_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:room_interface), description: "Get list of Room.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :channel_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:channel), description: "Get list of Room.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :message_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:message), description: "Get list of Message.")
  end

  @desc "Implement the structure to obtain additional data for the cursor pagination such as."
  object :metadata do
    field(:after, :string, description: "Fetch the records after this cursor.")
    field(:limit, :integer, description: "Number of results to return.")
    field(:total_count, :integer, description: "Number of all results.")
  end
end
