defmodule MessengerWeb.Schema.UserTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias MessengerWeb.Resolvers

  object :user_queries do
    @desc "Get a list of users."
    field :users, :user_search do
      arg(:room_id, :integer, description: "List of preset tags to get experts.")
      arg(:input, non_null(:string), description: "Search user by word. Where word is user nickname.")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.User.find_users/3)
    end

    @desc "Get a list of users."
    field :people, list_of(:user) do
      arg(:input, non_null(:string), description: "Search user by word. Where word is user nickname.")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.User.find_people/3)
    end
  end

  object :user_search do
    field :channel_members, list_of(:user)
    field :experts, list_of(:user)
    field :not_in_channel, list_of(:user)
  end
end
