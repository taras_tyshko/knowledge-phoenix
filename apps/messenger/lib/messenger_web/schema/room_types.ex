defmodule MessengerWeb.Schema.RoomTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias MessengerWeb.Resolvers

  @desc "Realizes the receipt of channel of data such as: rooms of rooms autocomplete."
  object :rooms_queries do
    @desc "Get cnannel data."
    field :get_channel, :channel do
      arg(:user_id, :integer, description: "Unique identifier User ID.")
      arg(:room_id, :integer, description: "Unique identifier room ID.")

      resolve(&Resolvers.Room.get_channel/3)
    end

    @desc "Get list of rooms via search by name."
    field :rooms_autocomplete, list_of(:room_interface) do
      arg(:input, :string, description: "Search rooms by field name.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Room.rooms_autocomplete/3)
    end

    @desc "Get the list of rooms in which the user is active."
    field :list_user_rooms, :channel_paginator do
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:after, :string, description: "Fetch the records after this cursor.")

      resolve(&Resolvers.Room.list_user_rooms/3)
    end
  end

  object :channel do
    @desc "Unique identifier of the Room."
    field :id, :integer do
      resolve(fn parent, _, _ ->
        {:ok, parent.id}
      end)
    end

    @desc "Get user Data."
    field :room, :room_interface do
      resolve(&Resolvers.Room.find_room/3)
    end

    @desc "Get a list of users that are active and this channel."
    field(:users, :channel_users_type) do
      resolve(&Resolvers.Room.get_user_rooms/3)
    end
  end

  object :user_typing do
    field(:status, :boolean, description: "User typing: true || false.")
    field(:room_id, :integer, description: "Unique identifier of the Room.")
    field(:user, :user, description: "Get a list of users who are typing and this channel.")
  end
end
