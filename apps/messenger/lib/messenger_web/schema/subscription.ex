defmodule MessengerWeb.Schema.Subscription do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.{Messages, Accounts}
  alias Messenger.RoomConnections

  object :subscriptions do
    @desc "Watch message notifications on mutations: [
      :leave_channel,
      :create_channel,
      :delete_people_from_channel,
      :add_people_in_channel,
      :clear_messages_channel,
      :add_messages,
      :delete_channel,
      :delete_messages,
      :delete_account_notifications
    ]"
    field :account_info, :account_interface do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: "account:#{user.id}"}
      end)

      trigger(
        :delete_account_notifications,
        topic: fn %Accounts.User{id: id} ->
          "account:#{id}"
        end
      )
    end

    @desc "Watch message notifications on mutations: [
      :leave_channel,
      :create_channel,
      :delete_people_from_channel,
      :add_people_in_channel,
      :clear_messages_channel,
      :add_messages
    ]"
    field :messages_notifications, :messages_notifications do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: "notifications:#{user.id}"}
      end)
    end

    @desc "Watch delete message notifications on mutation delete_account_notifications"
    field :delete_messages_notifications, list_of(:messages_notifications) do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: "notifications:#{user.id}"}
      end)

      trigger(
        :delete_account_notifications,
        topic: fn %{id: id} ->
          "notifications:#{id}"
        end
      )

      resolve(fn %{delete_notifications: notifications}, _args, _resolution ->
        {:ok, notifications}
      end)
    end

    @desc "This mutation implements the reflection which the user picks up the text. listen to mutation: :user_typing."
    field :typing_user, :user_typing do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: "user_typing:#{user.id}"}
      end)

      trigger(
        :user_typing,
        topic: fn data ->
          data.room_id
          |> RoomConnections.get_connections()
          |> List.delete(data.user.id)
          |> Enum.map(fn user_id ->
            "user_typing:#{user_id}"
          end)
        end
      )
    end

    @desc "Gets channel Info, listen to mutations: [
      :leave_channel,
      :create_channel,
      :delete_people_from_channel,
      :add_people_in_channel,
      :clear_messages_channel,
      :rename_channel,
      :add_messages,
      :edit_messages,
      :delete_channel,
      :delete_messages
    ]"
    field :channel_info, :channel_interface do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: "channel:#{user.id}"}
      end)

      trigger(
        [
          :leave_channel,
          :create_channel,
          :delete_people_from_channel,
          :add_people_in_channel,
          :clear_messages_channel,
          :rename_channel,
          :add_messages,
          :edit_messages,
          :delete_channel,
          :delete_messages
        ],
        topic: fn
          %{send_to_users: user_ids} ->
            user_ids
            |> Enum.map(fn id ->
              "channel:#{id}"
            end)

          model ->
            case model do
              %{room_id: id} -> id
              %Messages.Room{id: id} -> id
            end
            |> RoomConnections.get_connections()
            |> Enum.map(fn id ->
              "channel:#{id}"
            end)
        end
      )
    end
  end
end
