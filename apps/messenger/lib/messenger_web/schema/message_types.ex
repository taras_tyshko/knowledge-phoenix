defmodule MessengerWeb.Schema.MessageTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias MessengerWeb.Resolvers

  @desc "Realizes the receipt of messages of data."
  object :messages_queries do
    @desc "List Messeges"
    field :messages, :message_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:room_id, :integer, description: "Unique identifier of the Room.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(
        (&Resolvers.Message.list_messages/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.in_room/3
        ])
      )
    end
  end
end
