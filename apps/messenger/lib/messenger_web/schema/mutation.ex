defmodule MessengerWeb.Schema.Mutation do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias MessengerWeb.Middlewares.{Notifications, AccountNotifications}
  alias MessengerWeb.Resolvers

  @desc "Realizes the receipt of messages of data."
  object :mutations do
    @desc "Implements receipt Room ID if the channel exists or if users do not have a private channel then it is created."
    field :join_channel, :channel_interface do
      arg(:user_id, non_null(:integer), description: "Unique identifier of the User.")

      resolve(
        (&Resolvers.Room.join_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3
        ])
      )
    end

    @desc "This mutation implements the reflection which the user picks up the text."
    field :user_typing, :user_typing do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:status, non_null(:boolean), description: "User typing: true || false.")

      resolve(
        (&Resolvers.Room.user_typing/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )
    end

    @desc "Creation of public channel."
    field :create_channel, :channel_interface do
      arg(:name, :string, description: "Name of the channel.")
      arg(:user_id, non_null(list_of(:integer)), description: "Unique identifier of the User.")

      resolve(
        (&Resolvers.Room.create_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @desc "Renaming of public channel."
    field :rename_channel, :channel_interface do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:name, non_null(:string), description: "Name of the channel.")

      resolve(
        (&Resolvers.Room.rename_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )
    end

    @desc "Delete channel."
    field :delete_channel, :channel_interface do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")

      resolve(
        (&Resolvers.Room.delete_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(AccountNotifications, async: true)
    end

    @desc "Implements cleanup of all messages that were in the feed and creates a system message that 'cleanChannel'."
    field :clear_messages_channel, :channel_interface do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")

      resolve(
        (&Resolvers.Room.clear_messages_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @desc "Implements add users to the channel."
    field :add_people_in_channel, :channel_interface do
      arg(:user_id, non_null(list_of(:integer)), description: "Unique identifier of the User.")
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")

      resolve(
        (&Resolvers.Room.add_people_in_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @desc "Implements delete users since channel."
    field :delete_people_from_channel, :channel_interface do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:user_id, non_null(list_of(:integer)), description: "Unique identifier of the User.")

      resolve(
        (&Resolvers.Room.delete_people_from_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @desc "Leave the Channel."
    field :leave_channel, :channel_interface do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")

      resolve(
        (&Resolvers.Room.leave_channel/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @decs "Add a Messages in Channel."
    field :add_messages, :message do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:description, non_null(:string), description: "Content description.")

      resolve(
        (&Resolvers.Message.add_messages/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(Notifications, async: true)
      middleware(AccountNotifications, async: true)
    end

    @decs "Edit a Messages in a Channel."
    field :edit_messages, :message do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:description, non_null(:string), description: "Content description.")
      arg(:message_id, non_null(:integer), description: "Unique identifier of the Message.")

      resolve(
        (&Resolvers.Message.edit_messages/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )
    end

    @decs "Delete a message from the channel."
    field :delete_messages, :message do
      arg(:room_id, non_null(:integer), description: "Unique identifier of the Room.")
      arg(:message_id, non_null(:integer), description: "Unique identifier of the Message.")

      resolve(
        (&Resolvers.Message.delete_messages/3)
        |> Messenger.Resolver.permission([
          &Messenger.Permission.is_active_account/3,
          &Messenger.Permission.in_room/3
        ])
      )

      middleware(AccountNotifications, async: true)
    end

    @desc "Delete user answer and comment notifications"
    field :delete_account_notifications, :account do
      arg(:room_id, :integer, description: "Room ID`s.")

      resolve(&Resolvers.Accounts.delete_account_notifications/3)
    end
  end
end
