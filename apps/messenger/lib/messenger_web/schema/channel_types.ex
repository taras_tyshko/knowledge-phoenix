defmodule MessengerWeb.Schema.ChannelTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.{Messages, Accounts}

  interface :channel_interface do
    field(:id, :integer)

    resolve_type(fn
      %{mutation: :create_channel}, _ -> :create_channel
      %{mutation: :rename_channel}, _ -> :rename_channel
      %{mutation: :delete_channel}, _ -> :delete_channel
      %{mutation: :clear_messages_channel}, _ -> :clear_messages_channel
      %{mutation: :add_people_in_channel}, _ -> :add_people_in_channel
      %{mutation: :delete_people_from_channel}, _ -> :delete_people_from_channel
      %{mutation: :leave_channel}, _ -> :leave_channel
      %{mutation: :add_messages}, _ -> :add_messages
      %{mutation: :edit_messages}, _ -> :edit_messages
      %{mutation: :delete_messages}, _ -> :delete_messages
      %Messages.Room{}, _ -> :channel_room
    end)
  end

  object :create_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn room, _, _ ->
        {:ok, room}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :users, :channel_users_type do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.list_user_rooms(room_id: id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :rename_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn room, _, _ ->
        {:ok, room}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :delete_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn room, _, _ ->
        {:ok, room}
      end)
    end

    interface(:channel_interface)
  end

  object :clear_messages_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :add_people_in_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :users, :channel_users_type do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.list_user_rooms(room_id: id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :delete_people_from_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :users, :channel_users_type do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.list_user_rooms(room_id: id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :leave_channel do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :users, :channel_users_type do
      resolve(fn %{id: id}, _, _ ->
        {:ok, Messages.list_user_rooms(room_id: id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn %{system_message: message}, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :add_messages do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn message, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :edit_messages do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn message, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :delete_messages do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn %{room_id: id}, _, _ ->
        {:ok, Messages.get_room!(id)}
      end)
    end

    @desc "Get a message."
    field :message, :message do
      resolve(fn message, _, _ ->
        {:ok, message}
      end)
    end

    interface(:channel_interface)
  end

  object :channel_room do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :room, :room_interface do
      resolve(fn room, _, _ ->
        {:ok, room}
      end)
    end

    interface(:channel_interface)
  end

  object :channel_users_type do
    field(:owners, list_of(:user), description: "List of room owners") do
      resolve(fn parent, _args, _resolution ->
        {:ok,
         parent
         |> Enum.filter(&(&1.role == "owner"))
         |> Enum.map(& &1.user_id)
         |> Accounts.list_users()}
      end)
    end

    field(:customers, list_of(:user), description: "List of room customers") do
      resolve(fn parent, _args, _resolution ->
        {:ok,
         parent
         |> Enum.filter(&(&1.role == "customer"))
         |> Enum.map(& &1.user_id)
         |> Accounts.list_users()}
      end)
    end
  end
end
