defmodule MessengerWeb.Schema.AccountsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias MessengerWeb.Resolvers

  @desc "The given object implements the receipt of data pertaining to the user's account."
  object :account do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn parent, _, _ ->
        {:ok, parent.id}
      end)
    end

    @desc "Getting the list message ID of messages notifications on which the user has not seen yet."
    field :messages_notifications, list_of(:messages_notifications) do
      resolve(&Resolvers.Accounts.get_user_messages_notifications/3)
    end
  end
end
