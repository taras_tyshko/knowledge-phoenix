defmodule Messenger.Permission do
  @moduledoc false

  alias Messenger.RoomConnections

  @type result :: :ok | :error | {:error, String.t}
  @type t :: (any, map, map -> result) | (map, map -> result)

  def in_room(_, %{room_id: room_id}, %{context: %{current_user: user}}) do
    room_id
    |> RoomConnections.get_connections()
    |> Enum.member?(user.id)
    |> case do
      true -> :ok
      false -> error_message(nil)
    end
  end

  def in_room(_, _, _), do: :error

  def is_active_account(_, _, %{context: %{current_user: user}}) do
    case user.status do
      "active" -> :ok
      "hidden" -> :ok
      _ -> error_message(nil)
    end
  end

  def is_active_account(_, _, _), do: :error

  defp error_message(nil), do: :error
  defp error_message(message), do: {:error, message}
end
