defmodule Messenger.SystemMessages do
  alias SmileDB.Messages

  def generate(arguments) when is_map(arguments) do
    arguments
    |> make_up()
    |> Messages.create_message()
    |> case do
      {:ok, message} ->
        Map.put(arguments, :system_message, message)

      {:error, _changeset} ->
        Map.delete(arguments, :system_message)
    end
  end

  defp make_up(%{
         id: room_id,
         mutation: :create_channel,
         system_message: %{current_user: user, info_source: user_ids}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "createChannel",
      description:
        (user_ids -- [user.id])
        |> Enum.map(&"user:#{&1}")
        |> Enum.join()
    }
  end

  defp make_up(%{
         id: room_id,
         mutation: :add_people_in_channel,
         system_message: %{current_user: user, info_source: user_ids}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "addPeopleInChannel",
      description:
        user_ids
        |> Enum.map(&"user:#{&1}")
        |> Enum.join()
    }
  end

  defp make_up(%{
         id: room_id,
         mutation: :delete_people_from_channel,
         system_message: %{current_user: user, info_source: user_ids}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "deletePeopleInChannel",
      description:
        user_ids
        |> Enum.map(&"user:#{&1}")
        |> Enum.join()
    }
  end

  defp make_up(%{
         id: room_id,
         mutation: :clear_messages_channel,
         system_message: %{current_user: user, info_source: desc}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "cleanChannel",
      description: desc
    }
  end

  defp make_up(%{
         id: room_id,
         mutation: :rename_channel,
         system_message: %{current_user: user, info_source: room}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "renameChannel",
      description: room.name
    }
  end

  defp make_up(%{
         id: room_id,
         mutation: :leave_channel,
         system_message: %{current_user: user, info_source: user_ids}
       }) do
    %{
      room_id: room_id,
      user_id: user.id,
      event_name: "leaveChannel",
      description:
        user_ids
        |> Enum.map(&"user:#{&1}")
        |> Enum.join()
    }
  end
end
