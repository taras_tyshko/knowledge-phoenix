defmodule Messenger.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @spec start(Elixir.Application.app(), Elixir.Application.start_type()) :: :ok | {:error, term()}
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start your own worker by calling: Messenger.Worker.start_link(arg1, arg2, arg3)
      # worker(Messenger.Worker, [arg1, arg2, arg3]),
      worker(Messenger.RoomConnections, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Messenger.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
