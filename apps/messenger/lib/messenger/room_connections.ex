defmodule Messenger.RoomConnections do
  use GenServer

  alias SmileDB.Messages

  def init(state), do: {:ok, state}

  def handle_call({:get, room_id}, _from, state) do
    value =
      case List.keyfind(state, :"#{room_id}", 0) do
        nil ->
          [room_id: room_id]
          |> Messages.list_user_rooms()
          |> Enum.map(& &1.user_id)

        {^room_id, user_ids} ->
          user_ids
      end

    {:reply, value, replace(state, room_id, value)}
  end

  def handle_call({:add, room_id, user_ids}, _from, state) do
    value =
      case List.keyfind(state, :"#{room_id}", 0) do
        nil ->
          [room_id: room_id]
          |> Messages.list_user_rooms()
          |> Enum.map(& &1.user_id)

        {^room_id, user_ids} ->
          user_ids
      end

    {:reply, user_ids,
     replace(
       state,
       room_id,
       [user_ids | value]
       |> List.flatten()
       |> Enum.uniq()
     )}
  end

  def handle_call({:remove, room_id, user_ids}, _from, state) do
    value =
      case List.keyfind(state, :"#{room_id}", 0) do
        nil ->
          [room_id: room_id]
          |> Messages.list_user_rooms()
          |> Enum.map(& &1.user_id)

        {^room_id, user_ids} ->
          user_ids
      end

    {:reply, user_ids,
     replace(
       state,
       room_id,
       (value -- List.flatten([user_ids]))
       |> Enum.uniq()
     )}
  end

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_connections(room_id) do
    GenServer.call(__MODULE__, {:get, room_id})
  end

  def connect(room_id, user_ids) do
    GenServer.call(__MODULE__, {:add, room_id, user_ids})
  end

  def disconnect(room_id, user_ids) do
    GenServer.call(__MODULE__, {:remove, room_id, user_ids})
  end

  defp replace(state, key, value) do
    List.keyreplace(state, :"#{key}", 0, {:"#{key}", value})
  end
end
