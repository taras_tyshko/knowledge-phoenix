defmodule Notifications.Providers do
  alias SmileDB.Notifications

  @callback push_function(map(), Notifications.Notification.t(), list(any())) :: any()
end
