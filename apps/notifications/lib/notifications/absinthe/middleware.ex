defmodule Notifications.Absinthe.Middleware do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias Notifications.Publish

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{state: :resolved} = resolution, args) do
    Notifications.new(%{incoming_data: resolution})
    |> Publish.resolve_input_data(:list_to_publish, args[:list_to_publish])
    |> Publish.resolve_input_data(:source, args[:source])
    |> Publish.push_function(args[:push_to])
    |> Publish.create_notification_function()
    |> Publish.resolve_notifications()
    |> Task.async()

    resolution
  end

  def call(resolution, _config), do: resolution
end
