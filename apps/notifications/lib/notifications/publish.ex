defmodule Notifications.Publish do
  def resolve_input_data(%{incoming_data: data} = notification_struct, field, source) do
    Map.put(notification_struct, :"#{field}", fn
      %{state: :unprocessed} = notification_struct ->
        try do
          %{notification_struct | "#{field}": source.(data)}
        rescue
          _ -> %{notification_struct | state: :breaked_down}
        end

      notification_struct ->
        notification_struct
    end)
  end

  def create_notification_function(
        %{
          state: :unprocessed,
          list_to_publish: create_to_function,
          source: source_function
        } = notification_struct
      )
      when is_function(create_to_function) and is_function(source_function) do
    notification_struct
    |> create_to_function.()
    |> source_function.()
    |> create_notification_function()
  end

  def create_notification_function(
        %{
          state: :unprocessed,
          list_to_publish: create_to,
          source: source
        } = notification_struct
      )
      when is_list(create_to) and is_map(source) do
    notification_struct
    |> Map.put(:value, fn ->
      Notifications.create_notifications(create_to, source)
    end)
    |> Map.put(:state, :processed)
  end

  def create_notification_function(notification_struct), do: notification_struct

  def resolve_notifications(%{
        state: :processed,
        value: value_function,
        push_to: publish_function
      }) do
    fn ->
      Enum.each(value_function.(), &publish_function.(&1))
    end
  end

  def resolve_notifications(_notification_struct), do: fn -> nil end

  def push_function(%{incoming_data: data} = notification_struct, push_to_list \\ [:default]) do
    Map.put(notification_struct, :push_to, fn object ->
      Application.get_env(:notifications, :providers, [])
      |> Enum.each(fn provider ->
        Enum.each(push_to_list, &provider.push_function(data, object, &1))
      end)
    end)
  end
end
