defmodule Notifications.Resolver do
  @moduledoc """
  This module resolve the functions for supported ecto schemas.

  The following models are supported by the module:
    [`Content`](SmileDB.Source.Content.html),
    [`Answer`](SmileDB.Source.Answer.html),
    [`Comment`](SmileDB.Source.Comment.html),
    [`AnswerRating`](SmileDB.Ratings.AnswerRating.html),
    [`CommentRating`](SmileDB.Ratings.CommentRating.html),
    [`UserSubscription`](SmileDB.Subscriptions.UserSubscription.html).
  """

  alias SmileDB.{
    Source,
    Ratings,
    Subscriptions,
    Notifications
  }

  @doc """
  Creates the appropriate type of notifications to each of users.

  It expects two parameters. The first one is the list of user`s id who will receive the notification,
  and the second is the model that is the source of the data to create notification.

  If the second argument is a structure that is not supported, the error will be raised.

  The function returns the list of composed map of the data.

  ## Examples

      iex> Notifications.create_notifications([1], %SmileDB.Source.Content{id: 2})
      [%{user_id: 1, content_id: 2}]
  """
  @spec create_notifications(list(Integer.t()), map()) :: list(map())
  def create_notifications(users_id, %Source.Content{} = content) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        content_id: content.id
      }
    )
    |> Notifications.create_content_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        shadow: content.shadow,
        anonymous: content.anonymous
      })
    )
  end

  def create_notifications(users_id, %Source.Answer{} = answer) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        answer_id: answer.id
      }
    )
    |> Notifications.create_answer_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        shadow: answer.shadow,
        anonymous: answer.anonymous
      })
    )
  end

  def create_notifications(users_id, %Source.Comment{} = comment) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        comment_id: comment.id
      }
    )
    |> Notifications.create_comment_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        shadow: comment.shadow,
        anonymous: comment.anonymous
      })
    )
  end

  def create_notifications(users_id, %Ratings.AnswerRating{} = answer_rating) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        answer_rating_id: answer_rating.id
      }
    )
    |> Notifications.create_answer_rating_notifications()
  end

  def create_notifications(users_id, %Ratings.CommentRating{} = comment_rating) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        comment_rating_id: comment_rating.id
      }
    )
    |> Notifications.create_comment_rating_notifications()
  end

  def create_notifications(users_id, %Subscriptions.UserSubscription{} = user_subscription) do
    users_id
    |> Enum.map(
      &%{
        user_id: &1,
        user_subscription_id: user_subscription.id
      }
    )
    |> Notifications.create_user_subscription_notifications()
  end

  def create_notifications(_users_id, object),
    do: raise("#{object.__struct__} type does not supported Notifications module.")

  @doc """
  Deletes the notifications from the database storage.

  It expects two parameters. The first one is a type of notifications
  and the second is the list of notifications id that will be deleted.

  If the first argument is a structure that is not supported, the error will be raised.

  The function returns the list of composed map of the data with the updated `read` as true.

  ## Examples

      iex> Notifications.delete_notifications(SmileDB.Source.Content, [1])
      [%{content_id: 1, read: true}]
  """
  @spec delete_notifications(atom(), list(Integer.t())) :: list(map())
  def delete_notifications(Notifications.ContentNotification, list_content_notifications_id) do
    list_content_notifications_id
    |> Notifications.delete_content_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(Notifications.AnswerNotification, list_answer_notifications_id) do
    list_answer_notifications_id
    |> Notifications.delete_answer_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(Notifications.CommentNotification, list_comment_notifications_id) do
    list_comment_notifications_id
    |> Notifications.delete_comment_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(
        Notifications.AnswerRatingNotification,
        list_answer_rating_notifications_id
      ) do
    list_answer_rating_notifications_id
    |> Notifications.delete_answer_rating_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(
        Notifications.CommentRatingNotification,
        list_comment_rating_notifications_id
      ) do
    list_comment_rating_notifications_id
    |> Notifications.delete_comment_rating_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(
        Notifications.UserSubscriptionNotification,
        list_user_subscription_notifications_id
      ) do
    list_user_subscription_notifications_id
    |> Notifications.delete_user_subscription_notifications()
    |> Enum.map(
      &Map.merge(&1, %{
        read: true
      })
    )
  end

  def delete_notifications(schema, _list_notifications_id),
    do: raise("#{schema} type does not supported Notifications module.")
end
