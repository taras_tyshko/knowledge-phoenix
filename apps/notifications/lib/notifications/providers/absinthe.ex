defmodule Notifications.Providers.Absinthe do
  @behaviour Notifications.Providers

  def push_function(%{context: %{pubsub: pubsub}}, object, push_types) do
    publish_notification(push_types, object, pubsub)
  end

  defp publish_notification(
         :notifications,
         %{shadow: false, anonymous: false, user_id: user_id} = notification,
         pubsub
       ) do
    Absinthe.Subscription.publish(
      pubsub,
      notification,
      account_notifications: "account:#{user_id}"
    )
  end

  defp publish_notification(
         :account,
         %{shadow: false, anonymous: false, user_id: user_id},
         pubsub
       ) do
    Absinthe.Subscription.publish(
      pubsub,
      %{id: user_id, notifications: nil},
      account_info: "account:#{user_id}"
    )
  end

  defp publish_notification(:messages, %{user_id: user_id} = notification, pubsub) do
    Absinthe.Subscription.publish(
      pubsub,
      notification,
      messages_notifications: "notifications:#{user_id}"
    )
  end

  defp publish_notification(:messenger_account, %{user_id: user_id} = notification, pubsub) do
    Absinthe.Subscription.publish(
      pubsub,
      %{id: user_id, message_notifications: notification},
      account_info: "account:#{user_id}"
    )
  end

  defp publish_notification(_type, _notification, _pubsub), do: :ok
end
