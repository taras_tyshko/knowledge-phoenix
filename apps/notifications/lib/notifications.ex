defmodule Notifications do
  @moduledoc """
  Documentation for Notifications.
  """

  @type t :: %__MODULE__{
          incoming_data: any(),
          list_to_publish: function(),
          push_to: function(),
          source: function(),
          state: field_state,
          value: function()
        }
  defstruct(
    incoming_data: nil,
    list_to_publish: nil,
    push_to: nil,
    source: nil,
    state: :unprocessed,
    value: nil
  )

  use ExConstructor

  alias SmileDB.Repo
  alias SmileDB.Notifications, as: NotificationsContext

  alias Notifications.Resolver

  @type field_state :: :unprocessed | :processed | :breaked_down

  @spec create_notifications(list(Integer.t()), map()) ::
          list(NotificationsContext.Notification.t())
  def create_notifications(users_id, object) do
    users_id
    |> Resolver.create_notifications(object)
    |> Enum.map(&put_notifications(&1))
  end

  @doc """
  Deletes a [`Notification`](SmileDB.Notifications.Notification.html) and set the status unread for the elasticsearch document.

  It expects a list of binary id

  ## Examples

      iex> Notifications.delete_notifications([id])
      %Notification{}

  """
  @spec delete_notifications(list(String.t())) :: list(NotificationsContext.Notification.t())
  def delete_notifications(list_binary_id) when is_list(list_binary_id) do
    list_binary_id
    |> Enum.map(&Repo.decode(&1))
    |> Enum.group_by(& &1["schema"], & &1["id"])
    |> Enum.flat_map(fn {schema, list_notifications_id} ->
      schema
      |> String.to_existing_atom()
      |> Resolver.delete_notifications(list_notifications_id)
    end)
    |> Enum.map(&put_notifications(&1))
  end

  @spec put_notifications(map()) :: NotificationsContext.Notification.t()
  def put_notifications(notification) do
    NotificationsContext.put_notifications(notification)
  end
end
