defmodule Notifications.ResolverTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias Notifications.Resolver
  alias SmileDB.Test.Default, as: DefaultSchema
  alias SmileDB.{Ratings, Subscriptions, Notifications}

  describe "create_notifications/2" do
    setup [:create_comment]

    test "create_notifications for SmileDB.Source.Content schema.", %{
      user: user,
      content: content
    } do
      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            anonymous: content.anonymous,
            content_id: content.id,
            read: false,
            shadow: content.shadow,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], content)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         content_notification ->
          %{
            anonymous: content_notification.anonymous,
            content_id: content_notification.content_id,
            read: content_notification.read,
            shadow: content_notification.shadow,
            user_id: content_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for SmileDB.Source.Answer schema.", %{
      user: user,
      answer: answer
    } do
      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            anonymous: answer.anonymous,
            answer_id: answer.id,
            read: false,
            shadow: answer.shadow,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], answer)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         answer_notification ->
          %{
            anonymous: answer_notification.anonymous,
            answer_id: answer_notification.answer_id,
            read: answer_notification.read,
            shadow: answer_notification.shadow,
            user_id: answer_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for SmileDB.Source.Comment schema.", %{
      user: user,
      comment: comment
    } do
      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            anonymous: comment.anonymous,
            comment_id: comment.id,
            read: false,
            shadow: comment.shadow,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], comment)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         comment_notification ->
          %{
            anonymous: comment_notification.anonymous,
            comment_id: comment_notification.comment_id,
            read: comment_notification.read,
            shadow: comment_notification.shadow,
            user_id: comment_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for SmileDB.Ratings.AnswerRating schema.", %{
      user: user,
      answer: answer
    } do
      {:ok, answer_rating} =
        Ratings.create_answer_rating(%Ratings.AnswerRating{}, %{
          answer_id: answer.id,
          status: true,
          user_id: user.id
        })

      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            answer_rating_id: answer_rating.id,
            read: false,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], answer_rating)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         answer_rating_notification ->
          %{
            answer_rating_id: answer_rating_notification.answer_rating_id,
            read: answer_rating_notification.read,
            user_id: answer_rating_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for SmileDB.Ratings.CommentRating schema.", %{
      user: user,
      comment: comment
    } do
      {:ok, comment_rating} =
        Ratings.create_comment_rating(%Ratings.CommentRating{}, %{
          comment_id: comment.id,
          status: true,
          user_id: user.id
        })

      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            comment_rating_id: comment_rating.id,
            read: false,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], comment_rating)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         comment_rating_notification ->
          %{
            comment_rating_id: comment_rating_notification.comment_rating_id,
            read: comment_rating_notification.read,
            user_id: comment_rating_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for SmileDB.Subscriptions.UserSubscription schema.", %{
      user: user
    } do
      {:ok, user_subscription} =
        Subscriptions.create_user_subscription(%Subscriptions.UserSubscription{}, %{
          follower_id: user.id,
          status: true,
          user_id: user.id
        })

      expected_data =
        Enum.map([user.id], fn user_id ->
          %{
            user_subscription_id: user_subscription.id,
            read: false,
            user_id: user_id
          }
        end)

      reality_data =
        Resolver.create_notifications([user.id], user_subscription)
        |> Enum.map(fn %{id: _id, inserted_at: _inserted_at, updated_at: _updated_at} =
                         user_subscription_notification ->
          %{
            user_subscription_id: user_subscription_notification.user_subscription_id,
            read: user_subscription_notification.read,
            user_id: user_subscription_notification.user_id
          }
        end)

      assert expected_data == reality_data
    end

    test "create_notifications for unsupported type of schema." do
      invalid_schema = %DefaultSchema{}

      assert_raise RuntimeError,
                   "#{invalid_schema.__struct__()} type does not supported Notifications module.",
                   fn ->
                     Resolver.create_notifications([], invalid_schema)
                   end
    end
  end

  describe "delete_notifications/2" do
    setup [:create_comment]

    test "delete_notifications for SmileDB.Source.Content schema.", %{
      user: user,
      content: content
    } do
      list_content_notifications =
        Notifications.create_content_notifications([
          %{user_id: user.id, content_id: content.id}
        ])

      expected_data = Enum.map(list_content_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.ContentNotification,
          Enum.map(list_content_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for SmileDB.Source.Answer schema.", %{
      user: user,
      answer: answer
    } do
      list_answer_notifications =
        Notifications.create_answer_notifications([
          %{user_id: user.id, answer_id: answer.id}
        ])

      expected_data = Enum.map(list_answer_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.AnswerNotification,
          Enum.map(list_answer_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for SmileDB.Source.Comment schema.", %{
      user: user,
      comment: comment
    } do
      list_comment_notifications =
        Notifications.create_comment_notifications([
          %{user_id: user.id, comment_id: comment.id}
        ])

      expected_data = Enum.map(list_comment_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.CommentNotification,
          Enum.map(list_comment_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for SmileDB.Ratings.AnswerRating schema.", %{
      user: user,
      answer: answer
    } do
      {:ok, answer_rating} =
        Ratings.create_answer_rating(%Ratings.AnswerRating{}, %{
          answer_id: answer.id,
          status: true,
          user_id: user.id
        })

      list_answer_rating_notifications =
        Notifications.create_answer_rating_notifications([
          %{user_id: user.id, answer_rating_id: answer_rating.id}
        ])

      expected_data = Enum.map(list_answer_rating_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.AnswerRatingNotification,
          Enum.map(list_answer_rating_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for SmileDB.Ratings.CommentRating schema.", %{
      user: user,
      comment: comment
    } do
      {:ok, comment_rating} =
        Ratings.create_comment_rating(%Ratings.CommentRating{}, %{
          comment_id: comment.id,
          status: true,
          user_id: user.id
        })

      list_comment_rating_notifications =
        Notifications.create_comment_rating_notifications([
          %{user_id: user.id, comment_rating_id: comment_rating.id}
        ])

      expected_data = Enum.map(list_comment_rating_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.CommentRatingNotification,
          Enum.map(list_comment_rating_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for SmileDB.Subscriptions.UserSubscription schema.", %{
      user: user
    } do
      {:ok, user_subscription} =
        Subscriptions.create_user_subscription(%Subscriptions.UserSubscription{}, %{
          follower_id: user.id,
          status: true,
          user_id: user.id
        })

      list_user_subscription_notifications =
        Notifications.create_user_subscription_notifications([
          %{user_id: user.id, user_subscription_id: user_subscription.id}
        ])

      expected_data = Enum.map(list_user_subscription_notifications, &Map.put(&1, :read, true))

      reality_data =
        Resolver.delete_notifications(
          Notifications.UserSubscriptionNotification,
          Enum.map(list_user_subscription_notifications, & &1.id)
        )

      assert expected_data == reality_data
    end

    test "delete_notifications for unsupported type of schema." do
      invalid_schema = DefaultSchema

      assert_raise RuntimeError,
                   "#{invalid_schema} type does not supported Notifications module.",
                   fn ->
                     Resolver.delete_notifications(invalid_schema, [])
                   end
    end
  end
end
