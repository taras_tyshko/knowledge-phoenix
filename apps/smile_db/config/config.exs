# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :smile_db,
  namespace: SmileDB,
  ecto_repos: [SmileDB.Repo]

config :smile_db, :auth,
  facebook: [
    token_url: "https://graph.facebook.com/v3.1/oauth/access_token",
    get_info_url:
      "https://graph.facebook.com/me?fields=email,last_name,first_name,birthday,gender,location"
  ],
  google: [
    token_url: "https://accounts.google.com/o/oauth2/token",
    get_info_url: "https://www.googleapis.com/userinfo/v2/me"
  ]

config :oauth2,
  warn_missing_serializer: false,
  serializers: %{
    "application/vnd.api+json" => Jason
  }

# Guardian configuration
config :smile_db, SmileDB.Auth.Guardian,
  issuer: "Smile",
  secret_key: "Kz46muBJNwAgmEhhfFAGDPPzuodSm+dv55flblKyRcxDEse04LPCMgMCDLf3jE4W",
  allowed_algos: ["HS512"],
  verify_issuer: true,
  ttl: {30, :days},
  allowed_drift: 2000

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :files_uploader, :scope,
  users: "users",
  rooms: "rooms",
  offices: "offices",
  achieves: "achieves",
  contents: "users/@user_id/contents",
  answers: "users/@user_id/contents/@content_id/answers",
  comments: "users/@user_id/contents/@content_id/answers/@answer_id/comments",
  messages: "rooms/@room_id/messages"

config :elastic_search,
  indexes: [
    SmileDB.Source.Feed,
    SmileDB.Source.Content,
    SmileDB.Messages.Message,
    SmileDB.Source.Comment,
    SmileDB.Source.Answer,
    SmileDB.Accounts.User,
    SmileDB.Messages.Room,
    SmileDB.Messages.UserRoom,
    SmileDB.Tags.ContentTag,
    SmileDB.Tags.Tag,
    SmileDB.Contents.Meto,
    SmileDB.Users.Expert,
    SmileDB.Ratings.UserRating,
    SmileDB.Ratings.AnswerRating,
    SmileDB.Ratings.CommentRating,
    SmileDB.Ratings.OfficeRating,
    SmileDB.Notifications.Notification,
    SmileDB.Achievements.Achieve,
    SmileDB.Achievements.UserAchieve,
    SmileDB.Permissions.Permission,
    SmileDB.Permissions.RoleAccess,
    SmileDB.Category.Office
  ]

config :smile_db, :content_user,
  secret_key: "Kz46muBJNwAgmEhhfFAGDPPzuodSm+dv55flblKyRcxDEse04LPCMgMCDLf3jE4W"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
