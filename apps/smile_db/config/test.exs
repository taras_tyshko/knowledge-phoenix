use Mix.Config

# Print only warnings and errors during test
config :logger, level: :warn

config :files_uploader,
  path: "/Users/tatys/Project/knowledge-phoenix/uploads"

# Configure your database
config :smile_db, SmileDB.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "phoenix",
  password: "phoenix",
  database: "smile_db_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  types: SmileDB.PostgresTypes
