defmodule SmileDBWeb.Middlewares.HandleChangesetErrors do
  @moduledoc """
    This module is used to process errors that are played inside and output to a single format for further analysis.

    The module uses the documentation information. [`Absinthe.Middleware`](https://hexdocs.pm/absinthe/Absinthe.Middleware.html#content)

    This module analyzes the input data of the first argument. 
    If it has received a bug from the database then the error will be a iteration and output error of the desired format.
    To operate the module, it is necessary to add it to the call function middleware in [`Schema`](SmileDB.Graphql.html)
  """

  @behaviour Absinthe.Middleware

  @doc """
    This is the main middleware callback.

    It receives an `Absinthe.Resolution` struct and it needs to return an `Absinthe.Resolution` struct.
    The second argument will be whatever value was passed to the middleware call that setup the middleware.

    Callback implementation for `Absinthe.Middleware.call/2`.

  ## Example

      @behaviour Absinthe.Middleware
      
      def call(resolution, _config) do
        Absinthe.Resolution.put_result(
          resolution,
          {:error, Enum.flat_map(resolution.errors, &handle_error/1)}
        )
      end
     
  """
  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(resolution, _config) do
    Absinthe.Resolution.put_result(
      resolution,
      {:error, Enum.flat_map(resolution.errors, &handle_error/1)}
    )
  end

  @spec handle_error(Ecto.Changeset) :: map()
  defp handle_error(%Ecto.Changeset{} = changeset) do
    changeset
    |> Ecto.Changeset.traverse_errors(fn {err, _opts} -> err end)
    |> Enum.map(fn {k, v} -> "#{k}: #{v}" end)
  end

  defp handle_error(error), do: [error]
end
