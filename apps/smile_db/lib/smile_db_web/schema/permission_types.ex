defmodule SmileDBWeb.Schema.PermissionTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.Permissions

  @desc "In this object are the fields from the model User."
  object :role do
    field(:role, :string, description: "Unique identifier name of the Role.")
    field(:access_code, :integer, description: "The access code which contains a user account.")
    field(:inserted_at, :datetime, description: "User creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    @desc "Gets a list permission in user by role."
    field(:list_permissions, list_of(:string)) do
      resolve(fn parent, _, _ ->
        Permissions.list_permissions(parent.role)
      end)
    end
  end

  object :permission do
    field(:id, :id, description: "Unique identifier of the Permission.")
    field(:name, :string, description: "Name of the Permission.")
    field(:inserted_at, :datetime, description: "Permission creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end

  object :role_access do
    field(:id, :id, description: "Unique identifier of the RoleAccess.")
    field(:role, :string, description: "Role name of the RoleAccess.")
    field(:inserted_at, :datetime, description: "RoleAccess creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    field(:permission, :permission,
      resolve: dataloader(Permissions),
      description: "Get a permission with a RoleAccess."
    )
  end

  object :permission_access do
    field(:roles, list_of(:role), description: "List of the Roles.")
    field(:permissions, list_of(:permission), description: "List of the Permission.")
  end
end
