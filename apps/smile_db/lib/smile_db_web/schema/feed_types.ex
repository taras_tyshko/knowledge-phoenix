defmodule SmileDBWeb.Schema.FeedTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.Source

  union :feed_union do
    types([:feed_question, :feed_complaint, :feed_comment, :feed_answer])

    resolve_type(fn
      %Source.Feed{answer_id: nil, comment_id: nil, type: "question"}, _ -> :feed_question
      %Source.Feed{answer_id: nil, comment_id: nil, type: "complaint"}, _ -> :feed_complaint
      %Source.Feed{answer_id: nil, comment_id: _id}, _ -> :feed_comment
      %Source.Feed{answer_id: _id, comment_id: nil}, _ -> :feed_answer
    end)
  end

  object :feed_question do
    @desc "Get the list ID of the contents on which user subscribed."
    field :question, :question do
      resolve(fn
        %Source.Feed{} = feed, _, _ ->
          {:ok, Source.get_content!(feed.content_id)}
      end)
    end
  end

  object :feed_complaint do
    @desc "Get the list ID of the contents on which user subscribed."
    field :complaint, :complaint do
      resolve(fn
        %Source.Feed{} = feed, _, _ ->
          {:ok, Source.get_content!(feed.content_id)}
      end)
    end
  end

  object :feed_answer do
    @desc "Get the list ID of the answers on which user subscribed."
    field :answer, :answer do
      resolve(fn
        %Source.Feed{} = feed, _, _ ->
          {:ok, Source.get_answer!(feed.answer_id)}
      end)
    end
  end

  object :feed_comment do
    @desc "Get the list ID of the comments on which user subscribed."
    field :comment, :comment do
      resolve(fn
        %Source.Feed{} = feed, _, _ ->
          {:ok, Source.get_comment!(feed.comment_id)}
      end)
    end
  end
end
