defmodule SmileDBWeb.Schema.SubscriptionTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.Accounts

  @desc "In this object are the fields from the model UserSubscription."
  object :user_subscription do
    field(:id, :id, description: "Unique identifier of the UserSubscription.")
    field(:status, :boolean, description: "true -> subscribed, false -> unsubscribed.")
    field(:user, :user, resolve: dataloader(Accounts))
    field(:follower, :user, resolve: dataloader(Accounts))
    field(:inserted_at, :datetime, description: "Creation Date.")
    field(:updated_at, :datetime, description: "Last Update date.")
  end
end
