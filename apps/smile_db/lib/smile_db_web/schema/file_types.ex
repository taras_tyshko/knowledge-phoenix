defmodule SmileDBWeb.Schema.FileTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  import_types(Absinthe.Plug.Types)

  input_object :file_input do
    field(:add, non_null(list_of(:upload)), description: "Field that accepts uploaded file.")
    field(:remove, non_null(list_of(:id)), description: "Unique identifier of file for remove.")
  end

  @desc "It describes the fields available for uploaded files."
  object :file do
    field(:id, :string, description: "Unique identifier of the File.")
    field(:name, :string, description: "Name of the File.")

    field(
      :extension,
      :string,
      description: "It`s an identifier specified as a suffix to the name of the file."
    )

    field(:url, :string, description: "Url to access the file.") do
      resolve(fn %{path: path}, _args, %{context: %{pubsub: endpoint}} ->
        static_path = Application.get_env(:files_uploader, :path)
        {:ok, String.replace(path || static_path, static_path, endpoint.static_url())}
      end)
    end

    field(:inserted_at, :datetime, description: "File creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end
end
