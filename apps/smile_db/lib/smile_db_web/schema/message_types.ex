defmodule SmileDBWeb.Schema.MessageTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.{Repo, Source, Accounts, Messages}

  @desc "In this object are the fields from the model Message."
  object :message do
    field(:id, :id, description: "Unique identifier of the Message.")

    field(:description, :description) do
      resolve(fn %{description: description, event_name: event_name},
                 _,
                 %{context: %{pubsub: endpoint}} ->
        {:ok,
         Source.render_nicknames_from_description(%{
           description: FilesUploader.url(description, endpoint.static_url()),
           event_name: event_name
         })}
      end)
    end

    field(:inserted_at, :string, description: "Message creation Date.")
    field(:updated_at, :string, description: "Last update date.")

    field(
      :event_name,
      :string,
      description:
        "Displays the type of system message.Could be: ['createChannel', 'renameChannel', 'cleanChannel', 'addPeopleInChannel', 'deletePeopleInChannel', 'leaveChannel']."
    )

    field(
      :room,
      :room_interface,
      resolve: dataloader(Messages),
      description: "Get a Room with a Message."
    )

    field(
      :user,
      :user,
      resolve: dataloader(Accounts),
      description: "Get the User who created Message."
    )
  end

  @desc "Implements the receipt of the structure messages notifications."
  object :messages_notifications do
    field(:id, :string, description: "Unique identifier of the messages notifications.") do
      resolve(fn message_notification, _args, _resolution ->
        {:ok, Repo.replace_id(message_notification).id}
      end)
    end

    field(:room_id, :integer, description: "Unique identifier of the Room.")

    @desc "In this object are the fields from the model Message."
    field(
      :message,
      :message,
      resolve: dataloader(Messages),
      description: "Get a Room with a Message."
    )
  end
end
