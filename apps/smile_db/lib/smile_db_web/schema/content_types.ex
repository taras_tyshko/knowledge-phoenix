defmodule SmileDBWeb.Schema.ContentTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1, batch: 3]

  alias SmileDB.{Accounts, Source, Contents, Ratings, Category}

  @desc "Type for absinthe which implements decoding and correct recording of the path of commentary."
  scalar :ltree, name: "Ltree" do
    serialize(&EctoLtree.LabelTree.decode/1)
    parse(&EctoLtree.LabelTree.cast/1)
  end

  @desc "In this object are the fields from the model Content."
  object :complaint do
    field(:id, :id, description: "Unique identifier of the Content.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
    field(:answers_count, :integer, description: "Number of answers count.")
    field(:keywords, :string, description: "Keywords for Content.")
    field(:status, :string, description: "Status for Content.")
    field(:requests, :integer, description: "Number of requests count.")
    field(:reviews, list_of(:integer), description: "List users ID`s who a saw this Content.")
    field(:reviews_count, :integer, description: "Number of reviews count.")
    field(:comments_count, :integer, description: "Number of comments count.")
    field(:shadow, :boolean, description: "shadow -> true, not shadow -> false.")

    field(:files, list_of(:file), description: "Information about files.") do
      resolve(fn %{
                   files_visible: visible,
                   files: files,
                   user_id: user_id,
                   permission_tags: permission_tags
                 },
                 _args,
                 %{context: %{current_user: user}} ->
        (!visible ||
           (visible && user &&
              (user.id == user_id || Enum.all?(permission_tags, &Enum.member?(user.expert, &1)))))
        |> if do
          {:ok, files}
        else
          {:ok, nil}
        end
      end)
    end

    field(
      :files_visible,
      :boolean,
      description: "Files-Visible -> true, not Files-Visible -> false."
    )

    field(:tags, list_of(:string), description: "List content tags.")
    field(:permission_tags, list_of(:string), description: "List content permission tags.")
    field(:title, :string, description: "Title content.")

    field(:rating, :integer, description: "Content rating.") do
      resolve(fn parent, _, _ ->
        {:ok, Ratings.get_office_rating_by!(parent).score}
      end)
    end

    field(:description, :description, description: "Content description.") do
      resolve(fn %{description: description}, _, %{context: %{pubsub: endpoint}} ->
        {:ok,
         Source.render_nicknames_from_description(%{
           description: FilesUploader.url(description, endpoint.static_url())
         })}
      end)
    end

    field(:inserted_at, :datetime, description: "Content creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    field(:user, :user, description: "Get the user who created Content.") do
      resolve(fn parent, args, resolution ->
        parent = get_parent(parent, resolution.context)
        dataloader(Accounts).(parent, args, resolution)
      end)
    end

    @desc "List of answer ID`s current Content."
    field :answers_id, list_of(:integer) do
      resolve(fn parent, _, %{context: %{current_user: user}} ->
        {:ok, Source.list_answers_id!(parent.id, user)}
      end)
    end

    @desc "The list of users who had similar contents."
    field(:me_to, list_of(:user)) do
      resolve(fn parent, _args, _resolution ->
        {:ok,
         parent.id
         |> Contents.list_me_to()
         |> Enum.map(&ElasticSearch.Repo.get!(Accounts.User, &1))}
      end)
    end

    field(
      :office,
      :office,
      resolve: dataloader(Category),
      description: "Get a content with a Office."
    )
  end

  @desc "In this object are the fields from the model Content."
  object :question do
    field(:id, :id, description: "Unique identifier of the Question.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
    field(:answers_count, :integer, description: "Number of answers count.")
    field(:keywords, :string, description: "Keywords for Content.")
    field(:status, :string, description: "Status for Content.")
    field(:requests, :integer, description: "Number of requests count.")
    field(:reviews, list_of(:integer), description: "List users ID`s who a saw this Question.")
    field(:reviews_count, :integer, description: "Number of reviews count.")
    field(:shadow, :boolean, description: "shadow -> true, not shadow -> false.")
    field(:tags, list_of(:string), description: "List content tags.")
    field(:permission_tags, list_of(:string), description: "List content permission tags.")
    field(:title, :string, description: "Title content.")

    field(:description, :description, description: "Content description.") do
      resolve(fn %{description: description}, _, %{context: %{pubsub: endpoint}} ->
        {:ok,
         Source.render_nicknames_from_description(%{
           description: FilesUploader.url(description, endpoint.static_url())
         })}
      end)
    end

    field(:inserted_at, :datetime, description: "Content creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    field(:user, :user, description: "Get the user who created Content.") do
      resolve(fn parent, args, resolution ->
        parent = get_parent(parent, resolution.context)
        dataloader(Accounts).(parent, args, resolution)
      end)
    end

    @desc "List of answer ID`s current Content."
    field :answers_id, list_of(:integer) do
      resolve(fn
        parent, _, %{context: %{current_user: user}} ->
          {:ok, Source.list_answers_id!(parent.id, user)}

        parent, _, %{context: _context} ->
          {:ok, Source.list_answers_id!(parent.id, nil)}
      end)
    end

    @desc "The list of users who had similar contents."
    field(:me_to, list_of(:user)) do
      resolve(fn parent, _args, _resolution ->
        {:ok,
         parent.id
         |> Contents.list_me_to()
         |> Enum.map(&ElasticSearch.Repo.get!(Accounts.User, &1))}
      end)
    end
  end

  @desc "In this object are the fields from the model Answer."
  object :answer do
    field(:id, :id, description: "Unique identifier of the Answer.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
    field(:shadow, :boolean, description: "shadow -> true, not shadow -> false.")
    field(:comments_count, :integer, description: "Number of comment count.")
    field(:keywords, :string, description: "Keywords for Answer.")
    field(:ratings_count, :integer, description: "Number of ratings delivered by users.")
    field(:tags, list_of(:string), description: "List answer tags.")

    field(:description, :description, description: "Content description.") do
      resolve(fn %{description: description}, _, %{context: %{pubsub: endpoint}} ->
        {:ok,
         Source.render_nicknames_from_description(%{
           description: FilesUploader.url(description, endpoint.static_url())
         })}
      end)
    end

    field(:inserted_at, :datetime, description: "Answer creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    field(
      :content,
      :contents_union,
      resolve: dataloader(Source),
      description: "Get a content with a Comment."
    )

    field(:user, :user, description: "Get the user who created Answer.") do
      resolve(fn parent, args, resolution ->
        parent = get_parent(parent, resolution.context)
        dataloader(Accounts).(parent, args, resolution)
      end)
    end

    field :ratings, :ratings do
      resolve(fn answer, _args, _resolution ->
        batch({Ratings, :get_answers_rating}, answer.id, fn batch_results ->
          ratings =
            with ratings <- Map.get(batch_results, answer.id),
                 true <- not is_nil(ratings) do
              %{
                likes: Enum.filter(ratings, &(&1.status == true)) |> Enum.map(& &1.user_id),
                dislikes: Enum.filter(ratings, &(&1.status == false)) |> Enum.map(& &1.user_id)
              }
            else
              false -> %{likes: [], dislikes: []}
            end

          {:ok, ratings}
        end)
      end)
    end
  end

  @desc "In this object are the fields from the model Comment."
  object :comment do
    field(:id, :id, description: "Unique identifier of the Comment.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
    field(:shadow, :boolean, description: "shadow -> true, not shadow -> false.")
    field(:keywords, :string, description: "Keywords for Comment.")
    field(:path, :ltree, description: "Level of commentary.")
    field(:ratings_count, :integer, description: "Number of ratings delivered by users.")

    field(:parent, :comment, description: "Get parent comments.") do
      resolve(fn comment, _args, _resolution ->
        data =
          with %{path: %{labels: path}} <- comment,
               comments <- List.delete(path, "#{comment.id}"),
               comment_id when not is_nil(comment_id) <- List.last(comments) do
            Source.get_comment!(comment_id)
          end

        {:ok, data}
      end)
    end

    field(:description, :description, description: "Content description.") do
      resolve(fn %{description: description}, _, %{context: %{pubsub: endpoint}} ->
        {:ok,
         Source.render_nicknames_from_description(%{
           description: FilesUploader.url(description, endpoint.static_url())
         })}
      end)
    end

    field(:inserted_at, :datetime, description: "Comment creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")

    field(
      :answer,
      :answer,
      resolve: dataloader(Source),
      description: "Get a answer with a comment."
    )

    field(
      :content,
      :contents_union,
      resolve: dataloader(Source),
      description: "Get a content with a Comment."
    )

    field(:user, :user, description: "Get the user who created Comment.") do
      resolve(fn parent, args, resolution ->
        parent = get_parent(parent, resolution.context)
        dataloader(Accounts).(parent, args, resolution)
      end)
    end

    field :ratings, :ratings do
      resolve(fn comment, _args, _resolution ->
        batch({Ratings, :get_comments_rating}, comment.id, fn batch_results ->
          ratings =
            with ratings <- Map.get(batch_results, comment.id),
                 true <- not is_nil(ratings) do
              %{
                likes: Enum.filter(ratings, &(&1.status == true)) |> Enum.map(& &1.user_id),
                dislikes: Enum.filter(ratings, &(&1.status == false)) |> Enum.map(& &1.user_id)
              }
            else
              false -> %{likes: [], dislikes: []}
            end

          {:ok, ratings}
        end)
      end)
    end
  end

  @desc "Implements the field structure for feed data expansion."
  object :feed do
    field(:answer, :answer, resolve: dataloader(Content), description: "Getting answer data.")
    field(:comment, :comment, resolve: dataloader(Content), description: "Getting comment data.")

    field(:content, :contents_union, description: "Getting content data.") do
      resolve(fn parent, args, resolution ->
        parent =
          (parent.answer_id || parent.comment_id)
          |> if(do: Map.put(parent, :content_id, nil), else: parent)

        dataloader(Source).(parent, args, resolution)
      end)
    end
  end

  @desc "Getting the description of the desired structure."
  object :description do
    field(:desc, :string, description: "Content description.")
    field(:users, list_of(:user), description: "List гsers who were mentioned.")
  end

  @desc "This object displays the fields that are available in it."
  object :related_contents do
    field(:related_content_id, :integer, description: "Unique identifier of the Content.")
    field(:content_id, :integer, description: "Unique identifier of the Content.")
    field(:inserted_at, :datetime, description: "User creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end

  defp get_parent(parent, %{current_user: user}) do
    (!parent.anonymous || (parent.anonymous && user && user.id == parent.user_id))
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end

  defp get_parent(parent, %{}) do
    (!parent.anonymous || parent.anonymous)
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end

  union :contents_union do
    description("Content types.")

    types([:question, :complaint])

    resolve_type(fn
      %Source.Content{type: "question"}, _ -> :question
      %Source.Content{type: "complaint"}, _ -> :complaint
    end)
  end

  input_object :question_input do
    field(:title, :string, description: "Title content.")
    field(:description, :string, description: "Content description.")
    field(:tags, list_of(:string), description: "List content tags.")
    field(:files_visible, :boolean, description: "visible -> true, not visible -> false.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
  end

  input_object :complaint_input do
    field(:title, :string, description: "Title content.")
    field(:description, :string, description: "Content description.")
    field(:files, :file_input, description: "List of files for add or remove.")
    field(:tags, list_of(:string), description: "List content tags.")
    field(:office, :string, description: "Unique identifier of the Office.")
    field(:files_visible, :boolean, description: "visible -> true, not visible -> false.")
    field(:rating, :integer, description: "Unique identifier of the OfficeRating.")
    field(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")
  end
end
