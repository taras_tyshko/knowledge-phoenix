defmodule SmileDBWeb.Schema.AccountTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.{
    Accounts,
    Subscriptions,
    Notifications
  }

  interface :account_interface do
    field(:id, :integer)

    resolve_type(fn
      %Accounts.Setting{}, _ -> :account_setting
      %{subscribed_contents: _}, _ -> :account_content_subscription
      %{subscribed_offices: _}, _ -> :account_office_subscription
      %{subscribed_users: _}, _ -> :account_user_subscription
      %{subscribed_tags: _}, _ -> :account_tag_subscription
      %{notifications: _}, _ -> :account_notifications
      %{message_notifications: _}, _ -> :messenger_notifications
      %{followers: _}, _ -> :account_followers
      %Accounts.User{}, _ -> :account_user
    end)
  end

  object :account_setting do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :setting, :setting do
      resolve(fn setting, _, _ ->
        {:ok, setting}
      end)
    end

    interface(:account_interface)
  end

  object :account_user do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :user, :user do
      resolve(fn user, _, _ ->
        {:ok, user}
      end)
    end

    interface(:account_interface)
  end

  object :account_content_subscription do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the content on which user subscribed."
    field :subscribed_contents, list_of(:integer) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Subscriptions.list_contents_subscription(id)}
      end)
    end

    interface(:account_interface)
  end

  object :account_office_subscription do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list of the offices on which user subscribed."
    field :subscribed_offices, list_of(:string) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Subscriptions.list_offices_subscription(id)}
      end)
    end

    interface(:account_interface)
  end

  object :account_user_subscription do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list of the users on which user subscribed."
    field :subscribed_users, list_of(:user) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Enum.map(Subscriptions.list_users_subscription(id), &Accounts.get_user!(&1))}
      end)
    end

    interface(:account_interface)
  end

  object :account_tag_subscription do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list of the tags on which user subscribed."
    field :subscribed_tags, list_of(:string) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Subscriptions.list_tags_subscription(id)}
      end)
    end

    interface(:account_interface)
  end

  object :account_notifications do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the user`s notifications."
    field :notifications, list_of(:string) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Notifications.list_notifications_by_user_id(id)}
      end)
    end

    interface(:account_interface)
  end

  object :messenger_notifications do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the user`s notifications."
    field :messenger_notifications, list_of(:string) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok,
         id
         |> Notifications.get_message_notification()
         |> Enum.map(& &1.id)}
      end)
    end

    interface(:account_interface)
  end

  object :account_followers do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the list ID of the followers on which user subscribed."
    field :followers, list_of(:integer) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Subscriptions.list_user_id_users_subscription(id)}
      end)
    end

    interface(:account_interface)
  end

  @desc "Get current avatar from social network."
  object :provider_avatar do
    field(:google, :string, description: "Import user avatar from Google.")
    field(:facebook, :string, description: "Import user avatar from Facebook.")
  end
end
