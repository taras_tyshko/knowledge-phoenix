defmodule SmileDBWeb.Schema.TagTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "In this object are the fields from the model Tag."
  object :tag do
    field(:id, :id, description: "Unique identifier of the Tag.")
    field(:count, :integer, description: "Number of same tags.")
    field(:title, :string, description: "Name Tag.")
    field(:inserted_at, :datetime, description: "Creation Date.")
    field(:updated_at, :datetime, description: "Last Update date.")
  end
end
