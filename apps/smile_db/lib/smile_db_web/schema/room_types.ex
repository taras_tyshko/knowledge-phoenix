defmodule SmileDBWeb.Schema.RoomTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.Messages

  interface :room_interface do
    field(:id, :integer)
    field(:avatar, :string)
    field(:updated_at, :datetime)
    field(:inserted_at, :datetime)
    field(:name, :string)
    field(:message, :message)

    resolve_type(fn
      %Messages.Room{type: "privat"}, _ -> :privat_room
      %Messages.Room{type: "public"}, _ -> :public_room
    end)
  end

  object :privat_room do
    field(:id, :integer, description: "Unique identifier of the Room.")
    field(:updated_at, :datetime, description: "Last update date.")
    field(:inserted_at, :datetime, description: "Room creation Date.")
    field(:name, :string, description: "Name Channel.")

    field(:avatar, :string, description: "Avatar from Channel.") do
      resolve(fn %{avatar: avatar}, _, %{context: %{pubsub: endpoint}} ->
        {:ok, FilesUploader.url(avatar, endpoint.static_url())}
      end)
    end

    @desc "Get the last message in this channel."
    field(:message, :message) do
      resolve(fn
        %{last_message: message}, _args, _resolution when is_map(message) ->
          {:ok, struct(Messages.Message, message)}

        parent, _args, _resolution ->
          {:ok, Messages.get_last_message(parent.id)}
      end)
    end

    interface(:room_interface)
  end

  object :public_room do
    field(:id, :integer, description: "Unique identifier of the Room.")
    field(:avatar, :string, description: "Avatar from Channel.")
    field(:updated_at, :datetime, description: "Last update date.")
    field(:inserted_at, :datetime, description: "Room creation Date.")

    field(:name, :string, description: "Name Channel.") do
      resolve(fn
        %{name: name, uuid: uuid}, _args, _resolution when name == uuid ->
          {:ok, ""}

        parent, _args, _resolution ->
          {:ok, parent.name}
      end)
    end

    @desc "Get the last message in this channel."
    field(:message, :message) do
      resolve(fn
        %{last_message: message}, _args, _resolution when is_map(message) ->
          {:ok, struct(Messages.Message, message)}

        parent, _args, _resolution ->
          {:ok, Messages.get_last_message(parent.id)}
      end)
    end

    interface(:room_interface)
  end
end
