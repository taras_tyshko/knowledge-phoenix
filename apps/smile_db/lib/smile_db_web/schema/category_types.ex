defmodule SmileDBWeb.Schema.CategoryTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  alias SmileDB.Ratings

  @desc "In this object are the fields from the model Office."
  object :office do
    field(:id, :id, description: "Unique identifier of the Office.")
    field(:title, :string, description: "Name of the Office.")
    field(:email, :string, description: "Email of the Office.")

    field(:avatar, :string, description: "Avatar of the Office.") do
      resolve(fn %{avatar: avatar}, _, %{context: %{pubsub: endpoint}} ->
        {:ok, FilesUploader.url(avatar, endpoint.static_url())}
      end)
    end

    field(:location, :string, description: "Location of the Office.")
    field(:country, :string, description: "Country where location the Office.")

    field(:ratings, :integer, description: "Office ratings.") do
      resolve(fn parent, _args, _resolution ->
        {:ok, Ratings.get_office_rating(parent.id)}
      end)
    end

    field(:updated_at, :datetime, description: "Last update date.")
    field(:inserted_at, :datetime, description: "Office creation Date.")
  end
end
