defmodule SmileDBWeb.Schema.NotificationTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.{Accounts, Source, Ratings, Subscriptions}

  @desc "Implements the field structure for notification data expansion."
  interface :notification do
    field(:id, :id)
    field(:user, :user)

    resolve_type(fn
      %{content_id: id}, _ when not is_nil(id) -> :content_notification
      %{answer_id: id}, _ when not is_nil(id) -> :answer_notification
      %{comment_id: id}, _ when not is_nil(id) -> :comment_notification
      %{answer_rating_id: id}, _ when not is_nil(id) -> :answer_rating_notification
      %{comment_rating_id: id}, _ when not is_nil(id) -> :comment_rating_notification
      %{user_subscription_id: id}, _ when not is_nil(id) -> :user_subscription_notification
      _, _ -> nil
    end)
  end

  object :content_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")
    field(:content, :contents_union, resolve: dataloader(Source), description: "Getting content data.")

    interface(:notification)
  end

  object :answer_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")
    field(:answer, :answer, resolve: dataloader(Source), description: "Getting answer data.")

    interface(:notification)
  end

  object :comment_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")
    field(:comment, :comment, resolve: dataloader(Source), description: "Getting comment data.")

    interface(:notification)
  end

  object :answer_rating_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")

    field(:answer_rating, :answer_rating,
      resolve: dataloader(Ratings),
      description: "Getting answer rating data."
    )

    interface(:notification)
  end

  object :comment_rating_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")

    field(:comment_rating, :comment_rating,
      resolve: dataloader(Ratings),
      description: "Getting comment rating data."
    )

    interface(:notification)
  end

  object :user_subscription_notification do
    field(:id, :id, description: "Unique identifier of the notification.")
    field(:user, :user, resolve: dataloader(Accounts), description: "Getting user data.")

    field(:user_subscription, :user_subscription,
      resolve: dataloader(Subscriptions),
      description: "Getting user subscription data."
    )

    interface(:notification)
  end
end
