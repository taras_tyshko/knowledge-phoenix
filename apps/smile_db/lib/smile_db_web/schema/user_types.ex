defmodule SmileDBWeb.Schema.UserTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.{Users, Achievements}

  @desc "In this object are the fields from the model User."
  object :user do
    field(:id, :id, description: "Unique identifier of the User.")
    field(:answers_count, :integer, description: "Number of answers created.")
    field(:auth_provider, :string, description: "Authorization provider.")
    field(:biography, :string, description: "Additional information about the user.")
    field(:birthday, :string, description: "Date of birth")
    field(:comments_count, :integer, description: "Number of comments created.")
    field(:role, :string, description: "Unique identifier of the User.")
    field(:email, :string, description: "E-mail address.")

    field(
      :expert,
      list_of(:string),
      description: "The list of tags in which the user is an expert."
    )

    field(:avatar, :string, description: "User image.") do
      resolve(fn %{avatar: avatar}, _, %{context: %{pubsub: endpoint}} ->
        {:ok, FilesUploader.url(avatar, endpoint.static_url())}
      end)
    end

    field(:facebook_uid, :string, description: "Unique identifier of the user from Facebook.")
    field(:first_name, :string, description: "First name user.")
    field(:gender, :string, description: "Gender user.")
    field(:google_uid, :string, description: "Unique identifier of the user from Google.")
    field(:last_name, :string, description: "Last name user.")
    field(:likes_count, :integer, description: "Number of rating that the user received.")
    field(:location, :string, description: "Location address.")
    field(:name, :string, description: "First name and Last name.")
    field(:nickname, :string, description: "Unique identifier of the User.")
    field(:contents_count, :integer, description: "Number of contents created.")
    field(:status, :string, description: "User status.")
    field(:website, :string, description: "Links to Resources.")
    field(:inserted_at, :datetime, description: "User creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
    field(:start_expert, :datetime, description: "DateTime when user start to expert.")
    field(:ban_time, :datetime, description: "Ban user datatime.")

    @desc "List user of Achieves."
    field(:achieves, list_of(:achieve)) do
      resolve(fn parent, _args, _resolution ->
        {:ok, Achievements.list_achieves(parent.id)}
      end)
    end

    field(:list_expert_request_tags, list_of(:string)) do
      resolve(fn
        %{list_expert_requests: list_expert_requests}, _, _resolution ->
          {:ok, list_expert_requests}

        parent, _, _resolution ->
          {:ok, Users.list_expert_requests(parent.id)}
      end)
    end
  end

  @desc "In this object are the fields from the model ExpertRequest."
  object :expert_request do
    field(:id, :id, description: "Unique identifier of the ExpertRequest.")
    field(:role, :string, description: "Unique identifier name of the Role.")
    field(:tag, :string, description: "Identifier name tag of the Tag.")

    field(
      :user,
      :user,
      resolve: dataloader(Accounts),
      description: "Get a user with a ExpertRequest."
    )

    field(:inserted_at, :datetime, description: "User creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end
end
