defmodule SmileDBWeb.Schema.AchieveTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.Achievements

  object :user_achieves do
    field(:id, :id, description: "Unique identifier of the UserAchieves.")

    field(
      :user,
      :user,
      resolve: dataloader(Accounts),
      description: "Get a user with a UserAchieves."
    )

    field(
      :achieve,
      :achieve,
      resolve: dataloader(Achievements),
      description: "Get a achieves with a UserAchieves."
    )

    field(:inserted_at, :datetime, description: "UserAchieves creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end

  @desc "In this object are the fields from the model Achieve."
  object :achieve do
    field(:id, :id, description: "Unique identifier of the Achieves.")
    field(:title, :string, description: "Name of the Achieves.")

    field(:avatar, :string, description: "Avatar of the Achieves.") do
      resolve(fn %{avatar: avatar}, _, %{context: %{pubsub: endpoint}} ->
        {:ok, FilesUploader.url(avatar, endpoint.static_url())}
      end)
    end

    field(:inserted_at, :datetime, description: "Achieves creation Date.")
    field(:updated_at, :datetime, description: "Last update date.")
  end
end
