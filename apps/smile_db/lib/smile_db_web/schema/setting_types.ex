defmodule SmileDBWeb.Schema.SettingTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "In this object are the fields from the model Setting."
  object :setting do
    field(:id, :id, description: "Unique identifier of the Setting.")

    field(
      :by_answers,
      :boolean,
      description: "Subscribe automatically to discussions where you answered."
    )

    field(
      :by_comments,
      :boolean,
      description: "Subscribe automatically to discussions you comment on."
    )

    field(:email_enable, :boolean, description: "Receive emails from Smile.")

    field(
      :email_on_reply,
      :boolean,
      description: "Receive an email when someone replies to your comments."
    )

    field(:web_notif_upvote, :boolean, description: "Someone upvotes your comment.")

    field(
      :web_notif_mentions,
      :boolean,
      description: "Someone recommends a discussion you started."
    )

    field(:web_notif_recommend, :boolean, description: "Someone mentions you in a discussion.")
    field(:inserted_at, :datetime, description: "Setting creation Date.")
    field(:updated_at, :datetime, description: "Last Update date.")
  end
end
