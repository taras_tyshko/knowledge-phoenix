defmodule SmileDBWeb.Schema.RatingTypes do
  @moduledoc false

  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.{Source, Accounts}

  @desc "Get content Rating for the desired model."
  object :ratings do
    field(:likes, list_of(:integer), description: "Get a list of user ID's that put -> True.")
    field(:dislikes, list_of(:integer), description: "Get a list of user ID's that put -> False.")
  end

  @desc "In this object are the fields from the model AnswerRating."
  object :answer_rating do
    field(:id, :id, description: "Unique identifier of the AnswerRating.")
    field(:status, :boolean, description: "like -> true, dislike -> false.")
    field(:user, :user, resolve: dataloader(Accounts))
    field(:answer, :answer, resolve: dataloader(Source))
    field(:inserted_at, :datetime, description: "Creation Date.")
    field(:updated_at, :datetime, description: "Last Update date.")
  end

  @desc "In this object are the fields from the model CommentRating."
  object :comment_rating do
    field(:id, :id, description: "Unique identifier of the CommentRating.")
    field(:status, :boolean, description: "like -> true, dislike -> false.")
    field(:user, :user, resolve: dataloader(Accounts))
    field(:comment, :comment, resolve: dataloader(Source))
    field(:inserted_at, :datetime, description: "Creation Date.")
    field(:updated_at, :datetime, description: "Last Update date.")
  end
end
