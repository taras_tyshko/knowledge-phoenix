Postgrex.Types.define(
  SmileDB.PostgresTypes,
  [EctoLtree.Postgrex.Lquery, EctoLtree.Postgrex.Ltree] ++ Ecto.Adapters.Postgres.extensions(),
  json: Jason
)
