defmodule SmileDB.Subscriptions.OfficeSubscription do
  @moduledoc """
    This module describes the schema `offices_subscription` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Subscriptions.OfficeSubscription

    Examples of features to use this module are presented in the `SmileDB.Subscriptions`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Category.Office
  alias SmileDB.Accounts.User

  @typedoc """
    This type describes all the fields that are available in the `offices_subscription` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          office_id: String.t(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          office: Office.t(),
          user: User.t()
        }

  schema "offices_subscription" do
    belongs_to(:user, User)
    belongs_to(:office, Office, type: :string)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(office_subscription, attrs) do
          office_subscription
          # The fields that are allowed for the record.
          |> cast(attrs, [:user_id, :office_id])
          # The fields are required for recording.
          |> validate_required([:user_id, :office_id])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(office_subscription, attrs) do
    office_subscription
    |> cast(attrs, [:user_id, :office_id])
    |> validate_required([:user_id, :office_id])
    |> unique_constraint(:office_id_user_id)
    |> foreign_key_constraint(:office_id)
    |> foreign_key_constraint(:user_id)
  end
end
