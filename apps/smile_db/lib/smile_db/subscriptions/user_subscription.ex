defmodule SmileDB.Subscriptions.UserSubscription do
  @moduledoc """
    This module describes the schema `users_subscription` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Subscriptions.UserSubscription

    Examples of features to use this module are presented in the `SmileDB.Subscriptions`
  """
  @typedoc """
    This type describes all the fields that are available in the `users_subscription` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias SmileDB.Notifications.Notification

  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          follower_id: integer(),
          status: boolean(),
          user: User.t(),
          follower: User.t()
        }

  schema "users_subscription" do
    field(:status, :boolean)
    belongs_to(:follower, User)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(user_subscription, attrs) do
          user_subscription
          # The fields that are allowed for the record.
          |> cast(attrs, [:follower_id, :user_id])
          # The fields are required for recording.
          |> validate_required([:follower_id, :user_id])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(user_subscription, attrs) do
    user_subscription
    |> cast(attrs, [:follower_id, :user_id, :status])
    |> validate_required([:follower_id, :user_id, :status])
    |> unique_constraint(:follower_id_user_id)
    |> foreign_key_constraint(:follower_id)
    |> foreign_key_constraint(:user_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :users_subscription,
      type: :user_subscription,
      cascade_delete: [Notification]
    })
  end
end
