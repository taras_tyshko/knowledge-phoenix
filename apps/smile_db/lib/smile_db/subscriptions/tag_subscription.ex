defmodule SmileDB.Subscriptions.TagSubscription do
  @moduledoc """
    This module describes the schema `tags_subscription` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Subscriptions.TagSubscription

    Examples of features to use this module are presented in the `SmileDB.Subscriptions`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Tags.Tag
  alias SmileDB.Accounts.User

  @typedoc """
    This type describes all the fields that are available in the `tags_subscription` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          title: String.t(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          tag: Tag.t(),
          user: User.t()
        }

  schema "tags_subscription" do
    belongs_to(:user, User)
    belongs_to(:tag, Tag, foreign_key: :title, type: :string)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(tag_subscription, attrs) do
          tag_subscription
          # The fields that are allowed for the record.
          |> cast(attrs, [:user_id, :title])
          # The fields are required for recording.
          |> validate_required([:user_id, :title])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(tag_subscription, attrs) do
    tag_subscription
    |> cast(attrs, [:user_id, :title])
    |> validate_required([:user_id, :title])
    |> unique_constraint(:title_user_id)
    |> foreign_key_constraint(:title)
    |> foreign_key_constraint(:user_id)
  end
end
