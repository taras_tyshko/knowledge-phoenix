defmodule SmileDB.Subscriptions do
  @moduledoc """
    The `Subscriptions` context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following model are available for use in the current context:

        alias SmileDB.Subscriptions.TagSubscription
        alias SmileDB.Subscriptions.UserSubscription
        alias SmileDB.Subscriptions.ContentSubscription

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data. To do this you need to declare the appropriate for `alias`.
    In our case, you need to call the model: [`TagSubscription`](SmileDB.Subscriptions.TagSubscription.html), [`UserSubscription`](SmileDB.Subscriptions.UserSubscription.html) and  [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html)
    which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Gets a list subscription contents id with table [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html).
    Important before using the features to familiarize yourself with [`changeset`](SmileDB.Subscriptions.ContentSubscription.html#changeset/2)

        alias SmileDB.Subscriptions.ContentSubscription

        def list_contents_subscription(user.id) do
            ContentSubscription
            |> where([f], f.user_id == ^user.id)
            |> select([f], f.content_id)
            |> Repo.all()
        end

      iex> list_contents_subscription(1)
      {:ok, [1,2]}

      iex> list_contents_subscription(123)
      {:error, %Ecto.Changeset{}}

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Subscriptions

      iex> Subscriptions.list_contents_subscription(1)
      {:ok, [1,2]}

      iex> Subscriptions.list_contents_subscription(123)
      {:error, %Ecto.Changeset{}}
  """

  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Subscriptions.{ContentSubscription, TagSubscription, UserSubscription, OfficeSubscription}

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Returns the list of [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html).

  ## Examples

      iex> Subscriptions.list_contents_subscription(%User{})
      [...]

  """
  @spec list_contents_subscription(Integer.t()) :: list(Integer.t())
  def list_contents_subscription(user_id) do
    ContentSubscription
    |> where([f], f.user_id == ^user_id)
    |> select([f], f.content_id)
    |> Repo.all()
  end

  @doc """
  Returns the list of [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html).

  ## Examples

      iex> Subscriptions.list_users_id_from_contents_subscription(1)
      [...]

  """
  @spec list_users_id_from_contents_subscription(Integer.t()) :: list(Integer.t())
  def list_users_id_from_contents_subscription(content_id) do
    ContentSubscription
    |> where([f], f.content_id == ^content_id)
    |> select([f], f.user_id)
    |> Repo.all()
  end

  @doc """
  Creates a [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Subscriptions.ContentSubscription.html#changeset/2)

  ## Examples

      iex> Subscriptions.create_content_subscription(%{field: value})
      {:ok, %ContentSubscription{}}

      iex> Subscriptions.create_content_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_content_subscription(map()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_content_subscription(attrs \\ %{}) do
    %ContentSubscription{}
    |> ContentSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`ContentSubscription`](SmileDB.Subscriptions.ContentSubscription.html).

  ## Examples

      iex> Subscriptions.delete_content_subscription(content_subscription)
      {:ok, %ContentSubscription{}}

      iex> Subscriptions.delete_content_subscription(content_subscription)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_content_subscription(ContentSubscription.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_content_subscription(%ContentSubscription{} = content_subscription) do
    Repo.delete(content_subscription)
  end

  @doc """
  Returns the list of [`tags`](SmileDB.Tags.Tag.html#t:t/0) title.

  ## Examples

      iex> Subscriptions.list_tags_subscription(2)
      [...]

  """
  @spec list_tags_subscription(Integer.t()) :: list(String.t())
  def list_tags_subscription(user_id) do
    TagSubscription
    |> where([f], f.user_id == ^user_id)
    |> select([f], f.title)
    |> Repo.all()
  end

  @doc """
  Returns the list of `user_id` by tag title.

  ## Examples

      iex> Subscriptions.list_user_id_tags_subscription("tag")
      [...]

  """
  @spec list_user_id_tags_subscription(String.t()) :: list(Integer.t())
  def list_user_id_tags_subscription(tags_title) do
    TagSubscription
    |> where([f], f.title in ^tags_title)
    |> select([f], f.user_id)
    |> Repo.all()
  end

  @doc """
  Creates a [`TagSubscription`](SmileDB.Subscriptions.TagSubscription.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Subscriptions.TagSubscription.html#changeset/2)

  ## Examples

      iex> Subscriptions.create_tag_subscription(%{field: value})
      {:ok, %TagSubscription{}}

      iex> Subscriptions.create_tag_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_tag_subscription(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_tag_subscription(attrs \\ %{}) do
    %TagSubscription{}
    |> TagSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`TagSubscription`](SmileDB.Subscriptions.TagSubscription.html).

  ## Examples

      iex> Subscriptions.delete_tag_subscription(tag_subscription)
      {:ok, %TagSubscription{}}

      iex> Subscriptions.delete_tag_subscription(tag_subscription)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_tag_subscription(TagSubscription.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_tag_subscription(%TagSubscription{} = tag_subscription) do
    Repo.delete(tag_subscription)
  end

  @doc """
  Returns the list id of users follower.

  ## Examples

      iex> Subscriptions.list_users_subscription(1)
      [...]

  """
  @spec list_users_subscription(Integer.t()) :: list(Integer.t())
  def list_users_subscription(user_id) do
    UserSubscription
    |> where([f], f.user_id == ^user_id)
    |> where([f], f.status == true)
    |> select([f], f.follower_id)
    |> Repo.all()
  end

  @doc """
  Returns the list of users that subscription to current user.

  ## Examples

      iex> Subscriptions.list_users_subscription(1)
      [...]

  """
  @spec list_user_id_users_subscription(Integer.t()) :: list(Integer.t())
  def list_user_id_users_subscription(follower_id) do
    UserSubscription
    |> where([f], f.follower_id == ^follower_id)
    |> where([f], f.status == true)
    |> select([f], f.user_id)
    |> Repo.all()
  end

  @doc """
  Creates a [`UserSubscription`](SmileDB.Subscriptions.UserSubscription.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Subscriptions.UserSubscription.html#changeset/2)
  ## Examples

      iex> Subscriptions.create_user_subscription(%{field: value})
      {:ok, %UserSubscription{}}

      iex> Subscriptions.create_user_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user_subscription(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_user_subscription(%UserSubscription{} = user_subscription, attrs \\ %{}) do
    user_subscription
    |> UserSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`UserSubscription`](SmileDB.Subscriptions.UserSubscription.html).

  ## Examples

      iex> Subscriptions.delete_user_subscription(user_subscription)
      {:ok, %UserSubscription{}}

      iex> Subscriptions.delete_user_subscription(user_subscription)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user_subscription(UserSubscription.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_user_subscription(%UserSubscription{} = user_subscription) do
    user_subscription
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  @doc """
  Returns the list id of users offices subscription.

  ## Examples

      iex> Subscriptions.list_offices_subscription(1)
      [...]

  """
  @spec list_offices_subscription(Integer.t()) :: list(Integer.t())
  def list_offices_subscription(user_id) do
    OfficeSubscription
    |> where([o], o.user_id == ^user_id)
    |> select([o], o.office_id)
    |> Repo.all()
  end

  @doc """
  Creates a [`OfficeSubscription`](SmileDB.Subscriptions.OfficeSubscription.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Subscriptions.OfficeSubscription.html#changeset/2)
  ## Examples

      iex> Subscriptions.create_office_subscription(%{field: value})
      {:ok, %OfficeSubscription{}}

      iex> Subscriptions.create_office_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_office_subscription(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_office_subscription(attrs \\ %{}) do
    %OfficeSubscription{}
    |> OfficeSubscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`OfficeSubscription`](SmileDB.Subscriptions.OfficeSubscription.html).

  ## Examples

      iex> Subscriptions.delete_office_subscription(office_subscription)
      {:ok, %OfficeSubscription{}}

      iex> Subscriptions.delete_office_subscription(office_subscription)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_office_subscription(OfficeSubscription.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_office_subscription(%OfficeSubscription{} = office_subscription) do
    office_subscription
    |> Repo.delete()
  end
end
