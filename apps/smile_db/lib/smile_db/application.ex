defmodule SmileDB.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @spec start(Elixir.Application.app(), Elixir.Application.start_type()) :: :ok | {:error, term()}
  def start(_type, _args) do
    import Supervisor.Spec

    # ElasticSearch.setup()

    # Define workers and child supervisors to be supervised
    children = [
      # Start the Ecto repository
      supervisor(SmileDB.Repo, []),
      # Start your own worker by calling: SmileDB.Worker.start_link(arg1, arg2, arg3)
      # worker(SmileDB.Worker, [arg1, arg2, arg3]),
      worker(SmileDB.Accounts.Block, []),
      worker(SmileDB.Accounts.Hidden, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: SmileDB.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
