defmodule SmileDB.Notifications.MessageNotification do
  @moduledoc """
    This module describes the schema `messages_notifications` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Notifications.MessageNotification

    Examples of features to use this module are presented in the `Messenger.Notifications`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias SmileDB.Messages.Room
  alias SmileDB.Messages.Message

  @typedoc """
    This type describes all the fields that are available in the `messages_notifications` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          room_id: integer(),
          user_id: integer(),
          message_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          room: Room.t(),
          message: Message.t()
        }

  schema "messages_notifications" do
    belongs_to(:user, User)
    belongs_to(:room, Room)
    belongs_to(:message, Message)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(message_notification, attrs) do
          message_notification
          # The fields that are allowed for the record.
          |> cast(attrs, [:user_id, :message_id, :room_id])
          # The fields are required for recording.
          |> validate_required([:user_id, :message_id, :room_id])
          # The fields that should be unique.
          |> unique_constraint(:user_id)
          |> unique_constraint(:room_id)
          |> unique_constraint(:message_id)
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(message_notification, attrs) do
    message_notification
    |> cast(attrs, [:user_id, :message_id, :room_id])
    |> validate_required([:user_id, :message_id, :room_id])
    |> unique_constraint(:user_id_room_id_message_id)
  end
end
