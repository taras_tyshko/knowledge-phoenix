defmodule SmileDB.Notifications.Notification do
  @moduledoc """
    This module describes the schema `notification` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Notifications.Notification

    Examples of features to use this module are presented in the `SmileDB.Notifications`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Accounts.User
  alias SmileDB.Subscriptions.UserSubscription
  alias SmileDB.Source.{Content, Answer, Comment}
  alias SmileDB.Ratings.{AnswerRating, CommentRating}

  alias SmileDB.Notifications.{
    AnswerNotification,
    CommentNotification,
    ContentNotification,
    AnswerRatingNotification,
    CommentRatingNotification,
    UserSubscriptionNotification
  }

  @typedoc """
    This type describes all the fields that are available in the `notification` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: String.t(),
          read: boolean(),
          shadow: boolean(),
          anonymous: boolean(),
          user_id: integer(),
          content_id: integer(),
          answer_id: integer(),
          comment_id: integer(),
          answer_rating_id: integer(),
          user_subscription_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          answer: Answer.t(),
          comment: Comment.t(),
          content: Content.t(),
          answer_rating: AnswerRating.t(),
          comment_rating: CommentRating.t(),
          user_subscription: UserSubscription.t()
        }

  @primary_key {:id, :string, []}
  schema "notification" do
    field(:read, :boolean)
    field(:shadow, :boolean)
    field(:anonymous, :boolean)

    belongs_to(:user, User)
    belongs_to(:content, Content)
    belongs_to(:answer, Answer)
    belongs_to(:comment, Comment)
    belongs_to(:answer_rating, AnswerRating)
    belongs_to(:comment_rating, CommentRating)
    belongs_to(:user_subscription, UserSubscription)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(notification, attrs) do
        notification
        # The fields that are allowed for the record.
        |> cast(attrs, [:id, :read, :shadow, :user_id, :answer_id,
          :comment_id, :inserted_at, :content_id, :answer_rating_id,
          :comment_rating_id, :anonymous, :user_subscription_id
        ])
        # The fields are required for recording.
        |> validate_required([:id, :read, :user_id])
      end)
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(notification, attrs) do
    notification
    |> cast(attrs, [
      :id,
      :read,
      :shadow,
      :user_id,
      :anonymous,
      :answer_id,
      :comment_id,
      :inserted_at,
      :content_id,
      :answer_rating_id,
      :comment_rating_id,
      :user_subscription_id
    ])
    |> validate_required([:id, :read, :user_id])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :notifications,
      type: :notification,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime
          ]
        }),
      reindex_function: &SmileDB.Notifications.put_notifications/1
    })
  end

  def source_query do
    [
      from(cn in ContentNotification, select: cn),
      from(a in AnswerNotification, select: a),
      from(c in CommentNotification, select: c),
      from(ar in AnswerRatingNotification, select: ar),
      from(cr in CommentRatingNotification, select: cr),
      from(us in UserSubscriptionNotification, select: us)
    ]
  end
end
