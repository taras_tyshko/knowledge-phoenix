defmodule SmileDB.Notifications.AnswerNotification do
  @moduledoc """
    This module describes the schema `answers_notification` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Notifications.AnswerNotification

    Examples of features to use this module are presented in the `SmileDB.Notifications`
  """
  @typedoc """
    This type describes all the fields that are available in the `answers_notification` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias SmileDB.Source.Answer

  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          answer_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          answer: Answer.t()
        }

  schema "answers_notification" do
    field(:read, :boolean, default: false, virtual: true)

    belongs_to(:user, User)
    belongs_to(:answer, Answer)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(answer_notification, attrs) do
        answer_notification
        # The fields that are allowed for the record.
        |> cast(attrs, [:user_id, :answer_id])
        # The fields are required for recording.
        |> validate_required([:user_id, :answer_id])
      end)
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(answer_notification, attrs) do
    answer_notification
    |> cast(attrs, [:user_id, :answer_id])
    |> validate_required([:user_id, :answer_id])
    |> unique_constraint(:answer_id_user_id)
    |> foreign_key_constraint(:answer_id)
    |> foreign_key_constraint(:user_id)
  end
end
