defmodule SmileDB.Notifications do
  @moduledoc """
    The `Notifications` context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias SmileDB.Notifications.AnswerNotification
        alias SmileDB.Notifications.CommentNotification

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data. To do this you need to declare the appropriate for alias.
    In our case, you need to call the model: [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html), [`CommentNotification`](SmileDB.Notifications.CommentNotification.html)
    which will use it to fetch data from the base.

    The module`s functions support multi-insert and multi-delete data from the repository.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Create a [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html).

      alias SmileDB.Notifications.AnswerNotification

      def create_answer_notification(%{field: value}) do
        %AnswerNotification{}
        |> AnswerNotification.changeset(attrs)
        |> Repo.insert()
      end

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Notifications

      iex> Notifications.create_answer_notifications([%{field: value}])
      [%AnswerNotification{}]

      iex> Notifications.create_answer_notifications([%{field: bad_value}])
      []
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  alias SmileDB.Repo

  alias SmileDB.Notifications.{
    Notification,
    AnswerNotification,
    CommentNotification,
    MessageNotification,
    ContentNotification,
    AnswerRatingNotification,
    CommentRatingNotification,
    UserSubscriptionNotification
  }

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Creates all [`ContentNotification`](SmileDB.Notifications.ContentNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`ContentNotification`](SmileDB.Notifications.ContentNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_content_notifications([%{field: value}])
      [%ContentNotification{}]

  """
  @spec create_content_notifications(list(map())) :: list(ContentNotification.t()) | nil
  def create_content_notifications(data) when is_list(data) do
    {_count, records} =
      ContentNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:content_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`ContentNotification`](SmileDB.Notifications.ContentNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`ContentNotification`](SmileDB.Notifications.ContentNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_content_notifications([1])
      [%ContentNotification{}]

  """
  @spec delete_content_notifications(list(Integer.t())) :: list(ContentNotification.t())
  def delete_content_notifications(content_notifications_id)
      when is_list(content_notifications_id) do
    {_count, records} =
      ContentNotification
      |> where([q_notification], q_notification.id in ^content_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_answer_notifications([%{field: value}])
      [%AnswerNotification{}]

  """
  @spec create_answer_notifications(list(map())) :: list(AnswerNotification.t()) | nil
  def create_answer_notifications(data) when is_list(data) do
    {_count, records} =
      AnswerNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:answer_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_answer_notifications([1])
      [%AnswerNotification{}]

  """
  @spec delete_answer_notifications(list(Integer.t())) :: list(AnswerNotification.t())
  def delete_answer_notifications(answer_notifications_id)
      when is_list(answer_notifications_id) do
    {_count, records} =
      AnswerNotification
      |> where([a_notification], a_notification.id in ^answer_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`CommentNotification`](SmileDB.Notifications.CommentNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`CommentNotification`](SmileDB.Notifications.CommentNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_comment_notifications([%{field: value}])
      [%CommentNotification{}]

  """
  @spec create_comment_notifications(list(map())) :: list(CommentNotification.t()) | nil
  def create_comment_notifications(data) when is_list(data) do
    {_count, records} =
      CommentNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:comment_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`CommentNotification`](SmileDB.Notifications.CommentNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`CommentNotification`](SmileDB.Notifications.CommentNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_comment_notifications([1])
      [%CommentNotification{}]

  """
  @spec delete_comment_notifications(list(Integer.t())) :: list(CommentNotification.t())
  def delete_comment_notifications(comment_notifications_id)
      when is_list(comment_notifications_id) do
    {_count, records} =
      CommentNotification
      |> where([c_notification], c_notification.id in ^comment_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`AnswerRatingNotification`](SmileDB.Notifications.AnswerRatingNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`AnswerRatingNotification`](SmileDB.Notifications.AnswerRatingNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_answer_rating_notifications([%{field: value}])
      [%AnswerRatingNotification{}]

  """
  @spec create_answer_rating_notifications(list(map())) ::
          list(AnswerRatingNotification.t()) | nil
  def create_answer_rating_notifications(data) when is_list(data) do
    {_count, records} =
      AnswerRatingNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:answer_rating_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`AnswerRatingNotification`](SmileDB.Notifications.AnswerRatingNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`AnswerRatingNotification`](SmileDB.Notifications.AnswerRatingNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_answer_rating_notifications([1])
      [%AnswerRatingNotification{}]

  """
  @spec delete_answer_rating_notifications(list(Integer.t())) ::
          list(AnswerRatingNotification.t())
  def delete_answer_rating_notifications(answer_rating_notifications_id)
      when is_list(answer_rating_notifications_id) do
    {_count, records} =
      AnswerRatingNotification
      |> where([ar_notification], ar_notification.id in ^answer_rating_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`CommentRatingNotification`](SmileDB.Notifications.CommentRatingNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`CommentRatingNotification`](SmileDB.Notifications.CommentRatingNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_comment_rating_notifications([%{field: value}])
      [%CommentRatingNotification{}]

  """
  @spec create_comment_rating_notifications(list(map())) ::
          list(CommentRatingNotification.t()) | nil
  def create_comment_rating_notifications(data) when is_list(data) do
    {_count, records} =
      CommentRatingNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:comment_rating_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`CommentRatingNotification`](SmileDB.Notifications.CommentRatingNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`CommentRatingNotification`](SmileDB.Notifications.CommentRatingNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_comment_rating_notifications([1])
      [%CommentRatingNotification{}]

  """
  @spec delete_comment_rating_notifications(list(Integer.t())) ::
          list(CommentRatingNotification.t())
  def delete_comment_rating_notifications(comment_rating_notifications_id)
      when is_list(comment_rating_notifications_id) do
    {_count, records} =
      CommentRatingNotification
      |> where([cr_notification], cr_notification.id in ^comment_rating_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`UserSubscriptionNotification`](SmileDB.Notifications.UserSubscriptionNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`UserSubscriptionNotification`](SmileDB.Notifications.UserSubscriptionNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_user_subscription_notifications([%{field: value}])
      [%UserSubscriptionNotification{}]

  """
  @spec create_user_subscription_notifications(list(map())) ::
          list(UserSubscriptionNotification.t()) | nil
  def create_user_subscription_notifications(data) when is_list(data) do
    {_count, records} =
      UserSubscriptionNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:user_subscription_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`UserSubscriptionNotification`](SmileDB.Notifications.UserSubscriptionNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`UserSubscriptionNotification`](SmileDB.Notifications.UserSubscriptionNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_user_subscription_notifications([1])
      [%UserSubscriptionNotification{}]

  """
  @spec delete_user_subscription_notifications(list(Integer.t())) ::
          list(UserSubscriptionNotification.t())
  def delete_user_subscription_notifications(user_subscription_notifications_id)
      when is_list(user_subscription_notifications_id) do
    {_count, records} =
      UserSubscriptionNotification
      |> where([us_notification], us_notification.id in ^user_subscription_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Creates all [`MessageNotification`](SmileDB.Notifications.MessageNotification.html) into the repository.

  It expects  a list of entries to be inserted, either as keyword lists or as maps.

  Insert function replace all values on the existing row by the new entry or creates a new row in the database.
  It returns a list of [`MessageNotification`](SmileDB.Notifications.MessageNotification.html).
  If the database does not support RETURNING in INSERT statements or no return result was selected, the returned element will be nil.

  ## Examples

      iex> Notifications.create_message_notifications([%{field: value}])
      [%MessageNotification{}]

  """
  @spec create_message_notifications(list(map())) :: list(MessageNotification.t()) | nil
  def create_message_notifications(data) when is_list(data) do
    {_count, records} =
      MessageNotification
      |> Repo.insert_all(
        Enum.map(data, Repo.set_timestamp(DateTime.utc_now())),
        returning: true,
        on_conflict: :replace_all,
        conflict_target: [:message_id, :user_id]
      )

    records
  end

  @doc """
  Deletes all [`MessageNotification`](SmileDB.Notifications.MessageNotification.html) from the repository matching the given query.

  It expects a list of object`s id to be deleted.

  It returns a list of [`MessageNotification`](SmileDB.Notifications.MessageNotification.html).
  Note, however, not all databases support returning data from DELETEs.

  ## Examples

      iex> Notifications.delete_message_notifications([1])
      [%MessageNotification{}]

  """
  @spec delete_message_notifications(list(Integer.t())) :: list(MessageNotification.t())
  def delete_message_notifications(message_notifications_id)
      when is_list(message_notifications_id) do
    {_count, records} =
      MessageNotification
      |> where([us_notification], us_notification.id in ^message_notifications_id)
      |> Repo.delete_all(returning: true)

    records
  end

  @doc """
  Puts [`Notification`](SmileDB.Notifications.Notification.html) to the elasticsearch document.

  It expects one of the supported types for notifications.
  Notifications are available for the following types:
    [`ContentNotification`](SmileDB.Notifications.ContentNotification.html),
    [`AnswerNotification`](SmileDB.Notifications.AnswerNotification.html),
    [`CommentNotification`](SmileDB.Notifications.CommentNotification.html),
    [`AnswerRatingNotification`](SmileDB.Notifications.AnswerRatingNotification.html),
    [`CommentRatingNotification`](SmileDB.Notifications.CommentRatingNotification.html),
    [`UserSubscriptionNotification`](SmileDB.Notifications.UserSubscriptionNotification.html)

  ## Examples

      iex> Notifications.put_notifications(%notification_type{})
      %Notification{}

  """
  def put_notifications(notification) when is_map(notification) do
    %Notification{}
    |> Notification.changeset(Repo.replace_id(notification))
    |> Ecto.Changeset.apply_changes()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Return list of [`Notification`](SmileDB.Notifications.Notification.html). id for user.

  ## Examples

      iex> Notifications.list_notifications_by_user_id(user_id)
      [...]

  """
  @spec list_notifications_by_user_id(Integer.t()) :: list(Integer.t())
  def list_notifications_by_user_id(user_id) do
    search index: ElasticSearch.get_meta!(Notification).index do
      query do
        bool do
          must do
            term("user_id", user_id)
          end

          must_not do
            term("shadow", true)
          end

          filter do
            term("read", false)
          end
        end
      end

      sort do
        [inserted_at: :desc]
      end
    end
    |> ElasticSearch.Repo.all()
    |> Enum.map(& &1.id)
  end

  @doc """
  Gets a list of user [`MessageNotification`](SmileDB.Notifications.MessageNotification.html).

  ## Examples

      iex> Notifications.get_message_notification([1])
      [%MessageNotification{}]

      # 456 - Identifier is not valid and return empty list.
      iex> Notifications.get_message_notification([456])
      []

  """
  @spec get_message_notification(Integer.t()) :: list(MessageNotification.t())
  def get_message_notification(user_id) do
    MessageNotification
    |> where([m], m.user_id == ^user_id)
    |> order_by([m], asc: :inserted_at)
    |> Repo.all()
  end
end
