defmodule SmileDB.Notifications.ContentNotification do
  @moduledoc """
    This module describes the schema `contents_notification` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Notifications.ContentNotification

    Examples of features to use this module are presented in the `SmileDB.Notifications`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias SmileDB.Source.Content

  @typedoc """
    This type describes all the fields that are available in the `contents_notification` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          content_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          content: Content.t()
        }

  schema "contents_notification" do
    field(:read, :boolean, default: false, virtual: true)

    belongs_to(:user, User)
    belongs_to(:content, Content)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(content_notification, attrs) do
        content_notification
        # The fields that are allowed for the record.
        |> cast(attrs, [:user_id, :content_id])
        # The fields are required for recording.
        |> validate_required([:user_id, :content_id])
      end)
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(content_notification, attrs) do
    content_notification
    |> cast(attrs, [:user_id, :content_id])
    |> validate_required([:user_id, :content_id])
    |> unique_constraint(:user_id_content_id)
  end
end
