defmodule SmileDB.Repo do
  @moduledoc """
  Defines a repository.

  A repository maps to an underlying data store, controlled by the adapter.
  For example, Ecto ships with a Postgres adapter that stores data into a PostgreSQL database.

  See the [`Ecto.Repo`](https://hexdocs.pm/ecto/Ecto.Repo.html#content)
  docs for more details.
  """

  use Ecto.Repo, otp_app: :smile_db

  use Paginator,
    include_total_count: true,
    sort_direction: :desc,
    total_count_limit: :infinity

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
  end

  def encode(map) do
    map
    |> Jason.encode!()
    |> Base.encode64()
  end

  def decode(map) do
    map
    |> Base.decode64!()
    |> Jason.decode!()
  end

  def replace_id(model) when is_map(model) do
    model
    |> Map.from_struct()
    |> get_and_update_in(
      [:id],
      &{&1,
       encode(%{
         schema: model.__struct__,
         id: &1
       })}
    )
    |> elem(1)
  end

  def set_timestamp(date) do
    fn
      map when is_map(map) ->
        map
        |> Map.put(:inserted_at, date)
        |> Map.put(:updated_at, date)

      keyword_list when is_list(keyword_list) ->
        keyword_list
        |> Keyword.put(:inserted_at, date)
        |> Keyword.put(:updated_at, date)
    end
  end

  @doc """
  Updates all a data in Repo and ElastiSearch.

  ## Examples

      iex> update_all_db_elastic(querytable, attrs)
      {:ok, [%Schema{}]}

      iex> update_all_db_elastic(querytable, attrs)
      {:error, %Ecto.Changeset{}}

  """
  @spec update_all_db_elastic(Ecto.Queryable.t(), map()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_all_db_elastic(querytable, attrs) do
    querytable
    |> update_all(set: Map.to_list(attrs))
    |> Tuple.to_list()
    |> Enum.fetch!(1)
    |> Enum.map(&ElasticSearch.Repo.update(&1))
  end
end
