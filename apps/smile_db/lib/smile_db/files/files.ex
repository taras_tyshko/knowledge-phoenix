defmodule SmileDB.Files do
  @moduledoc """
    The Files context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias SmileDB.Files.Content

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.
  """

  alias __MODULE__

  @spec create(String.t()) :: Files.Content.t()
  def create(attrs) do
    Files.Content.changeset(%Files.Content{}, attrs)
  end
end
