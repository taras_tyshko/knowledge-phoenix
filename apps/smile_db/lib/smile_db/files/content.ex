defmodule SmileDB.Files.Content do
  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}

  @available_files_extensions ~w(jpg jpeg svg png xlsx xlsm xlsb xltx xltm xls xlt xml xlam xla xlw xlr dotm docm txt dot rtf dotx docx doc pdf)

  @typedoc """
    This type describes all the fields that are available in the embedded schema.
  """
  @type t :: %__MODULE__{
          id: String.t(),
          name: String.t(),
          path: String.t(),
          extension: String.t(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  embedded_schema do
    field(:extension)
    field(:name)
    field(:path)
    timestamps(type: :utc_datetime)
  end

  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(content, attrs) do
    content
    |> cast(attrs, [:extension, :name, :path])
    |> validate_required([:extension, :name, :path])
    |> validate_inclusion(:extension, @available_files_extensions)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
