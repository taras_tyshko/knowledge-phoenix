defmodule SmileDB.Graphql do
  alias SmileDB.{
    Ratings,
    Source,
    Category,
    Messages,
    Accounts,
    Permissions,
    Achievements,
    Subscriptions,
    Notifications
  }

  alias SmileDBWeb.{
    Schema,
    Middlewares
  }

  defmacro __using__(_opts) do
    quote do
      use Absinthe.Schema

      import_types(Absinthe.Type.Custom)
      import_types(Schema.TagTypes)
      import_types(Schema.FileTypes)
      import_types(Schema.FeedTypes)
      import_types(Schema.UserTypes)
      import_types(Schema.RatingTypes)
      import_types(Schema.MessageTypes)
      import_types(Schema.CategoryTypes)
      import_types(Schema.RoomTypes)
      import_types(Schema.ContentTypes)
      import_types(Schema.AccountTypes)
      import_types(Schema.AchieveTypes)
      import_types(Schema.SettingTypes)
      import_types(Schema.PermissionTypes)
      import_types(Schema.SubscriptionTypes)
      import_types(Schema.NotificationTypes)

      @spec context(map()) :: map()
      def context(ctx) do
        loader =
          Dataloader.new()
          |> Dataloader.add_source(Source, Source.data())
          |> Dataloader.add_source(Ratings, Ratings.data())
          |> Dataloader.add_source(Category, Category.data())
          |> Dataloader.add_source(Messages, Messages.data())
          |> Dataloader.add_source(Accounts, Accounts.data())
          |> Dataloader.add_source(Permissions, Permissions.data())
          |> Dataloader.add_source(Achievements, Achievements.data())
          |> Dataloader.add_source(Subscriptions, Subscriptions.data())
          |> Dataloader.add_source(Notifications, Notifications.data())

        Map.put(ctx, :loader, loader)
      end

      @spec plugins :: list
      def plugins do
        [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
      end

      @spec middleware(list, map(), map()) :: list
      def middleware(middleware, _field, _object) do
        middleware ++ [Middlewares.HandleChangesetErrors]
      end

      defoverridable(plugins: 0, middleware: 3)
    end
  end
end
