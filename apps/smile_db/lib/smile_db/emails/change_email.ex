defmodule SmileDB.Emails.ChangeEmail do
  @moduledoc """
    This module describes the schema `change_emails` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Emails.ChangeEmail

    Examples of features to use this module are presented in the `SmileDB.Emails`
  """
  @typedoc """
    This type describes all the fields that are available in the `change_emails` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User

  @type t :: %__MODULE__{
        id: integer(),
        hash: String.t(),
        email: String.t(),
        user_id: integer(),
        updated_at: timeout(),
        inserted_at: timeout(),
        user: User.t()
      }

  schema "change_emails" do
    field(:hash, :string)
    field(:email, :string)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(change_email, attrs) do
        change_email
        # The fields that are allowed for the record.
        |> cast(attrs, [:hash, :email, :user_id])
        # The fields are required for recording.
        |> validate_required([:hash, :email, :user_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(change_email, attrs) do
    change_email
    |> cast(attrs, [:hash, :email, :user_id])
    |> validate_required([:hash, :email, :user_id])
  end
end
