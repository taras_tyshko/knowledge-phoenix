defmodule SmileDB.Emails do
  @moduledoc """
    The `Emails` context. This context describes how to use models and to build functions for future use. 
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias SmileDB.Emails.ChangeEmail

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo
      
    To work this module we use the library [`Bamboo`](https://hex.pm/packages/bamboo) which allows you to release send email users.
    The official documentation is located at the following address [`Bamboo.Phoenix`](https://hexdocs.pm/bamboo/Bamboo.Phoenix.html).

        # Call a library for work and view template
        use Bamboo.Phoenix, view: SmileDBWeb.EmailView
    
  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Emails

      iex> Emails.update_change_email(change_email, %{field: new_value})
      {:ok, %ChangeEmail{}}

      iex> Emails.update_change_email(change_email, %{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """

  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Emails.ChangeEmail

  @doc """
  Gets a single [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html) changes by field `:user_id`.

  Raises `Ecto.NoResultsError` if the [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html) changes. does not exist.

  ## Examples

      iex> Emails.get_change_email_by_user_id(1)
      [%ChangeEmail{}]

      iex> Emails.get_change_email_by_user_id(1)
      ** (Ecto.NoResultsError)

  """
  @spec get_change_email_by_user_id(String.t()) :: SmileDB.Emails.ChangeEmail.t()
  def get_change_email_by_user_id(user_id) do
    ChangeEmail
    |> where([e], e.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
  Gets a single [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html) changes by field `:hash`.

  Raises `Ecto.NoResultsError` if the [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html) changes. does not exist.

  ## Examples

      iex> Emails.get_change_email_by_hash!("hash")
      %ChangeEmail{}

      iex> Emails.get_change_email_by_hash!("hash")
      ** (Ecto.NoResultsError)

  """
  @spec get_change_email_by_hash!(String.t()) :: SmileDB.Emails.ChangeEmail.t()
  def get_change_email_by_hash!(hash), do: Repo.get_by!(ChangeEmail, hash: hash)

  @doc """
  Creates a [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Emails.ChangeEmail.html#changeset/2).

  ## Examples

      iex> Emails.create_change_email(%{field: value})
      {:ok, %ChangeEmail{}}

      iex> Emails.create_change_email(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_change_email(map()) :: SmileDB.Emails.ChangeEmail.t()
  def create_change_email(attrs \\ %{}) do
    %ChangeEmail{}
    |> ChangeEmail.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`ChangeEmail`](SmileDB.Emails.ChangeEmail.html) changes..

  ## Examples

      iex> Emails.delete_change_email(change_email)
      {:ok, %ChangeEmail{}}

      iex> Emails.delete_change_email(change_email)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_change_email(ChangeEmail.t()) :: ChangeEmail.t()
  def delete_change_email(%ChangeEmail{} = change_email) do
    Repo.delete(change_email)
  end
end
