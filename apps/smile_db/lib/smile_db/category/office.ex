defmodule SmileDB.Category.Office do
  @moduledoc """
    This module describes the schema `offices` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Category.Office

    Examples of features to use this module are presented in the `SmileDB.Category`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Ratings.OfficeRating

  @typedoc """
    This type describes all the fields that are available in the `offices` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: String.t(),
          uuid: String.t(),
          title: String.t(),
          email: String.t(),
          country: String.t(),
          avatar: String.t(),
          location: String.t(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  @primary_key {:id, :string, []}
  schema "offices" do
    field(:uuid, :string)
    field(:avatar, :string)
    field(:email, :string)
    field(:country, :string)
    field(:location, :string)
    field(:title, :string)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(office, attrs) do
        office
        # The fields that are allowed for the record.
        |> cast(attrs, [:id, :title, :location, :email, :avatar])
        # The fields are required for recording.
        |> validate_required([:id, :title, :location, :uuid])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(office, attrs) do
    office
    |> cast(attrs, [:id, :title, :location, :email, :avatar, :country])
    |> check_uuid()
    |> validate_required([:id, :title, :location, :uuid])  
    |> unique_constraint(:id, name: :offices_pkey)

  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :offices,
      type: :office,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:title],
            autocomplete: [:title]
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      cascade_delete: [OfficeRating]
    })
  end
end
