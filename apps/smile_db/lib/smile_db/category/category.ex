defmodule SmileDB.Category do
  @moduledoc """
    The Category context.
  """

  import Ecto.Query, warn: false
  alias SmileDB.Repo

  alias SmileDB.Category.Office
  
  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Gets a single [`Office`](SmileDB.Category.Office.html).

  Raises `Ecto.NoResultsError` if the Office does not exist.

  ## Examples

      iex> Category.get_office!(123)
      %Office{}

      iex> Category.get_office!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_office!(Integer.t()) :: Office.t() | Ecto.NoResultsError.t()
  def get_office!(id), do: ElasticSearch.Repo.get!(Office, id)

  @doc """
  Creates a [`Office`](SmileDB.Category.Office.html).

  ## Examples

      iex> Category.create_office(%{field: value})
      {:ok, %Office{}}

      iex> Category.create_office(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_office(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_office(attrs \\ %{}) do
    %Office{}
    |> Office.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`Office`](SmileDB.Category.Office.html).

  ## Examples

      iex> Category.update_office(office, %{field: new_value})
      {:ok, %Office{}}

      iex> Category.update_office(office, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_office(Office.t(), map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_office(%Office{} = office, attrs) do
    office
    |> Office.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Office`](SmileDB.Category.Office.html).

  ## Examples

      iex> Category.delete_office(office)
      {:ok, %Office{}}

      iex> Category.delete_office(office)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_office(Office.t()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_office(%Office{} = office) do
    with {:ok, office} <- Repo.delete(office) do
      # FilesUploader.delete(FilesUploader.Definition.Media, office)
      {:ok, ElasticSearch.Repo.delete(office)}
    end
  end
end
