defmodule SmileDB.Permissions.RoleAccess do
  @moduledoc """
    This module describes the schema `role_access` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Permissions.RoleAccess

    Examples of features to use this module are presented in the `SmileDB.Permissions`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Accounts.Role
  alias SmileDB.Permissions.Permission

  @typedoc """
    This type describes all the fields that are available in the `role_access` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          state: Integer.t(),
          role_name: String.t(),
          permission_id: Integer.t(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  schema "role_access" do
    field(:state, :integer, default: 0)

    belongs_to(:permission, Permission)
    belongs_to(:role_name, Role, type: :string, foreign_key: :role, references: :role)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(role_access, attrs) do
          role_access
          # The fields that are allowed for the record.
          |> cast(attrs, [:permission_id, :role, :state])
          # The fields are required for recording.
          |> validate_required([:permission_id, :role])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(role_access, attrs) do
    role_access
    |> cast(attrs, [:permission_id, :role, :state])
    |> validate_required([:permission_id, :role])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :role_access,
      type: :role_acces,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
