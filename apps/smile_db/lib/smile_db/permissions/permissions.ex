defmodule SmileDB.Permissions do
  @moduledoc """
      The Permissions context. This context describes how to use models and to build functions for future use.
      The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

      The following models are available for use in the current context:

          alias SmileDB.Permissions.RoleAccess
          alias SmileDB.Permissions.Permission

      To work with this schema, you need to use a dependency.

          # To build samples from the database you need to use
          import Ecto.Query, warn: false
          # To work with the repository base you need to use.
          alias SmileDB.Repo

      If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

      For example, you can get the data used by `permission.id`. To do this you need to declare the appropriate for alias.
      In our case, you need to call the model [`Permission`](SmileDB.Permissions.Permission.html), [`Role`](SmileDB.Permissions.RoleAccess.html),
      which will use it to fetch data from the base.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  alias SmileDB.Repo

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  alias SmileDB.Accounts
  alias SmileDB.Permissions.{Permission, RoleAccess}

  @doc """
  Gets a single permission.

  Raises `Ecto.NoResultsError` if the Permission does not exist.

  ## Examples

      iex> get_permission!(123)
      %Permission{}

      iex> get_permission!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_permission!(Integer.t()) :: Permission.t() | Ecto.NoResultsError.t()
  def get_permission!(id) do
    ElasticSearch.Repo.get!(Permission, id)
  end

  @doc """
    Returns a list permission by user role.

  ## Examples

      iex>  Permissions.list_permissions("customer")
      {:ok, [...]}

      iex>  Permissions.list_permissions("test")
      {:ok, []}
  """
  @spec list_permissions(String.t()) :: list(String.t()) | list()
  def list_permissions(role) do
    permissions =
      role
      |> list_role_access()
      |> Enum.map(fn role_access ->
        get_permission!(role_access.permission_id)
        |> Map.get(:name)
      end)

    {:ok, permissions}
  end

  @doc """
  Creates a permission.

  ## Examples

      iex> create_permission(%{field: value})
      {:ok, %Permission{}}

      iex> create_permission(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_permission(map()) :: {:ok, Permission.t()} | {:error, Ecto.ChangeError.t()}
  def create_permission(attrs \\ %{}) do
    %Permission{}
    |> Permission.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a permission.

  ## Examples

      iex> update_permission(permission, %{field: new_value})
      {:ok, %Permission{}}

      iex> update_permission(permission, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_permission(Permission.t(), map()) ::
          {:ok, Permission.t()} | {:error, Ecto.ChangeError.t()}
  def update_permission(%Permission{} = permission, attrs) do
    permission
    |> Permission.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a Permission.

  ## Examples

      iex> delete_permission(permission)
      {:ok, %Permission{}}

      iex> delete_permission(permission)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_permission(Permission.t()) ::
          {:ok, Permission.t()} | {:error, Ecto.ChangeError.t()}
  def delete_permission(%Permission{} = permission) do
    permission
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  def assign_permission_by_role(roles) do
    Enum.map(roles, fn role ->
      access_code =
        search(index: ElasticSearch.get_meta!(RoleAccess).index) do
          query do
            term("role.keyword", role.role)
          end

          sort do
            [permission_id: :asc]
          end
        end
        |> ElasticSearch.Repo.all()
        |> Enum.map(& &1.state)
        |> Enum.join()
        |> String.to_integer(2)

      Accounts.update_role(role, %{access_code: access_code})
    end)
  end

  @doc """
  Gets a single role_access.

  Raises `Ecto.NoResultsError` if the Role access does not exist.

  ## Examples

      iex> get_role_access!([123])
      [%RoleAccess{}]

      iex> get_role_access!([456])
      []

  """
  @spec get_role_access!(list) :: RoleAccess.t()
  def get_role_access!(id), do: ElasticSearch.Repo.get!(RoleAccess, id)

  @doc """
  Gets a list access by user role.

  ## Examples

      iex> list_role_access("customer")
      [...]

      iex> list_role_access("test")
      []

  """
  @spec list_role_access(String.t()) :: list(Integer.t()) | list()
  def list_role_access(name) do
    search(index: ElasticSearch.get_meta!(RoleAccess).index) do
      query do
        term("role", name)
      end

      sort do
        [permission_id: :asc]
      end
    end
    |> ElasticSearch.Repo.all()
  end

  @doc """
  Creates a role_access.

  ## Examples

      iex> create_role_access(%{field: value})
      {:ok, %RoleAccess{}}

      iex> create_role_access(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_role_access(map()) :: {:ok, RoleAccess.t()} | {:error, Ecto.ChangeError.t()}
  def create_role_access(attrs \\ %{}) do
    %RoleAccess{}
    |> RoleAccess.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updated a role_access.

  ## Examples

      iex> update_role_access(%{field: value})
      {:ok, %RoleAccess{}}

      iex> update_role_access(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_role_access(RoleAccess.t(), map()) ::
          {:ok, RoleAccess.t()} | {:error, Ecto.ChangeError.t()}
  def update_role_access(%RoleAccess{} = permission, attrs) do
    permission
    |> RoleAccess.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a RoleAccess.

  ## Examples

      iex> delete_role_access(role_access)
      {:ok, %RoleAccess{}}

      iex> delete_role_access(role_access)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_role_access(RoleAccess.t()) ::
          {:ok, RoleAccess.t()} | {:error, Ecto.ChangeError.t()}
  def delete_role_access(%RoleAccess{} = role_access) do
    role_access
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end
end
