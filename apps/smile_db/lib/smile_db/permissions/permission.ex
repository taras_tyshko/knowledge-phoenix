defmodule SmileDB.Permissions.Permission do
  @moduledoc """
    This module describes the schema `permissions` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Permissions.Permission

    Examples of features to use this module are presented in the `SmileDB.Permissions`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Permissions.RoleAccess

  @typedoc """
    This type describes all the fields that are available in the `permissions` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          name: String.t(),
          updated_at: timeout(),
          inserted_at: timeout(),
          role_access: RoleAccess.t()
        }

  schema "permissions" do
    field(:name, :string)

    has_many(:role_access, RoleAccess)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(permission, attrs) do
          permission
          # The fields that are allowed for the record.
          |> cast(attrs, [:name])
          # The fields are required for recording.
          |> validate_required([:name])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(permission, attrs) do
    permission
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :permissions,
      type: :permission,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:name],
            autocomplete: [:name]
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      cascade_decrement: [
        {RoleAccess, :id}
      ]
    })
  end
end
