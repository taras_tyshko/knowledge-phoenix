defmodule SmileDB.Tags.ContentTag do
  @moduledoc """
    This module describes the schema `contents_tags` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Tags.ContentTag

    Examples of features to use this module are presented in the `SmileDB.Tags`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Tags.Tag
  alias SmileDB.Source.Content

  @typedoc """
    This type describes all the fields that are available in the `contents_tags` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          tag_id: integer(),
          content_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          content: Content.t(),
          tag: Tag.t()
        }

  schema "contents_tags" do
    belongs_to(:tag, Tag)
    belongs_to(:content, Content)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(content_tag, attrs) do
        content_tag
        # The fields that are allowed for the record.
        |> cast(attrs, [:tag_id, :content_id])
        # The fields are required for recording.
        |> validate_required([:tag_id, :content_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(content_tag, attrs) do
    content_tag
    |> cast(attrs, [:tag_id, :content_id])
    |> validate_required([:tag_id, :content_id])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :contents_tags,
      type: :content_tag,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      trigger_update:
        Entity.TriggerUpdate.new(%{
          counter: [
            Entity.TriggerUpdate.Counter.new(%{
              related_model: Tag,
              update_field: :count,
              condition: & &1[:tag_id]
            })
          ]
        })
    })
  end
end
