defmodule SmileDB.Tags.Tag do
  @moduledoc """
    This module describes the schema `tags` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Tags.Tag

    Examples of features to use this module are presented in the `SmileDB.Tags`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}

  alias SmileDB.Users.Expert
  alias SmileDB.Source.Content
  alias SmileDB.Subscriptions.TagSubscription

  @typedoc """
    This type describes all the fields that are available in the `tags` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          title: String.t(),
          count: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          contents: Content.t(),
          tags_subscription: TagSubscription.t()
        }

  schema "tags" do
    field(:title, :string)
    field(:count, :integer, default: 0)

    has_many(:tags_subscription, TagSubscription, foreign_key: :title)

    many_to_many(:contents, Content, join_through: "contents_tags")

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(tag, attrs) do
        tag
        # The fields that are allowed for the record.
        |> cast(attrs, [:title, :count, :description, :keywords])
        # The fields are required for recording.
        |> validate_required([:title, :description, :keywords])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(tag, attrs) do
    tag
    |> cast(attrs, [:title, :count])
    |> validate_required([:title])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :tags,
      type: :tag,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:title],
            autocomplete: [:title]
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      cascade_decrement: [
        {Expert, :title}
      ]
    })
  end
end
