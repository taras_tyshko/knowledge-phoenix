defmodule SmileDB.Tags do
  @moduledoc """
    The `Tags` context. This context describes how to use models and to build functions for future use. 
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following model are available for use in the current context:

        alias SmileDB.Tags.Tag
        alias SmileDB.Tags.ContentTag

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo
      
    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.
        
    For example, you can get the data. To do this you need to declare the appropriate for `alias`.
    In our case, you need to call the model: [`Tag`](SmileDB.Tags.Tag.html), [`ContentTag`](SmileDB.Tags.ContentTag.html)
    which will use it to fetch data from the base.

  ## Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Gets or creates [`Tag`](SmileDB.Tags.Tag.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Tags.Tag.html#changeset/2)
  This feature uses the context feature [`create_tag`](SmileDB.Tags.html#create_tag/1)

      alias SmileDB.Tags.Tag
    
      def get_or_insert_tag(title) do
        case Repo.get_by(Tag, title: title) do
          nil -> create_tag(%{title: title})
          tag -> {:ok, tag}
        end
      end
      
      # Entry with this tag exists.
      iex> get_or_insert_tag(test)
      {:ok, %SmileDB.Tags.Tag{}}

      # Entry with this tag does not exist.
      iex> get_or_insert_tag(test1)
      {:ok, %SmileDB.Tags.Tag{}}

  """

  import Ecto.Query, warn: false
  alias SmileDB.Repo
  alias SmileDB.Source.Content
  alias SmileDB.Tags.{Tag, ContentTag}

  @doc """
    The function implements the creation of a single table tags which are in content.

    When a user creates a content or refreshes the data (tags) then this feature deletes all tags that were created by the issue and created them again.

  ## Examples

      iex> Tags.put_tags(%Content{})
      {:ok, %Content{}}

      iex> Tags.put_tags({:ok, %Content{}})
      {:ok, %Content{}}

  """

  @spec put_tags(Content.t() | {:ok, map()} | {:error, map()}) ::
          {:ok, Content.t()} | {:error, map()} | {:error, Ecto.ChangeError.t()}
  def put_tags(object, true), do: put_tags(object)
  def put_tags(object, false), do: object

  def put_tags({:ok, model}), do: put_tags(model)
  def put_tags({:error, details}), do: {:error, details}

  def put_tags(%Content{} = content) do
    ContentTag
    |> where([c], c.content_id == ^content.id)
    |> Repo.all()
    |> Enum.each(&delete_content_tag(&1))

    Enum.each(content.tags, fn tag ->
      case get_or_insert_tag(tag) do
        {:ok, tag} ->
          create_content_tag(%{tag_id: tag.id, content_id: content.id})

        {:error, changeset} ->
          {:error, changeset}
      end
    end)

    {:ok, content}
  end

  @doc """
  Gets or creates a [`Tag`](SmileDB.Tags.Tag.html).

  This feature implements the receipt or creation of a tag if not found with the tag that is accepted.

  ## Examples
      # Tag with this title is a database.
      iex> Tags.get_or_insert_tag(test)
      {:ok, %SmileDB.Tags.Tag{}}

      # Tag with this title is not in a database but it is being created.
      iex> Tags.get_or_insert_tag(test1)
      {:ok, %SmileDB.Tags.Tag{}}

  """
  @spec get_or_insert_tag(String.t()) :: {:ok, Tag.t()} | nil
  def get_or_insert_tag(title) do
    case Repo.get_by(Tag, title: title) do
      nil -> create_tag(%{title: title})
      tag -> {:ok, tag}
    end
  end

  @doc """
  Creates a [`Tag`](SmileDB.Tags.Tag.html).

  ## Examples

      iex> Tags.create_tag(%{field: value})
      {:ok, %SmileDB.Tags.Tag{}}

      iex> Tags.create_tag(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_tag(map()) :: {:ok, Tag.t()} | {:error, Ecto.ChangeError.t()}
  def create_tag(attrs \\ %{}) do
    %Tag{}
    |> Tag.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Creates a [`ContentTag`](SmileDB.Tags.ContentTag.html).

  ## Examples

      iex> Tags.create_content_tag(%{field: new_value})
      {:ok, %SmileDB.Tags.ContentTag{}}

      iex> Tags.create_content_tag(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_content_tag(map()) :: {:ok, ContentTag.t()} | {:error, Ecto.ChangeError.t()}
  def create_content_tag(attrs \\ %{}) do
    %ContentTag{}
    |> ContentTag.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
    Deletes a [`ContentTag`](SmileDB.Tags.ContentTag.html).

  ## Examples

      iex> Tags.delete_content_tag(content_tag)
      {:ok, %SmileDB.Tags.ContentTag{}}

      iex> Tags.delete_content_tag(content_tag)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_content_tag(ContentTag.t()) ::
          {:ok, ContentTag.t()} | {:error, Ecto.ChangeError.t()}
  def delete_content_tag(%ContentTag{} = content_tag) do
    content_tag
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end
end
