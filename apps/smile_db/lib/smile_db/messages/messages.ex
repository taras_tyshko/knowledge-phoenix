defmodule SmileDB.Messages do
  @moduledoc """
    The `Messages` context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

      alias SmileDB.Messages.Room
      alias SmileDB.Messages.Message
      alias SmileDB.Messages.UserRoom

    To work with this schema, you need to use a dependency.

      # To build samples from the database you need to use
      import Ecto.Query, warn: false
      # To work with the repository base you need to use.
      alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data used model `UserRoom`. To do this you need to declare the appropriate for alias.
    In our case, you need to call the model [`UserRoom`](Messenger.Messages.UserRoom.html) which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

      alias SmileDB.Messages.UserRoom

  #### Get a list `UserRoom` by list of arguments.

      def list_user_rooms(attrs) do
        search index: ElasticSearch.get_meta!(UserRoom).index do
          query do
            bool do
              must do
              end
            end
          end
        end
        |> put_in(
          [:search, :query, :bool, :must],
          Enum.map(attrs, fn
            {key, value} when is_list(value) ->
              [terms: [{key, value}]]

            {key, value} ->
              [term: [{key, value}]]
          end)
        )
        |> ElasticSearch.Repo.all()
      end

      iex> list_user_rooms(user_id: 1)
      [...]

      # 123 - Identifier is not valid and return empty list.
      iex> list_user_rooms(user_id: 123)
      []

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Messages

  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.Repo

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  alias SmileDB.Messages.UserRoom

  @doc """
  Gets a List of user rooms who are room users where room_id is the identity of the [`Room`](Messenger.Messages.Room.html).

  ## Examples

      iex> Messages.list_user_rooms(1)
      [...]

      # 123 - Identifier is not valid and return empty list.
      iex> Messages.list_user_rooms(123)
      []

  """
  @spec list_user_rooms(Integer.t()) :: list(Integer.t())
  def list_user_rooms(attrs) do
    search index: ElasticSearch.get_meta!(UserRoom).index do
      query do
        bool do
          must do
          end
        end
      end
    end
    |> put_in(
      [:search, :query, :bool, :must],
      Enum.map(attrs, fn
        {key, value} when is_list(value) ->
          [terms: [{key, value}]]

        {key, value} ->
          [term: [{key, value}]]
      end)
    )
    |> ElasticSearch.Repo.all()
  end

  @doc """
  Creates a [`UserRoom`](Messenger.Messages.UserRoom.html).

  To create a `user_room` need to check with the model [`UserRoom`](Messenger.Messages.UserRoom.html).
  In which there is a function of [`changeset`](Messenger.Messages.UserRoom.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  ## Examples

      iex> Messages.create_user_room(%{field: value})
      {:ok, %UserRoom{}}

      iex> Messages.create_user_room(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user_room(map()) :: UserRoom.t()
  def create_user_room(attrs \\ %{}) do
    %UserRoom{}
    |> UserRoom.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Creates a [`UserRoom`](Messenger.Messages.UserRoom.html).

  To create a `user_room` need to check with the model [`UserRoom`](Messenger.Messages.UserRoom.html).
  In which there is a function of [`changeset`](Messenger.Messages.UserRoom.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  ## Examples

      iex> Messages.create_user_rooms(%{field: value})
      {:ok, %UserRoom{}}

      iex> Messages.create_user_rooms(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user_rooms(list(map())) :: list(UserRoom.t())
  def create_user_rooms(attrs) when is_list(attrs) do
    UserRoom
    |> Repo.insert_all(Enum.map(attrs, &Repo.set_timestamp(&1)), returning: true)
    |> elem(1)
    |> Enum.map(&ElasticSearch.Repo.put(&1))
  end

  @doc """
  Updates a [`UserRoom`](Messenger.Messages.UserRoom.html) data.

  Raises `Ecto.NoResultsError` if the [`UserRoom`](Messenger.Messages.UserRoom.html) does not exist.

  ## Examples

      iex> Messages.update_user_room(user_room, %{field: new_value})
      {:ok, %UserRoom{}}

      iex> Messages.update_user_room(user_room, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_user_room(UserRoom.t(), map()) :: UserRoom.t()
  def update_user_room(%UserRoom{} = user_room, attrs) do
    user_room
    |> UserRoom.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`UserRoom`](Messenger.Messages.UserRoom.html) data.

  Raises `Ecto.NoResultsError` if the [`UserRoom`](Messenger.Messages.UserRoom.html) does not exist.

  ## Examples

      iex> Messages.delete_user_room(user_room)
      {:ok, %UserRoom{}}

      iex> Messages.delete_user_room(user_room)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user_room(UserRoom.t()) :: UserRoom.t()
  def delete_user_room(%UserRoom{} = user_room) do
    user_room
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  @doc """
  Deletes a [`UserRoom`](Messenger.Messages.UserRoom.html) data.

  Raises `Ecto.NoResultsError` if the [`UserRoom`](Messenger.Messages.UserRoom.html) does not exist.

  ## Examples

      iex> Messages.delete_user_room(user_room)
      {:ok, %UserRoom{}}

      iex> Messages.delete_user_room(user_room)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user_rooms(Ecto.Queryable.t()) :: list(UserRoom.t())
  def delete_user_rooms(querytable) do
    querytable
    |> Repo.delete_all(returning: true)
    |> elem(1)
    |> Enum.map(&ElasticSearch.Repo.delete(&1))
  end

  alias SmileDB.Messages.Room

  @doc """
  Gets a `Room` data by `ID`.

  Raises `Ecto.NoResultsError` if the [`Room`](Messenger.Messages.Room.html) does not exist.

  ## Examples

      iex> Messages.get_room!(123)
      %Room{}

      iex> Messages.get_room!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_room!(Integer.t()) :: Room.t()
  def get_room!(id), do: ElasticSearch.Repo.get!(Room, id)

  @doc """
  Creates a room.

  To create a room need to check with the model [`Room`](Messenger.Messages.Room.html).
  In which there is a function of [`changeset`](Messenger.Messages.Room.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  Raises `Ecto.NoResultsError` if the [`Room`](Messenger.Messages.Room.html) does not exist.

  ## Examples

      iex> Messages.create_room(%{field: value})
      {:ok, %Room{}}

      iex> Messages.create_room(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_room(map()) :: Room.t()
  def create_room(attrs \\ %{}) do
    %Room{}
    |> Room.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`Room`](Messenger.Messages.Room.html) data.

  Raises `Ecto.NoResultsError` if the [`Room`](Messenger.Messages.Room.html) does not exist.

  ## Examples

      iex> Messages.update_room(room, %{field: new_value})
      {:ok, %Room{}}

      iex> Messages.update_room(room, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_room(Room.t(), map()) :: Room.t()
  def update_room(%Room{} = room, attrs) do
    room
    |> Room.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Room`](Messenger.Messages.Room.html) data.

  Raises `Ecto.NoResultsError` if the [`Room`](Messenger.Messages.Room.html) does not exist.

  ## Examples

      iex> Messages.delete_room(room)
      {:ok, %Room{}}

      iex> Messages.delete_room(room)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_room(Room.t()) :: Room.t()
  def delete_room(%Room{} = room) do
    with {:ok, room} <-
           room
           |> Repo.delete()
           |> ElasticSearch.Repo.delete() do
      # FilesUploader.delete(FilesUploader.Definition.RoomAvatar, room)
      {:ok, room}
    end
  end

  alias SmileDB.Messages.Message

  @doc """
  Gets a `Message` data by `ID`.

  Raises `Ecto.NoResultsError` if the [`Message`](Messenger.Messages.Message.html) does not exist.

  ## Examples

      iex> Messages.get_message!(123)
      %Message{}

      iex> Messages.get_message!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_message!(Integer.t()) :: Message.t()
  def get_message!(id), do: Repo.get!(Message, id)

  @doc """
  Creates a [`Message`](Messenger.Messages.Message.html).

  To create a `message` need to check with the model [`Message`](Messenger.Messages.Message.html).
  In which there is a function of [`changeset`](Messenger.Messages.Message.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  ## Examples

      iex> Messages.create_message(%{field: value})
      {:ok, %Message{}}

      iex> Messages.create_message(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_message(map()) :: Message.t()
  def create_message(attrs \\ %{}) do
    %Message{}
    |> Message.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`Message`](Messenger.Messages.Message.html) data.

  Raises `Ecto.NoResultsError` if the [`Message`](Messenger.Messages.Message.html) does not exist.

  ## Examples

      iex> Messages.update_message(message, %{field: new_value})
      {:ok, %Message{}}

      iex> Messages.update_message(message, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_message(Message.t(), map()) :: Message.t()
  def update_message(%Message{} = message, attrs) do
    message
    |> Message.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Message`](Messenger.Messages.Message.html) data.

  Raises `Ecto.NoResultsError` if the [`Message`](Messenger.Messages.Message.html) does not exist.
  ## Examples

      iex> Message.delete_message(message)
      {:ok, %Message{}}

      iex> Message.delete_message(message)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_message(Message.t()) :: Message.t()
  def delete_message(%Message{} = message) do
    with {:ok, message} <-
           message
           |> Repo.delete()
           |> ElasticSearch.Repo.delete() do
      # FilesUploader.delete(FilesUploader.Definition.Messenger, message)
      {:ok, message}
    end
  end

  @doc """
  Deletes a [`Message`](Messenger.Messages.Message.html) data.

  Raises `Ecto.NoResultsError` if the [`Message`](Messenger.Messages.Message.html) does not exist.
  ## Examples

      iex> Message.delete_message(message)
      {:ok, %Message{}}

      iex> Message.delete_message(message)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_messages(Ecto.Queryable.t()) :: list(Message.t())
  def delete_messages(querytable) do
    querytable
    |> Repo.delete_all(returning: true)
    |> elem(1)
    |> Enum.map(&ElasticSearch.Repo.delete(&1))
  end

  @doc """
  Gets a last message.

  ## Examples

      iex> get_last_message!(123)
      %Message{}

      iex> get_last_message!(456)
      nil

  """
  @spec get_last_message(Integer.t()) :: Message.t() | nil
  def get_last_message(room_id) do
    search index: ElasticSearch.get_meta!(Message).index do
      query do
        term("room_id", room_id)
      end

      sort do
        [inserted_at: :desc, id: :desc]
      end
    end
    |> ElasticSearch.Repo.all(1)
    |> List.first()
  end
end
