defmodule SmileDB.Messages.Room do
  @moduledoc """
    This module describes the schema `rooms` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Messages.Room

    Examples of features to use this module are presented in the `Messenger.Messages`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Accounts.User
  alias SmileDB.InitialsAvatar
  alias SmileDB.Messages.{Message, UserRoom}

  @typedoc """
    This type describes all the fields that are available in the `rooms` schema and links to other tables in the tray on the Primary key.
  """

  @type t :: %__MODULE__{
          id: integer(),
          name: String.t(),
          type: String.t(),
          uuid: String.t(),
          avatar: String.t(),
          updated_at: timeout(),
          inserted_at: timeout(),
          users: User.t(),
          last_message: map(),
          messages: Message.t()
        }

  schema "rooms" do
    field(:avatar, :string)
    field(:name, :string, default: "")
    field(:type, :string, default: "privat")
    field(:uuid, :string)
    field(:last_message, :map, virtual: true)

    has_many(:messages, Message)

    many_to_many(:users, User, join_through: "user_rooms", on_delete: :delete_all)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(room, attrs) do
          room
          # The fields that are allowed for the record.
          |> cast(attrs, [:name])
          # The fields are required for recording.
          # The fields that should be unique.
          |> unique_constraint(:name)
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(room, attrs) do
    room
    |> cast(attrs, [:name, :type])
    |> check_uuid()
    |> storage_cast(generate_avatar(attrs), [:avatar], type: FilesUploader.Definition.Avatar)
    |> validate_required([:type])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :rooms,
      type: :room,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:name],
            autocomplete: [:name]
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      cascade_delete: [Message, UserRoom]
    })
  end

  def es_encode(model) do
    result = super(model)

    if byte_size(model.name) > 0 do
      result
    else
      Keyword.put(result, :name, model.uuid)
    end
  end

  defp generate_avatar(attrs) do
    case Map.fetch(attrs, :name) do
      {:ok, name} ->
        Map.put(attrs, :avatar, InitialsAvatar.create_avatar_from_initials(name))

      :error ->
        attrs
    end
  end
end
