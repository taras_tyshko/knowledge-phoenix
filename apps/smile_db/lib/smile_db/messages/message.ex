defmodule SmileDB.Messages.Message do
  @moduledoc """
    This module describes the schema `messages` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Messages.Message

    Examples of features to use this module are presented in the `Messenger.Messages`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Accounts.User
  alias SmileDB.Messages.Room
  alias SmileDB.Notifications.MessageNotification

  @typedoc """
    This type describes all the fields that are available in the `messages` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          room_id: integer(),
          event_name: String.t(),
          description: String.t(),
          updated_at: timeout(),
          inserted_at: timeout(),
          room: Room.t(),
          user: User.t(),
          messages_notifications: list(MessageNotification.t())
        }

  schema "messages" do
    field(:event_name, :string)
    field(:description, :string)
    field(:uuid, :string)

    belongs_to(:room, Room)
    belongs_to(:user, User)

    has_many(:messages_notifications, MessageNotification)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(message, attrs) do
        message
        # The fields that are allowed for the record.
        |> cast(attrs, [:description, :user_id, :room_id, :event_name])
        # The fields are required for recording.
        |> validate_required([:description, :user_id, :room_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:description, :user_id, :room_id, :event_name])
    |> check_uuid()
    |> storage_cast(attrs, [:description], type: FilesUploader.Definition.Media)
    |> check_description()
    |> validate_required([:description, :user_id, :room_id])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :messages,
      type: :message,
      fields:
        Entity.Fields.new(%{
          nested: [
            Entity.Fields.Nested.new(%{
              model: Room,
              model_fetch_field: :id,
              field: :last_message,
              current_fetch_field: :room_id
            })
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
