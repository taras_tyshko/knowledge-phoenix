defmodule SmileDB.Messages.UserRoom do
  @moduledoc """
    This module describes the schema `user_rooms` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Messages.UserRoom

    Examples of features to use this module are presented in the `Messenger.Messages`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Accounts.User
  alias SmileDB.Messages.Room

  @typedoc """
    This type describes all the fields that are available in the `user_rooms` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          role: String.t(),
          user_id: integer(),
          room_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          room: Room.t()
        }

  schema "user_rooms" do
    field(:role, :string)

    belongs_to(:user, User)
    belongs_to(:room, Room)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(user_room, attrs) do
        user_room
        # The fields that are allowed for the record.
        |> cast(attrs, [:user_id, :room_id, :type, :role])
        # The fields are required for recording.
        |> validate_required([:user_id, :room_id, :type, :role])
        # The fields that should be unique.
        |> unique_constraint(:user_id_room_id_type)
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(user_room, attrs) do
    user_room
    |> cast(attrs, [:user_id, :room_id, :role])
    |> validate_required([:user_id, :room_id, :role])
    |> unique_constraint(:user_id_room_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :users_rooms,
      type: :users_room,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
