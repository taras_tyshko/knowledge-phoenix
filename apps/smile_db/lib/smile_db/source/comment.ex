defmodule SmileDB.Source.Comment do
  @moduledoc """
    This module describes the schema `comments` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Source.Comment

    Examples of features to use this module are presented in the `SmileDB.Source`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Accounts.User
  alias SmileDB.Ratings.CommentRating
  alias EctoLtree.LabelTree, as: Ltree
  alias SmileDB.Notifications.Notification
  alias SmileDB.Source.{Answer, Feed, Content}

  @typedoc """
    This type describes all the fields that are available in the `comments` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          author: String.t(),
          user_id: integer(),
          keywords: String.t(),
          shadow: boolean(),
          anonymous: boolean(),
          description: String.t(),
          path: list(String.t()),
          answer_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          content_id: integer(),
          ratings_count: integer(),
          user: User.t(),
          answer: Answer.t(),
          content: Content.t()
        }

  schema "comments" do
    field(:anonymous, :boolean, default: false)
    field(:shadow, :boolean, default: false)
    field(:author, :string)
    field(:description, :string)
    field(:keywords, :string)
    field(:path, Ltree)
    field(:ratings_count, :integer, default: 0)
    field(:uuid, :string)

    belongs_to(:answer, Answer)
    belongs_to(:content, Content)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(comment, attrs) do
        comment
        # The fields that are allowed for the record.
        |> cast(attrs, [
          :description, :anonymous, :keywords, :user_id,
          :answer_id, :path, :content_id, :ratings_count, :shadow
        ])
        # The fields are required for recording.
        |> validate_required([:description, :anonymous, :user_id, :content_id, :shadow])
        |> replace_base_to_url
      end

  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(comment, attrs) do
    comment
    |> cast(attrs, [
      :anonymous,
      :keywords,
      :author,
      :shadow,
      :answer_id,
      :path,
      :content_id,
      :ratings_count,
      :user_id
    ])
    |> check_uuid()
    |> storage_cast(attrs, [:description], type: FilesUploader.Definition.Media)
    |> check_description()
    |> validate_required([:description, :anonymous, :author, :content_id, :user_id, :shadow])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :comments,
      type: :comment,
      fields:
        Entity.Fields.new(%{
          convert: [
            path: Type.LTree,
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      trigger_update:
        Entity.TriggerUpdate.new(%{
          counter: [
            Entity.TriggerUpdate.Counter.new(%{
              related_model: User,
              update_field: :comments_count,
              condition: & &1[:user_id]
            }),
            Entity.TriggerUpdate.Counter.new(%{
              related_model: Answer,
              update_field: :comments_count,
              condition: & &1[:answer_id]
            }),
            Entity.TriggerUpdate.Counter.new(%{
              related_model: Content,
              update_field: :comments_count,
              condition: fn comment ->
                unless comment[:answer_id], do: comment[:content_id]
              end
            })
          ]
        }),
      cascade_delete: [Feed, Notification, CommentRating]
    })
  end
end
