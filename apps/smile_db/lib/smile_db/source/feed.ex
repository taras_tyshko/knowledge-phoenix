defmodule SmileDB.Source.Feed do
  @moduledoc """
    This module describes the schema `feeds` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Source.Comment

    Examples of features to use this module are presented in the `SmileDB.Source`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Accounts.User
  alias SmileDB.Source.{Answer, Content, Comment}

  @typedoc """
    This type describes all the fields that are available in the `feeds` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          type: String.t(),
          author: String.t(),
          tags: list(String.t()),
          answer_id: integer(),
          anonymous: boolean(),
          shadow: boolean(),
          inserted_at: timeout(),
          content_id: integer(),
          comment_id: integer(),
          user_id: integer(),
          user: User.t(),
          content: Content.t(),
          answer: Answer.t(),
          comment: Comment.t()
        }

  @primary_key {:id, :string, []}
  schema "feeds" do
    field(:anonymous, :boolean, default: false)
    field(:shadow, :boolean, default: false)
    field(:author, :string)
    field(:type, :string)
    field(:tags, {:array, :string}, default: [])

    belongs_to(:answer, Answer)
    belongs_to(:comment, Comment)
    belongs_to(:content, Content)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime, updated_at: false)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(feed, attrs) do
        feed
        # The fields that are allowed for the record.
        |> cast(attrs, [:id, :author, :anonymous, :shadow, :content_id,
        :answer_id, :comment_id, :tags, :user_id, :inserted_at,
        :type
        ])
        # The fields are required for recording.
        |> validate_required([:id, :author, :anonymous, :shadow, :user_id, :type])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(feed, attrs) do
    feed
    |> cast(attrs, [
      :id,
      :author,
      :anonymous,
      :shadow,
      :content_id,
      :answer_id,
      :comment_id,
      :type,
      :tags,
      :user_id,
      :inserted_at
    ])
    |> validate_required([:id, :author, :anonymous, :shadow, :user_id, :type])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :knowledge_phoenix,
      type: :feed,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime
          ]
        }),
      reindex_function: &SmileDB.Source.put_feed/1
    })
  end

  def source_query do
    [
      from(co in Content, select: co),
      from(a in Answer, select: a),
      from(c in Comment, select: c)
    ]
  end
end
