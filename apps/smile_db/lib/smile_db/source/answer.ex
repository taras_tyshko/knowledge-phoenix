defmodule SmileDB.Source.Answer do
  @moduledoc """
    This module describes the schema `answers` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Source.Answer

    Examples of features to use this module are presented in the `SmileDB.Source`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Accounts.User
  alias SmileDB.Ratings.AnswerRating
  alias SmileDB.Source.{Content, Comment, Feed}
  alias SmileDB.Notifications.{Notification, AnswerNotification}

  @typedoc """
    This type describes all the fields that are available in the `answers` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          author: String.t(),
          keywords: String.t(),
          tags: list(String.t()),
          shadow: boolean(),
          anonymous: boolean(),
          description: String.t(),
          comments_count: integer(),
          ratings_count: integer(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          content_id: integer(),
          user: User.t(),
          comments: Comment.t(),
          content: Content.t(),
          answers_rating: AnswerRating.t(),
          answers_notification: AnswerNotification.t()
        }

  schema "answers" do
    field(:anonymous, :boolean, default: false)
    field(:shadow, :boolean, default: false)
    field(:author, :string)
    field(:comments_count, :integer, default: 0)
    field(:description, :string)
    field(:keywords, :string)
    field(:ratings_count, :integer, default: 0)
    field(:tags, {:array, :string})
    field(:uuid, :string)

    belongs_to(:content, Content)
    belongs_to(:user, User)

    has_many(:comments, Comment)
    has_many(:answers_rating, AnswerRating)
    has_many(:answers_notification, AnswerNotification)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(answer, attrs) do
        answer
        # The fields that are allowed for the record.
        |> cast(attrs, [
          :anonymous, :tags, :description, :keywords, :user_id,
          :content_id,  :comments_count, :ratings_count, :shadow
        ])
        # The fields are required for recording.
        |> validate_required([:anonymous, :description, :user_id, :content_id. :shadow])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(answer, attrs) do
    answer
    |> cast(attrs, [
      :anonymous,
      :tags,
      :keywords,
      :author,
      :shadow,
      :content_id,
      :comments_count,
      :ratings_count,
      :user_id
    ])
    |> check_uuid()
    |> storage_cast(attrs, [:description], type: FilesUploader.Definition.Media)
    |> check_description()
    |> validate_required([:anonymous, :description, :author, :content_id, :user_id, :shadow])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :answers,
      type: :answer,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      trigger_update:
        Entity.TriggerUpdate.new(%{
          counter: [
            Entity.TriggerUpdate.Counter.new(%{
              related_model: User,
              update_field: :answers_count,
              condition: & &1[:user_id]
            }),
            Entity.TriggerUpdate.Counter.new(%{
              related_model: Content,
              update_field: :answers_count,
              condition: & &1[:content_id]
            })
          ]
        }),
      cascade_delete: [Comment, Feed, AnswerRating, Notification]
    })
  end
end
