defmodule SmileDB.Source do
  @moduledoc """
    The Content context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias SmileDB.Source.Content
        alias SmileDB.Source.Answer
        alias SmileDB.Source.Comment

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data used by `content.id`. To do this you need to declare the appropriate for alias.
    In our case, you need to call the model: [`Content`](SmileDB.Source.Content.html), [`Answer`](SmileDB.Source.Answer.html) or [`Comment`](SmileDB.Source.Comment.html)
    which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Get a content.

      alias SmileDB.Source.Content

      def get_content!(id) do
        Content
        |> where([c], c.id == ^id)
        |> Repo.one
      end

      iex> get_content!(123)
      %Content{}

      iex> get_content!(456)
      ** (Ecto.NoResultsError)

  #### Get a Answer.

      alias SmileDB.Source.Answer

      def get_answer!(content_id) do
        Answer
        |> where([a], a.content_id == ^content_id)
        |> Repo.one
      end

      iex> get_answer!(1)
      %Answer{}

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Source

      iex> Source.get_content!(123)
      %Content{}

      iex> Source.get_content!(456)
      ** (Ecto.NoResultsError)

      iex> Source.get_answer!(123)
      %Setting{}

      iex> Source.get_answer!(456)
      ** (Ecto.NoResultsError)
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  alias SmileDB.Repo

  alias SmileDB.{Accounts, Tags}
  alias SmileDB.Source.{Feed, Content, Answer, Comment}

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Gets a single Content

  Raises `Ecto.NoResultsError` if the [`Content`](SmileDB.Source.Content.html) does not exist.

  ## Examples

      iex> Source.get_content!(123)
      %Content{}

      iex> Source.get_content!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_content!(Integer.t()) :: Content.t()
  def get_content!(id) do
    Repo.get!(Content, id)
  end

  @doc """
  Creates a [`Content`](SmileDB.Source.Content.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Source.Content.html#changeset/2).

  ## Examples

      iex> Source.create_content(%{field: value})
      {:ok, %Content{}}

      iex> Source.create_content(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_content(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_content(attrs \\ %{}) do
    %Content{}
    |> Content.changeset(attrs)
    |> Repo.insert()
    |> Tags.put_tags()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`Content`](SmileDB.Source.Content.html).

  ## Examples

      iex> Source.update_content(content, %{field: new_value})
      {:ok, %Content{}}

      iex> Source.update_content(content, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_content(Content.t(), map()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_content(%Content{} = content, attrs) do
    content
    |> Content.changeset(attrs)
    |> Repo.update()
    |> Tags.put_tags(Map.has_key?(attrs, :tags))
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Content`](SmileDB.Source.Content.html).

  ## Examples

      iex> Source.delete_content(content)
      {:ok, %Content{}}

      iex> Source.delete_content(content)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_content(Content.t()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_content(%Content{} = content) do
    with {:ok, content} <- Repo.delete(content) do
      # FilesUploader.delete(FilesUploader.Definition.Media, content)
      {:ok, ElasticSearch.Repo.delete(content)}
    end
  end

  @doc """
  Gets a single answer with preloaded [`User`](SmileDB.Accounts.User.html).

  Raises `Ecto.NoResultsError` if the [`Answer`](SmileDB.Source.Answer.html) does not exist.

  ## Examples

      iex> Source.get_answer!(123)
      %Answer{}

      iex> Source.get_answer!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_answer!(Integer.t()) :: Answer.t()
  def get_answer!(id) do
    Repo.get!(Answer, id)
  end

  @doc """
    Gets a list answer ID for [`Content`](SmileDB.Source.Content.html).

  ## Examples

      iex> Source.list_answers_id!(123)
      [1,2,3]

      iex> Source.list_answers_id!(456)
      []

  """
  @spec list_answers_id!(Integer.t(), Integer.t()) :: list(Integer.t())
  def list_answers_id!(content_id, user) do
    query =
      search index: ElasticSearch.get_meta!(Answer).index do
        query do
          bool do
            must do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          [inserted_at: :asc, id: :desc]
        end
      end

    if user && user.status == "hidden" do
      put_in(query, [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
        term: [user_id: user.id]
      )
    else
      query
    end
    |> ElasticSearch.Repo.all()
    |> Enum.map(& &1.id)
  end

  @doc """
  Creates a [`Answer`](SmileDB.Source.Answer.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Source.Answer.html#changeset/2).

  ## Examples

      iex> Source.create_answer(%{field: value})
      {:ok, %Answer{}}

      iex> Source.create_answer(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_answer(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_answer(attrs \\ %{}) do
    %Answer{}
    |> Answer.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`Answer`](SmileDB.Source.Answer.html).

  ## Examples

      iex> Source.update_answer(answer, %{field: new_value})
      {:ok, %Answer{}}

      iex> Source.update_answer(answer, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_answer(Answer.t(), map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_answer(%Answer{} = answer, attrs) do
    answer
    |> Answer.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Answer`](SmileDB.Source.Answer.html).

  ## Examples

      iex> Source.delete_answer(answer)
      {:ok, %Answer{}}

      iex> Source.delete_answer(answer)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_answer(Answer.t()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_answer(%Answer{} = answer) do
    with {:ok, answer} <- Repo.delete(answer) do
      # Delete folder with files
      # FilesUploader.delete(FilesUploader.Definition.Media, answer)
      {:ok, ElasticSearch.Repo.delete(answer)}
    end
  end

  @doc """
  Returns the list of [`Comment`](SmileDB.Source.Comment.html).

  ## Examples

      iex> Source.list_comments(%Comment{})
      [%Comment{}, ...]

  """
  @spec list_comments(Comment.t()) :: list({:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()})
  def list_comments(comment) do
    search index: ElasticSearch.get_meta!(comment).index do
      query do
        bool do
          must do
            term("path", comment.id)
          end

          must_not do
            term("shadow", true)
          end
        end
      end
    end
    |> ElasticSearch.Repo.all()
  end

  @doc """
  The function to be called with the aggregate batch information.

  It comes in both a 2 tuple and 3 tuple form. The first two elements are the module and function name.
  The third element is an arbitrary parameter that is passed as the first argument to the batch function.
  """
  def comments_by_id(_, comment_ids) do
    comments =
      search index: ElasticSearch.get_meta!(Comment).index do
        query do
          terms("id", comment_ids)
        end
      end
      |> ElasticSearch.Repo.all()

    Map.new(comments, fn comment -> {comment.id, comment} end)
  end

  @doc """
  Gets a single [`Comment`](SmileDB.Source.Comment.html).

  Raises `Ecto.NoResultsError` if the [`Comment`](SmileDB.Source.Comment.html) does not exist.

  ## Examples

      iex> Source.get_comment!(123)
      %Comment{}

      iex> Source.get_comment!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_comment!(Integer.t()) :: Comment.t()
  def get_comment!(id) do
    Repo.get!(Comment, id)
  end

  @doc """
  Creates a [`Comment`](SmileDB.Source.Comment.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Source.Comment.html#changeset/2).

  ## Examples

      iex> Source.create_comment(%{field: value})
      {:ok, %Comment{}}

      iex> Source.create_comment(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_comment(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_comment(attrs \\ %{}) do
    with {:ok, comment} <-
           %Comment{}
           |> Comment.changeset(attrs)
           |> Repo.insert() do
      path =
        if comment.path do
          [comment.path.labels | ["#{comment.id}"]]
          |> List.flatten()
          |> Enum.join(".")
        else
          "#{comment.id}"
        end

      comment
      |> Comment.changeset(%{path: path})
      |> Repo.update()
      |> ElasticSearch.Repo.put()
    end
  end

  @doc """
  Updates a [`Comment`](SmileDB.Source.Comment.html).

  ## Examples

      iex> Source.update_comment(comment, %{field: new_value})
      {:ok, %Comment{}}

      iex> Source.update_comment(comment, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_comment(Comment.t(), map()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_comment(%Comment{} = comment, attrs) do
    comment
    |> Comment.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`Comment`](SmileDB.Source.Comment.html).

  ## Examples

      iex> Source.delete_comment(comment)
      {:ok, %Comment{}}

      iex> Source.delete_comment(comment)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_comment(Comment.t()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_comment(%Comment{} = comment) do
    with {:ok, comment} <- Repo.delete(comment) do
      Comment
      |> where([c], fragment("? <@ ?", c.path, ^Enum.join(comment.path.labels, ".")))
      |> Repo.delete_all()

      search index: ElasticSearch.get_meta!(Comment).index do
        query do
          term("path", comment.id)
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.each(&ElasticSearch.Repo.delete(&1))

      # Delete folder with files
      # FilesUploader.delete(FilesUploader.Definition.Media, comment)
      {:ok, comment}
    end
  end

  @doc """
  Creates a [`Feed`](SmileDB.Source.Feed.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Source.Feed.html#changeset/2).

  ## Examples

      iex> Source.create_feed(%{field: value})
      {:ok, %Feed{}}

      iex> Source.create_feed(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec put_feed(map()) :: map()
  def put_feed(model) do
    %Feed{}
    |> Feed.changeset(set_feed_fields(model))
    |> Ecto.Changeset.apply_changes()
    |> ElasticSearch.Repo.put()
  end

  @spec set_feed_fields(map()) :: map()
  defp set_feed_fields(%Content{} = content) do
    %{
      id: Repo.replace_id(content).id,
      content_id: content.id,
      user_id: content.user_id,
      author: content.author,
      shadow: content.shadow,
      anonymous: content.anonymous,
      type: content.type,
      tags: content.tags,
      inserted_at: content.inserted_at
    }
  end

  defp set_feed_fields(%Answer{} = answer) do
    %{
      id: Repo.replace_id(answer).id,
      answer_id: answer.id,
      content_id: answer.content_id,
      user_id: answer.user_id,
      author: answer.author,
      shadow: answer.shadow,
      anonymous: answer.anonymous,
      tags: answer.tags,
      inserted_at: answer.inserted_at
    }
  end

  defp set_feed_fields(%Comment{} = comment) do
    %{
      id: Repo.replace_id(comment).id,
      comment_id: comment.id,
      content_id: comment.content_id,
      user_id: comment.user_id,
      author: comment.author,
      shadow: comment.shadow,
      anonymous: comment.anonymous,
      inserted_at: comment.inserted_at
    }
  end

  defp set_feed_fields(model) do
    raise "Feed does not support #{model.__struct__} structure type."
  end

  @doc """
  Function to handle the nickname of the users.

  It receives data from the model description and checks
  it through a regular expression for the presence in the database.

  Returns a list of nicknames that exists in database.

  ## Parameters

    - model: [`Content`](SmileDB.Source.Content.html), [`Answer`](SmileDB.Source.Answer.html) or [`Comment`](SmileDB.Source.Comment.html) schema map data coming from a repository
    - device: Type of user agent, this is optional parameter

  ## Examples

  For example, assume that the database has a user with the nickname `testnickname`

      model = %Comment{id: 1, description: "@testnickname, hello!"}
      Nickname.render_nicknames_from_description(model)
      [
        %{
          id: id,
          first_name: first_name,
          last_name: last_name,
          nickname: nickname,
          avatar: avatar,
          expert: expert,
          inserted_at: inserted_at
        }
      ]

  If the regular expression does not find anything, then the result will return a blank list

      model = %Comment{id: 1, description: "hello!"}
      Nickname.render_nicknames_from_description(model)
      []

  """
  @spec render_nicknames_from_description(%{description: String.t(), event_name: String.t()}) ::
          %{desc: String.t(), users: list(Accounts.User.t())}
  def render_nicknames_from_description(%{description: description, event_name: event_name})
      when not is_nil(event_name) and not is_nil(description) do
    users =
      ~r/(?<=user:)([0-9*]+)/m
      |> Regex.scan(description)
      |> List.flatten()
      |> Enum.uniq()
      |> Enum.map(&Accounts.get_user!(String.to_integer(&1)))
      |> Enum.filter(fn user -> not is_nil(user) end)

    %{desc: description, users: users}
  end

  @spec render_nicknames_from_description(String.t()) :: String.t()
  def render_nicknames_from_description(%{description: description})
      when not is_nil(description) do
    key = Application.get_env(:smile_db, :content_user)[:secret_key] |> Regex.escape()

    ids =
      ~r/(?<=#{key}_{"user":)([0-9*]+)/m
      |> Regex.scan(description)
      |> List.flatten()
      |> Enum.uniq()

    Enum.reduce(ids, %{desc: description, users: []}, fn id, %{desc: description, users: users} ->
      user = SmileDB.Accounts.get_user!(id)

      description =
        String.replace(
          description,
          "#{Application.get_env(:smile_db, :content_user)[:secret_key]}_{\"user\":#{id}}",
          "@#{user.nickname}"
        )

      %{desc: description, users: [user | users]}
    end)
  end

  def render_nicknames_from_description(_), do: %{desc: "", users: []}
end
