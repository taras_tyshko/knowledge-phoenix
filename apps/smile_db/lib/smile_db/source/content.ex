defmodule SmileDB.Source.Content do
  @moduledoc """
    This module describes the schema `contents` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Source.Content

    Examples of features to use this module are presented in the `SmileDB.Source`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Files
  alias SmileDB.Accounts.User
  alias SmileDB.Category.Office
  alias SmileDB.Tags.ContentTag
  alias SmileDB.Ratings.OfficeRating
  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Notifications.Notification
  alias SmileDB.Source.{Answer, Feed, Comment}
  alias SmileDB.Contents.{Meto, RelatedContent}
  alias SmileDB.Subscriptions.ContentSubscription

  @typedoc """
    This type describes all the fields that are available in the `contents` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          uuid: String.t(),
          title: String.t(),
          shadow: boolean(),
          author: String.t(),
          status: String.t(),
          requests: integer(),
          keywords: String.t(),
          anonymous: boolean(),
          files_visible: boolean(),
          tags: list(String.t()),
          description: String.t(),
          type: String.t(),
          reviews: list(integer()),
          reviews_count: integer(),
          answers_count: integer(),
          comments_count: integer(),
          permission_tags: list(String.t()),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          answers: Answer.t(),
          comments: Comment.t(),
          contents: __MODULE__.t(),
          offices_rating: OfficeRating.t(),
          me_to: Meto.t(),
          related_contents: RelatedContent.t(),
          contents_subscription: ContentSubscription.t()
        }

  schema "contents" do
    field(:anonymous, :boolean, default: false)
    field(:answers_count, :integer, default: 0)
    field(:author, :string)
    field(:description, :string)
    field(:keywords, :string)
    field(:requests, :integer)
    field(:reviews, {:array, :integer})
    field(:reviews_count, :integer, default: 0)
    field(:comments_count, :integer, default: 0)
    field(:shadow, :boolean, default: false)
    field(:files_visible, :boolean, default: true)
    field(:status, :string, default: "open")
    field(:tags, {:array, :string}, default: [])
    field(:permission_tags, {:array, :string}, default: [])
    field(:title, :string)
    field(:type, :string, default: "question")
    field(:uuid, :string)

    belongs_to(:user, User)
    belongs_to(:office, Office, type: :string)

    embeds_many(:files, Files.Content, on_replace: :delete)

    has_many(:me_to, Meto)
    has_many(:answers, Answer)
    has_many(:comments, Comment)
    has_many(:contents, RelatedContent)
    has_many(:offices_rating, OfficeRating)
    has_many(:related_contents, RelatedContent)
    has_many(:contents_subscription, ContentSubscription)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(content, attrs) do
        content
        # The fields that are allowed for the record.
        |> cast(attrs, [
          :title, :reviews, :requests, :tags, :shadow,
          :anonymous, :me_to, :description, :keywords,
          :user_id, answers_count, :reviews_count, :status,
          :type, :comments_count, :files_visible
        ])
        # The fields are required for recording.
        |> validate_required([:title, :anonymous, :user_id, :author, :shadow, :type, :files_visible])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(content, attrs) do
    content
    |> cast(attrs, [
      :anonymous,
      :answers_count,
      :author,
      :comments_count,
      :keywords,
      :office_id,
      :files_visible,
      :requests,
      :reviews,
      :permission_tags,
      :reviews_count,
      :shadow,
      :status,
      :tags,
      :title,
      :type,
      :user_id
    ])
    |> check_uuid()
    |> storage_cast(attrs, [:description], type: FilesUploader.Definition.Media)
    |> check_description()
    |> validate_required([:anonymous, :author, :shadow, :title, :type, :user_id])
    |> storage_embed_cast(attrs, [:files], type: FilesUploader.Definition.Sources)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :contents,
      type: :content,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:title],
            new_autocomplete: [:title]
          ],
          convert: [
            files: Type.File,
            shadow: Type.Boolean,
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      trigger_update:
        Entity.TriggerUpdate.new(%{
          counter: [
            Entity.TriggerUpdate.Counter.new(%{
              related_model: User,
              update_field: :contents_count,
              condition: & &1[:user_id]
            })
          ]
        }),
      cascade_delete: [
        ContentTag,
        Answer,
        Comment,
        Meto,
        Feed,
        Notification,
        OfficeRating
      ]
    })
  end
end
