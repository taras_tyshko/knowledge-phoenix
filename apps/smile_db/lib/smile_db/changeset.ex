defmodule SmileDB.Changeset do
  defmacro __using__(_opts) do
    quote do
      import Ecto.Changeset
      import SmileDB.Changeset
      use FilesUploader.Ecto.Schema
      use ElasticSearch.Ecto.Schema
    end
  end

  defmacro check_uuid(changeset) do
    quote bind_quoted: [changeset: changeset] do
      case get_field(changeset, :uuid) do
        nil ->
          force_change(changeset, :uuid, UUID.uuid1())

        _ ->
          changeset
      end
    end
  end

  defmacro storage_embed_cast(changeset_or_data, params, fields, options \\ []) do
    quote bind_quoted: [
            changeset_or_data: changeset_or_data,
            params: params,
            fields: fields,
            options: options
          ] do
      model =
        case changeset_or_data do
          %Ecto.Changeset{} -> Ecto.Changeset.apply_changes(changeset_or_data)
          %{__meta__: _} -> changeset_or_data
        end

      [fields]
      |> List.flatten()
      |> Enum.reduce(change(changeset_or_data), fn field, changeset ->
        with true <- changeset.valid?,
             scope <- FilesUploader.Scope.get!(model),
             files when not is_nil(files) <- Map.get(model, field),
             %{add: create, remove: delete} <- get_in(params, [field]) do
          {new_files, changeset} =
            Enum.map(create, fn %{filename: filename, path: path} ->
              SmileDB.Files.create(%{
                path: path,
                name: filename,
                extension:
                  ~r/\.([a-zA-Z]{1,})$/
                  |> Regex.run(filename)
                  |> List.last()
              })
            end)
            |> Enum.map_reduce(changeset, fn file_changeset, acc_changeset ->
              case file_changeset.valid? do
                true ->
                  {file_changeset, acc_changeset}

                false ->
                  {file_changeset,
                   traverse_errors(file_changeset, fn {msg, _opts} -> msg end)
                   |> Enum.reduce(acc_changeset, fn {k, v}, acc ->
                     add_error(acc, field, "#{k} #{to_string(v)}")
                   end)}
              end
            end)

          case changeset.valid? do
            true ->
              files_acc =
                Enum.reduce(delete, files, fn file_id, acc ->
                  with index when is_integer(index) <- Enum.find_index(acc, &(&1.id == file_id)),
                       :ok <-
                         FilesUploader.delete(options[:type], Enum.at(files, index).name, scope) do
                    List.delete_at(acc, index)
                  else
                    _ -> acc
                  end
                end)

              files =
                new_files
                |> Enum.map(fn %{changes: %{path: tmp_file_path, name: filename}} = file_changeset ->
                  path =
                    FilesUploader.store(
                      options[:type],
                      %Plug.Upload{path: tmp_file_path, filename: filename},
                      scope
                    )

                  put_change(file_changeset, :path, path)
                end)
                |> List.flatten(files_acc)

              put_change(changeset, field, files)

            false ->
              changeset
          end
        else
          _ -> changeset
        end
      end)
    end
  end

  defmacro check_description(changeset) do
    quote bind_quoted: [changeset: changeset] do
      case get_change(changeset, :description) do
        nil ->
          changeset

        description ->
          nicknames =
            ~r/(?<=^@{1}|\W@{1})[^\d\n\f\r\s\t\vA-Z!@#$%^&*()_\-+=;:'"?.>,<`~{}[\]]{5,}(?=\W|$)/m
            |> Regex.scan(description)
            |> List.flatten()

          description =
            Enum.reduce(nicknames, description, fn nickname, acc ->
              with user <- SmileDB.Accounts.get_user_by_nickname(nickname),
                   true <- not is_nil(user) do
                String.replace(
                  acc,
                  "@#{nickname}",
                  "#{Application.get_env(:smile_db, :content_user)[:secret_key]}_#{
                    Jason.encode!(%{user: user.id})
                  }"
                )
              else
                _ -> acc
              end
            end)

          force_change(changeset, :description, description)
      end
    end
  end
end
