defmodule SmileDB.Accounts.Setting do
  @moduledoc """
    This module describes the schema `settings` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Accounts.Setting

    Examples of features to use this module are presented in the `SmileDB.Accounts`
  """
  @typedoc """
    This type describes all the fields that are available in the `settings` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User

  @type t :: %__MODULE__{
          id: integer(),
          by_answers: boolean(),
          by_comments: boolean(),
          email_enable: boolean(),
          email_on_reply: boolean(),
          web_notif_upvote: boolean(),
          web_notif_mentions: boolean(),
          web_notif_recommend: boolean(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t()
        }

  schema "settings" do
    field(:by_answers, :boolean, default: true)
    field(:by_comments, :boolean, default: true)
    field(:email_enable, :boolean, default: true)
    field(:email_on_reply, :boolean, default: true)
    field(:web_notif_mentions, :boolean, default: true)
    field(:web_notif_recommend, :boolean, default: true)
    field(:web_notif_upvote, :boolean, default: true)
    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(setting, attrs) do
        setting
        # The fields that are allowed for the record.
        |> cast(attrs, [ 
          :by_answers, :email_enable, :email_on_reply, :user_id,
          :web_notif_upvote, :web_notif_recommend, :web_notif_mentions,
        ])
        # The fields are required for recording.
        |> validate_required([
          :by_comments, :by_answers,  :email_enable, :email_on_reply,
          :web_notif_upvote, :web_notif_recommend, :web_notif_mentions,
          :user_id
        ])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(setting, attrs) do
    setting
    |> cast(attrs, [
      :by_comments,
      :by_answers,
      :email_enable,
      :email_on_reply,
      :web_notif_upvote,
      :web_notif_recommend,
      :web_notif_mentions,
      :user_id
    ])
    |> validate_required([
      :by_comments,
      :by_answers,
      :email_enable,
      :email_on_reply,
      :web_notif_upvote,
      :web_notif_recommend,
      :web_notif_mentions,
      :user_id
    ])
  end
end
