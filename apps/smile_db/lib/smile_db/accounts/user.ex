defmodule SmileDB.Accounts.User do
  @moduledoc """
    This module describes the schema `users` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Accounts.User

    Examples of features to use this module are presented in the `SmileDB.Accounts`
  """

  @typedoc """
    This type describes all the fields that are available in the `"users"` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}

  alias SmileDB.Contents.Meto
  alias SmileDB.Achievements.UserAchieve
  alias SmileDB.Accounts.{Setting, Role}
  alias SmileDB.Users.{Expert, ChangePassword}
  alias SmileDB.Messages.{Message, Room, UserRoom}
  alias SmileDB.Source.{Content, Answer, Comment, Feed}
  alias SmileDB.Ratings.{AnswerRating, CommentRating, UserRating}
  alias SmileDB.Subscriptions.{ContentSubscription, UserSubscription, TagSubscription}

  alias SmileDB.Notifications.{
    Notification,
    AnswerNotification,
    CommentNotification,
    MessageNotification
  }

  @type t :: %__MODULE__{
          id: integer(),
          uuid: String.t(),
          name: String.t(),
          role: String.t(),
          email: String.t(),
          avatar: String.t(),
          gender: String.t(),
          status: String.t(),
          website: String.t(),
          password: String.t(),
          nickname: String.t(),
          location: String.t(),
          birthday: String.t(),
          biography: String.t(),
          last_name: String.t(),
          first_name: String.t(),
          google_uid: String.t(),
          likes_count: String.t(),
          updated_at: timeout(),
          start_expert: timeout(),
          ban_time: timeout(),
          expert: list(String.t()),
          inserted_at: timeout(),
          facebook_uid: String.t(),
          auth_provider: String.t(),
          password_hash: String.t(),
          answers_count: integer(),
          comments_count: integer(),
          contents_count: integer(),
          role_name: Role.t(),
          me_to: Meto.t(),
          rooms: Room.t(),
          answers: Answer.t(),
          setting: Setting.t(),
          messages: Message.t(),
          comments: Comment.t(),
          contents: Content.t(),
          expert_requests: Expert.t(),
          answers_rating: AnswerRating.t(),
          comments_rating: CommentRating.t(),
          change_password: ChangePassword.t(),
          tags_subscription: TagSubscription.t(),
          users_subscription: UserSubscription.t(),
          answers_notification: AnswerNotification.t(),
          comments_notification: CommentNotification.t(),
          messages_notifications: MessageNotification.t(),
          contents_subscription: ContentSubscription.t()
        }

  schema "users" do
    field(:answers_count, :integer, default: 0)
    field(:auth_provider, :string)
    field(:avatar, :string)
    field(:ban_time, :utc_datetime)
    field(:biography, :string)
    field(:birthday, :string)
    field(:comments_count, :integer, default: 0)
    field(:email, :string)
    field(:expert, {:array, :string}, default: [])
    field(:facebook_uid, :string)
    field(:first_name, :string)
    field(:gender, :string)
    field(:google_uid, :string)
    field(:last_name, :string)
    field(:likes_count, :integer, default: 0)
    field(:location, :string)
    field(:name, :string, virtual: true)
    field(:nickname, :string)
    field(:password, :string, virtual: true)
    field(:password_hash, :string)
    field(:start_expert, :utc_datetime)
    field(:status, :string, default: "active")
    field(:contents_count, :integer, default: 0)
    field(:uuid, :string)
    field(:website, :string)

    belongs_to(:role_name, Role, type: :string, references: :role, foreign_key: :role)

    has_one(:setting, Setting)

    has_many(:answers, Answer)
    has_many(:answers_notification, AnswerNotification)
    has_many(:answers_rating, AnswerRating)
    has_many(:comments, Comment)
    has_many(:comments_notification, CommentNotification)
    has_many(:comments_rating, CommentRating)
    has_many(:change_password, ChangePassword)
    has_many(:expert_requests, Expert)
    has_many(:messages, Message)
    has_many(:messages_notifications, MessageNotification)
    has_many(:me_to, Meto)
    has_many(:contents, Content)
    has_many(:contents_subscription, ContentSubscription)
    has_many(:tags_subscription, TagSubscription)
    has_many(:user_achieves, UserAchieve)
    has_many(:users_subscription, UserSubscription)

    many_to_many(:rooms, Room, join_through: "user_rooms")

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(user, attrs) do
        user
        # The fields that are allowed for the record.
        |> cast(attrs, [ :first_name, :last_name, :nickname, :email,
          :auth_provider, :facebook_uid, :google_uid, :avatar, :expert,
          :gender, :birthday, :location, :status, :comments_count,
          :contents_count, :answers_count,:likes_count, :biography,
          :website, :updated_at, :password_hash, :role, :start_expert
        ])
        # The fields are required for recording.
        |> validate_required([:first_name, :last_name, :nickname, :email, :auth_provider, :avatar])
        # The fields that should be unique.
        |> unique_constraint(:nickname)
        |> unique_constraint(:facebook_uid)
        |> unique_constraint(:google_uid)
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :first_name,
      :last_name,
      :nickname,
      :password_hash,
      :password,
      :email,
      :auth_provider,
      :facebook_uid,
      :google_uid,
      :expert,
      :gender,
      :birthday,
      :location,
      :status,
      :comments_count,
      :contents_count,
      :answers_count,
      :likes_count,
      :biography,
      :website,
      :updated_at,
      :start_expert,
      :ban_time
    ])
    |> check_role()
    |> validate_length(:password, min: 5, max: 20)
    |> unique_constraint(:nickname)
    |> unique_constraint(:facebook_uid)
    |> unique_constraint(:google_uid)
    |> check_uuid()
    |> storage_cast(attrs, [:avatar], type: FilesUploader.Definition.Avatar)
    |> gen_nickname()
    |> validate_required([
      :first_name,
      :last_name,
      :nickname,
      :uuid,
      :email,
      :avatar
    ])
    |> prepare_changes(&ElasticSearch.Ecto.Schema.trigger_update/1)
    |> put_password_hash()
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :users,
      type: :user,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:name, :nickname],
            autocomplete: [:name, :nickname]
          ],
          convert: [
            ban_time: Type.DateTime,
            start_expert: Type.DateTime,
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      trigger_update:
        Entity.TriggerUpdate.new(%{
          fields: [
            nickname: [
              {Content, "author"},
              {Answer, "author"},
              {Comment, "author"},
              {Feed, "author"}
            ]
          ]
        }),
      cascade_delete: [
        Meto,
        Feed,
        Answer,
        Expert,
        Comment,
        Message,
        UserRoom,
        Content,
        UserRating,
        UserAchieve,
        Notification
      ]
    })
  end

  @doc """
    This function implements the creation of a virtual name field, and generates the content of the next type.

    The field name will be generated based on the fields which comes in  [`Ecto.Changeset`](https://hexdocs.pm/ecto/Ecto.Changeset.html) and is created from fields `:first_name`, `:last_name`.

  ## Examples

      iex> User.es_encode(%User{first_name: "First", last_name: "Last"})
      %User{
        name: "First Last"
      }
  """
  @spec es_encode(__MODULE__.t()) :: __MODULE__.t()
  def es_encode(model) do
    data = super(model)
    Keyword.put(data, :name, "#{data[:first_name]} #{data[:last_name]}")
  end

  defp gen_nickname(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{nickname: nickname}} ->
        put_change(changeset, :nickname, String.downcase(nickname))

      %Ecto.Changeset{
        valid?: true,
        data: %{nickname: nickname},
        changes: %{first_name: first, last_name: last}
      }
      when is_nil(nickname) ->
        put_change(changeset, :nickname, SmileDB.Accounts.gen_nickname(first, last))

      _ ->
        changeset
    end
  end

  @spec put_password_hash(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: pass}} ->
        put_change(changeset, :password_hash, Comeonin.Pbkdf2.hashpwsalt(pass))

      _ ->
        changeset
    end
  end

  @spec check_role(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp check_role(changeset) do
    case get_field(changeset, :role) do
      nil ->
        force_change(changeset, :role, "customer")

      _ ->
        changeset
    end
  end
end
