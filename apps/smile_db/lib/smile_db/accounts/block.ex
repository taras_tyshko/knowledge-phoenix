defmodule SmileDB.Accounts.Block do
  use GenServer

  alias SmileDB.Accounts

  def init(state), do: {:ok, state}

  def handle_cast({:ban, _user_id}, state) do
    {:noreply, state}
  end

  def handle_cast({:ban, user_id, time}, state) do
    new_state =
      [
        {
          (DateTime.utc_now()
            |> DateTime.to_unix()) + time,
          user_id
        }
        | state
      ]

    {:noreply, new_state}
  end

  def handle_cast({:unban, user_id}, state) do
    Accounts.unban_account(user_id)
    {:noreply, state}
  end

  def handle_info(:unban, state) do
    {data, state} =
      state
      |> Enum.sort()
      |> List.pop_at(0)

    if data do
      user_id = elem(data, 1)
      GenServer.cast(__MODULE__, {:unban, user_id})
    end

    {:noreply, state}
  end

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def ban(user_id, nil), do: GenServer.cast(__MODULE__, {:ban, user_id})

  def ban(user_id, time) do
    with :ok <- GenServer.cast(__MODULE__, {:ban, user_id, time}) do
      unban_schedule(time)
      :ok
    end
  end

  def unban(user_id), do: GenServer.cast(__MODULE__, {:unban, user_id})

  defp unban_schedule(time) do
    Process.send_after(__MODULE__, :unban, time)
  end
end
