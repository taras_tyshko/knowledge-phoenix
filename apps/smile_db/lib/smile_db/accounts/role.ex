defmodule SmileDB.Accounts.Role do
  @moduledoc """
    This module describes the schema `roles` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Accounts.Role

    Examples of features to use this module are presented in the `SmileDB.Accounts`
  """
  @typedoc """
    This type describes all the fields that are available in the `roles` schema and links to other tables in the tray on the Primary key.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  @type t :: %__MODULE__{
          role: String.t(),
          access_code: Integer.t(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  @primary_key false
  schema "roles" do
    field(:access_code, :integer)
    field(:role, :string, primary_key: true)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(role, attrs) do
        role
        # The fields that are allowed for the record.
        |> cast(attrs, [:role])
        # Unique role ID.
        |> foreign_key_constraint([:role])
        # The fields are required for recording.
        |> validate_required([:role])
        # The entry in the database will be a unique.
        |> unique_constraint([:role])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(setting, attrs) do
    setting
    |> cast(attrs, [:role, :access_code])
    |> validate_required([:role, :access_code])
    |> unique_constraint(:role, name: :roles_pkey)
  end
end
