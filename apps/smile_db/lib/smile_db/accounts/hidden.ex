defmodule SmileDB.Accounts.Hidden do
  use GenServer

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.{Repo, Source, Notifications}

  @spec init(any()) :: {:ok, any()}
  def init(state), do: {:ok, state}

  def handle_cast({:hidden_contents, user_id, value}, state) do
    from(c in Source.Content, where: c.user_id == ^user_id)
    |> Repo.update_all_db_elastic(%{shadow: value})
    |> hidden_feeds()

    {:noreply, state}
  end

  def handle_cast({:hidden_answers, user_id, value}, state) do
    from(a in Source.Answer, where: a.user_id == ^user_id)
    |> Repo.update_all_db_elastic(%{shadow: value})
    |> hidden_feeds()

    {:noreply, state}
  end

  def handle_cast({:hidden_comments, user_id, value}, state) do
    from(c in Source.Comment, where: c.user_id == ^user_id)
    |> Repo.update_all_db_elastic(%{shadow: value})
    |> hidden_feeds()

    {:noreply, state}
  end

  def handle_cast({:hidden_notifications, user_id, value}, state) do
    search index: ElasticSearch.get_meta!(Notifications.Notification).index do
      query do
        term("user_id", user_id)
      end
    end
    |> ElasticSearch.Repo.all()
    |> Enum.map(fn notification ->
      notification
      |> Map.put(:shadow, value)
      |> ElasticSearch.Repo.update()
    end)

    {:noreply, state}
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  @spec start_link() :: :ignore | {:error, any()} | {:ok, pid()}
  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @spec visible(integer(), boolean()) :: atom()
  def visible(user_id, value) do
    GenServer.cast(__MODULE__, {:hidden_contents, user_id, value})
    GenServer.cast(__MODULE__, {:hidden_answers, user_id, value})
    GenServer.cast(__MODULE__, {:hidden_comments, user_id, value})
    GenServer.cast(__MODULE__, {:hidden_notifications, user_id, value})
  end

  @spec hidden_feeds(list(struct)) :: list(struct)
  defp hidden_feeds(data) do
    Task.async(fn -> Enum.map(data, &Source.put_feed(&1)) end)
  end
end
