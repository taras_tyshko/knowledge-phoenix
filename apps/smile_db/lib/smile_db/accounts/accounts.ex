defmodule SmileDB.Accounts do
  @moduledoc """
    The Accounts context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following models are available for use in the current context:

        alias SmileDB.Accounts.User
        alias SmileDB.Accounts.Role
        alias SmileDB.Accounts.Setting

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data used by `user.id`. To do this you need to declare the appropriate for alias.
    In our case, you need to call the model [`User`](SmileDB.Accounts.User.html), [`Role`](SmileDB.Accounts.Role.html) or [`Setting`](SmileDB.Accounts.Setting.html),
    which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Get a single [`User`](SmileDB.Accounts.User.html).

      alias SmileDB.Accounts.User

      def get_user!(id) do
        User
        |> where([u], u.id == ^id)
        |> Repo.one
      end

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  #### Get a [`Setting`](SmileDB.Accounts.Setting.html).

      alias SmileDB.Accounts.Setting

      def get_setting!(user_id) do
        Setting
        |> where([s], s.user_id == ^user_id)
        |> Repo.one
      end

      iex> get_setting!(1)
      %Setting{}

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Accounts

      iex> Accounts.get_user!(123)
      %User{}

      iex> Accounts.get_user!(456)
      ** (Ecto.NoResultsError)

      iex> Accounts.get_setting!(123)
      %Setting{}

      iex> Accounts.get_setting!(456)
      ** (Ecto.NoResultsError)
  """

  import Exgravatar
  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Auth.Guardian

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  alias SmileDB.Accounts.User

  @doc """
  Gets a user data by `ID`.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_user!(123)
      %User{}

      iex> Accounts.get_user!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_user!(Integer.t()) :: User.t()
  def get_user!(id), do: ElasticSearch.Repo.get!(User, id)

  @doc """
  Gets a user data by `ID`.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_user!(123)
      {:ok, %User{}}

      iex> Accounts.get_user!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_user(Integer.t()) :: {:ok, User.t()}
  def get_user(id), do: ElasticSearch.Repo.get(User, id)

  @doc """
  Gets list of user data by `ID`.

  ## Examples

      iex> Accounts.list_users([123])
      [%User{}]

      iex> Accounts.list_users([456])
      []

  """
  @spec list_users(list(Integer.t())) :: User.t()
  def list_users(ids) do
    search index: ElasticSearch.get_meta!(User).index do
      query do
        terms("id", ids)
      end
    end
    |> ElasticSearch.Repo.all()
  end

  @doc """
  Gets a user data by `nickname`.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_user_by_nickname!("test")
      %User{}

      iex> Accounts.get_user_by_nickname!("1234")
      ** (Ecto.NoResultsError)

  """

  @spec get_user_by_nickname!(String.t()) :: User.t()
  def get_user_by_nickname!(nickname),
    do: ElasticSearch.Repo.get_by!(User, "nickname.keyword": nickname)

  @doc """
  Gets a user data by `nickname`.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_user_by_nickname("test")
      %User{}

      iex> Accounts.get_user_by_nickname("1234")
      ** (Ecto.NoResultsError)

  """
  @spec get_user_by_nickname(String.t()) :: User.t()
  def get_user_by_nickname(nickname),
    do: ElasticSearch.Repo.get_by(User, "nickname.keyword": nickname)

  @doc """
  Gets a user data by `email`.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_user_by_email("test@example.com")
      %User{}

      iex> Accounts.get_user_by_email("1234")
      ** (Ecto.NoResultsError)

  """
  @spec get_user_by_email(String.t()) :: User.t()
  def get_user_by_email(email), do: ElasticSearch.Repo.get_by(User, "email.keyword": email)

  @doc """
  Creates a user.

  To create a user need to check with the user model [`User`](SmileDB.Accounts.User.html).
  In which there is a function of [`changeset`](SmileDB.Accounts.User.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.create_user(%{field: value})
      {:ok, %User{}}

      iex> Accounts.create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user(map(), Integer.t()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def create_user(attrs \\ %{}, number_of_letters \\ 2) do
    number_of_letters = number_of_letters + 1

    case %User{}
         |> User.changeset(attrs)
         |> Repo.insert()
         |> ElasticSearch.Repo.put() do
      {:ok, user} ->
        {:ok, user}

      {:error, %{errors: [nickname: {"has already been taken", []}]}} ->
        attrs
        |> Map.put(
          :nickname,
          gen_nickname(
            attrs.first_name,
            1,
            attrs.last_name,
            number_of_letters
          )
        )
        |> create_user(number_of_letters)

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a [`User`](SmileDB.Accounts.User.html) data.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> Accounts.update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_user(User.t(), map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`User`](SmileDB.Accounts.User.html) data with database.

  Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.delete_user(user)
      {:ok, %User{}}

      iex> Accounts.delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user(User.t()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def delete_user(%User{} = user) do
    user
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  alias SmileDB.Accounts.Setting

  @doc """
    Get a [`Setting`](SmileDB.Accounts.Setting.html) for [`User`](SmileDB.Accounts.User.html).

    The function must be transferred to the [`User`](SmileDB.Accounts.User.html) model object to retrieve data.

    Raises `Ecto.NoResultsError` if the [`User`](SmileDB.Accounts.User.html) does not exist.

  ## Examples

      iex> Accounts.get_setting!(%User{id: 1})
      {:ok, %Setting{}}

      iex> Accounts.get_setting!(%User{id: 123)
      {:error, %Ecto.Changeset{}}
  """
  @spec get_setting!(User.t()) :: Setting.t()
  def get_setting!(user) do
    Repo.get_by(Setting, user_id: user.id)
  end

  @doc """
  Creates a setting.

  To create a setting need to check with the setting model [`Setting`](SmileDB.Accounts.Setting.html).
  In which there is a function of [`changeset`](SmileDB.Accounts.Setting.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  Raises `Ecto.NoResultsError` if the [`Setting`](SmileDB.Accounts.Setting.html) does not exist.


  ## Examples

      iex> Accounts.create_setting(%{field: value})
      {:ok, %Setting{}}

      iex> Accounts.create_setting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """
  @spec create_setting(map()) :: {:ok, Setting.t()} | {:error, Ecto.Changeset.t()}
  def create_setting(attrs \\ %{}) do
    %Setting{}
    |> Setting.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a setting data.

  This feature updates the settings and returns the [`Setting`](SmileDB.Accounts.Setting.html) model object.

  Raises `Ecto.NoResultsError` if the [`Setting`](SmileDB.Accounts.Setting.html) does not exist.

  ## Examples

      iex> Accounts.update_setting(setting, %{field: new_value})
      {:ok, %Setting{}}

      iex> Accounts.update_setting(setting, %{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """
  @spec update_setting(Setting.t(), map()) :: {:ok, Setting.t()} | {:error, Ecto.Changeset.t()}
  def update_setting(%Setting{} = setting, attrs) do
    setting
    |> Setting.changeset(attrs)
    |> Repo.update()
  end

  alias SmileDB.Accounts.Role

  @doc """
  Gets a role data by `ID`.

  Raises `Ecto.NoResultsError` if the [`Role`](SmileDB.Accounts.Role.html) does not exist.

  ## Examples

      iex> Accounts.get_role!("user")
      %Role{}

      iex> Accounts.get_role!("non-user")
      ** (Ecto.NoResultsError)

  """
  @spec get_role!(String.t()) :: Role.t()
  def get_role!(name) do
    Role
    |> where([ra], ra.role == ^name)
    |> Repo.one!()
  end

  @doc """
  Gets a all role data.

  Raises `Ecto.NoResultsError` if the [`Role`](SmileDB.Accounts.Role.html) does not exist.

  ## Examples

      iex> Accounts.list_roles()
      []

      iex> Accounts.list_roles()
      ** (Ecto.NoResultsError)

  """
  @spec list_roles() :: list(Role.t())
  def list_roles() do
    Role
    |> order_by(asc: :inserted_at)
    |> Repo.all()
  end

  @doc """
  Creates a [`Role`](SmileDB.Accounts.Role.html).

  To create a role need to check with the role in  model [`Role`](SmileDB.Accounts.Role.html).
  In which there is a function of [`changeset`](SmileDB.Accounts.Role.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  Raises `Ecto.NoResultsError` if the [`Role`](SmileDB.Accounts.Role.html) does not exist.


  ## Examples

      iex> Accounts.create_role(%{field: value})
      {:ok, %Role{}}

      iex> Accounts.create_role(%{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """
  @spec create_role(map()) :: {:ok, Role.t()} | {:error, Ecto.Changeset.t()}
  def create_role(attrs \\ %{}) do
    %Role{}
    |> Role.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all, conflict_target: :role)
  end

  @doc """
  Updates a [`Role`](SmileDB.Accounts.Role.html) data.

  Raises `Ecto.NoResultsError` if the [`Role`](SmileDB.Accounts.Role.html) does not exist.

  ## Examples

      iex> Accounts.update_role(role, %{field: new_value})
      {:ok, %Role{}}

      iex> Accounts.update_role(role, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_role(Role.t(), map()) :: {:ok, Role.t()} | {:error, Ecto.Changeset.t()}
  def update_role(%Role{} = role, attrs) do
    role
    |> Role.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  This function provider create a user and return [`User`](SmileDB.Accounts.User.html) data.

  To retrieve data, you must transfer the two parameter:

      user: Object [`User`](SmileDB.Accounts.User.html) data
      number_of_letters: Number of letters in the first_name and last_family of the user to generate nickname

  ## Examples

      iex> Accounts.create_account(user, number_of_letters)
      {:ok, jwt}
  """
  @spec create_account(map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def create_account(user) do
    with user <-
           ElasticSearch.Repo.get_by(
             User,
             "#{user.auth_provider}_uid": get_in(user, [:"#{user.auth_provider}_uid"])
           ),
         true <- not is_nil(user) do
      user
      |> Guardian.encode_and_sign()
      |> Tuple.delete_at(2)
    else
      false ->
        case create_user(user) do
          {:ok, user} ->
            create_setting(%{user_id: user.id})

            user
            |> Guardian.encode_and_sign()
            |> Tuple.delete_at(2)

          {:error, changeset} ->
            {:error, changeset}
        end
    end
  end

  @doc """
  This function provider updates a user data and return [`User`](SmileDB.Accounts.User.html).

  To retrieve data, you must transfer the two parameter:

      user: Current [`User`](SmileDB.Accounts.User.html) data
      info:  Unique Indenfier from provider side

  ## Examples

      iex> Accounts.update_account(user, info)
      {:ok, jwt}
  """
  @spec update_account(User.t(), map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def update_account(user, info) do
    case update_user(user, info) do
      {:ok, user} ->
        user
        |> Guardian.encode_and_sign()
        |> Tuple.delete_at(2)

      {:error, %{errors: [google_uid: {"has already been taken", []}]}} ->
        {:ok, ElasticSearch.Repo.get_by!(User, google_uid: info.google_uid)}

      {:error, %{errors: [facebook_uid: {"has already been taken", []}]}} ->
        {:ok, ElasticSearch.Repo.get_by!(User, facebook_uid: info.facebook_uid)}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  This function implements the change in the status of the "status".

  To retrieve data, you must transfer the two parameter:

      user_id: Current [`User`](SmileDB.Accounts.User.html) data ID.

  ## Examples

      iex> Accounts.ban_account(1)
      :ok

      # current user doesn`t exist.
      iex> Accounts.ban_account(1)
      :error
  """
  @spec ban_account(Integer.t(), Integer.t()) :: :ok | :error
  def ban_account(user_id, time) do
    user_id
    |> get_user!()
    |> update_user(%{
      ban_time: ((DateTime.utc_now() |> DateTime.to_unix()) + time) |> DateTime.from_unix!(),
      status: "banned"
    })
  end

  def ban_account(user_id) do
    user_id
    |> get_user!()
    |> update_user(%{ban_time: nil, status: "banned"})
  end

  @doc """
  This function implements the change in the status of the "status".

  To retrieve data, you must transfer the two parameter:

      user_id: Current [`User`](SmileDB.Accounts.User.html) data ID.

  ## Examples

      iex> Accounts.unban_account(1)
      :ok

      # current user doesn`t exist.
      iex> Accounts.unban_account(1)
      :error
  """
  @spec unban_account(Intege.t()) :: :ok | :error
  def unban_account(user_id) do
    user_id
    |> get_user!()
    |> update_user(%{ban_time: nil, status: "active"})
  end

  @doc """
  This function provider deletes a user data and return [`User`](SmileDB.Accounts.User.html).

  To retrieve data, you must transfer the two parameter:

      user: Current [`User`](SmileDB.Accounts.User.html) data

  ## Examples

      iex> Accounts.delete_account(user)
      {:ok, %User{}}
  """
  @spec delete_account(User.t()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def delete_account(user) do
    user
    |> ElasticSearch.Repo.delete(
      cascade_delete: [
        SmileDB.Source.Feed,
        SmileDB.Source.Content,
        SmileDB.Source.Comment,
        SmileDB.Source.Answer,
        SmileDB.Contents.Meto,
        SmileDB.Users.Expert
      ]
    )
    |> ElasticSearch.Repo.put()
    |> update_user(%{
      status: "deleted"
    })
  end

  @doc """
    This feature implements the receipt of default user avatar. Use a module for dependence, and function [`gravatar_url`](https://github.com/scrogson/exgravatar#usage) to react.
    Official docs [`Exgravatar`](https://github.com/scrogson/exgravatar)

      import Exgravatar

    This function generate a user photo against an email.

  ## Examples

      iex> Accounts.default_avatar(user)
      "url_photo"

  """
  @spec default_avatar(User.t()) :: String.t()
  def default_avatar(email) do
    gravatar_url(email, d: "identicon", s: "500")
  end

  @doc """
  This function provider import avatar from social media.

  To retrieve data, you must transfer the two parameter:

      provider:  Name authorization provider
      user: Current [`User`](SmileDB.Accounts.User.html) data

  ## Examples

      iex> Accounts.import_from("google", user)
      {:ok, %%{google: "url"}}

      iex> Accounts.import_from("facebook", user)
      {:ok, %{facebook: "url"}}
      # What would get a photo from the two social networks need to pass the parameter "account_image".
      iex> Accounts.import_from("account_image", user)
      {:ok, %{google: "url", facebook: "url"}}

      iex> Accounts.import_from("", user)
      ** (RuntimeError) No matching provider available
  """
  @spec import_from(User.t()) :: map()
  def import_from(user) do
    %{
      facebook: SmileDB.Auth.get_facebook_avatar(user.facebook_uid),
      google: SmileDB.Auth.get_google_avatar(user.google_uid)
    }
  end

  @spec gen_nickname(String.t(), Integer.t(), String.t(), Integer.t()) :: String.t()
  def gen_nickname(first_name, first_name_letters \\ 1, last_name, last_name_letters \\ 2) do
    (String.slice(first_name, 0..first_name_letters) <>
       String.slice(last_name, 0..last_name_letters))
    |> String.downcase()
  end
end
