defmodule SmileDB.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Users.Expert

  @doc """
  Gets all expert request by user.

  ## Examples

      iex> get_experts_by_user(["test"], 1)
      [...]

      iex> get_experts_by_user(["test1"], 2)
      []

  """
  @spec get_experts_by_user(list(String.t()), Integer.t()) :: list(Expert.t()) | list()
  def get_experts_by_user(tags, user_id) do
    Expert
    |> where([e], e.tag in ^tags)
    |> where([e], e.user_id == ^user_id)
    |> Repo.all()
  end

  def get_experts_by_user(user_id) do
    Expert
    |> where([e], e.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
  Gets list a tags expert.

  ## Examples

      iex> list_expert_requests(123)
      [...]

  """
  @spec list_expert_requests(Integer.t()) :: list(String.t())
  def list_expert_requests(user_id) do
    (ElasticSearch.Repo.get(Expert, user_id) || %{})
    |> Map.get(:tags)
  end

  @doc """
  Creates a expert.

  ## Examples

      iex> create_expert(%{field: value})
      {:ok, %Expert{}}

      iex> create_expert(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_expert(attrs \\ %{}) do
    %Expert{}
    |> Expert.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a Expert.

  ## Examples

      iex> delete_expert(expert)
      {:ok, %Expert{}}

      iex> delete_expert(expert)
      {:error, %Ecto.Changeset{}}

  """
  def delete_expert(%Expert{} = expert) do
    expert
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  alias SmileDB.Users.ChangePassword

  @doc """
  Gets a single change_password.

  Raises `Ecto.NoResultsError` if the Change password does not exist.

  ## Examples

      iex> get_change_password!(123)
      %ChangePassword{}

      iex> get_change_password!(456)
      ** (Ecto.NoResultsError)

  """
  def get_change_password!(id), do: Repo.get!(ChangePassword, id)

  @doc """
  Gets all change_password request by user.

  ## Examples

      iex> get_change_password_by_user(1)
      [...]

      iex> get_change_password_by_user(2)
      []

  """
  @spec get_change_password_by_user(Integer.t()) :: list(ChangePassword.t()) | list()
  def get_change_password_by_user(user_id) do
    ChangePassword
    |> where([cp], cp.user_id == ^user_id)
    |> Repo.all()
  end

  @doc """
    Check a active hash.

    Raises `Ecto.NoResultsError` if the Change password does not exist.

  ## Examples

      iex> check_active_hash!("asdadasdad123asd")
      %ChangePassword{}

      iex> check_active_hash!("asdasd")
      ** (Ecto.NoResultsError)

  """
  @spec get_change_password_by_hash(String.t()) :: ChangePassword.t() | nil
  def get_change_password_by_hash(hash), do: Repo.get_by(ChangePassword, hash: hash)

  @doc """
    Creates a change_password.

  ## Examples

      iex> create_change_password(%{field: value})
      {:ok, %ChangePassword{}}

      iex> create_change_password(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_change_password(attrs \\ %{}) do
    %ChangePassword{}
    |> ChangePassword.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a ChangePassword.

  ## Examples

      iex> delete_change_password(change_password)
      {:ok, %ChangePassword{}}

      iex> delete_change_password(change_password)
      {:error, %Ecto.Changeset{}}

  """
  def delete_change_password(%ChangePassword{} = change_password) do
    Repo.delete(change_password)
  end
end
