defmodule SmileDB.Users.ChangePassword do
  @moduledoc """
    This module describes the schema `change_password` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset
    
    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Users.ChangePassword

    Examples of features to use this module are presented in the `SmileDB.Users`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User

  @typedoc """
    This type describes all the fields that are available in the `change_password` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          hash: String.t(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t()
        }

  schema "change_password" do
    field(:hash, :string)

    belongs_to(:user, User)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(change_password, attrs) do
          change_password
          # The fields that are allowed for the record.
          |> cast(attrs, [:hash, :user_id])
          # The fields are required for recording.
          |> validate_required([:hash, :user_id])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(change_password, attrs) do
    change_password
    |> cast(attrs, [:hash, :user_id])
    |> validate_required([:hash, :user_id])
  end
end
