defmodule SmileDB.Users.Expert do
  @moduledoc """
    This module describes the schema `expert_requests` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Users.Expert

    Examples of features to use this module are presented in the `SmileDB.Users`
  """

  import Ecto.Query

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Type
  alias ElasticSearch.Entity

  alias SmileDB.Tags.Tag
  alias SmileDB.Accounts.User

  @typedoc """
    This type describes all the fields that are available in the `expert_requests` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          tag_name: String.t(),
          user_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          tag: Tag.t(),
          user: User.t()
        }

  schema "expert_requests" do
    field(:tags, {:array, :string}, default: [], virtual: true)

    belongs_to(:user, User)
    belongs_to(:tag_name, Tag, foreign_key: :tag, type: :string, references: :title)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(expert, attrs) do
          expert
          # The fields that are allowed for the record.
          |> cast(attrs, [:tag, :user_id])
          # The fields are required for recording.
          |> validate_required([:tag, :user_id])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(expert, attrs) do
    expert
    |> cast(attrs, [:tag, :user_id])
    |> validate_required([:tag, :user_id])
    |> unique_constraint(:user_id_tag)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :expert_requests,
      type: :expert_request,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime
          ],
          array: [
            Entity.Fields.Array.new(%{
              id: :user_id,
              field: :tags,
              value: :tag
            })
          ]
        })
    })
  end
end
