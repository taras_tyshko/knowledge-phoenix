defmodule SmileDB.Contents do
  @moduledoc """
    The `Contents` context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following model are available for use in the current context:

        alias SmileDB.Contents.Meto
        alias SmileDB.Contents.RelatedContent

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data. To do this you need to declare the appropriate for `alias`.
    In our case, you need to call the model: [`RelatedContent`](SmileDB.Contents.RelatedContent.html), [`Meto`](SmileDB.Contents.Meto.html)
    which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Gets a list related contents id with table [`RelatedContent`](SmileDB.Contents.RelatedContent.html).

      alias SmileDB.Contents.RelatedContent

      def list_related_contents_id(id) do
        RelatedContent
        |> where([r], r.content_id == ^id)
        |> select([r], r.related_content_id)
        |> Repo.all()
      end

      iex> list_related_contents(1)
      [...]

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Contents

      iex> Contents.list_related_contents_id(1)
      [...]

      iex> Contents.list_related_contents_id(123)
      []
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Contents.RelatedContent

  @doc """
  Returns the list of related contents id for [`Content`](SmileDB.Source.Content.html).

  ## Examples

      iex> Contents.list_related_contents(1)
      [...]

  """
  @spec list_related_contents_id(Integer.t()) :: list(RelatedContent.t())
  def list_related_contents_id(id) do
    RelatedContent
    |> where([r], r.content_id == ^id)
    |> select([r], r.related_content_id)
    |> Repo.all()
  end

  @doc """
  Gets a [`RelatedContent`](SmileDB.Contents.RelatedContent.html).

  ## Examples

      iex> Contents.get_related_content(%{content_id: 1, related_content_id: 2})
      {:ok, %RelatedContent{}}

      # related content 12 - doesn`t exist in db
      iex> Contents.get_related_content(%{content_id: 1, related_content_id: 12})
      {:error, %Ecto.ChangeError{}}

  """
  @spec get_related_content(%{content_id: Integer.t(), related_content_id: Integer.t()}) ::
          {:ok, RelatedContent.t()} | {:error, Ecto.ChangeError.t()}
  def get_related_content(data) do
    RelatedContent
    |> where([r], r.content_id == ^data.content_id)
    |> where([r], r.related_content_id == ^data.related_content_id)
    |> Repo.one()
  end

  @doc """
  Creates a [`RelatedContent`](SmileDB.Contents.RelatedContent.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Contents.RelatedContent.html#changeset/2)

  ## Examples

      iex> Contents.create_related_content(%{field: value})
      {:ok, %RelatedContent{}}

      iex> Contents.create_related_content(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_related_content(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_related_content(attrs \\ %{}) do
    %RelatedContent{}
    |> RelatedContent.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a [`RelatedContent`](SmileDB.Contents.RelatedContent.html) with database.

  ## Examples

      iex> Contents.delete_related_content(related_content)
      {:ok, %RelatedContent{}}

      iex> Contents.delete_related_content(related_content)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_related_content(RelatedContent.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_related_content(%RelatedContent{} = related_content) do
    Repo.delete(related_content)
  end

  alias SmileDB.Contents.Meto

  @doc """
  Get a list users for [`Content`](SmileDB.Source.Content.html) for field me_to.

  ## Examples

      iex> Contents.list_me_to(1)
      [2,3,4]

      iex> Contents.list_me_to(456)
      []

  """
  @spec list_me_to(Integer.t()) :: list(Integer.t())
  def list_me_to(content_id) do
    search index: ElasticSearch.get_meta!(Meto).index do
      query do
        term("content_id", content_id)
      end
    end
    |> ElasticSearch.Repo.all()
    |> Enum.map(& &1.user_id)
  end

  @doc """
  Get a list contents id for [`Content`](SmileDB.Source.Content.html) for field me_to.

  ## Examples

      iex> Contents.list_contents_id_from_me_to(1)
      [...]

  """
  @spec list_contents_id_from_me_to(Integer.t()) :: list(Integer.t())
  def list_contents_id_from_me_to(user) do
    search index: ElasticSearch.get_meta!(Meto).index do
      query do
        term("user_id", user.id)
      end
    end
    |> ElasticSearch.Repo.all()
    |> Enum.map(& &1.content_id)
  end

  @doc """
  Creates a [`Meto`](SmileDB.Contents.Meto.html).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Meto.create_me_to.html#changeset/2)

  ## Examples

      iex> Contents.create_me_to(%{field: value})
      {:ok, %Meto{}}

      iex> Contents.create_me_to(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_me_to(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_me_to(attrs \\ %{}) do
    %Meto{}
    |> Meto.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a [`Meto`](SmileDB.Contents.Meto.html) with database.

  ## Examples

      iex> Contents.delete_me_to(me_to)
      {:ok, %Meto{}}

      iex> Contents.delete_me_to(me_to)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_me_to(Meto.t()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_me_to(%Meto{} = me_to) do
    me_to
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end
end
