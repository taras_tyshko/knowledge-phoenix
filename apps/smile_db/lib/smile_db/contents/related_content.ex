defmodule SmileDB.Contents.RelatedContent do
  @moduledoc """
    This module describes the schema `related_contents` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Contents.RelatedContent

    Examples of features to use this module are presented in the `SmileDB.Contents`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Source.Content

  @typedoc """
    This type describes all the fields that are available in the `related_contents` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          content_id: integer(),
          related_content_id: integer(),
          content: Content.t(),
          related_content: Content.t()
        }

  schema "related_contents" do
    belongs_to(:related_content, Content)
    belongs_to(:content, Content)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(related_content, attrs) do
        related_content
        # The fields that are allowed for the record.
        |> cast(attrs, [:related_content_id :content_id])
        # The fields are required for recording.
        |> validate_required([:related_content_id, :content_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(related_content, attrs) do
    related_content
    |> cast(attrs, [:related_content_id, :content_id])
    |> validate_required([:related_content_id, :content_id])
    |> unique_constraint(:related_content_id)
  end
end
