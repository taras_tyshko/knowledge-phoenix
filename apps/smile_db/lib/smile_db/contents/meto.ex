defmodule SmileDB.Contents.Meto do
  @moduledoc """
    This module describes the schema `me_to` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Contents.Meto

    Examples of features to use this module are presented in the `SmileDB.Contents`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Accounts.User
  alias SmileDB.Source.Content

  @typedoc """
    This type describes all the fields that are available in the `me_to` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          content_id: integer(),
          user_id: integer(),
          content: Content.t(),
          user: User.t()
        }

  schema "me_to" do
    belongs_to(:user, User)
    belongs_to(:content, Content)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(meto, attrs) do
        meto
        |> cast(attrs, [:user_id, :content_id])
        |> validate_required([:user_id, :content_id])
        |> unique_constraint([:content_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(meto, attrs) do
    meto
    |> cast(attrs, [:user_id, :content_id])
    |> validate_required([:user_id, :content_id])
    |> unique_constraint(:content_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :me_to,
      type: :me_to,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
