defmodule SmileDB.Achievements.Achieve do
  @moduledoc """
    This module describes the schema `achieves` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        import Ecto.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Achievements.Achieve

    Examples of features to use this module are presented in the `SmileDB.Achievements`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Achievements.UserAchieve

  @typedoc """
    This type describes all the fields that are available in the `achieves` schema and links to other tables in the tray on the Primary key.
  """

  @type t :: %__MODULE__{
          id: integer(),
          uuid: String.t(),
          title: String.t(),
          avatar: String.t(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user_achieves: UserAchieve.t()
        }

  schema "achieves" do
    field(:avatar, :string)
    field(:title, :string)
    field(:uuid, :string)

    has_many(:user_achieves, UserAchieve)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(achieve, attrs) do
          achieve
          # The fields that are allowed for the record.
          |> cast(attrs, [:title, :avatar])
          # The fields are required for recording.
          |> validate_required([:title, :avatar])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(achieve, attrs) do
    achieve
    |> cast(attrs, [:title, :avatar])
    |> check_uuid()
    |> storage_cast(attrs, [:avatar], type: FilesUploader.Definition.Avatar)
    |> validate_required([:title, :avatar])
    |> unique_constraint(:title)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :achieves,
      type: :achieve,
      fields:
        Entity.Fields.new(%{
          additional: [
            search: [:title],
            autocomplete: [:title]
          ],
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        }),
      cascade_decrement: [
        {UserAchieve, :id}
      ]
    })
  end
end
