defmodule SmileDB.Achievements do
  @moduledoc """
  The Achievements context.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  alias SmileDB.Repo

  alias SmileDB.Achievements.Achieve

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Gets a single achieve.

  Raises `Ecto.NoResultsError` if the Achieve does not exist.

  ## Examples

      iex> get_achieve!(123)
      %Achieve{}

      iex> get_achieve!(456)
      ** (Ecto.NoResultsError)

  """
  def get_achieve!(id), do: Repo.get!(Achieve, id)

  @doc """
  Creates a achieve.

  ## Examples

      iex> create_achieve(%{field: value})
      {:ok, %Achieve{}}

      iex> create_achieve(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_achieve(attrs \\ %{}) do
    %Achieve{}
    |> Achieve.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a achieve.

  ## Examples

      iex> update_achieve(achieve, %{field: new_value})
      {:ok, %Achieve{}}

      iex> update_achieve(achieve, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_achieve(%Achieve{} = achieve, attrs) do
    achieve
    |> Achieve.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a Achieve.

  ## Examples

      iex> delete_achieve(achieve)
      {:ok, %Achieve{}}

      iex> delete_achieve(achieve)
      {:error, %Ecto.Changeset{}}

  """
  def delete_achieve(%Achieve{} = achieve) do
    achieve
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  alias SmileDB.Achievements.UserAchieve

  @doc """
    Gets list of user [`Achieve`](SmileDB.Achievements.Achieve.html).

  ## Examples

    iex> list_achieves(1)
    [%Achieve{}...]

    iex> list_achieves(1)
    []
  """
  @spec list_achieves(Integer.t()) :: list(Achievements.Achieve.t())
  def list_achieves(user_id) do
    case ElasticSearch.Repo.get_by(UserAchieve, user_id: user_id) do
      nil ->
        []

      user_achieve ->
        search(index: ElasticSearch.get_meta!(Achieve).index) do
          query do
            bool do
              must do
                terms("id", Map.fetch!(user_achieve, :achieves))
              end
            end
          end
        end
        |> ElasticSearch.Repo.all()
    end
  end

  @doc """
  Creates a user_achieve.

  ## Examples

      iex> create_user_achieve(%{field: value})
      {:ok, %UserAchieve{}}

      iex> create_user_achieve(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user_achieve(attrs \\ %{}) do
    %UserAchieve{}
    |> UserAchieve.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a UserAchieve.

  ## Examples

      iex> delete_user_achieve(user_achieve)
      {:ok, %UserAchieve{}}

      iex> delete_user_achieve(user_achieve)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user_achieve(%UserAchieve{} = user_achieve) do
    user_achieve
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end
end
