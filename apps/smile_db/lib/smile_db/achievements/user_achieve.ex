defmodule SmileDB.Achievements.UserAchieve do
  @moduledoc """
    This module describes the schema `user_achieves` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Achievements.UserAchieve

    Examples of features to use this module are presented in the `SmileDB.Achievements`
  """

  import Ecto.Query

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias ElasticSearch.{Type, Entity}
  alias SmileDB.Achievements.Achieve

  @typedoc """
    This type describes all the fields that are available in the `user_achieves` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          achieve_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          achieve: Achieve.t()
        }

  schema "user_achieves" do
    field(:achieves, {:array, :integer}, default: [], virtual: true)

    belongs_to(:user, User)
    belongs_to(:achieve, Achieve)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

        def changeset(user_achieve, attrs) do
          user_achieve
          # The fields that are allowed for the record.
          |> cast(attrs, [:user_id, :achieve_id])
          # The fields are required for recording.
          |> validate_required([:user_id, :achieve_id])
        end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(user_achieve, attrs) do
    user_achieve
    |> cast(attrs, [:user_id, :achieve_id])
    |> validate_required([:user_id, :achieve_id])
    |> unique_constraint(:user_id_achieve_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :user_achieves,
      type: :user_achieve,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ],
          array: [
            Entity.Fields.Array.new(%{
              id: :user_id,
              field: :achieves,
              value: :achieve_id
            })
          ]
        })
    })
  end
end
