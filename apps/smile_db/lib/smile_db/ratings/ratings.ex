defmodule SmileDB.Ratings do
  @moduledoc """
    The `Ratings` context. This context describes how to use models and to build functions for future use.
    The official documentation is located at the following address [`Phoenix.Contexts`](https://hexdocs.pm/phoenix/contexts.html#content).

    The following model are available for use in the current context:

        alias SmileDB.Ratings.AnswerRating
        alias SmileDB.Ratings.CommentRating

    To work with this schema, you need to use a dependency.

        # To build samples from the database you need to use
        import Ecto.Query, warn: false
        # To work with the repository base you need to use.
        alias SmileDB.Repo

    If you need to work with the context then you need to ask `alias` to use it and retrieve database data.

    For example, you can get the data. To do this you need to declare the appropriate for `alias`.
    In our case, you need to call the model: [`AnswerRating`](SmileDB.Ratings.AnswerRating.html), [`CommentRating`](SmileDB.Ratings.CommentRating.html)
    which will use it to fetch data from the base.

  ### Example (Create function in context)
    Set `alias` to a prieber model to use it when a function is created in context.

  #### Create answer rating to work used table [`AnswerRating`](SmileDB.Ratings.CommentRating.html).
    Important before using the features to familiarize yourself with [`changeset`](SmileDB.Ratings.AnswerRating.html#changeset/2)

      alias SmileDB.Ratings.AnswerRating

      def create_answer_rating(attrs \\ %{}) do
        %AnswerRating{}
        |> AnswerRating.changeset(attrs)
        |> Repo.insert()
      end

      iex> create_answer_rating(%{field: value})
      {:ok, %AnswerRating{}}

      iex> create_answer_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  ### Example (Used function in other module)
    Specific `alias` context to use functions that are available.

      alias SmileDB.Ratings

      iex> Ratings.create_answer_rating(%{field: value})
      {:ok, %AnswerRating{}}

      iex> Ratings.create_answer_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  alias SmileDB.Repo

  alias SmileDB.Ratings.{AnswerRating, CommentRating, UserRating, OfficeRating}

  @doc false
  def data() do
    Dataloader.Ecto.new(Repo, query: &query/2)
  end

  @doc false
  def query(queryable, _params) do
    queryable
  end

  @doc """
  Creates a [`UserRating`](SmileDB.Ratings.UserRating.html).

  To create a `user_rating` need to check with the model [`UserRating`](SmileDB.Ratings.UserRating.html).
  In which there is a function of [`changeset`](SmileDB.Ratings.UserRating.html#changeset/2) in which the fields can be specified for entry to the base and which are required.

  ## Examples

      iex> Ratings.create_user_rating(%{field: value})
      {:ok, %UserRating{}}

      iex> Ratings.create_user_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user_rating(map()) :: UserRating.t()
  def create_user_rating(attrs \\ %{}) do
    %UserRating{}
    |> UserRating.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a [`UserRating`](SmileDB.Ratings.UserRoom.html) data.

  Raises `Ecto.NoResultsError` if the [`UserRating`](SmileDB.Ratings.UserRating.html) does not exist.

  ## Examples

      iex> Ratings.delete_user_rating(user_rating)
      {:ok, %UserRating{}}

      iex> Ratings.delete_user_rating(user_rating)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user_rating(UserRating.t()) :: UserRating.t()
  def delete_user_rating(%UserRating{} = user_rating) do
    user_rating
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  @doc """
  Function to handle the id of the model.

  It receives data from the model [`Answer`](SmileDB.Source.Answer.html).

  Returns a list of models.

  ## Parameters

    - answer: a list of [`Answer`](SmileDB.Source.Answer.html) schema map data coming from a repository

  ## Examples

      model = %Answer{id: 1}

      iex> Rating.get_answers_rating([answer])
      [
        %Answer{
          id: 1,
          likes: [1],
          dislikes: [1]
          }
        }
      ]
  """
  @spec get_answers_rating(list(), list(Integer.t())) :: AnswerRating.t()
  def get_answers_rating(_, answers_ids) do
    AnswerRating
    |> where([a], a.answer_id in ^answers_ids)
    |> Repo.all()
    |> Enum.group_by(& &1.answer_id)
  end

  @doc """
  Creates a [`AnswerRating`](SmileDB.Ratings.AnswerRating).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Ratings.AnswerRating.html#changeset/2)

  ## Examples

      iex> Ratings.create_answer_rating(%{field: value})
      {:ok, %AnswerRating{}}

      iex> Ratings.create_answer_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_answer_rating(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_answer_rating(%AnswerRating{} = answer_rating, attrs \\ %{}) do
    answer_rating
    |> AnswerRating.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a [`AnswerRating`](SmileDB.Ratings.AnswerRating).

  ## Examples

      iex> Ratings.delete_answer_rating(answer_rating)
      {:ok, %AnswerRating{}}

      iex> Ratings.delete_answer_rating(answer_rating)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_answer_rating(SmileDB.Ratings.AnswerRating.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_answer_rating(%AnswerRating{} = answer_rating) do
    answer_rating
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  @doc """
  Function to handle the id of the model.

  It receives data from the model [`Comment`](SmileDB.Source.Comment.html).

  Returns a list of models.

  ## Parameters

    - answer: a list of [`Comment`](SmileDB.Source.Comment.html) schema map data coming from a repository

  ## Examples

      model = %Comment{id: 1}

      iex> Rating.get_comments_rating([comment])
      [
        %Comment{
          id: 1,
          likes: [1],
          dislikes: [1]
          }
        }
      ]
  """
  @spec get_comments_rating(list(), list(Integer.t())) :: CommentRating.t()
  def get_comments_rating(_, comments_ids) do
    CommentRating
    |> where([c], c.comment_id in ^comments_ids)
    |> Repo.all()
    |> Enum.group_by(& &1.comment_id)
  end

  @doc """
  Creates a [`CommentRating`](SmileDB.Ratings.CommentRating).

  Important before using the features to familiarize yourself with [`changeset`](SmileDB.Ratings.CommentRating.html#changeset/2)

  ## Examples

      iex> Ratings.create_comment_rating(%{field: value})
      {:ok, %CommentRating{}}

      iex> Ratings.create_comment_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_comment_rating(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_comment_rating(%CommentRating{} = comment_rating, attrs \\ %{}) do
    comment_rating
    |> CommentRating.changeset(attrs)
    |> Repo.insert()
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Deletes a [`CommentRating`](SmileDB.Ratings.CommentRating).

  ## Examples

      iex> Ratings.delete_comment_rating(comment_rating)
      {:ok, %CommentRating{}}

      iex> Ratings.delete_comment_rating(comment_rating)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_comment_rating(CommentRating.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_comment_rating(%CommentRating{} = comment_rating) do
    comment_rating
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end

  @doc """
  Gets a single [`OfficeRating`](SmileDB.Ratings.OfficeRating).

  Raises `Ecto.NoResultsError` if the Office rating does not exist.

  ## Examples

      iex> Ratings.get_office_rating("test_office")
      1

      iex> Ratings.get_office_rating(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_office_rating(Integer.t()) :: OfficeRating.t()
  def get_office_rating(office_id) do
    search(index: ElasticSearch.get_meta!(OfficeRating).index) do
      query do
        bool do
          must do
            term("office_id", office_id)
          end
        end
      end
    end
    |> ElasticSearch.Repo.all()
    |> office_rating_average()
  end

  defp office_rating_average(ratings) when length(ratings) > 0 do
    ratings
    |> Enum.map(& &1.score)
    |> (&(Enum.sum(&1) / length(&1))).()
    |> round()
  end

  defp office_rating_average(_ratings), do: 0

  @doc """
  Gets a single [`OfficeRating`](SmileDB.Ratings.OfficeRating).

  Raises `Ecto.NoResultsError` if the Office rating does not exist.

  ## Examples

      iex> Ratings.get_office_rating_by!(%Content{id: 123, office_id: "test_1"})
      %OfficeRating{}

      iex> Ratings.get_office_rating_by!(%Content{id: 456, office_id: "test_1"})
      ** (Ecto.NoResultsError)

  """
  @spec get_office_rating_by!(Integer.t()) :: OfficeRating.t()
  def get_office_rating_by!(model) do
    ElasticSearch.Repo.get_by!(OfficeRating, content_id: model.id, office_id: model.office_id)
  end

  @doc """
  Creates a [`OfficeRating`](SmileDB.Ratings.OfficeRating).

  ## Examples

      iex> Ratings.create_office_rating(%{field: value})
      {:ok, %OfficeRating{}}

      iex> Ratings.create_office_rating(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_office_rating(map()) :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def create_office_rating(attrs \\ %{}) do
    %OfficeRating{}
    |> OfficeRating.changeset(attrs)
    |> Repo.insert(on_conflict: :replace_all, conflict_target: [:office_id, :content_id])
    |> ElasticSearch.Repo.put()
  end

  @doc """
  Updates a [`OfficeRating`](SmileDB.Ratings.OfficeRating).

  ## Examples

      iex> Ratings.update_office_rating(office_rating, %{field: new_value})
      {:ok, %OfficeRating{}}

      iex> Ratings.update_office_rating(office_rating, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_office_rating(OfficeRating.t(), map()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def update_office_rating(%OfficeRating{} = office_rating, attrs) do
    office_rating
    |> OfficeRating.changeset(attrs)
    |> Repo.update()
    |> ElasticSearch.Repo.update()
  end

  @doc """
  Deletes a [`OfficeRating`](SmileDB.Ratings.OfficeRating).

  ## Examples

      iex> Ratings.delete_office_rating(office_rating)
      {:ok, %OfficeRating{}}

      iex> Ratings.delete_office_rating(office_rating)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_office_rating(OfficeRating.t()) ::
          {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  def delete_office_rating(%OfficeRating{} = office_rating) do
    office_rating
    |> Repo.delete()
    |> ElasticSearch.Repo.delete()
  end
end
