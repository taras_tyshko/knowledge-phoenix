defmodule SmileDB.Ratings.UserRating do
  @moduledoc """
    This module describes the schema `users_rating` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Ratings.UserRating

    Examples of features to use this module are presented in the `SmileDB.Ratings`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias ElasticSearch.Entity

  alias SmileDB.Accounts.User
  alias SmileDB.Ratings.{AnswerRating, CommentRating}

  @typedoc """
    This type describes all the fields that are available in the `users_rating` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          user_id: integer(),
          answer_rating_id: integer(),
          comment_rating_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          answer_rating: AnswerRating.t(),
          comment_rating: CommentRating.t()
        }

  schema "users_rating" do
    belongs_to(:user, User)
    belongs_to(:answer_rating, AnswerRating)
    belongs_to(:comment_rating, CommentRating)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(answer_rating, attrs) do
        answer_rating
        # The fields that are allowed for the record.
        |> cast(attrs, [:status, :user_id, :answer_id])
        # The fields are required for recording.
        |> validate_required([:status, :user_id, :answer_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(answer_rating, attrs) do
    answer_rating
    |> cast(attrs, [:user_id, :answer_rating_id, :comment_rating_id])
    |> validate_required([:user_id])
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :users_rating,
      type: :user_rating,
      trigger_update:
        Entity.TriggerUpdate.new(%{
          counter: [
            Entity.TriggerUpdate.Counter.new(%{
              related_model: User,
              update_field: :likes_count,
              condition: & &1[:user_id]
            })
          ]
        })
    })
  end
end
