defmodule SmileDB.Ratings.AnswerRating do
  @moduledoc """
    This module describes the schema `answers_rating` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Ratings.AnswerRating

    Examples of features to use this module are presented in the `SmileDB.Ratings`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Accounts.User
  alias SmileDB.Source.Answer
  alias SmileDB.Ratings.UserRating
  alias SmileDB.Notifications.Notification

  @typedoc """
    This type describes all the fields that are available in the `answers_rating` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          status: boolean(),
          user_id: integer(),
          answer_id: integer(),
          updated_at: timeout(),
          inserted_at: timeout(),
          user: User.t(),
          answer: Answer.t()
        }

  schema "answers_rating" do
    field(:status, :boolean)
    belongs_to(:user, User)
    belongs_to(:answer, Answer)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(answer_rating, attrs) do
        answer_rating
        # The fields that are allowed for the record.
        |> cast(attrs, [:status, :user_id, :answer_id])
        # The fields are required for recording.
        |> validate_required([:status, :user_id, :answer_id])
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(answer_rating, attrs) do
    answer_rating
    |> cast(attrs, [:status, :user_id, :answer_id])
    |> validate_required([:status, :user_id, :answer_id])
    |> foreign_key_constraint(:answer_id)
    |> unique_constraint(:answer_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :answers_rating,
      type: :answer_rating,
      cascade_delete: [Notification, UserRating]
    })
  end
end
