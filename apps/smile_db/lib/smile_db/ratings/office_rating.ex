defmodule SmileDB.Ratings.OfficeRating do
  @moduledoc """
    This module describes the schema `offices_rating` and its all fields with the data types used to work with this module.

    To work with this schema, you need to use a dependency.

        use Ecto.Schema
        use SmileDB.Changeset

    To work with the scheme should be declared alias and make requests to the database.

        alias SmileDB.Ratings.OfficeRating

    Examples of features to use this module are presented in the `SmileDB.Ratings`
  """

  use Ecto.Schema
  use SmileDB.Changeset

  alias SmileDB.Category.Office
  alias SmileDB.Source.Content
  alias ElasticSearch.{Type, Entity}

  @typedoc """
    This type describes all the fields that are available in the `offices_rating` schema and links to other tables in the tray on the Primary key.
  """
  @type t :: %__MODULE__{
          id: integer(),
          score: integer(),
          content_id: integer(),
          updated_at: timeout(),
          office_id: String.t(),
          inserted_at: timeout(),
          content: Content.t(),
          office: Office.t()
        }

  schema "offices_rating" do
    field(:score, :integer)

    belongs_to(:office, Office, type: :string)
    belongs_to(:content, Content)

    timestamps(type: :utc_datetime)
  end

  @doc """
    This feature shows the fields that are required to record, and you can record fields that are unique.

      def changeset(office_rating, attrs) do
        office_rating
        # The fields that are allowed for the record.
        |> cast(attrs, [:score, :content_id, :office_id])
        # The fields are required for recording.
        |> validate_required([:score, :content_id, :office_id])
        |> unique_constraint(:office_id)
      end
  """
  @spec changeset(__MODULE__.t(), map()) :: Ecto.Changeset.t()
  def changeset(office_rating, attrs) do
    office_rating
    |> cast(attrs, [:score, :content_id, :office_id])
    |> validate_required([:score, :content_id, :office_id])
    |> foreign_key_constraint(:office_id)
    |> unique_constraint(:office_id)
  end

  def __schema__(:elastic_search) do
    ElasticSearch.new(%{
      index: :offices_rating,
      type: :office_rating,
      fields:
        Entity.Fields.new(%{
          convert: [
            inserted_at: Type.DateTime,
            updated_at: Type.DateTime
          ]
        })
    })
  end
end
