defmodule SmileDB.Test.Default do
  @moduledoc """
    This module describes the fields by default and can be useed to test the ecto scheme.
  """

  use Ecto.Schema
  use SmileDB.Changeset

  @typedoc """
    This type describes all the fields that are available in the `default` schema.
  """
  @type t :: %__MODULE__{
          id: integer(),
          updated_at: timeout(),
          inserted_at: timeout()
        }

  schema "default" do
    timestamps(type: :utc_datetime)
  end
end
