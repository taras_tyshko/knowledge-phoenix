defmodule SmileDB.Auth do
  @doc """
  This function provider gets a user oauth data and return [`User`](SmileDB.Accounts.User.html).

  To retrieve data, you must transfer the two parameter: `Token` and outh `provider`.


  To retrieve data, you must transfer the two parameter:

      client: Object containing the customer's data of the token authorization
      provider: Name authorization provider

  ## Examples

      iex> Auth.get_user_info("google", %{token: ""})
      %{}

      iex> Auth.get_user_info("facebook", %{token: ""})
      %{}

      iex> Auth.get_user_info("", %{token: ""})
      ** (RuntimeError) No matching provider available
  """
  @spec get_user_info(String.t(), String.t()) :: map()
  def get_user_info(provider, access_token) do
    config = Application.fetch_env!(:smile_db, :auth)[:"#{provider}"]

    info =
      config
      |> OAuth2.Client.new()
      |> Map.put(:token, OAuth2.AccessToken.new(access_token))
      |> OAuth2.Client.get!(config[:get_info_url])
      |> Map.get(:body)
      |> Jason.decode!()

    apply(__MODULE__, :"set_#{provider}_user", [info])
  end

  @doc """
    This feature releases the receipt of data from the social network and forms the structure of the user.
  """
  @spec set_google_user(map()) :: map()
  def set_google_user(info) do
    avatar =
      if info["picture"] ==
           "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg" do
        SmileDB.Accounts.default_avatar(info["email"])
      else
        info["picture"]
      end

    %{
      avatar: avatar,
      email: info["email"],
      auth_provider: "google",
      first_name: info["given_name"],
      last_name: info["family_name"],
      nickname: SmileDB.Accounts.gen_nickname(info["given_name"], info["family_name"]),
      google_uid: info["id"],
      gender: info["gender"],
      birthday: info["birthday"],
      location: info["location"]
    }
  end

  @doc """
    This feature releases the receipt of data from the social network and forms the structure of the user.
  """
  @spec set_facebook_user(map()) :: map()
  def set_facebook_user(info) do
    avatar =
      HTTPoison.get!(
        "https://graph.facebook.com/v3.0/#{info["id"]}/picture?redirect=false&type=large"
      )
      |> Map.get(:body)
      |> Jason.decode!()

    avatar =
      if avatar["data"]["is_silhouette"] do
        SmileDB.Accounts.default_avatar(info["email"])
      else
        avatar["data"]["url"]
      end

    birthday =
      if info["birthday"] do
        data =
          info["birthday"]
          |> String.splitter("/")
          |> Enum.take(3)

        "#{Enum.at(data, 2)}-#{Enum.at(data, 0)}-#{Enum.at(data, 1)}"
      end

    %{
      avatar: avatar,
      email: info["email"],
      auth_provider: "facebook",
      first_name: info["first_name"],
      last_name: info["last_name"],
      nickname: SmileDB.Accounts.gen_nickname(info["first_name"], info["last_name"]),
      facebook_uid: info["id"],
      gender: info["gender"],
      birthday: birthday,
      location: info["location"]["name"]
    }
  end

  @doc """
    This feature releases the import of the user's photo from a social network.

    ## Examples

        iex> Auth.get_google_avatar(123456789)
        "url"
  """
  @spec get_google_avatar(Integer.t()) :: String.t() | nil
  def get_google_avatar(google_uid) when not is_nil(google_uid) do
    info =
      HTTPoison.get!("http://picasaweb.google.com/data/entry/api/user/#{google_uid}?alt=json")
      |> Map.get(:body)
      |> Jason.decode!()

    info["entry"]["gphoto$thumbnail"]["$t"] <> "?sz=500"
  end

  def get_google_avatar(_), do: nil

  @doc """
    This feature releases the import of the user's photo from a social network.

    ## Examples

        iex> Auth.get_facebook_avatar(123456789)
        "url"
  """
  @spec get_facebook_avatar(Integer.t()) :: String.t() | nil
  def get_facebook_avatar(facebook_uid) when not is_nil(facebook_uid) do
    info =
      HTTPoison.get!(
        "https://graph.facebook.com/v2.12/#{facebook_uid}/picture?redirect=false&type=large"
      )
      |> Map.get(:body)
      |> Jason.decode!()

    info["data"]["url"]
  end

  def get_facebook_avatar(_), do: nil
end
