defmodule SmileDB.Auth.HttpErrorHandler do
  @moduledoc false

  import Plug.Conn

  @spec auth_error(Plug.Conn.t(), {map(), map()}, map()) :: map()
  def auth_error(conn, {type, _reason}, _opts) do
    body = Jason.encode!(%{message: to_string(type)})
    send_resp(conn, 401, body)
  end
end
