defmodule SmileDB.Auth.Guardian.Pipeline.AuthPipeline do
  @moduledoc false

  use Guardian.Plug.Pipeline,
    otp_app: :smile_db,
    module: SmileDB.Auth.Guardian,
    error_handler: SmileDB.Auth.HttpErrorHandler

  plug(Guardian.Plug.VerifyHeader, realm: "Bearer", claims: %{"typ" => "access"})
  plug(Guardian.Plug.EnsureAuthenticated)
end
