defmodule SmileDB.Auth.Guardian do
  @moduledoc """
  This module is applications implementation for a particular type/configuration of token.

  We do this by using library [`Guardian`](https://hex.pm/packages/guardian) in our module and adding the relevant configuration.
  The official documentation is located at [`Guardian`](https://hexdocs.pm/guardian/introduction-overview.html)

  For this module it is necessary to call a module of the following form, where `otp_app:` the name of the application and the name of the library:

      use Guardian, otp_app: :knowledge_phoenix

  """

  use Guardian, otp_app: :smile_db

  alias SmileDB.{Accounts, Repo}

  @doc """
  Fetches the resource that is represented by claims.

  For JWT this would normally be found in the sub field
  """
  @spec subject_for_token(Guardian.Token.resource(), Guardian.Token.claims()) :: {:ok, String.t()}
  def subject_for_token(%Accounts.User{} = resource, _claims) do
    with %{id: id, inserted_at: inserted_at, role_name: %{access_code: access_code}} <-
           Repo.preload(resource, :role_name) do
      {:ok,
       Map.new()
       |> Map.put(:id, id)
       |> Map.put(:inserted_at, inserted_at)
       |> Map.put(:access_code, access_code)
       |> Jason.encode!()}
    end
  end

  @doc """
   An optional callback invoked after the claims have been validated
  """
  @spec resource_from_claims(Guardian.Token.claims()) :: {:ok, Guardian.Token.resource()}
  def resource_from_claims(claims) do
    with {:ok, params} <- Jason.decode(claims["sub"]),
         %{"id" => id, "inserted_at" => inserted_at, "access_code" => access_code} <- params,
         %Accounts.User{role_name: %{access_code: ^access_code}} = resource <-
           ElasticSearch.Repo.get_by(Accounts.User, id: id, inserted_at: inserted_at)
           |> Repo.preload(:role_name) do
      {:ok, resource}
    else
      _ -> {:error, "unauthenticated"}
    end
  end
end
