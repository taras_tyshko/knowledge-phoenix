# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     KnowledgePhoenix.Repo.insert!(%KnowledgePhoenix.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will halt execution if something goes wrong.

import Ecto.Query

alias Faker
alias SmileDB.{Repo, Tags, Accounts, Source, Category, Ratings}
alias SmileDB.Contents.RelatedContent
alias SmileDB.Subscriptions.ContentSubscription
alias SmileDB.Ratings.{AnswerRating, CommentRating}
alias SmileDB.Notifications.{AnswerNotification, CommentNotification}

# Enabled module Faker
Faker.start()

# Object configuration which specifies the number of created data.
config = %{
  user: 9999,
  content: 999,
  answer: 1
  # comment: 1
}

# The variable that stores the tag src where is the picture that will be added in the description.
photo =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/b3709e48-fd58-11e8-8367-5254008f7291/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yMy4wNC5wbmc.png?v=63711761004\">"

photo_1 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/dbca83a4-fd58-11e8-9378-5254008f7291/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yNC4yMC5wbmc.png?v=63711761072\">"

photo_2 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/f9c8a21e-fd58-11e8-a726-5254008f7291/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yNS4xMS5wbmc.png?v=63711761122\">"

photo_3 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/56448fbc-fd59-11e8-a824-52540015d91d/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yNS40OC5wbmc.png?v=63711761278\">"

photo_4 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/74f5318c-fd59-11e8-bbb5-5254008f7291/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yOC40MS5wbmc.png?v=63711761329\">"

#  Specify the "fields" it Faker value for the table "Content".

photo_5 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/9d5f22ae-fd59-11e8-923e-52540015d91d/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4yOS41MC5wbmc.png?v=63711761397\">"

photo_6 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/c26cc97a-fd59-11e8-8cf6-52540015d91d/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4zMC40Ni5wbmc.png?v=63711761459\">"

photo_7 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/fe3b377a-fd59-11e8-adaf-52540015d91d/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4zMi4yNy5wbmc.png?v=63711761559\">"

photo_8 =
  "<img src=\"https://knowledge-cluster.smile-magento.com/uploads/media/desktop/users/2/contents/347e38be-fd5a-11e8-8b0d-52540015d91d/U2NyZWVuIFNob3QgMjAxOC0xMi0xMSBhdCAxNy4zNC4wMC5wbmc.png?v=63711761650\">"

0..config[:user]
|> Enum.each(fn _ ->
  first_name = Faker.Name.first_name()
  last_name = UUID.uuid1()

  # Link to table User
  {:ok, user} =
    Accounts.create_user(%{
      first_name: first_name,
      last_name: last_name,
      nickname: Accounts.gen_nickname(first_name, last_name),
      email: Faker.Internet.email(),
      password: "!123mn",
      avatar: Faker.Avatar.image_url(),
      auth_provider: Enum.random(["facebook", "google"]),
      start_expert: DateTime.utc_now(),
      facebook_uid: UUID.uuid1(),
      google_uid: UUID.uuid1(),
      expert: Faker.Lorem.words(),
      role: "customer",
      gender: Enum.random(["man", "woman"]),
      birthday: Date.to_string(Faker.Date.date_of_birth()),
      location: Faker.Address.city(),
      status: "active",
      website: "https://vuejs-dev.smile-magento.com",
      biography: Faker.Lorem.sentence(%Range{first: Enum.random(5..12), last: Enum.random(8..30)})
    })

  # Filling the table Content
  0..config[:content]
  |> Enum.each(fn _ ->
    office = %{id: "lutsk"}

    question = %{
      type: "question",
      anonymous: Enum.random([true, false]),
      title: Faker.Lorem.sentence(%Range{first: Enum.random(5..12), last: Enum.random(8..30)}),
      reviews: [user.id],
      requests: Enum.random(0..100),
      tags: Faker.Lorem.words(%Range{first: Enum.random(5..20), last: Enum.random(4..15)}),
      description:
        "<p>" <>
          Faker.Lorem.sentence(%Range{first: Enum.random(3..30), last: Enum.random(10..100)}) <>
          Enum.random([
            photo,
            photo_1,
            photo_2,
            photo_3,
            photo_4,
            photo_5,
            photo_6,
            photo_7,
            photo_8
          ]) <>
          Faker.Lorem.sentence(%Range{first: Enum.random(5..30), last: Enum.random(10..100)}) <>
          "</p>",
      status: "open",
      keywords: Faker.Lorem.word(),
      files_visible: Enum.random([true, false]),
      user_id: user.id,
      author: user.nickname
    }

    complaint = %{
      type: "complaint",
      anonymous: Enum.random([true, false]),
      title: Faker.Lorem.sentence(%Range{first: Enum.random(5..12), last: Enum.random(8..30)}),
      reviews: [user.id],
      requests: Enum.random(0..100),
      permission_tags: [office.id],
      tags: [
        office.id | Faker.Lorem.words(%Range{first: Enum.random(5..20), last: Enum.random(4..15)})
      ],
      description:
        "<p>" <>
          Faker.Lorem.sentence(%Range{first: Enum.random(3..30), last: Enum.random(10..100)}) <>
          Enum.random([
            photo,
            photo_1,
            photo_2,
            photo_3,
            photo_4,
            photo_5,
            photo_6,
            photo_7,
            photo_8
          ]) <>
          Faker.Lorem.sentence(%Range{first: Enum.random(5..30), last: Enum.random(10..100)}) <>
          "</p>",
      status: "open",
      files: [],
      keywords: Faker.Lorem.word(),
      files_visible: Enum.random([true, false]),
      user_id: user.id,
      office_id: office.id,
      author: user.nickname
    }

    {:ok, content} =
      [question, complaint]
      |> Enum.random()
      |> Source.create_content()

    if content.type == "complaint" do
      {:ok, _rating} =
        Ratings.create_office_rating(%{
          content_id: content.id,
          office_id: office.id,
          score: Enum.random(1..5)
        })
    end

    Source.put_feed(content)

    # Repo.transaction(fn ->
    # Repo.insert!(%ContentSubscription{
    #   user_id: user.id,
    #   content_id: content.id
    # })

    # Repo.insert!(%RelatedContent{
    #   related_content_id: content.id,
    #   content_id: content.id
    # })
    # end)

    # Filling the table Answer
    0..config[:answer]
    |> Enum.each(fn _ ->
      {:ok, answer} =
        Source.create_answer(%{
          anonymous: Enum.random([true, false]),
          tags: Faker.Lorem.words(%Range{first: Enum.random(3..20), last: Enum.random(4..20)}),
          description:
            "<p>" <>
              Faker.Lorem.sentence(%Range{first: Enum.random(5..50), last: Enum.random(20..100)}) <>
              Enum.random([
                photo,
                photo_1,
                photo_2,
                photo_3,
                photo_4,
                photo_5,
                photo_6,
                photo_7,
                photo_8
              ]) <>
              Faker.Lorem.sentence(%Range{first: Enum.random(5..50), last: Enum.random(20..100)}) <>
              "</p>",
          keywords: Faker.Lorem.word(),
          user_id: user.id,
          author: user.nickname,
          content_id: content.id
        })

      Source.put_feed(answer)

      # Repo.transaction(fn ->
      #   Repo.insert!(%AnswerNotification{
      #     answer_id: answer.id,
      #     user_id: user.id
      #   })

      #   Repo.insert!(%AnswerRating{
      #     answer_id: answer.id,
      #     user_id: user.id,
      #     status: Faker.Util.pick([true, false])
      #   })
      # end)

      # Filling the table Comment
      # 0..config[:comment]
      # |> Enum.each(fn _ ->
      #   {:ok, c} =
      #     Source.create_comment(%{
      #       description:
      #         "<p>" <>
      #           Enum.random([
      #             photo,
      #             photo_1,
      #             photo_2,
      #             photo_3,
      #             photo_4,
      #             photo_5,
      #             photo_6,
      #             photo_7,
      #             photo_8
      #           ]) <>
      #           Faker.Lorem.sentence(%Range{first: Enum.random(1..50), last: Enum.random(1..100)}) <>
      #           "</p>",
      #       keywords: Faker.Lorem.word(),
      #       anonymous: Faker.Util.pick([true, false]),
      #       content_id: content.id,
      #       user_id: user.id,
      #       author: user.nickname,
      #       answer_id: answer.id
      #     })

      #   {:ok, c} = Source.update_comment(c, %{path: Integer.to_string(c.id)})

      #   Source.put_feed(c)

      # Repo.transaction(fn ->
      #   Repo.insert!(%CommentNotification{
      #     comment_id: c.id,
      #     user_id: user.id
      #   })

      #   Repo.insert!(%CommentRating{
      #     comment_id: c.id,
      #     user_id: user.id,
      #     status: Faker.Util.pick([true, false])
      #   })
      # end)

      # {:ok, cx} =
      #   Source.create_comment(%{
      #     description:
      #       "<p>" <>
      #         Faker.Lorem.sentence(%Range{first: Enum.random(1..50), last: Enum.random(1..100)}) <>
      #         "</p>",
      #     keywords: Faker.Lorem.word(),
      #     anonymous: Faker.Util.pick([true, false]),
      #     content_id: content.id,
      #     user_id: user.id,
      #     author: user.nickname,
      #     answer_id: answer.id
      #   })

      # {:ok, cx} =
      #   Source.update_comment(cx, %{
      #     path: Enum.join(c.path.labels ++ [Integer.to_string(cx.id)], ".")
      #   })

      # Source.put_feed(cx)

      # Repo.transaction(fn ->
      # Repo.insert!(%CommentNotification{
      #   comment_id: cx.id,
      #   user_id: user.id
      # })

      #   Repo.insert!(%CommentRating{
      #     comment_id: cx.id,
      #     user_id: user.id,
      #     status: Faker.Util.pick([true, false])
      #   })
      # end)

      # {:ok, cxl} =
      #   Source.create_comment(%{
      #     description:
      #       "<p>" <>
      #         Faker.Lorem.sentence(%Range{first: Enum.random(1..50), last: Enum.random(1..100)}) <>
      #         "</p>",
      #     keywords: Faker.Lorem.word(),
      #     anonymous: Faker.Util.pick([true, false]),
      #     content_id: content.id,
      #     user_id: user.id,
      #     author: user.nickname,
      #     answer_id: answer.id
      #   })

      # {:ok, cxl} =
      #   Source.update_comment(cxl, %{
      #     path: Enum.join(cx.path.labels ++ [Integer.to_string(cxl.id)], ".")
      #   })

      # Source.put_feed(cxl)

      # Repo.transaction(fn ->
      # Repo.insert!(%CommentNotification{
      #   comment_id: cxl.id,
      #   user_id: user.id
      # })

      # Repo.insert!(%CommentRating{
      #   comment_id: cxl.id,
      #   user_id: user.id,
      #   status: Faker.Util.pick([true, false])
      # })
      # end)
      # end)
    end)
  end)
end)
