defmodule SmileDB.Repo.Migrations.AlterTableContentsAddFieldPermissionTags do
  use Ecto.Migration

  def change do
    alter table(:contents) do
      add :permission_tags, {:array, :string}, default: []
    end
  end
end
