defmodule SmileDB.Repo.Migrations.CreateContentsTags do
  use Ecto.Migration

  def change do
    create table(:contents_tags) do
      add :content_id, references(:contents, on_delete: :delete_all), null: false
      add :tag_id, references(:tags, on_delete: :delete_all), null: false

      timestamps()
    end

   create index(:contents_tags, [:content_id])
   create index(:contents_tags, [:tag_id])
  end
end
