defmodule SmileDB.Repo.Migrations.CreateTagsSubscription do
  use Ecto.Migration

  def change do
    create table(:tags_subscription) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)

      add(:title, references(:tags, on_delete: :delete_all, column: :title, type: :string),
        null: false
      )

      timestamps()
    end

    create(unique_index(:tags_subscription, [:title, :user_id]))
    create(index(:tags_subscription, [:title]))
    create(index(:tags_subscription, [:user_id]))
  end
end
