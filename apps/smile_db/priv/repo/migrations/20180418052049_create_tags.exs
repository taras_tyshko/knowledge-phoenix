defmodule SmileDB.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :title, :string
      add :count, :integer

      timestamps()
    end

    create unique_index(:tags, [:title])
    create index(:tags, ["count DESC NULLS LAST"], name: :tags_count_desc)
  end
end
