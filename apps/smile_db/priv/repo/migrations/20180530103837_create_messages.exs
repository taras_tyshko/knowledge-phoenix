defmodule Messenger.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :uuid, :string
      add :event_name, :string
      add :description, :text, null: false
      add :room_id, references(:rooms, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

  create index(:messages, [:room_id])
  create index(:messages, [:user_id])
  end
end
