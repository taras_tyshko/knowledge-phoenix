defmodule SmileDB.Repo.Migrations.CreateOfficeSubscription do
  use Ecto.Migration

  def change do
    create table(:offices_subscription) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:office_id, references(:offices, on_delete: :delete_all, type: :string), null: false)

      timestamps()
    end

    create(unique_index(:offices_subscription, [:office_id, :user_id]))
    create(index(:offices_subscription, [:office_id]))
    create(index(:offices_subscription, [:user_id]))
  end
end
