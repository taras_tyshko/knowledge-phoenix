defmodule Messenger.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def change do
    create table(:rooms) do
      add(:uuid, :string)
      add(:avatar, :string)
      add(:access_code, :bigint)
      add :name, :string, default: ""
      add :type, :string, default: "privat", null: false

      timestamps()
    end
  end
end