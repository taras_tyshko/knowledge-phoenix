defmodule SmileDB.Repo.Migrations.CreateAchieves do
  use Ecto.Migration

  def change do
    create table(:achieves) do
      add(:avatar, :text)
      add(:title, :string)
      add(:uuid, :string)

      timestamps()
    end

    create unique_index(:achieves, [:title])
  end
end
