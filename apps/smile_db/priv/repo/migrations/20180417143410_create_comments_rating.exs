defmodule SmileDB.Repo.Migrations.CreateCommentsRating do
  use Ecto.Migration

  def change do
    create table(:comments_rating) do
      add :status, :boolean, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :comment_id, references(:comments, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:comments_rating, [:comment_id, :user_id])
    create index(:comments_rating, [:user_id])
    create index(:comments_rating, [:comment_id])
  end
end
