defmodule Messenger.Repo.Migrations.CreateMessagesNotifications do
  use Ecto.Migration

  def change do
    create table(:messages_notifications) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:room_id, references(:rooms, on_delete: :delete_all), null: false)
      add(:message_id, references(:messages, on_delete: :delete_all), null: false)

      timestamps()
    end

   create(index(:messages_notifications, [:user_id]))
   create(index(:messages_notifications, [:room_id]))
   create(index(:messages_notifications, [:message_id]))
   create(unique_index(:messages_notifications, [:message_id, :user_id]))
  end
end
