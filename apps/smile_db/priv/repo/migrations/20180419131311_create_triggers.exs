defmodule SmileDB.Repo.Migrations.CreateTriggers do
  use Ecto.Migration

  def change do
    execute("CREATE OR REPLACE FUNCTION update_users_likes_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT') THEN
        UPDATE users SET likes_count = likes_count + 1
        WHERE NEW.user_id = users.id;
        RETURN NEW;
      ELSIF (TG_OP = 'DELETE') THEN
        UPDATE users SET likes_count = likes_count - 1
        WHERE OLD.user_id = users.id;
        RETURN OLD;
      END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS add_likes_count ON users_rating;")
    execute("CREATE TRIGGER add_likes_count
    AFTER INSERT
    ON users_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_users_likes_count();")

    execute("DROP TRIGGER IF EXISTS remove_likes_count ON users_rating;")
    execute("CREATE TRIGGER remove_likes_count
    BEFORE DELETE
    ON users_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_users_likes_count();")

    execute("CREATE OR REPLACE FUNCTION update_answers_likes_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
        IF (NEW.status = true) THEN
          UPDATE answers SET ratings_count = ratings_count + 1
          WHERE NEW.answer_id = answers.id;
          RETURN NEW;
        ELSIF (NEW.status = false) THEN
          UPDATE answers SET ratings_count = ratings_count - 1
          WHERE NEW.answer_id = answers.id;
          RETURN NEW;
        END IF;
      ELSIF (TG_OP = 'DELETE') THEN
        IF (OLD.status = true) THEN
          UPDATE answers SET ratings_count = ratings_count - 1
          WHERE OLD.answer_id = answers.id;
        ELSIF (OLD.status = false) THEN
          UPDATE answers SET ratings_count = ratings_count + 1
          WHERE OLD.answer_id = answers.id;
        END IF;
        RETURN OLD;
      END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS add_likes_count ON answers_rating;")
    execute("CREATE TRIGGER add_likes_count
    AFTER INSERT OR UPDATE OF status
    ON answers_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_answers_likes_count();")

    execute("DROP TRIGGER IF EXISTS remove_likes_count ON answers_rating;")
    execute("CREATE TRIGGER remove_likes_count
    BEFORE DELETE
    ON answers_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_answers_likes_count();")

    execute("CREATE OR REPLACE FUNCTION update_comments_likes_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
        IF (NEW.status = true) THEN
          UPDATE comments SET ratings_count = ratings_count + 1
          WHERE NEW.comment_id = comments.id;
          RETURN NEW;
        ELSIF (NEW.status = false) THEN
          UPDATE comments SET ratings_count = ratings_count - 1
          WHERE NEW.comment_id = comments.id;
          RETURN NEW;
        END IF;
      ELSIF (TG_OP = 'DELETE') THEN
        IF (OLD.status = true) THEN
          UPDATE comments SET ratings_count = ratings_count - 1
          WHERE OLD.comment_id = comments.id;
        ELSIF (OLD.status = false) THEN
          UPDATE comments SET ratings_count = ratings_count + 1
          WHERE OLD.comment_id = comments.id;
        END IF;
        RETURN OLD;
      END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS add_likes_count ON comments_rating;")
    execute("CREATE TRIGGER add_likes_count
    AFTER INSERT OR UPDATE OF status
    ON comments_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_comments_likes_count();")

    execute("DROP TRIGGER IF EXISTS remove_likes_count ON comments_rating;")
    execute("CREATE TRIGGER remove_likes_count
    BEFORE DELETE
    ON comments_rating
    FOR EACH ROW
    EXECUTE PROCEDURE update_comments_likes_count();")

    execute("CREATE OR REPLACE FUNCTION update_user_contents_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT') THEN
          UPDATE users SET contents_count = contents_count + 1
          WHERE users.id = NEW.user_id;
      ELSIF (TG_OP = 'DELETE') THEN
          UPDATE users SET contents_count = contents_count - 1
          WHERE users.id = OLD.user_id;
        END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS update_contents_count ON contents;")
    execute("CREATE TRIGGER update_contents_count
    AFTER INSERT OR DELETE
    ON contents
    FOR EACH ROW
    EXECUTE PROCEDURE update_user_contents_count();")

    execute("CREATE OR REPLACE FUNCTION update_user_answers_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT') THEN
          UPDATE users SET answers_count = answers_count + 1
          WHERE users.id = NEW.user_id;
          UPDATE contents SET answers_count = answers_count + 1
          WHERE contents.id = NEW.content_id;
      ELSIF (TG_OP = 'DELETE') THEN
          UPDATE users SET answers_count = answers_count - 1
          WHERE users.id = OLD.user_id;
          UPDATE contents SET answers_count = answers_count - 1
          WHERE contents.id = OLD.content_id;
        END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS update_answers_count ON answers;")
    execute("CREATE TRIGGER update_answers_count
    AFTER INSERT OR DELETE
    ON answers
    FOR EACH ROW
    EXECUTE PROCEDURE update_user_answers_count();")

    execute("CREATE OR REPLACE FUNCTION update_comments_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT') THEN
          UPDATE users SET comments_count = comments_count + 1
          WHERE users.id = NEW.user_id;
          UPDATE answers SET comments_count = comments_count + 1
          WHERE answers.id = NEW.answer_id;
          UPDATE contents SET comments_count = comments_count + 1
          WHERE contents.id = NEW.content_id AND NEW.answer_id IS NULL;
      ELSIF (TG_OP = 'DELETE') THEN
          UPDATE users SET comments_count = comments_count - 1
          WHERE users.id = OLD.user_id;
          UPDATE answers SET comments_count = comments_count - 1
          WHERE answers.id = OLD.answer_id;
          UPDATE contents SET comments_count = comments_count - 1
          WHERE contents.id = OLD.content_id AND OLD.answer_id IS NULL;
        END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS update_comments_count ON comments;")
    execute("CREATE TRIGGER update_comments_count
    AFTER INSERT OR DELETE
    ON comments
    FOR EACH ROW
    EXECUTE PROCEDURE update_comments_count();")

    execute("CREATE OR REPLACE FUNCTION update_author()
    RETURNS trigger AS $$
    BEGIN
      UPDATE contents SET author = NEW.nickname
      WHERE contents.user_id = OLD.id;
      UPDATE answers SET author = NEW.nickname
      WHERE answers.user_id = OLD.id;
      UPDATE comments SET author = NEW.nickname
      WHERE comments.user_id = OLD.id;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS update_author ON users;")
    execute("CREATE TRIGGER update_author
    AFTER UPDATE OF nickname
    ON users
    FOR EACH ROW
    EXECUTE PROCEDURE update_author();")

    execute("CREATE OR REPLACE FUNCTION update_tags_count()
    RETURNS trigger AS $$
    BEGIN
      IF (TG_OP = 'INSERT') THEN
          UPDATE tags SET count = count + 1
          WHERE tags.id = NEW.tag_id;
      ELSIF (TG_OP = 'DELETE') THEN
          UPDATE tags SET count = count - 1
          WHERE tags.id = OLD.tag_id;
        END IF;
      RETURN NULL;
    END;
    $$ LANGUAGE plpgsql;")

    execute("DROP TRIGGER IF EXISTS update_tags_count ON contents_tags;")
    execute("CREATE TRIGGER update_tags_count
    AFTER INSERT OR DELETE
    ON contents_tags
    FOR EACH ROW
    EXECUTE PROCEDURE update_tags_count();")
  end
end
