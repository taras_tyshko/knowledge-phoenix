defmodule SmileDB.Repo.Migrations.CreateCommentsRatingNotification do
  use Ecto.Migration

  def change do
    create table(:comments_rating_notification) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :comment_rating_id, references(:comments_rating, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:comments_rating_notification, [:comment_rating_id, :user_id])
    create index(:comments_rating_notification, [:user_id])
    create index(:comments_rating_notification, [:comment_rating_id])
  end
end
