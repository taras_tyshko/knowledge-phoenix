defmodule SmileDB.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :uuid, :string
      add :email, :string
      add :gender, :string
      add :avatar, :text
      add :status, :string
      add :website, :string
      add :nickname, :string
      add :birthday, :string
      add :location, :string
      add :biography, :string
      add :last_name, :string
      add :google_uid, :string
      add :first_name, :string
      add :facebook_uid, :string
      add :auth_provider, :string
      add :password_hash, :string
      add :ban_time, :timestamptz
      add :start_expert, :timestamptz
      add :likes_count, :integer, default: 0
      add :answers_count, :integer, default: 0
      add :comments_count, :integer, default: 0
      add :contents_count, :integer, default: 0
      add :expert, {:array, :string}, default: []
      add :role, references(:roles, [on_delete: :delete_all, on_update: :update_all, column: :role, type: :string]), default: "customer", null: false

      timestamps()
    end

    create(index(:users, [:role]))
    create unique_index(:users, [:nickname])
    create unique_index(:users, [:google_uid])
    create unique_index(:users, [:facebook_uid])
    create index(:users, [:expert], where: "array_length(expert, 1) > 0")
    create index(:users, ["contents_count DESC NULLS LAST"], name: :users_contents_count_desc)
  end
end
