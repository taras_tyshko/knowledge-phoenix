defmodule SmileDB.Repo.Migrations.CreateAnswers do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :uuid, :string
      add :author, :string
      add :keywords, :string
      add :description, :text
      add :ratings_count, :integer, default: 0
      add :tags, {:array, :string}, default: []
      add :comments_count, :integer, default: 0
      add :shadow, :boolean, default: false, null: false
      add :anonymous, :boolean, default: false, null: false 
      add :content_id, references(:contents, on_delete: :delete_all), null: false
      add :user_id, references(:users, [on_delete: :delete_all]), null: false

      timestamps()
    end

    create index(:answers, [:user_id])
    create index(:answers, [:content_id])
    create index(:answers, [:tags], using: :gin)
    create index(:answers, [:anonymous], where: "anonymous = false")
    create index(:answers, ["ratings_count DESC NULLS LAST"], name: :answers_ratings_count_desc)
    create index(:answers, ["comments_count DESC NULLS LAST"], name: :answers_comments_count_desc)
  end
end
