defmodule SmileDB.Repo.Migrations.CreateUsersSubscription do
  use Ecto.Migration

  def change do
    create table(:users_subscription) do
      add(:status, :boolean, null: false)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:follower_id, references(:users, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:users_subscription, [:user_id]))
    create(index(:users_subscription, [:follower_id]))
    create(unique_index(:users_subscription, [:follower_id, :user_id]))
  end
end
