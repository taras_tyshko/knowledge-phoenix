defmodule SmileDB.Repo.Migrations.CreateRoleAccess do
  use Ecto.Migration

  def change do
    create table(:role_access) do
      add(:state, :integer, default: 0)
      add :permission_id, references(:permissions, on_delete: :delete_all), null: false
      add(:role, references(:roles, [on_delete: :delete_all, on_update: :update_all, column: :role, type: :string]), null: false)

      timestamps()
    end

    create(index(:role_access, [:role]))
    create(index(:role_access, [:permission_id]))
  end
end
