defmodule SmileDB.Repo.Migrations.CreateRoles do
  use Ecto.Migration

  def change do
    create table(:roles, primary_key: false) do
      add(:access_code, :bigint)
      add(:role, :string, primary_key: true, null: false)

      timestamps()
    end
  end
end
