defmodule SmileDB.Repo.Migrations.CreateOffices do
  use Ecto.Migration

  def change do
    create table(:offices, primary_key: false) do
      add(:id, :string, primary_key: true)
      add(:title, :string, null: false)
      add(:uuid, :string)
      add(:country, :string)
      add(:location, :string)
      add(:email, :string)
      add(:avatar, :string)

      timestamps()
    end
  end
end
