defmodule SmileDB.Repo.Migrations.CreateAnswersNotification do
  use Ecto.Migration

  def change do
    create table(:answers_notification) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :answer_id, references(:answers, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:answers_notification, [:user_id])
    create index(:answers_notification, [:answer_id])
    create unique_index(:answers_notification, [:answer_id, :user_id])
  end
end
