defmodule SmileDB.Repo.Migrations.CreateChangeEmail do
  use Ecto.Migration

  def change do
    create table(:change_emails) do
      add(:hash, :string, null: false)
      add(:email, :string, null: false)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)

      timestamps()
    end

    create index(:change_emails, [:user_id])
  end
end
