defmodule SmileDB.Repo.Migrations.CreateContentsNotification do
  use Ecto.Migration

  def change do
    create table(:contents_notification) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :content_id, references(:contents, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:contents_notification, [:user_id])
    create index(:contents_notification, [:content_id])
    create unique_index(:contents_notification, [:content_id, :user_id])
  end
end
