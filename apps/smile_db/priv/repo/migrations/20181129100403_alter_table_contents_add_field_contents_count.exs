defmodule SmileDB.Repo.Migrations.AlterTableContentsAddFieldContentsCount do
  use Ecto.Migration

  def change do
    alter table(:contents) do
      add(:comments_count, :integer, default: 0)
    end
  end
end
