defmodule SmileDB.Repo.Migrations.CreateContents do
  use Ecto.Migration

  def change do
    create table(:contents) do
      add(:files, {:array, :map}, default: [])
      add(:uuid, :string)
      add(:title, :string)
      add(:status, :string)
      add(:author, :string)
      add(:keywords, :string)
      add(:description, :text)
      add(:type, :string, null: false)
      add(:requests, :integer, default: 0)
      add(:answers_count, :integer, default: 0)
      add(:reviews_count, :integer, default: 0)
      add(:tags, {:array, :string}, default: [])
      add(:reviews, {:array, :integer}, default: [])
      add(:shadow, :boolean, default: false, null: false)
      add(:files_visible, :boolean, default: true, null: false)
      add(:anonymous, :boolean, default: false, null: false)
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:office_id, references(:offices, on_delete: :delete_all, type: :string))

      timestamps()
    end

    create(index(:contents, [:user_id]))
    create(index(:contents, [:tags], using: :gin))
    create(index(:contents, [:anonymous], where: "anonymous = false"))
    create(index(:contents, ["inserted_at DESC NULLS LAST"], name: :contents_inserted_at_desc))

    create(
      index(:contents, ["reviews_count DESC NULLS LAST"], name: :contents_reviews_count_desc)
    )

    create(
      index(:contents, ["answers_count DESC NULLS LAST"], name: :contents_answers_count_desc)
    )
  end
end
