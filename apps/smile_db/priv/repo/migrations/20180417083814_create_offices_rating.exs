defmodule SmileDB.Repo.Migrations.CreateOfficesRatting do
  use Ecto.Migration

  def change do
    create table(:offices_rating) do
      add(:score, :integer)
      add(:office_id, references(:offices, [on_delete: :delete_all, type: :string]), null: false)
      add(:content_id, references(:contents, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:offices_rating, [:office_id]))
    create(index(:offices_rating, [:content_id]))
    create unique_index(:offices_rating, [:office_id, :content_id])
  end
end
