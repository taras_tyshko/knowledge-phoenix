defmodule SmileDB.Repo.Migrations.CreateUserAchieves do
  use Ecto.Migration

  def change do
    create table(:user_achieves) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:achieve_id, references(:achieves, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:user_achieves, [:user_id]))
    create(index(:user_achieves, [:achieve_id]))
  end
end
