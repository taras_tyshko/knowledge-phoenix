defmodule SmileDB.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :uuid, :string
      add :path, :ltree
      add :author, :string
      add :keywords, :string
      add :description, :text
      add :ratings_count, :integer, default: 0
      add :shadow, :boolean, default: false, null: false
      add :anonymous, :boolean, default: false, null: false
      add :answer_id, references(:answers, on_delete: :delete_all)
      add :user_id, references(:users, [on_delete: :delete_all]), null: false
      add :content_id, references(:contents, [on_delete: :delete_all]), null: false

      timestamps()
    end

    create index(:comments, [:user_id])
    create index(:comments, [:answer_id])
    create index(:comments, [:content_id])
    create index(:comments, [:path], using: :gist)
    create index(:comments, ["ratings_count DESC NULLS LAST"], name: :comments_ratings_count_desc)
  end
end
