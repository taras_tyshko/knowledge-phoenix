defmodule SmileDB.Repo.Migrations.CreateUsersRating do
  use Ecto.Migration

  def change do
    create table(:users_rating) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :answer_rating_id, references(:answers_rating, on_delete: :delete_all)
      add :comment_rating_id, references(:comments_rating, on_delete: :delete_all)

      timestamps()
    end

     create index(:users_rating, [:user_id])
     create index(:users_rating, [:answer_rating_id])
     create index(:users_rating, [:comment_rating_id])
  end
end
