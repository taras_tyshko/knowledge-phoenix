defmodule SmileDB.Repo.Migrations.CreateContentsSubscription do
  use Ecto.Migration

  def change do
    create table(:contents_subscription) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :content_id, references(:contents, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:contents_subscription, [:content_id, :user_id])
    create index(:contents_subscription, [:user_id])
    create index(:contents_subscription, [:content_id])
  end
end
