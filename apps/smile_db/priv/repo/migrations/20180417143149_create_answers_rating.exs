defmodule SmileDB.Repo.Migrations.CreateAnswersRating do
  use Ecto.Migration

  def change do
    create table(:answers_rating) do
      add :status, :boolean, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :answer_id, references(:answers, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:answers_rating, [:answer_id, :user_id])
    create index(:answers_rating, [:user_id])
    create index(:answers_rating, [:answer_id])
    create index(:answers_rating, [:status], where: "status = true")
  end
end
