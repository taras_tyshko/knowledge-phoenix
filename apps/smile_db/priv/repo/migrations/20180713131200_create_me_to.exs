defmodule SmileDB.Repo.Migrations.CreateMeTo do
  use Ecto.Migration

  def change do
    create table(:me_to) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:content_id, references(:contents, on_delete: :delete_all), null: true)

      timestamps()
    end

   create(index(:me_to, [:user_id]))
   create(index(:me_to, [:content_id]))
   create(unique_index(:me_to, [:user_id, :content_id]))
  end
end
