defmodule SmileDB.Repo.Migrations.RunSeedAdmin do
  use Ecto.Migration

  def change do
    role_data = ["customer", "admin", "expert", "moderator", "moderator +"]

    permisions_data = [
      "user-permission",
      "expert-permission",
      "moderation-permission",
      "admin-permission",
      "content-moderation",
      "answer-moderation",
      "update-data",
      "delete-data",
      "achievements-create",
      "achievements-assign",
      "achievements-delete",
      "role-create",
      "role-assign",
      "role-delete",
      "permission-create",
      "permission-change",
      "expert-create",
      "expert-change",
      "expert-delete",
      "admin-login",
      "user-create-from-admin-panel",
      "user-update-from-admin-panel",
      "user-delete-from-admin-panel",
      "ban-user-permanently-on_time",
      "hide-user-content",
      "content-edit",
      "content-delete",
      "answer-create",
      "answer-edit",
      "answer-delete",
      "comment-create",
      "comment-edit",
      "comment-delete",
      "relate-content-create",
      "relate-content-edit",
      "relate-content-delete",
      "expert-request-create",
      "expert-request-delete"
    ]

    Enum.map(role_data, &SmileDB.Accounts.create_role(%{role: &1, access_code: 274_877_906_943}))

    Enum.map(permisions_data, fn data ->
      {:ok, permission} = SmileDB.Permissions.create_permission(%{name: data})

      Enum.map(
        role_data,
        &SmileDB.Permissions.create_role_access(%{permission_id: permission.id, role: &1})
      )
    end)
  end
end
