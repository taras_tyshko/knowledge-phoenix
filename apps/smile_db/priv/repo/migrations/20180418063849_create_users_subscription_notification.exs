defmodule SmileDB.Repo.Migrations.CreateUserSubscriptionNotification do
  use Ecto.Migration

  def change do
    create table(:users_subscription_notification) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :user_subscription_id, references(:users_subscription, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:users_subscription_notification, [:user_subscription_id, :user_id])
    create index(:users_subscription_notification, [:user_id])
    create index(:users_subscription_notification, [:user_subscription_id])
  end
end
