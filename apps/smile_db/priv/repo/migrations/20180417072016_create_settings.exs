defmodule SmileDB.Repo.Migrations.CreateSettings do
  use Ecto.Migration

  def change do
    create table(:settings) do
      add :by_comments, :boolean, default: true, null: false
      add :by_answers, :boolean, default: true, null: false
      add :email_enable, :boolean, default: true, null: false
      add :email_on_reply, :boolean, default: true, null: false
      add :web_notif_upvote, :boolean, default: true, null: false
      add :web_notif_recommend, :boolean, default: true, null: false
      add :web_notif_mentions, :boolean, default: true, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:settings, [:user_id])
  end
end
