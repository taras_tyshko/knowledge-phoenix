defmodule SmileDB.Repo.Migrations.CreateCommentsNotification do
  use Ecto.Migration

  def change do
    create table(:comments_notification) do
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :comment_id, references(:comments, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:comments_notification, [:user_id])
    create index(:comments_notification, [:comment_id])
    create unique_index(:comments_notification, [:comment_id, :user_id])
  end
end
