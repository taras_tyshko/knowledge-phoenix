defmodule SmileDB.Repo.Migrations.CreateAnswersRatingNotification do
  use Ecto.Migration

  def change do
    create table(:answers_rating_notification) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:answer_rating_id, references(:answers_rating, on_delete: :delete_all), null: false)

      timestamps()
    end

    create unique_index(:answers_rating_notification, [:answer_rating_id, :user_id])
    create index(:answers_rating_notification, [:user_id])
    create index(:answers_rating_notification, [:answer_rating_id])
  end
end
