defmodule SmileDB.Repo.Migrations.CreateRelatedContents do
  use Ecto.Migration

  def change do
    create table(:related_contents) do
      add :related_content_id, references(:contents, on_delete: :delete_all), null: false
      add :content_id, references(:contents, on_delete: :delete_all), null: false

      timestamps()
    end

    create unique_index(:related_contents, [:related_content_id, :content_id])
    create index(:related_contents, [:content_id])
    create index(:related_contents, [:related_content_id])
  end
end
