defmodule SmileDB.Repo.Migrations.CreateExpertRequests do
  use Ecto.Migration

  def change do
    create table(:expert_requests) do
      add(:user_id, references(:users, on_delete: :delete_all), null: false)
      add(:tag, references(:tags, [on_delete: :delete_all, on_update: :update_all, column: :title, type: :string]))

      timestamps()
    end

    create(index(:expert_requests, [:tag]))
    create(index(:expert_requests, [:user_id]))
    create unique_index(:expert_requests, [:user_id, :tag])
  end
end
