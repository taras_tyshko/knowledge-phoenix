defmodule SmileDB.EmailsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Emails

  describe "change_emails" do
    setup [:create_user]

    @valid_attrs %{hash: "some hash", email: "some@email.com"}
    @invalid_attrs %{hash: nil, email: nil, user_id: nil}

    def change_email_fixture(attrs \\ %{}, user) do
      {:ok, change_email} =
        attrs
        |> Enum.into(Map.put(@valid_attrs, :user_id, user.id))
        |> Emails.create_change_email()

      change_email
    end

    test "create_change_email/1 with valid data creates a change_email", %{user: user} do
      assert {:ok, %Emails.ChangeEmail{} = change_email} =
               Emails.create_change_email(Map.put(@valid_attrs, :user_id, user.id))

      assert change_email.hash == "some hash"
      assert change_email.email == "some@email.com"
      assert change_email.user_id == user.id
    end

    test "create_change_email/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Emails.create_change_email(@invalid_attrs)
    end

    test "delete_change_email/1 deletes the change_email", %{user: user} do
      change_email = change_email_fixture(user)
      assert {:ok, %Emails.ChangeEmail{}} = Emails.delete_change_email(change_email)
    end
    
    test "get_change_email_by_hash!/1 deletes the change_email", %{user: user} do
      change_email = change_email_fixture(user)
      assert change_email == Emails.get_change_email_by_hash!(change_email.hash)
    end
    
    test "get_change_email_by_user_id/1 deletes the change_email", %{user: user} do
      change_email = change_email_fixture(user)
      assert [change_email] == Emails.get_change_email_by_user_id(change_email.user_id)
    end
  end
end
