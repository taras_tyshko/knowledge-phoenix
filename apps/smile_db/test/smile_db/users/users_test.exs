defmodule SmileDB.UsersTest do
  use SmileDB.DataCase

  alias SmileDB.Users

  describe "expert_requests" do
    setup [:create_user]
    setup [:create_tag]
    alias SmileDB.Users.Expert

    @valid_attrs %{tag: "some_tag"}
    @invalid_attrs %{tag: nil, user_id: nil}

    def expert_fixture(tag, user) do
      {:ok, expert} =
        @valid_attrs
        |> Map.put(:user_id, user.id)
        |> Map.put(:tag, tag.title)
        |> Users.create_expert()

      expert
    end

    test "list_expert_requests/0 returns all expert_requests", %{user: user, tag: tag} do
      expert = expert_fixture(tag, user)
      assert Users.list_expert_requests(expert.user_id) == [expert.tag]
    end

    test "get_experts_by_user/2 returns the expert with given id", %{user: user, tag: tag} do
      expert = expert_fixture(tag, user)
      assert Users.get_experts_by_user([expert.tag], expert.user_id) == [expert]
    end

    test "get_experts_by_user/1 returns the expert with given id", %{user: user, tag: tag} do
      expert = expert_fixture(tag, user)
      assert Users.get_experts_by_user(expert.user_id) == [expert]
    end

    test "create_expert/1 with valid data creates a expert", %{user: user, tag: tag} do
      assert {:ok, %Expert{} = expert} =
               Users.create_expert(
                 @valid_attrs
                 |> Map.put(:user_id, user.id)
                 |> Map.put(:tag, tag.title)
               )
    end

    test "create_expert/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_expert(@invalid_attrs)
    end

    test "delete_expert/1 deletes the expert", %{user: user, tag: tag} do
      expert = expert_fixture(tag, user)
      assert {:ok, %Expert{}} = Users.delete_expert(expert)
    end
  end

  describe "change_password" do
    setup [:create_user]
    alias SmileDB.Users.ChangePassword

    @valid_attrs %{hash: "some hash"}
    @invalid_attrs %{hash: nil, user_id: nil}

    def change_password_fixture(user) do
      {:ok, change_password} =
        @valid_attrs
        |> Map.put(:user_id, user.id)
        |> Users.create_change_password()

      change_password
    end

    test "get_change_password!/1 returns the change_password with given id", %{user: user} do
      change_password = change_password_fixture(user)
      assert Users.get_change_password!(change_password.id) == change_password
    end

    test "get_change_password_by_hash/1 returns the change_password with given id", %{user: user} do
      change_password = change_password_fixture(user)
      assert Users.get_change_password_by_hash(change_password.hash) == change_password
    end

    test "create_change_password/1 with valid data creates a change_password", %{user: user} do
      assert {:ok, %ChangePassword{} = change_password} =
               Users.create_change_password(Map.put(@valid_attrs, :user_id, user.id))

      assert change_password.hash == "some hash"
    end

    test "create_change_password/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_change_password(@invalid_attrs)
    end

    test "delete_change_password/1 deletes the change_password", %{user: user} do
      change_password = change_password_fixture(user)
      assert {:ok, %ChangePassword{}} = Users.delete_change_password(change_password)
      assert_raise Ecto.NoResultsError, fn -> Users.get_change_password!(change_password.id) end
    end
  end
end
