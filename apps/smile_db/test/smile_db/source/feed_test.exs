defmodule SmileDB.FeedTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Source

  describe "feeds" do
    setup [:create_answer]

    test "create_feed/1 create record in feed for answer with valid data", %{
      user: user,
      answer: answer
    } do
      assert feed = Source.put_feed(answer)

      assert feed.tags == answer.tags
      assert feed.author == user.nickname
      assert feed.answer_id == answer.id
      assert feed.anonymous == answer.anonymous
      assert feed.shadow == answer.shadow
    end

    test "create_feed/1 create record in feed for content with valid data", %{
      user: user,
      content: content
    } do
      assert feed = Source.put_feed(content)

      assert feed.tags == content.tags
      assert feed.author == user.nickname
      assert feed.content_id == content.id
      assert feed.anonymous == content.anonymous
      assert feed.shadow == content.shadow
    end
  end
end
