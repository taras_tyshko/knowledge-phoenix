defmodule SmileDB.SourceTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Source

  describe "contents" do
    setup [:create_user]

    @valid_attrs %{
      anonymous: true,
      description: "some description",
      keywords: "some keywords",
      requests: 42,
      reviews: [],
      shadow: true,
      tags: [],
      title: "some title",
      answers_count: 42,
      reviews_count: 43,
      author: "some author"
    }
    @update_attrs %{
      anonymous: false,
      description: "some updated description",
      keywords: "some updated keywords",
      requests: 43,
      reviews: [],
      shadow: false,
      tags: [],
      title: "some updated title",
      answers_count: 42,
      reviews_count: 43,
      author: "some author"
    }
    @invalid_attrs %{
      anonymous: nil,
      description: nil,
      keywords: nil,
      requests: nil,
      reviews: nil,
      shadow: nil,
      tags: nil,
      title: nil,
      author: nil,
      reviews_count: nil,
      answers_count: nil
    }

    def content_fixture(attrs \\ %{}, user) do
      {:ok, content} =
        attrs
        |> Enum.into(Map.put(@valid_attrs, :user_id, user.id))
        |> Source.create_content()

      content
    end

    test "get_content!/1 returns the content with given id", %{user: user} do
      content = content_fixture(user)
      assert Source.get_content!(content.id) == content
    end

    test "create_content/1 with valid data creates a content", %{user: user} do
      assert {:ok, %Source.Content{} = content} =
               Source.create_content(Map.put(@valid_attrs, :user_id, user.id))

      assert content.anonymous == true
      assert content.description == "some description"
      assert content.keywords == "some keywords"
      assert content.requests == 42
      assert content.reviews == []
      assert content.shadow == true
      assert content.tags == []
      assert content.title == "some title"
      assert content.author == "some author"
      assert content.answers_count == 42
      assert content.reviews_count == 43
    end

    test "create_content/1 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} =
               Source.create_content(Map.put(@invalid_attrs, :user_id, user.id))
    end

    test "update_content/2 with valid data updates the content", %{user: user} do
      content = content_fixture(user)

      assert {:ok, content} =
               Source.update_content(content, Map.put(@update_attrs, :user_id, user.id))

      assert %Source.Content{} = content
      assert content.anonymous == false
      assert content.description == "some updated description"
      assert content.keywords == "some updated keywords"
      assert content.requests == 43
      assert content.reviews == []
      assert content.shadow == false
      assert content.tags == []
      assert content.title == "some updated title"
      assert content.author == "some author"
      assert content.answers_count == 42
      assert content.reviews_count == 43
    end

    test "update_content/2 with invalid data returns error changeset", %{user: user} do
      content = content_fixture(user)

      assert {:error, %Ecto.Changeset{}} =
               Source.update_content(content, Map.put(@invalid_attrs, :user_id, user.id))

      assert content == Source.get_content!(content.id)
    end

    test "delete_content/1 deletes the content", %{user: user} do
      content = content_fixture(user)
      assert {:ok, %Source.Content{}} = Source.delete_content(content)
      assert_raise Ecto.NoResultsError, fn -> Source.get_content!(content.id) end
    end
  end

  describe "answers" do
    setup [:create_type]

    @valid_attrs %{
      anonymous: true,
      description: "some description",
      keywords: "some keywords",
      tags: [],
      title: "some title",
      author: "some author",
      ratings_count: 42,
      comments_count: 43
    }
    @update_attrs %{
      anonymous: false,
      description: "some updated description",
      keywords: "some updated keywords",
      tags: [],
      title: "some updated title",
      author: "some updated author",
      ratings_count: 42,
      comments_count: 43
    }
    @invalid_attrs %{
      anonymous: nil,
      description: nil,
      keywords: nil,
      tags: nil,
      title: nil,
      author: nil,
      ratings_count: nil,
      comments_count: nil,
      user_id: nil,
      content_id: nil
    }

    def answer_fixture(attrs \\ %{}, user, content) do
      {:ok, answer} =
        attrs
        |> Enum.into(answer_attrs(@valid_attrs, user, content))
        |> Source.create_answer()

      answer
    end

    defp answer_attrs(attrs, user, content) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:content_id, content.id)
    end

    test "get_answer!/1 returns the answer with given id", %{user: user, content: content} do
      answer = answer_fixture(user, content)
      assert Source.get_answer!(answer.id) == answer
    end

    test "list_answers_id!/1 returns the answer with given id", %{user: user, content: content} do
      answer = answer_fixture(user, content)
      assert Source.list_answers_id!(content.id, user) == [answer.id]
    end

    test "create_answer/1 with valid data creates a answer", %{user: user, content: content} do
      assert {:ok, %Source.Answer{} = answer} =
               Source.create_answer(answer_attrs(@valid_attrs, user, content))

      assert answer.anonymous == true
      assert answer.description == "some description"
      assert answer.keywords == "some keywords"
      assert answer.tags == []
      assert answer.author == "some author"
      assert answer.ratings_count == 42
      assert answer.comments_count == 43
      assert answer.user_id == user.id
      assert answer.content_id == content.id
    end

    test "create_answer/1 with invalid data returns error changeset", %{
      user: user,
      content: content
    } do
      assert {:error, %Ecto.Changeset{}} =
               Source.create_answer(answer_attrs(@invalid_attrs, user, content))
    end

    test "update_answer/2 with valid data updates the answer", %{user: user, content: content} do
      answer = answer_fixture(user, content)

      assert {:ok, answer} =
               Source.update_answer(answer, answer_attrs(@update_attrs, user, content))

      assert %Source.Answer{} = answer
      assert answer.anonymous == false
      assert answer.description == "some updated description"
      assert answer.keywords == "some updated keywords"
      assert answer.tags == []
      assert answer.author == "some updated author"
      assert answer.ratings_count == 42
      assert answer.comments_count == 43
      assert answer.user_id == user.id
      assert answer.content_id == content.id
    end

    test "update_answer/2 with invalid data returns error changeset", %{
      user: user,
      content: content
    } do
      answer = answer_fixture(user, content)
      assert {:error, %Ecto.Changeset{}} = Source.update_answer(answer, @invalid_attrs)
      assert answer == Source.get_answer!(answer.id)
    end

    test "delete_answer/1 deletes the answer", %{user: user, content: content} do
      answer = answer_fixture(user, content)
      assert {:ok, %Source.Answer{}} = Source.delete_answer(answer)
      assert_raise Ecto.NoResultsError, fn -> Source.get_answer!(answer.id) end
    end
  end

  describe "comments" do
    setup [:create_answer]

    @valid_attrs %{
      anonymous: false,
      description: "some description",
      keywords: "some keywords",
      author: "some author",
      ratings_count: 42,
      path: ""
    }
    @update_attrs %{
      anonymous: true,
      description: "some updated description",
      keywords: "some updated keywords",
      author: "some updated author",
      ratings_count: 43
    }
    @invalid_attrs %{
      anonymous: nil,
      description: nil,
      keywords: nil,
      content_id: nil,
      user_id: nil,
      author: nil,
      answer_id: nil,
      path: nil,
      ratings_count: nil
    }

    def comment_fixture(attrs \\ %{}, user, content, answer) do
      {:ok, comment} =
        attrs
        |> Enum.into(comment_attrs(@valid_attrs, user, content, answer))
        |> Source.create_comment()

      comment
    end

    defp comment_attrs(attrs, user, content, answer) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:content_id, content.id)
      |> Map.put(:answer_id, answer.id)
    end

    test "get_comment!/1 returns the comment with given id", %{
      user: user,
      content: content,
      answer: answer
    } do
      comment = comment_fixture(user, content, answer)
      assert Source.get_comment!(comment.id) == comment
    end

    test "list_comments/1 returns the list comment with given id", %{
      user: user,
      content: content,
      answer: answer
    } do
      comment = comment_fixture(user, content, answer)

      list_comments = Source.list_comments(comment) |> List.first()
      assert list_comments.anonymous == comment.anonymous
      assert list_comments.answer_id == comment.answer_id
      assert list_comments.author == comment.author
      assert list_comments.description == comment.description
      assert list_comments.id == comment.id
      assert list_comments.keywords == comment.keywords
      assert list_comments.path == comment.path
      assert list_comments.content_id == comment.content_id
      assert list_comments.ratings_count == comment.ratings_count
      assert list_comments.user_id == comment.user_id
      assert list_comments.uuid == comment.uuid
    end

    test "create_comment/1 with valid data creates a comment", %{
      user: user,
      content: content,
      answer: answer
    } do
      assert {:ok, %Source.Comment{} = comment} =
               Source.create_comment(comment_attrs(@valid_attrs, user, content, answer))

      assert comment.anonymous == false
      assert comment.description == "some description"
      assert comment.keywords == "some keywords"
      assert comment.author == "some author"
      assert comment.ratings_count == 42
      assert comment.user_id == user.id
      assert comment.answer_id == answer.id
      assert comment.content_id == content.id
      assert comment.path == %EctoLtree.LabelTree{labels: [Integer.to_string(comment.id)]}
    end

    test "create_comment/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Source.create_comment(@invalid_attrs)
    end

    test "update_comment/2 with valid data updates the comment", %{
      user: user,
      content: content,
      answer: answer
    } do
      comment = comment_fixture(user, content, answer)

      assert {:ok, comment} =
               Source.update_comment(
                 comment,
                 comment_attrs(@update_attrs, user, content, answer)
               )

      assert %Source.Comment{} = comment
      assert comment.anonymous == true
      assert comment.description == "some updated description"
      assert comment.keywords == "some updated keywords"
      assert comment.author == "some updated author"
      assert comment.path == comment.path
      assert comment.ratings_count == 43
      assert comment.content_id == content.id
      assert comment.answer_id == answer.id
      assert comment.user_id == user.id
    end

    test "update_comment/2 with invalid data returns error changeset", %{
      user: user,
      content: content,
      answer: answer
    } do
      comment = comment_fixture(user, content, answer)
      assert {:error, %Ecto.Changeset{}} = Source.update_comment(comment, @invalid_attrs)
      assert comment == Source.get_comment!(comment.id)
    end

    test "delete_comment/1 deletes the comment", %{user: user, content: content, answer: answer} do
      comment = comment_fixture(user, content, answer)
      assert {:ok, %Source.Comment{}} = Source.delete_comment(comment)
      assert_raise Ecto.NoResultsError, fn -> Source.get_comment!(comment.id) end
    end

    test "render_nicknames_from_description/1 Returns a list of nicknames that exists in database",
         %{user: user} do
      assert user_render =
               Source.render_nicknames_from_description(%{
                 description:
                   "#{Application.get_env(:smile_db, :content_user)[:secret_key]}_#{
                    Jason.encode!(%{user: user.id})
                   } nickname test description"
               })

      assert %{desc: "@#{user.nickname} nickname test description", users: [user]} == user_render
    end

    test "render_nicknames_from_description/1 Returns a empty map becase wrong argument." do
      assert %{desc: "", users: []} == Source.render_nicknames_from_description([])
    end
  end
end
