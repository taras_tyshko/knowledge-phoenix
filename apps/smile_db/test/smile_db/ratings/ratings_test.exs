defmodule SmileDB.RatingsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Ratings

  describe "users_rating" do
    setup [:create_comment]

    test "create_user_rating/1 insert or update the answer_rating", %{user: user, answer: answer} do
      answer_rating = answer_rating_fixture(user, answer)

      assert {:ok, %Ratings.UserRating{} = user_ratings} =
               Ratings.create_user_rating(%{user_id: user.id, answer_rating_id: answer_rating.id})

      assert answer_rating.id == user_ratings.answer_rating_id
      assert user.id == user_ratings.user_id
    end

    test "create_user_rating/1 insert or update the comment_rating", %{
      user: user,
      comment: comment
    } do
      comment_rating = comment_rating_fixture(user, comment)

      assert {:ok, %Ratings.UserRating{} = user_ratings} =
               Ratings.create_user_rating(%{
                 user_id: user.id,
                 comment_rating_id: comment_rating.id
               })

      assert comment_rating.id == user_ratings.comment_rating_id
      assert user.id == user_ratings.user_id
    end

    test "delete_user_rating/1 insert or update the answer_rating", %{user: user, answer: answer} do
      answer_rating = answer_rating_fixture(user, answer)

      assert {:ok, %Ratings.UserRating{} = user_ratings} =
               Ratings.create_user_rating(%{user_id: user.id, answer_rating_id: answer_rating.id})

      assert answer_rating.id == user_ratings.answer_rating_id
      assert user.id == user_ratings.user_id
      assert {:ok, %Ratings.UserRating{}} = Ratings.delete_user_rating(user_ratings)
    end

    test "delete_user_rating/1 insert or update the comment_rating", %{
      user: user,
      comment: comment
    } do
      comment_rating = comment_rating_fixture(user, comment)

      assert {:ok, %Ratings.UserRating{} = user_ratings} =
               Ratings.create_user_rating(%{
                 user_id: user.id,
                 comment_rating_id: comment_rating.id
               })

      assert comment_rating.id == user_ratings.comment_rating_id
      assert user.id == user_ratings.user_id
      assert {:ok, %Ratings.UserRating{}} = Ratings.delete_user_rating(user_ratings)
    end
  end

  describe "answers_rating" do
    setup [:create_answer]

    @valid_attrs %{status: true}

    def answer_rating_fixture(attrs \\ %{}, user, answer) do
      attrs = Enum.into(attrs, answer_rating_attrs(@valid_attrs, user, answer))

      {:ok, answer_rating} =
        %Ratings.AnswerRating{}
        |> Ratings.create_answer_rating(attrs)

      answer_rating
    end

    defp answer_rating_attrs(attrs, user, answer) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:answer_id, answer.id)
    end

    test "insert_answer_rating/1 insert or update the answer_rating", %{
      user: user,
      answer: answer
    } do
      assert %Ratings.AnswerRating{} = answer_rating_fixture(user, answer)
    end

    test "delete_answer_rating/1 deletes the answer_rating", %{user: user, answer: answer} do
      answer_rating = answer_rating_fixture(user, answer)
      assert {:ok, %Ratings.AnswerRating{}} = Ratings.delete_answer_rating(answer_rating)
    end

    test "get_answers_rating/1 Returns a list of models", %{user: user, answer: answer} do
      answer_rating = answer_rating_fixture(user, answer)
      assert %{answer.id => [answer_rating]} == Ratings.get_answers_rating([], [answer.id])
    end
  end

  describe "comments_rating" do
    setup [:create_comment]

    @valid_attrs %{status: true}

    def comment_rating_fixture(attrs \\ %{}, user, comment) do
      attrs = Enum.into(attrs, comment_rating_attrs(@valid_attrs, user, comment))

      {:ok, comment_rating} =
        %Ratings.CommentRating{}
        |> Ratings.create_comment_rating(attrs)

      comment_rating
    end

    defp comment_rating_attrs(attrs, user, comment) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:comment_id, comment.id)
    end

    test "insert_or_update_comment_rating/1 insert or update the comment_rating", %{
      user: user,
      comment: comment
    } do
      assert %Ratings.CommentRating{} = comment_rating_fixture(user, comment)
    end

    test "delete_comment_rating/1 deletes the comment_rating", %{user: user, comment: comment} do
      comment_rating = comment_rating_fixture(user, comment)
      assert {:ok, %Ratings.CommentRating{}} = Ratings.delete_comment_rating(comment_rating)
    end

    test "get_comments_rating/1 Returns a list of models", %{user: user, comment: comment} do
      comment_rating = comment_rating_fixture(user, comment)
      assert %{comment.id => [comment_rating]} == Ratings.get_comments_rating([], [comment.id])
    end
  end

  describe "offices_rating" do
    setup [:create_type]
    setup [:create_office]

    @valid_attrs %{score: 42}
    @update_attrs %{score: 43}
    @invalid_attrs %{score: nil, content_id: nil, office_id: nil}

    def office_rating_fixture(attrs \\ %{}, content, office) do

      {:ok, office_rating} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Map.put(:content_id, content.id)
        |> Map.put(:office_id, office.id)
        |> Ratings.create_office_rating()

      office_rating
    end

    test "create_office_rating/1 with valid data creates a office_rating", %{content: content, office: office} do
      assert {:ok, %Ratings.OfficeRating{} = office_rating} =
        @valid_attrs
        |> Map.put(:content_id, content.id)
        |> Map.put(:office_id, office.id)
        |> Ratings.create_office_rating()

      assert office_rating.score == 42
    end

    test "create_office_rating/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ratings.create_office_rating(@invalid_attrs)
    end

    test "update_office_rating/2 with valid data updates the office_rating", %{content: content, office: office} do
      office_rating = office_rating_fixture(content, office)
      assert {:ok, office_rating} = Ratings.update_office_rating(office_rating, @update_attrs)
      assert %Ratings.OfficeRating{} = office_rating
      assert office_rating.score == 43
    end

    test "update_office_rating/2 with invalid data returns error changeset", %{content: content, office: office} do
      office_rating = office_rating_fixture(content, office)
      assert {:error, %Ecto.Changeset{}} = Ratings.update_office_rating(office_rating, @invalid_attrs)
    end

    test "delete_office_rating/1 deletes the office_rating", %{content: content, office: office} do
      office_rating = office_rating_fixture(content, office)
      assert {:ok, %Ratings.OfficeRating{}} = Ratings.delete_office_rating(office_rating)
    end
  end
end
