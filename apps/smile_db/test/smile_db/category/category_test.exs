defmodule SmileDB.CategoryTest do
  use SmileDB.DataCase

  alias SmileDB.Category

  describe "offices" do
    alias SmileDB.Category.Office

    @valid_attrs %{id: "some_id", avatar: "some avatar", email: "some email", location: "some location", title: "some title", country: "some country"}
    @update_attrs %{id: "some_id", avatar: "some updated avatar", email: "some updated email", location: "some updated location", title: "some updated title", country: "some updated country"}
    @invalid_attrs %{id: nil, avatar: nil, email: nil, location: nil, title: nil, country: nil}

    def office_fixture(attrs \\ %{}) do
      {:ok, office} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Category.create_office()

      office
    end

    test "get_office!/1 returns the office with given id" do
      office = office_fixture()
      assert Category.get_office!(office.id) == office
    end

    test "create_office/1 with valid data creates a office" do
      assert {:ok, %Office{} = office} = Category.create_office(@valid_attrs)
      assert office.avatar == "some avatar"
      assert office.email == "some email"
      assert office.location == "some location"
      assert office.title == "some title"
    end

    test "create_office/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Category.create_office(@invalid_attrs)
    end

    test "update_office/2 with valid data updates the office" do
      office = office_fixture()
      assert {:ok, office} = Category.update_office(office, @update_attrs)
      assert %Office{} = office
      assert office.avatar == "some updated avatar"
      assert office.email == "some updated email"
      assert office.location == "some updated location"
      assert office.title == "some updated title"
    end

    test "update_office/2 with invalid data returns error changeset" do
      office = office_fixture()
      assert {:error, %Ecto.Changeset{}} = Category.update_office(office, @invalid_attrs)
      assert office == Category.get_office!(office.id)
    end

    test "delete_office/1 deletes the office" do
      office = office_fixture()
      assert {:ok, %Office{}} = Category.delete_office(office)
    end
  end
end
