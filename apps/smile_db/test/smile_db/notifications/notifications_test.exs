defmodule SmileDB.NotificationsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.{Notifications, Subscriptions, Ratings}

  describe "contents_notifications" do
    setup [:create_type]

    test "list_notifications_by_user_id/0 returns all ContentNotification", %{
      user: user,
      content: content
    } do
      assert [%Notifications.ContentNotification{} = content_notification] =
               Notifications.create_content_notifications([%{user_id: user.id, content_id: content.id}])

      assert %Notifications.Notification{id: id} =
               Notifications.put_notifications(content_notification)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_content_notification/1 with valid data creates a ContentNotification", %{
      user: user,
      content: content
    } do
      assert [%Notifications.ContentNotification{} = content_notification] =
               Notifications.create_content_notifications([%{user_id: user.id, content_id: content.id}])

      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(content_notification)

      assert notification.content_id == content.id
      assert notification.user_id == user.id
    end

    test "delete_answer_notification/1 function implements the removal ContentNotification", %{
      user: user,
      content: content
    } do
      assert [%Notifications.ContentNotification{} = notification] =
               Notifications.create_content_notifications([%{user_id: user.id, content_id: content.id}])

      assert notification = Notifications.delete_content_notifications([notification.id])
    end
  end

  describe "answers_notifications" do
    setup [:create_answer]

    test "list_notifications_by_user_id/0 returns all answers notifications", %{
      user: user,
      answer: answer
    } do
      assert [%Notifications.AnswerNotification{} = answer_notification] =
               Notifications.create_answer_notifications([%{user_id: user.id, answer_id: answer.id}])
      
      assert %Notifications.Notification{id: id} =
               Notifications.put_notifications(answer_notification)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_answer_notification/1 with valid data creates a answer_notification", %{
      user: user,
      answer: answer
    } do
      assert [%Notifications.AnswerNotification{} = answer_notification] =
               Notifications.create_answer_notifications([%{user_id: user.id, answer_id: answer.id}])
      
      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(answer_notification)

      assert notification.answer_id == answer.id
      assert notification.user_id == user.id
    end

    test "delete_answer_notification/1 function implements the removal AnswerNotification", %{
      user: user,
      answer: answer
    } do
      assert [%Notifications.AnswerNotification{} = notification] =
               Notifications.create_answer_notifications([%{user_id: user.id, answer_id: answer.id}])

      assert notification = Notifications.delete_answer_notifications([notification.id])
    end
  end

  describe "comments_notifications" do
    setup [:create_comment]

    test "list_notifications_by_user_id/0 returns all CommentNotification", %{
      user: user,
      comment: comment
    } do
      assert [%Notifications.CommentNotification{} = comment_notification] =
               Notifications.create_comment_notifications([%{user_id: user.id, comment_id: comment.id}])

      assert %Notifications.Notification{id: id} =
               Notifications.put_notifications(comment_notification)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_comment_notifications/1 with valid data creates a CommentNotification", %{
      user: user,
      comment: comment
    } do
      assert [%Notifications.CommentNotification{} = comment_notification] =
               Notifications.create_comment_notifications([%{user_id: user.id, comment_id: comment.id}])
      
      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(comment_notification)

      assert notification.comment_id == comment.id
      assert notification.user_id == user.id
    end

    test "delete_comment_notification/1 function implements the removal CommentNotification", %{
      user: user,
      comment: comment
    } do
      assert [%Notifications.CommentNotification{} = notification] =
               Notifications.create_comment_notifications([%{user_id: user.id, comment_id: comment.id}])

      assert notification = Notifications.delete_comment_notifications([notification.id])
    end
  end

  describe "user_subscription_notifications" do
    setup [:create_user]

    def user_subscription_fixture(user) do
      %Subscriptions.UserSubscription{}
      |> Subscriptions.create_user_subscription(user_subscription_attrs(%{status: true}, user))
    end

    defp user_subscription_attrs(attrs, user) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:follower_id, user.id)
    end

    test "list_notifications_by_user_id/0 returns all UserSubscription", %{user: user} do
      assert {:ok, user_subscription} = user_subscription_fixture(user)

      assert [%Notifications.UserSubscriptionNotification{} = user_subscription] =
               Notifications.create_user_subscription_notifications([%{user_id: user.id, user_subscription_id: user_subscription.id}])

      assert %Notifications.Notification{id: id} =
               Notifications.put_notifications(user_subscription)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_user_subscription_notifications/1 with valid data creates a UserSubscription", %{
      user: user
    } do
      assert {:ok, user_subscription} = user_subscription_fixture(user)

      assert [%Notifications.UserSubscriptionNotification{} = user_subscription] =
               Notifications.create_user_subscription_notifications([%{user_id: user.id, user_subscription_id: user_subscription.id}])

      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(user_subscription)

      assert notification.user_subscription_id == user_subscription.user_subscription_id
      assert notification.user_id == user.id
    end

    test "delete_user_subscription_notification/1 function implements the removal UserSubscription",
         %{user: user} do
      assert {:ok, user_subscription} = user_subscription_fixture(user)

      assert [%Notifications.UserSubscriptionNotification{} = notification] =
               Notifications.create_user_subscription_notifications([%{user_id: user.id, user_subscription_id: user_subscription.id}])

      assert notification = Notifications.delete_user_subscription_notifications([notification.id])
    end
  end

  describe "answer_rating_notifications" do
    setup [:create_answer]

    def answer_rating_fixture(user, answer) do
      %Ratings.AnswerRating{}
      |> Ratings.create_answer_rating(answer_rating_attrs(%{status: true}, user, answer))
    end

    defp answer_rating_attrs(attrs, user, answer) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:answer_id, answer.id)
    end

    test "list_notifications_by_user_id/0 returns all AnswerRatingNotification", %{
      user: user,
      answer: answer
    } do
      assert {:ok, answer_rating} = answer_rating_fixture(user, answer)

      assert [%Notifications.AnswerRatingNotification{} = answer_rating_notification] =
               Notifications.create_answer_rating_notifications([%{user_id: user.id, answer_rating_id: answer_rating.id}])

      assert %Notifications.Notification{id: id} =
      Notifications.put_notifications(answer_rating_notification)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_answer_rating_notifications/1 with valid data creates a AnswerRatingNotification",
         %{user: user, answer: answer} do
      assert {:ok, answer_rating} = answer_rating_fixture(user, answer)

      assert [%Notifications.AnswerRatingNotification{} = answer_rating_notification] =
               Notifications.create_answer_rating_notifications([%{user_id: user.id, answer_rating_id: answer_rating.id}])
      
      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(answer_rating_notification)

      assert notification.answer_rating_id == answer_rating.id
      assert notification.user_id == user.id
    end

    test "delete_answer_rating_notification/1 function implements the removal AnswerRatingNotification",
         %{user: user, answer: answer} do
      assert {:ok, answer_rating} = answer_rating_fixture(user, answer)

      assert [%Notifications.AnswerRatingNotification{} = notification] =
               Notifications.create_answer_rating_notifications([%{user_id: user.id, answer_rating_id: answer_rating.id}])

      assert notification = Notifications.delete_answer_rating_notifications([notification.id])
    end
  end

  describe "comment_rating_notifications" do
    setup [:create_comment]

    def comment_rating_fixture(user, comment) do
      %Ratings.CommentRating{}
      |> Ratings.create_comment_rating(comment_rating_attrs(%{status: true}, user, comment))
    end

    defp comment_rating_attrs(attrs, user, comment) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:comment_id, comment.id)
    end

    test "list_notifications_by_user_id/0 returns all CommentRatingNotification", %{
      user: user,
      comment: comment
    } do
      assert {:ok, comment_rating} = comment_rating_fixture(user, comment)

      assert [%Notifications.CommentRatingNotification{} = comment_rating_notification] =
               Notifications.create_comment_rating_notifications([%{user_id: user.id, comment_rating_id: comment_rating.id}])

      assert %Notifications.Notification{id: id} =
               Notifications.put_notifications(comment_rating_notification)

      assert Notifications.list_notifications_by_user_id(user.id) == [id]
    end

    test "create_comment_rating_notifications/1 with valid data creates a CommentRatingNotification",
         %{user: user, comment: comment} do
      assert {:ok, comment_rating} = comment_rating_fixture(user, comment)

      assert [%Notifications.CommentRatingNotification{} = comment_rating_notification] =
               Notifications.create_comment_rating_notifications([%{user_id: user.id, comment_rating_id: comment_rating.id}])
      
      assert %Notifications.Notification{} = notification =
               Notifications.put_notifications(comment_rating_notification)

      assert notification.comment_rating_id == comment_rating.id
      assert notification.user_id == user.id
    end

    test "delete_comment_rating_notification/1 function implements the removal CommentRatingNotification",
         %{user: user, comment: comment} do
      assert {:ok, comment_rating} = comment_rating_fixture(user, comment)

      assert [%Notifications.CommentRatingNotification{} = notification] =
               Notifications.create_comment_rating_notifications([%{user_id: user.id, comment_rating_id: comment_rating.id}])

      assert notification = Notifications.delete_comment_rating_notifications([notification.id])
    end
  end

  describe "messages_notifications" do
    setup [:create_message]

    test "list_notifications_by_user_id/0 returns all MessageNotifications", %{
      user: user,
      message: message
    } do
      assert [%Notifications.MessageNotification{} = notification] =
               Notifications.create_message_notifications([%{user_id: user.id, message_id: message.id, room_id: message.room_id}])

      assert Notifications.get_message_notification(user.id) == [notification]
    end

    test "create_message_notifications/1 with valid data creates a MessageNotifications", %{
      user: user,
      message: message
    } do
      assert [%Notifications.MessageNotification{} = notification] =
               Notifications.create_message_notifications([%{user_id: user.id, message_id: message.id, room_id: message.room_id}])

      assert notification.message_id == message.id
      assert notification.user_id == user.id
    end

    test "delete_message_notification/1 function implements the removal MessageNotifications", %{
      user: user,
      message: message
    } do
      assert [%Notifications.MessageNotification{} = notification] =
               Notifications.create_message_notifications([%{user_id: user.id, message_id: message.id, room_id: message.room_id}])

      assert notification = Notifications.delete_message_notifications([notification.id])
    end
  end
end
