defmodule SmileDB.PermissionsTest do
  use SmileDB.DataCase

  alias SmileDB.Permissions

  describe "permissions" do
    alias SmileDB.Permissions.Permission

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def permission_fixture(attrs \\ %{}) do
      {:ok, permission} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Permissions.create_permission()

      permission
    end

    test "get_permission!/1 returns the permission with given id" do
      permission = permission_fixture()
      assert Permissions.get_permission!(permission.id) == permission
    end

    test "create_permission/1 with valid data creates a permission" do
      assert {:ok, %Permission{} = permission} = Permissions.create_permission(@valid_attrs)
      assert permission.name == "some name"
    end

    test "create_permission/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Permissions.create_permission(@invalid_attrs)
    end

    test "update_permission/2 with valid data updates the permission" do
      permission = permission_fixture()
      assert {:ok, permission} = Permissions.update_permission(permission, @update_attrs)
      assert %Permission{} = permission
      assert permission.name == "some updated name"
    end

    test "update_permission/2 with invalid data returns error changeset" do
      permission = permission_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Permissions.update_permission(permission, @invalid_attrs)

      assert permission == Permissions.get_permission!(permission.id)
    end

    test "delete_permission/1 deletes the permission" do
      permission = permission_fixture()
      assert {:ok, %Permission{} = permission_new} = Permissions.delete_permission(permission)
      assert permission.id == permission_new.id
    end
  end

  describe "role_access" do
    setup [:create_user]
    setup [:create_permission]

    alias SmileDB.Permissions.RoleAccess

    @valid_attrs %{}
    @invalid_attrs %{role_name: nil, permission_id: nil}

    def role_access_fixture(user, permission) do
      {:ok, role_access} =
        @valid_attrs
        |> Map.put(:role, user.role)
        |> Map.put(:permission_id, permission.id)
        |> Permissions.create_role_access()

      role_access
    end

    test "list_role_access/0 returns all role_access", %{user: user, permission: permission} do
      role_access = role_access_fixture(user, permission)
      assert Permissions.list_role_access(role_access.role) == [role_access]
    end

    test "create_role_access/1 with valid data creates a role_access", %{
      user: user,
      permission: permission
    } do
      assert {:ok, %RoleAccess{} = role_access} =
               @valid_attrs
               |> Map.put(:role, user.role)
               |> Map.put(:permission_id, permission.id)
               |> Permissions.create_role_access()
    end

    test "create_role_access/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Permissions.create_role_access(@invalid_attrs)
    end

    test "delete_role_access/1 deletes the role_access", %{
      user: user,
      permission: permission
    } do
      role_access = role_access_fixture(user, permission)
      assert {:ok, %RoleAccess{}} = Permissions.delete_role_access(role_access)
    end
  end
end
