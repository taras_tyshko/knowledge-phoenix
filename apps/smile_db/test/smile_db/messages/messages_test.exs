defmodule SmileDB.MessagesTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.{Messages, Source}

  describe "user_rooms" do
    setup [:create_room]

    @valid_attrs %{type: "privat", role: "customer"}
    @update_attrs %{type: "public", role: "owner"}
    @invalid_attrs %{type: nil, user_id: nil, room_id: nil, role: nil}

    def user_room_fixture(attrs \\ %{}, user, room) do
      {:ok, user_room} =
        attrs
        |> Enum.into(user_room_attrs(@valid_attrs, user, room))
        |> Messages.create_user_room()

      user_room
    end

    defp user_room_attrs(attrs, user, room) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:room_id, room.id)
    end

    test "list_user_rooms/1 returns the user_room with given id", %{
      user_one: user_one,
      room: room
    } do
      user_room = user_room_fixture(user_one, room)
      assert Messages.list_user_rooms(%{user_id: user_one.id}) == [user_room]
    end

    test "create_user_room/1 with valid data creates a user_room", %{
      user_one: user_one,
      room: room
    } do
      assert {:ok, %Messages.UserRoom{}} =
               Messages.create_user_room(user_room_attrs(@valid_attrs, user_one, room))
    end

    test "create_user_room/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Messages.create_user_room(@invalid_attrs)
    end

    test "update_user_room/2 with valid data updates the user_room", %{
      user_one: user_one,
      room: room
    } do
      user_room = user_room_fixture(user_one, room)

      assert {:ok, user_room} =
               Messages.update_user_room(
                 user_room,
                 user_room_attrs(@update_attrs, user_one, room)
               )

      assert %Messages.UserRoom{} = user_room
    end

    test "update_user_room/2 with invalid data returns error changeset", %{
      user_one: user_one,
      room: room
    } do
      user_room = user_room_fixture(user_one, room)
      assert {:error, %Ecto.Changeset{}} = Messages.update_user_room(user_room, @invalid_attrs)
    end

    test "delete_user_room/1 deletes the user_room", %{user_one: user_one, room: room} do
      user_room = user_room_fixture(user_one, room)
      assert {:ok, %Messages.UserRoom{}} = Messages.delete_user_room(user_room)
    end
  end

  describe "rooms" do
    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def room_fixture(attrs \\ %{}) do
      {:ok, room} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Messages.create_room()

      room
    end

    test "get_room!/1 returns the room with given id" do
      room = room_fixture()
      assert room_data = Messages.get_room!(room.id)
      assert room.id == room_data.id
    end

    test "create_room/1 with valid data creates a room" do
      assert {:ok, %Messages.Room{} = room} = Messages.create_room(@valid_attrs)

      assert room.name == "some name"
    end

    test "update_room/2 with valid data updates the room" do
      room = room_fixture()
      assert {:ok, room} = Messages.update_room(room, @update_attrs)
      assert %Messages.Room{} = room
      assert room.name == "some updated name"
    end

    test "delete_room/1 deletes the room" do
      room = room_fixture()
      assert {:ok, %Messages.Room{}} = Messages.delete_room(room)
    end
  end

  describe "messages" do
    setup [:create_room]

    @valid_attrs %{description: "some description", event_name: "some event_name"}
    @update_attrs %{
      description: "some updated description",
      event_name: "some updated event_name"
    }
    @invalid_attrs %{description: nil, user_id: nil, room_id: nil}

    def message_fixture(attrs \\ %{}, user, room) do
      {:ok, message} =
        attrs
        |> Enum.into(message_attrs(@valid_attrs, user, room))
        |> Messages.create_message()

      message
    end

    defp message_attrs(attrs, user, room) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:room_id, room.id)
    end

    test "get_message!/1 returns the message with given id", %{user: user, room: room} do
      message = message_fixture(user, room)
      assert Messages.get_message!(message.id)
      # == message
    end

    test "create_message/1 with valid data creates a message", %{user: user, room: room} do
      assert {:ok, %Messages.Message{} = message} =
               Messages.create_message(message_attrs(@valid_attrs, user, room))

      assert message.description == "some description"
      assert message.event_name == "some event_name"
      assert message.user_id == user.id
      assert message.room_id == room.id
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Messages.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message", %{user: user, room: room} do
      message = message_fixture(user, room)

      assert {:ok, message} =
               Messages.update_message(message, message_attrs(@update_attrs, user, room))

      assert %Messages.Message{} = message
      assert message.description == "some updated description"
    end

    test "update_message/2 with invalid data returns error changeset", %{user: user, room: room} do
      message = message_fixture(user, room)
      assert {:error, %Ecto.Changeset{}} = Messages.update_message(message, @invalid_attrs)
      assert message == Messages.get_message!(message.id)
    end

    test "delete_message/1 deletes the message", %{user: user, room: room} do
      message = message_fixture(user, room)
      assert {:ok, %Messages.Message{}} = Messages.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Messages.get_message!(message.id) end
    end

    test "render_users_from_description/1 render users from description the messages, not argument" do
      assert %{desc: "", users: []} == Source.render_nicknames_from_description("test")
    end

    test "render_users_from_description/1 render users from description the messages, argument it",
         %{user: user} do
      assert %{desc: "user:#{user.id}", users: [user]} ==
               Source.render_nicknames_from_description(%{
                 description: "user:#{user.id}",
                 event_name: "createChannel"
               })
    end
  end
end
