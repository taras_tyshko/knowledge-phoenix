defmodule SmileDB.ContentTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Contents

  describe "related_contents" do
    setup [:create_content]

    @valid_attrs %{}
    @invalid_attrs %{related_content_id: nil, content_id: nil}

    def related_content_fixture(attrs \\ %{}, content) do
      {:ok, related_content} =
        attrs
        |> Enum.into(related_content_attrs(@valid_attrs, content))
        |> Contents.create_related_content()

      related_content
    end

    defp related_content_attrs(attrs, content) do
      attrs
      |> Map.put(:content_id, content.id)
      |> Map.put(:related_content_id, content.id)
    end

    test "list_related_contents_id/0 returns all related_contents", %{content: content} do
      related_content = related_content_fixture(content)

      assert Contents.list_related_contents_id(content.id) == [
               related_content.related_content_id
             ]
    end

    test "create_related_content/1 with valid data creates a related_content", %{
      content: content
    } do
      assert {:ok, %Contents.RelatedContent{}} =
               Contents.create_related_content(related_content_attrs(@valid_attrs, content))
    end

    test "create_related_content/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contents.create_related_content(@invalid_attrs)
    end

    test "delete_related_content/1 deletes the related_content", %{content: content} do
      related_content = related_content_fixture(content)

      assert {:ok, %Contents.RelatedContent{}} = Contents.delete_related_content(related_content)
    end
  end

  describe "me_to" do
    setup [:create_content]

    @valid_attrs %{}
    @invalid_attrs %{user_id: nil, content_id: nil}

    def me_to_fixture(attrs \\ %{}, user, content) do
      {:ok, me_to} =
        attrs
        |> Enum.into(me_to_attrs(@valid_attrs, user, content))
        |> Contents.create_me_to()

      me_to
    end

    defp me_to_attrs(attrs, user, content) do
      attrs
      |> Map.put(:content_id, content.id)
      |> Map.put(:user_id, user.id)
    end

    test "list_me_to/0 returns list me_to for content", %{content: content, user: user} do
      me_to = me_to_fixture(@valid_attrs, user, content)
      assert Contents.list_me_to(me_to.content_id) == [user.id]
    end

    test "create_me_to/1 with valid data creates a me_to", %{user: user, content: content} do
      assert {:ok, %Contents.Meto{}} =
               Contents.create_me_to(%{user_id: user.id, content_id: content.id})
    end

    test "create_me_to/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Contents.create_me_to(@invalid_attrs)
    end

    test "delete_me_to/1 deletes the me_to", %{content: content, user: user} do
      me_to = me_to_fixture(user, content)

      assert {:ok, %Contents.Meto{}} = Contents.delete_me_to(me_to)
    end
  end
end
