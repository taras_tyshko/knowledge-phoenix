defmodule SmileDB.TagsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Tags
  alias SmileDB.Source.Content

  describe "tags" do
    setup [:create_content]

    @valid_attrs %{
      count: 42,
      title: "some title"
    }

    test "create_tag/1 with valid data creates a tag" do
      assert {:ok, %Tags.Tag{} = tag} = Tags.create_tag(@valid_attrs)
      assert tag.count == 42
      assert tag.title == "some title"
    end

    test "get_or_insert_tag/1 with valid data create a tag!", %{content: content} do
      assert {:ok, %Tags.Tag{} = tag} = Tags.get_or_insert_tag(List.first(content.tags))
      assert tag.count == 1
      assert List.first(content.tags) == tag.title
    end

    test "get_or_insert_tag/1 with valid data gets a tag!" do
      assert {:ok, %Tags.Tag{} = tag} = Tags.get_or_insert_tag("some title")
      assert tag.count == 0
      assert "some title" == tag.title
    end
  end

  describe "content_tag" do
    setup [:create_content, :create_tag]

    @invalid_attrs %{
      tag_id: nil,
      content_id: nil
    }

    test "create_content_tag/1 with valid data creates a tag in table content_tag.", %{
      content: content,
      tag: tag
    } do
      assert {:ok, %Tags.ContentTag{} = content_tag} =
               Tags.create_content_tag(%{tag_id: tag.id, content_id: content.id})

      assert content_tag.tag_id == tag.id
      assert content_tag.content_id == content.id
    end

    test "create_content_tag/1 with invalid data creates a tag in table content_tag." do
      assert {:error, %Ecto.Changeset{}} = Tags.create_content_tag(@invalid_attrs)
    end

    test "delete_content_tag/1 with valid data delete a tag with table content_tag.", %{
      content: content,
      tag: tag
    } do
      assert {:ok, %Tags.ContentTag{} = content_tag} =
               Tags.create_content_tag(%{tag_id: tag.id, content_id: content.id})

      assert content_tag.tag_id == tag.id
      assert content_tag.content_id == content.id

      assert {:ok, %Tags.ContentTag{} = delete_content_tag} = Tags.delete_content_tag(content_tag)

      assert delete_content_tag.tag_id == tag.id
      assert delete_content_tag.content_id == content.id
    end

    test "put_tags/1 Get a current content and list tags for it.", %{content: content} do
      assert {:ok, %Content{} = new_content} = Tags.put_tags(content)
      assert new_content.id == content.id
    end

    test "put_tags/1 Get a current content in structure and update list tags for it.", %{
      content: content
    } do
      assert {:ok, %Content{} = new_content} = Tags.put_tags({:ok, content})
      assert new_content.id == content.id
    end

    test "put_tags/1 Get error and transmit it." do
      assert {:error, %{test: :test}} = Tags.put_tags({:error, %{test: :test}})
    end
  end
end
