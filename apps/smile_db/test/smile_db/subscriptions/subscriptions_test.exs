defmodule SmileDB.SubscriptionsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  alias SmileDB.Subscriptions

  describe "contents_subscription" do
    setup [:create_content]

    @valid_attrs %{}
    @invalid_attrs %{user_id: nil, content_id: nil}

    def content_subscription_fixture(attrs \\ %{}, user, content) do
      {:ok, content_subscription} =
        attrs
        |> Enum.into(content_subscription_attrs(@valid_attrs, user, content))
        |> Subscriptions.create_content_subscription()

      content_subscription
    end

    defp content_subscription_attrs(attrs, user, content) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:content_id, content.id)
    end

    test "list_contents_subscription/0 returns all contents_subscription", %{
      user: user,
      content: content
    } do
      content_subscription = content_subscription_fixture(user, content)

      assert Subscriptions.list_contents_subscription(user.id) == [
               content_subscription.content_id
             ]
    end

    test "list_users_id_from_contents_subscription/0 returns list all contents_subscription", %{
      user: user,
      content: content
    } do
      content_subscription_fixture(user, content)
      assert Subscriptions.list_users_id_from_contents_subscription(content.id) == [user.id]
    end

    test "create_content_subscription/1 with valid data creates a content_subscription", %{
      user: user,
      content: content
    } do
      assert {:ok, %Subscriptions.ContentSubscription{}} =
               Subscriptions.create_content_subscription(
                 content_subscription_attrs(@valid_attrs, user, content)
               )
    end

    test "create_content_subscription/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Subscriptions.create_content_subscription(@invalid_attrs)
    end

    test "delete_content_subscription/1 deletes the content_subscription", %{
      user: user,
      content: content
    } do
      content_subscription = content_subscription_fixture(user, content)

      assert {:ok, %Subscriptions.ContentSubscription{}} =
               Subscriptions.delete_content_subscription(content_subscription)
    end
  end

  describe "tags_subscription" do
    setup [:create_user]
    setup [:create_tag]

    @valid_attrs %{}
    @invalid_attrs %{title: nil, user_id: nil}

    def tag_subscription_fixture(attrs \\ %{}, user, tag) do
      {:ok, tag_subscription} =
        attrs
        |> Enum.into(tag_subscription_attrs(@valid_attrs, user, tag))
        |> Subscriptions.create_tag_subscription()

      tag_subscription
    end

    defp tag_subscription_attrs(attrs, user, tag) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:title, tag.title)
    end

    test "list_tags_subscription/0 returns all tags_subscription", %{user: user, tag: tag} do
      tag_subscription = tag_subscription_fixture(user, tag)
      assert Subscriptions.list_tags_subscription(user.id) == [tag_subscription.title]
    end

    test "create_tag_subscription/1 with valid data creates a tag_subscription", %{
      user: user,
      tag: tag
    } do
      assert {:ok, %Subscriptions.TagSubscription{}} =
               Subscriptions.create_tag_subscription(
                 tag_subscription_attrs(@valid_attrs, user, tag)
               )
    end

    test "create_tag_subscription/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Subscriptions.create_tag_subscription(@invalid_attrs)
    end

    test "delete_tag_subscription/1 deletes the tag_subscription", %{user: user, tag: tag} do
      tag_subscription = tag_subscription_fixture(user, tag)

      assert {:ok, %Subscriptions.TagSubscription{}} =
               Subscriptions.delete_tag_subscription(tag_subscription)
    end
  end

  describe "users_subscription" do
    setup [:create_user]

    @valid_attrs %{status: true}
    @invalid_attrs %{follower_id: nil, user_id: nil, status: nil}

    def user_subscription_fixture(user) do
      {:ok, user_subscription} =
        %Subscriptions.UserSubscription{}
        |> Subscriptions.create_user_subscription(user_subscription_attrs(@valid_attrs, user))

      user_subscription
    end

    defp user_subscription_attrs(attrs, user) do
      attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:follower_id, user.id)
    end

    test "list_users_subscription/0 returns all users_subscription", %{user: user} do
      user_subscription = user_subscription_fixture(user)
      assert Subscriptions.list_users_subscription(user.id) == [user_subscription.follower_id]
      assert Subscriptions.list_users_subscription(user.id) == [user.id]
    end

    test "list_user_id_users_subscription/0 returns all users_subscription", %{user: user} do
      user_subscription = user_subscription_fixture(user)

      assert Subscriptions.list_user_id_users_subscription(user_subscription.follower_id) == [
               user_subscription.user_id
             ]

      assert Subscriptions.list_user_id_users_subscription(user_subscription.follower_id) == [
               user.id
             ]
    end

    test "create_user_subscription/1 with valid data creates a user_subscription", %{user: user} do
      assert {:ok, %Subscriptions.UserSubscription{}} =
               Subscriptions.create_user_subscription(%Subscriptions.UserSubscription{}, user_subscription_attrs(@valid_attrs, user))
    end

    test "create_user_subscription/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Subscriptions.create_user_subscription(%Subscriptions.UserSubscription{}, @invalid_attrs)
    end

    test "delete_user_subscription/1 deletes the user_subscription", %{user: user} do
      user_subscription = user_subscription_fixture(user)

      assert {:ok, %Subscriptions.UserSubscription{}} =
               Subscriptions.delete_user_subscription(user_subscription)
    end
  end
end
