defmodule SmileDB.AccountsTest do
  use SmileDB.DataCase
  use ExUnit.Case, async: true

  import Exgravatar
  alias SmileDB.Accounts

  describe "users" do
    setup [:create_user]

    @valid_attrs %{
      answers_count: 42,
      auth_provider: "some auth_provider",
      avatar: "some avatar",
      biography: "some biography",
      email: "some email",
      expert: [],
      facebook_uid: "some some facebook_uid",
      first_name: "some first_name",
      gender: "some gender",
      google_uid: "some some google_uid",
      last_name: "some last_name",
      likes_count: 42,
      location: "some location",
      nickname: "some some nickname",
      contents_count: 42,
      comments_count: 42,
      status: "some status",
      website: "some website"
    }
    @update_attrs %{
      answers_count: 43,
      auth_provider: "some updated auth_provider",
      avatar: "some updated avatar",
      biography: "some updated biography",
      email: "some updated email",
      expert: [],
      facebook_uid: "some some updated facebook_uid",
      first_name: "some updated first_name",
      gender: "some updated gender",
      google_uid: "some some updated google_uid",
      last_name: "some updated last_name",
      likes_count: 43,
      location: "some updated location",
      contents_count: 43,
      comments_count: 43,
      status: "some updated status",
      website: "some updated website"
    }
    @invalid_attrs %{
      answers_count: nil,
      auth_provider: nil,
      avatar: nil,
      biography: nil,
      email: nil,
      expert: nil,
      facebook_uid: nil,
      first_name: nil,
      gender: nil,
      google_uid: nil,
      last_name: nil,
      likes_count: nil,
      location: nil,
      contents_count: nil,
      status: nil,
      website: nil
    }

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Accounts.create_user()

      user
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()

      assert Accounts.get_user!(user.id) == Map.put(user, :name, "#{user.first_name} #{user.last_name}")
    end

    test "get_user_by_nickname!/1 returns the user with given id" do
      user = user_fixture()

      assert Accounts.get_user_by_nickname!(user.nickname) == Map.put(user, :name, "#{user.first_name} #{user.last_name}")
    end

    test "default_avatar/1 returns the user avatar" do
      user = user_fixture()
      assert Accounts.default_avatar(user.email) == gravatar_url(user.email, d: "identicon", s: "500")
    end

    test "gen_nickname/1 returns the user nickname" do
      user = user_fixture()

      assert Accounts.gen_nickname(user.first_name, user.last_name) ==
               String.downcase(
                 String.slice(user.first_name, 0..1) <> String.slice(user.last_name, 0..2)
               )
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %Accounts.User{} = user} = Accounts.create_user(@valid_attrs)
      assert user.answers_count == 42
      assert user.auth_provider == "some auth_provider"
      assert user.avatar == "some avatar"
      assert user.biography == "some biography"
      assert user.email == "some email"
      assert user.expert == []
      assert user.facebook_uid == "some some facebook_uid"
      assert user.first_name == "some first_name"
      assert user.gender == "some gender"
      assert user.google_uid == "some some google_uid"
      assert user.last_name == "some last_name"
      assert user.likes_count == 42
      assert user.location == "some location"
      assert user.nickname == "some some nickname"
      assert user.contents_count == 42
      assert user.comments_count == 42
      assert user.status == "some status"
      assert user.website == "some website"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Accounts.update_user(user, @update_attrs)
      assert %Accounts.User{} = user
      assert user.answers_count == 43
      assert user.auth_provider == "some updated auth_provider"
      assert user.avatar == "some updated avatar"
      assert user.biography == "some updated biography"
      assert user.email == "some updated email"
      assert user.expert == []
      assert user.facebook_uid == "some some updated facebook_uid"
      assert user.first_name == "some updated first_name"
      assert user.gender == "some updated gender"
      assert user.google_uid == "some some updated google_uid"
      assert user.last_name == "some updated last_name"
      assert user.likes_count == 43
      assert user.location == "some updated location"
      assert user.contents_count == 43
      assert user.comments_count == 43
      assert user.status == "some updated status"
      assert user.website == "some updated website"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %Accounts.User{}} = Accounts.delete_user(user)
    end
  end

  describe "settings" do
    setup [:create_user]

    @valid_attrs %{
      by_answers: true,
      by_comments: true,
      email_enable: true,
      email_on_reply: true,
      web_notif_mentions: true,
      web_notif_recommend: true,
      web_notif_upvote: true
    }
    @update_attrs %{
      by_answers: false,
      by_comments: false,
      email_enable: false,
      email_on_reply: false,
      web_notif_mentions: false,
      web_notif_recommend: false,
      web_notif_upvote: false
    }
    @invalid_attrs %{
      user_id: nil,
      by_answers: nil,
      by_comments: nil,
      email_enable: nil,
      email_on_reply: nil,
      web_notif_mentions: nil,
      web_notif_recommend: nil,
      web_notif_upvote: nil
    }

    def setting_fixture(attrs \\ %{}, user) do
      {:ok, setting} =
        attrs
        |> Enum.into(Map.put(@valid_attrs, :user_id, user.id))
        |> Accounts.create_setting()

      setting
    end

    test "get_setting!/1 returns the setting with given id", %{user: user} do
      setting = setting_fixture(user)
      assert Accounts.get_setting!(user) == setting
    end

    test "create_setting/1 with valid data creates a setting", %{user: user} do
      assert {:ok, %Accounts.Setting{} = setting} =
               Accounts.create_setting(Map.put(@valid_attrs, :user_id, user.id))

      assert setting.user_id == user.id
      assert setting.by_answers == true
      assert setting.by_comments == true
      assert setting.email_enable == true
      assert setting.email_on_reply == true
      assert setting.web_notif_mentions == true
      assert setting.web_notif_recommend == true
      assert setting.web_notif_upvote == true
    end

    test "create_setting/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Accounts.create_setting(@invalid_attrs)
    end

    test "update_setting/2 with valid data updates the setting", %{user: user} do
      setting = setting_fixture(user)

      assert {:ok, setting} =
               Accounts.update_setting(setting, Map.put(@update_attrs, :user_id, user.id))

      assert %Accounts.Setting{} = setting
      assert setting.user_id == user.id
      assert setting.by_answers == false
      assert setting.by_comments == false
      assert setting.email_enable == false
      assert setting.email_on_reply == false
      assert setting.web_notif_mentions == false
      assert setting.web_notif_recommend == false
      assert setting.web_notif_upvote == false
    end

    test "update_setting/2 with invalid data returns error changeset", %{user: user} do
      setting = setting_fixture(user)
      assert {:error, %Ecto.Changeset{}} = Accounts.update_setting(setting, @invalid_attrs)
      assert setting == Accounts.get_setting!(user)
    end
  end
end
