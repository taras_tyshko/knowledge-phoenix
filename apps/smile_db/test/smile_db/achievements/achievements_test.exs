defmodule SmileDB.AchievementsTest do
  use SmileDB.DataCase

  alias SmileDB.Achievements

  describe "achieves" do
    alias SmileDB.Achievements.Achieve

    @valid_attrs %{avatar: "some avatar", title: "some title"}
    @update_attrs %{avatar: "some updated avatar", title: "some updated title"}
    @invalid_attrs %{avatar: nil, title: nil, uuid: nil}

    def achieve_fixture(attrs \\ %{}) do
      {:ok, achieve} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Achievements.create_achieve()

      achieve
    end

    test "get_achieve!/1 returns the achieve with given id" do
      achieve = achieve_fixture()
      assert Achievements.get_achieve!(achieve.id) == achieve
    end

    test "create_achieve/1 with valid data creates a achieve" do
      assert {:ok, %Achieve{} = achieve} = Achievements.create_achieve(@valid_attrs)
      assert achieve.avatar == "some avatar"
      assert achieve.title == "some title"
    end

    test "create_achieve/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Achievements.create_achieve(@invalid_attrs)
    end

    test "update_achieve/2 with valid data updates the achieve" do
      achieve = achieve_fixture()
      assert {:ok, achieve} = Achievements.update_achieve(achieve, @update_attrs)
      assert %Achieve{} = achieve
      assert achieve.avatar == "some updated avatar"
      assert achieve.title == "some updated title"
    end

    test "update_achieve/2 with invalid data returns error changeset" do
      achieve = achieve_fixture()
      assert {:error, %Ecto.Changeset{}} = Achievements.update_achieve(achieve, @invalid_attrs)
      assert achieve == Achievements.get_achieve!(achieve.id)
    end

    test "delete_achieve/1 deletes the achieve" do
      achieve = achieve_fixture()
      assert {:ok, %Achieve{}} = Achievements.delete_achieve(achieve)
      assert_raise Ecto.NoResultsError, fn -> Achievements.get_achieve!(achieve.id) end
    end
  end

  describe "user_achieves" do
    setup [:create_user]
    setup [:create_achieves]

    alias SmileDB.Achievements.UserAchieve

    @valid_attrs %{}
    @invalid_attrs %{}

    def user_achieve_fixture(user, achieve) do
      {:ok, user_achieve} =
        @valid_attrs
        |> Map.put(:user_id, user.id)
        |> Map.put(:achieve_id, achieve.id)
        |> Achievements.create_user_achieve()

      user_achieve
    end

    test "list_achieves/1 returns the list achieves with given id", %{user: user, achieve: achieve} do
      achieves = user_achieve_fixture(user, achieve)
      assert Achievements.list_achieves(achieves.user_id) == [achieve]
    end

    test "create_user_achieve/1 with valid data creates a user_achieve", %{
      user: user,
      achieve: achieve
    } do
      assert {:ok, %UserAchieve{} = user_achieve} =
               @valid_attrs
               |> Map.put(:user_id, user.id)
               |> Map.put(:achieve_id, achieve.id)
               |> Achievements.create_user_achieve()
    end

    test "create_user_achieve/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Achievements.create_user_achieve(@invalid_attrs)
    end

    test "delete_user_achieve/1 deletes the user_achieve", %{user: user, achieve: achieve} do
      user_achieve = user_achieve_fixture(user, achieve)
      assert {:ok, %UserAchieve{}} = Achievements.delete_user_achieve(user_achieve)
    end
  end
end
