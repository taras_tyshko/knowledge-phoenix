defmodule SmileDB.ContentHelpers do
  @moduledoc """
    This [`ContentHelpers`](SmileDB.ContentHelpers.html) is used to create test data to use them in the tested.

    To use the functions as there is a helpery to import it into a necessary case: [`DataCase`](SmileDB.DataCase.html), [`ConnCase`](SmileDB.ConnCase.html) d to call the following way
    Offician docs for used [`describe`](https://hexdocs.pm/ex_unit/ExUnit.Case.html#describe/2).

      setup [create_tag]
      setup [create_user]
      setup [create_room]
      setup [create_answer]
      setup [create_office]
      setup [create_setting]
      setup [create_comment]
      setup [create_message]
      setup [create_type]
      setup [create_content]
  """

  alias SmileDB.{Tags, Source, Accounts, Category, Messages, Permissions, Achievements, Notifications}

  @user_attrs %{
    answers_count: 42,
    auth_provider: Enum.random(["google", "facebook"]),
    avatar: "some avatar",
    biography: "some biography",
    birthday: DateTime.utc_now() |> DateTime.to_iso8601(),
    email: "some email",
    name: "Jon Smith",
    expert: ["q_test_tag", "a_test_tag"],
    facebook_uid: "sssome facebook_uid",
    first_name: "Jon",
    gender: "some gender",
    google_uid: "sssome google_uid",
    last_name: "Smith",
    likes_count: 42,
    location: "some location",
    nickname: "josmi",
    contents_count: 42,
    comments_count: 42,
    status: "some status",
    website: "some website"
  }
  @setting_attrs %{
    by_answers: true,
    by_comments: true,
    email_enable: true,
    email_on_reply: true,
    web_notif_mentions: true,
    web_notif_recommend: true,
    web_notif_upvote: true
  }
  @content_attrs %{
    anonymous: false,
    keywords: "some keywords",
    requests: 42,
    reviews: [],
    shadow: false,
    tags: ["q_test_tag", "test_tag"],
    title: "some title",
    answers_count: 42,
    reviews_count: 43
  }
  @answer_attrs %{
    anonymous: false,
    keywords: "some keywords",
    tags: ["a_test_tag"],
    title: "some title",
    ratings_count: 42,
    comments_count: 43
  }
  @comment_attrs %{
    anonymous: false,
    keywords: "some keywords",
    ratings_count: 42
  }
  @tag_attrs %{
    count: 42,
    description: "some description",
    keywords: "some keywords",
    title: "some title"
  }
  @office_attrs %{
    id: "some_id",
    title: "some title",
    email: "some email",
    country: "some country",
    avatar: "some avatar",
    location: "some location"
  }

  @role_attrs %{role: "customer", access_code: 1}
  @achieve_attrs %{title: "some title", avatar: "some avatar"}
  @room_attrs %{name: "some name", avatar: "some avatar"}
  @permission_attrs %{name: "some name"}
  @user_room_attrs %{type: "privat", role: "owner"}
  @message_attrs %{event_name: "some event_name"}

  @doc """
    This function implements the creation of user and its use of data for testing.

    This function can be invoked as follows and get the user's data.

      setup [:create_user]
  """
  @spec create_role(list()) :: {:ok, %{user: Accounts.User.t()}}
  def create_role(_) do
    {:ok, role} = Accounts.create_role(@role_attrs)
    {:ok, %{role: role}}
  end

  @doc """
    This function implements the creation of office and its use of data for testing.

    This function can be invoked as follows and get the office's data.

      setup [:create_office]
  """
  @spec create_office(list()) :: {:ok, %{user: Category.Office.t()}}
  def create_office(_) do
    {:ok, office} = Category.create_office(@office_attrs)
    {:ok, %{office: office}}
  end

  @doc """
    This function implements the creation of user and its use of data for testing.

    This function can be invoked as follows and get the user's data.

      setup [:create_achieves]
  """
  @spec create_achieves(list()) :: {:ok, %{achieve: Achievements.Achieve.t()}}
  def create_achieves(_) do
    {:ok, achieve} = Achievements.create_achieve(@achieve_attrs)
    {:ok, %{achieve: achieve}}
  end

  @doc """
    This function implements the creation of user and its use of data for testing.

    This function can be invoked as follows and get the user's data.

      setup [:create_user]
  """
  @spec create_user(list()) :: {:ok, %{user: Accounts.User.t()}}
  def create_user(_) do
    {:ok, %{role: role}} = create_role([])
    {:ok, user} = Accounts.create_user(Map.put(@user_attrs, :role, role.role))
    {:ok, user_one} = create_user_one([])
    {:ok, %{user: user, user_one: user_one}}
  end

  @doc """
      This function implements the creation test data of user and its use of data for testing.

      This function can be invoked as follows and get the user's data.

      setup [:create_user_one]
  """
  def create_user_one(_) do
    @user_attrs
    |> Map.put(:role, "customer")
    |> Map.put(:nickname, "filas")
    |> Map.put(:name, "First Last")
    |> Map.put(:first_name, "First")
    |> Map.put(:last_name, "Last")
    |> Map.put(:google_uid, "ssssome google_uid")
    |> Map.put(:facebook_uid, "ssssome facebook_uid")
    |> Accounts.create_user()
  end

  @doc """
      This function implements the creation test data of user and its use of data for testing.

      This function can be invoked as follows and get the user's data.

      setup [:create_permission]
  """
  def create_permission(_) do
    {:ok, permission} = Permissions.create_permission(@permission_attrs)
    {:ok, %{permission: permission}}
  end

  @doc """
    This function implements the creation of [`User`](KnowledgePhoenix.Accounts.User.html) and users [`Setting`](KnowledgePhoenix.Accounts.Setting.html) its use of data for testing.

    This function can be invoked as follows and get the user's data `%{setting: setting, user: user}`.

      setup [:create_setting]
  """
  @spec create_setting(list()) ::
          {:ok,
           %{
             user: Accounts.User.t(),
             setting: Accounts.Setting.t()
           }}
  def create_setting(_) do
    {:ok, %{user: user, user_one: user_one}} = create_user([])
    {:ok, setting} = Accounts.create_setting(Map.put(@setting_attrs, :user_id, user.id))

    {:ok, %{setting: setting, user: user, user_one: user_one}}
  end

  @doc """
    This function implements the creation of [`User`](KnowledgePhoenix.Accounts.User.html), [`Content`](KnowledgePhoenix.Source.Content.html) its use of data for testing.

    This function can be invoked as follows and get the user's data `%{content: content, user: user, setting: setting}`.

      setup [:create_type]
  """
  @spec create_type(list()) ::
          {:ok,
           %{
             user: Accounts.User.t(),
             setting: Accounts.Setting.t(),
             content: Source.Content.t()
           }}
  def create_type(_) do
    {:ok, %{user: user, setting: setting, user_one: user_one}} = create_setting([])

    {:ok, content} =
      @content_attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:author, user.nickname)
      |> Map.put(:description, "@#{user.nickname} some description.")
      |> Source.create_content()

    # {:ok, feed_content} = Source.create_feed(content)

    {:ok,
     %{
       content: Source.get_content!(content.id),
       user: Accounts.get_user!(user.id),
       setting: setting,
       user_one: user_one
       # feed_content: feed_content
     }}
  end

  @doc """
    This function implements the creation of [`User`](KnowledgePhoenix.Accounts.User.html), [`Content`](KnowledgePhoenix.Source.Content.html), [`Answer`](KnowledgePhoenix.Source.Answer.html) its use of data for testing.

    This function can be invoked as follows and get the content's data `%{answer: answer, content: content, user: user}`.

        setup [:create_answer]
  """
  @spec create_answer(list()) ::
          {:ok,
           %{
             user: Accounts.User.t(),
             answer: Source.Answer.t(),
             setting: Accounts.Setting.t(),
             content: Source.Content.t()
           }}
  def create_answer(_) do
    {:ok,
     %{
       content: content,
       user: user,
       setting: setting,
       #  feed_content: feed_content,
       user_one: user_one
     }} = create_type([])

    {:ok, answer} =
      @answer_attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:author, user.nickname)
      |> Map.put(:content_id, content.id)
      |> Map.put(:description, "@#{user.nickname} some description.")
      |> Source.create_answer()

    # {:ok, feed_answer} = Source.create_feed(answer)

    {:ok,
     %{
       user_one: user_one,
       answer: Source.get_answer!(answer.id),
       content: Source.get_content!(content.id),
       user: Accounts.get_user!(user.id),
       setting: setting
       #  feed_answer: feed_answer,
       #  feed_content: feed_content
     }}
  end

  @doc """
    This function implements the creation of [`User`](KnowledgePhoenix.Accounts.User.html), [`Content`](KnowledgePhoenix.Source.Content.html), [`Answer`](KnowledgePhoenix.Source.Answer.html), [`Comment`](KnowledgePhoenix.Source.Comment.html) its use of data for testing.

    This function can be invoked as follows and get the content's data `%{comment: comment, content: content, answer: answer, user: user}`.

      setup [:create_answer]
  """
  @spec create_comment(list()) ::
          {:ok,
           %{
             user: Accounts.User.t(),
             answer: Source.Answer.t(),
             comment: Source.Comment.t(),
             setting: Accounts.Setting.t(),
             content: Source.Content.t()
           }}
  def create_comment(_) do
    {:ok,
     %{
       user_one: user_one,
       answer: answer,
       content: content,
       user: user,
       setting: setting
       #  feed_answer: feed_answer,
       #  feed_content: feed_content
     }} = create_answer([])

    {:ok, comment} =
      @comment_attrs
      |> Map.put(:user_id, user.id)
      |> Map.put(:answer_id, answer.id)
      |> Map.put(:author, user.nickname)
      |> Map.put(:content_id, content.id)
      |> Map.put(:description, "@#{user.nickname} some description.")
      |> Source.create_comment()

    {:ok,
     %{
       user_one: user_one,
       comment: Source.get_comment!(comment.id),
       content: Source.get_content!(content.id),
       answer: Source.get_answer!(answer.id),
       user: Accounts.get_user!(user.id),
       #  feed_content: feed_content,
       #  feed_answer: feed_answer,
       setting: setting
     }}
  end

  @doc """
      This function implements the creation of room and its use of data for testing.

      This function can be invoked as follows and get the user's data.

      setup [:create_room]
  """
  @spec create_room(list()) ::
          {:ok,
           %{
             room: Messages.Room.t(),
             user: Accounts.User.t()
           }}
  def create_room(_) do
    {:ok,
     %{
       user_one: user_one,
       comment: comment,
       content: content,
       answer: answer,
       user: user,
       #  feed_content: feed_content,
       #  feed_answer: feed_answer,
       setting: setting
     }} = create_comment([])

    {:ok, room} = Messages.create_room(@room_attrs)

    {:ok, user_room} =
      Messages.create_user_room(
        Map.put(Map.put(@user_room_attrs, :user_id, user.id), :room_id, room.id)
      )

    {:ok,
     %{
       user: user,
       room: room,
       user_one: user_one,
       user_room: user_room,
       setting: setting,
       content: content,
       comment: comment,
       #  feed_answer: feed_answer,
       #  feed_content: feed_content,
       answer: answer
     }}
  end

  @doc """
      This function implements the creation of message and its use of data for testing.

      This function can be invoked as follows and get the user's data.

      setup [:create_message]
  """
  @spec create_message(list()) ::
          {:ok,
           %{
             message: Messages.Message.t(),
             user: Accounts.User.t(),
             room: Messages.Room.t()
           }}
  def create_message(_) do
    {:ok,
     %{
       user: user,
       room: room,
       user_one: user_one,
       user_room: user_room,
       setting: setting,
       content: content,
       comment: comment,
       #  feed_answer: feed_answer,
       #  feed_content: feed_content,
       answer: answer
     }} = create_room([])

    {:ok, message} =
      @message_attrs
      |> Map.put(:room_id, room.id)
      |> Map.put(:user_id, user.id)
      |> Map.put(:description, "@#{user.nickname} some description.")
      |> Messages.create_message()

    {:ok,
     %{
       user: user,
       room: room,
       user_room: user_room,
       setting: setting,
       message: message,
       user_one: user_one,
       content: content,
       comment: comment,
       #  feed_answer: feed_answer,
       #  feed_content: feed_content,
       answer: answer
     }}
  end

  @doc """
      This function implements the creation of message and its use of data for testing.

      This function can be invoked as follows and get the user's data.

      setup [:create_message]
  """
  @spec create_content(list()) ::
          {:ok,
           %{
             message: Messages.Message.t(),
             user: Accounts.User.t(),
             room: Messages.Room.t()
           }}
  def create_content(_) do
    {:ok,
     %{
       user: user,
       room: room,
       user_one: user_one,
       user_room: user_room,
       setting: setting,
       content: content,
       comment: comment,
       #  feed_answer: feed_answer,
       #  feed_content: feed_content,
       answer: answer
     }} = create_room([])

    [answer_notification] = Notifications.create_answer_notifications([%{user_id: user.id, answer_id: answer.id}])

    [comment_notification] = Notifications.create_comment_notifications([%{user_id: user.id, comment_id: comment.id}])

    {:ok, message} =
      @message_attrs
      |> Map.put(:room_id, room.id)
      |> Map.put(:user_id, user.id)
      |> Map.put(:description, "@#{user.nickname} some description.")
      |> Messages.create_message()

    [message_notification] = Notifications.create_message_notifications([%{user_id: user.id, message_id: message.id, room_id: message.room_id}])

    {:ok,
     %{
       user: user,
       room: room,
       user_room: user_room,
       setting: setting,
       message: message,
       user_one: user_one,
       message_notification: message_notification,
       content: content,
       comment: comment,
       comment_notification: comment_notification,
       answer_notification: answer_notification,
       #  feed_answer: feed_answer,
       #  feed_content: feed_content,
       answer: answer
     }}
  end

  @doc """
    This function implements the creation of [`Tag`](KnowledgePhoenix.Tags.Tag.html) its use of data for testing.

    This function can be invoked as follows and get the content's data `%{tag: tag}`.

      setup [:create_setting]
  """
  @spec create_tag(list()) :: {:ok, %{tag: Tags.Tag.t()}}
  def create_tag(_) do
    {:ok, tag} = Tags.create_tag(@tag_attrs)
    {:ok, %{tag: tag}}
  end

  @spec fetch_data_with_model(list(struct()), map()) :: map()
  def fetch_data_with_model(models, keys) when is_list(models) do
    Enum.map(models, fn model ->
      result =
        for {key, value} <- Map.to_list(model), into: %{} do
          {Recase.to_camel(Atom.to_string(key)), value}
        end

      keys
      |> Enum.map(&{&1, Map.fetch!(result, &1)})
      |> Enum.into(%{})
    end)
  end

  @spec fetch_data_with_model(struct(), map()) :: map()
  def fetch_data_with_model(model, keys) do
    result =
      for {key, value} <- Map.to_list(model), into: %{} do
        {Recase.to_camel(Atom.to_string(key)), value}
      end

    keys
    |> Enum.map(&{&1, Map.fetch!(result, &1)})
    |> Enum.into(%{})
  end

  @spec conver_datatimes_and_id(map()) :: map()
  def conver_datatimes_and_id(model) do
    model
    |> Map.put("id", Integer.to_string(model["id"]))
    |> Map.put("insertedAt", DateTime.to_string(model["insertedAt"]))
    |> Map.put("updatedAt", DateTime.to_string(model["updatedAt"]))
  end

  @spec conver_datatimes_and_id_to_iso8601(map()) :: map()
  def conver_datatimes_and_id_to_iso8601(model) do
    model
    |> Map.put("id", Integer.to_string(model["id"]))
    |> Map.put("insertedAt", DateTime.to_iso8601(model["insertedAt"], :extended))
    |> Map.put("updatedAt", DateTime.to_iso8601(model["updatedAt"], :extended))
  end

  @spec model_fetch_user_data(Accounts.User.t(), struct()) :: map()
  def model_fetch_user_data(user, model_data) do
    user
    |> fetch_data_with_model(
      model_data
      |> Map.fetch!("user")
      |> Map.keys()
    )
    |> conver_datatimes_and_id_to_iso8601()
  end

  @spec convert_description(String.t()) :: String.t()
  def convert_description(description) do
    Source.render_nicknames_from_description(%{description: description}).desc
  end
end
