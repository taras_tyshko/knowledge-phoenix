defmodule SmileDB.Mixfile do
  use Mix.Project

  def project do
    [
      app: :smile_db,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {SmileDB.Application, []},
      extra_applications: [
        :logger,
        :jason,
        :runtime_tools,
        :logger_json,
        :files_uploader,
        :elastic_search
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:files_uploader, in_umbrella: true},
      {:elastic_search, in_umbrella: true},
      {:phoenix_ecto, "~> 3.4"},
      {:postgrex, ">= 0.0.0"},
      {:timex, "~> 3.4", override: true},
      {:logger_json, "~> 1.2.1"},
      {:logger_logstash_backend, "~> 5.0.0"},
      {:jason, "~> 1.1"},
      {:exgravatar, "~> 2.0.1"},
      {:guardian, "~> 1.1.1"},
      {:oauth2, "~> 0.9"},
      {:faker, "~> 0.11.1"},
      {:paginator, "~> 0.5"},
      {:pbkdf2_elixir, "~> 0.12.3"},
      {:comeonin, "~> 4.1"},
      {:ecto_ltree, "~> 0.1.0"},
      {:uuid, "~> 1.1.8"},
      {:toml, "~> 0.3.0"},
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4.6"},
      {:dataloader, "~> 1.0.4"},
      {:httpoison, "~> 1.4"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
