defmodule KnowledgePhoenixWeb.Middlewares.Profile do
  @moduledoc """
    This module is used to process errors that are played inside and output to a single format for further analysis.

    The module uses the documentation information. [`Absinthe.Middleware`](https://hexdocs.pm/absinthe/Absinthe.Middleware.html#content)

    This module analyzes the input data of the first argument. 
    If it has received a bug from the database then the error will be a iteration and output error of the desired format.
    To operate the module, it is necessary to add it to the call function middleware in [`Schema`](MessengerWeb.Graphql.html)
  """

  @behaviour Absinthe.Middleware
  @doc """
    This is the main middleware callback.

    It receives an `Absinthe.Resolution` struct and it needs to return an `Absinthe.Resolution` struct.
    The second argument will be whatever value was passed to the middleware call that setup the middleware.

    Callback implementation for `Absinthe.Middleware.call/2`.

  ## Example

      @behaviour Absinthe.Middleware
      
      def call(%{context: context, arguments: %{nickname: _}} = resolution, _config) do
        %{resolution | context: Map.put(context, :current_user, nil)}
      end

      def call(resolution, _config) do
        resolution
      end
  """
  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{context: context, arguments: %{nickname: _}} = resolution, _config) do
    %{resolution | context: Map.put(context, :current_user, nil)}
  end

  def call(resolution, _config) do
    resolution
  end
end
