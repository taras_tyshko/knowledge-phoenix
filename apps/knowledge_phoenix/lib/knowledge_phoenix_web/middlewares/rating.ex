defmodule KnowledgePhoenixWeb.Middlewares.Rating do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias SmileDB.{Accounts, Source, Ratings}

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{value: value} = resolution, _config) when is_nil(value), do: resolution

  def call(
        %{
          arguments: %{complaint: %{rating: rating}},
          value: %{id: content_id, office_id: office_id},
          state: :resolved
        } = resolution,
        _config
      ) do
    {:ok, _office_rating} =
      Ratings.create_office_rating(%{
        office_id: office_id,
        score: rating,
        content_id: content_id
      })

    resolution
  end

  def call(
        %{
          value: %Ratings.AnswerRating{answer_id: answer_id} = answer_rating,
          context: %{pubsub: pubsub},
          state: :resolved
        } = resolution,
        _config
      ) do
    answer =
      answer_id
      |> Source.get_answer!()
      |> ElasticSearch.Repo.update()
      |> put_like_to_user(answer_rating)
      |> case do
        {_ref, %{anonymous: true} = answer} ->
          answer

        {_ref, %{user_id: user_id} = answer} ->
          Absinthe.Subscription.publish(
            pubsub,
            Accounts.get_user!(user_id),
            user_account: "room:lobby"
          )

          answer
      end

    %{
      resolution
      | context:
          Map.put(
            resolution.context,
            :notification_source,
            answer_rating
          )
    }
    |> Absinthe.Resolution.put_result({:ok, answer})
  end

  def call(
        %{
          value: %Ratings.CommentRating{comment_id: comment_id} = comment_rating,
          context: %{pubsub: pubsub},
          state: :resolved
        } = resolution,
        _config
      ) do
    comment =
      comment_id
      |> Source.get_comment!()
      |> ElasticSearch.Repo.update()
      |> put_like_to_user(comment_rating)
      |> case do
        {_ref, %{anonymous: true} = comment} ->
          comment

        {_ref, %{user_id: user_id} = comment} ->
          Absinthe.Subscription.publish(
            pubsub,
            Accounts.get_user!(user_id),
            user_account: "room:lobby"
          )

          comment
      end

    %{
      resolution
      | context:
          Map.put(
            resolution.context,
            :notification_source,
            comment_rating
          )
    }
    |> Absinthe.Resolution.put_result({:ok, comment})
  end

  def call(resolution, _config), do: resolution

  defp put_like_to_user(%Source.Answer{user_id: user_id} = answer, %Ratings.AnswerRating{
         __meta__: %{state: :loaded},
         id: id,
         status: true
       }) do
    {run_create_user_rating(%{
       user_id: user_id,
       answer_rating_id: id
     }), answer}
  end

  defp put_like_to_user(%Source.Comment{user_id: user_id} = comment, %Ratings.CommentRating{
         __meta__: %{state: :loaded},
         id: id,
         status: true
       }) do
    {run_create_user_rating(%{
       user_id: user_id,
       comment_rating_id: id
     }), comment}
  end

  defp put_like_to_user(object, _), do: {nil, object}

  defp run_create_user_rating(map) do
    Task.async(Ratings, :create_user_rating, [map])
  end
end
