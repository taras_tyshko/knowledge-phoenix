defmodule KnowledgePhoenixWeb.Middlewares.Skip do
  @moduledoc false

  @behaviour Absinthe.Middleware

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(resolution, new_value: value) do
    resolution
    |> Absinthe.Resolution.put_result({:ok, value})
    |> Map.put(:state, :unresolved)
  end
end
