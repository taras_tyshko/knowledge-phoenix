defmodule KnowledgePhoenixWeb.Middlewares.Authentication do
  @moduledoc false

  @behaviour Absinthe.Middleware
  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{context: %{current_user: user}} = resolution, _config) when is_nil(user) do
    Absinthe.Resolution.put_result(resolution, {:error, "unauthenticated"})
  end

  def call(resolution, _config) do
    resolution
  end
end
