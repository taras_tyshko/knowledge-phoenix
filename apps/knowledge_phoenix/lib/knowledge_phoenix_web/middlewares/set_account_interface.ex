defmodule KnowledgePhoenixWeb.Middlewares.SetAccountInterface do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias SmileDB.Accounts

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{value: %Accounts.User{} = user} = resolution, key) do
    Absinthe.Resolution.put_result(resolution, {:ok, Map.put(user, key, nil)})
  end

  def call(%{value: value, context: %{current_user: user}} = resolution, key) do
    Absinthe.Resolution.put_result(resolution, {:ok, Map.put(user, key, value)})
  end

  def call(resolution, _key), do: resolution
end
