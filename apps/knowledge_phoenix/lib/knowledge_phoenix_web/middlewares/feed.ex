defmodule KnowledgePhoenixWeb.Middlewares.Feed do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias SmileDB.{Subscriptions, Source, Contents}

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{value: nil} = resolution, _config), do: resolution

  def call(%{value: value, context: %{pubsub: pubsub}} = resolution, _config) do
    Task.async(fn ->
      with %{
             anonymous: anonymous,
             content_id: content_id,
             shadow: false,
             tags: tags,
             user_id: user_id
           } = feed <- Source.put_feed(value) do
        Subscriptions.list_users_id_from_contents_subscription(content_id)
        |> List.flatten(Subscriptions.list_user_id_tags_subscription(tags))
        |> List.flatten(Contents.list_me_to(content_id))
        |> List.flatten(
          if anonymous do
            Subscriptions.list_user_id_users_subscription(user_id)
          else
            []
          end
        )
        |> Enum.uniq()
        |> List.delete(user_id)
        |> Enum.map(&Absinthe.Subscription.publish(pubsub, feed, feed: &1))
      end
    end)

    resolution
  end
end
