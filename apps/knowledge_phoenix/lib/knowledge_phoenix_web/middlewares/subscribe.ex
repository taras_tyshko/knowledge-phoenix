defmodule KnowledgePhoenixWeb.Middlewares.Subscribe do
  @moduledoc false

  @behaviour Absinthe.Middleware

  alias SmileDB.Source
  alias SmileDB.Subscriptions

  @spec call(Absinthe.Resolution.t(), term()) :: Absinthe.Resolution.t()
  def call(%{value: value} = resolution, _config) when is_nil(value), do: resolution

  def call(
        %{
          value: %Source.Content{} = content,
          context: %{current_user: user, pubsub: pubsub},
          state: :resolved
        } = resolution,
        _config
      ) do
    fn ->
      Subscriptions.create_content_subscription(%{
        content_id: content.id,
        user_id: user.id
      })
      |> case do
        {:ok, %{user_id: user_id}} ->
          Absinthe.Subscription.publish(
            pubsub,
            %{id: user_id, subscribed_contents: nil},
            account_info: "account:#{user_id}"
          )

        {:error, changeset} ->
          {:error, changeset}
      end
    end
    |> Task.async()

    resolution
  end

  def call(
        %{
          value: %Subscriptions.UserSubscription{} = user_subscription,
          context: %{current_user: user, pubsub: pubsub},
          state: :resolved
        } = resolution,
        _config
      ) do
    Absinthe.Subscription.publish(
      pubsub,
      %{id: user_subscription.follower_id, followers: nil},
      user_account: "room:lobby"
    )

    Absinthe.Resolution.put_result(resolution, {:ok, user})
  end

  def call(%{context: %{current_user: user}, state: :resolved} = resolution, _config) do
    Absinthe.Resolution.put_result(resolution, {:ok, user})
  end

  def call(resolution, _config), do: resolution
end
