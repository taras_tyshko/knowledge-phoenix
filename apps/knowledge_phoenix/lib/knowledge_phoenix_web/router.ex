defmodule KnowledgePhoenixWeb.Router do
  @moduledoc false

  use KnowledgePhoenixWeb, :router

  @spec authorization(Plug.Conn.t(), tuple()) :: Plug.Conn.t()
  defp authorization(conn, _opts) do
    user = SmileDB.Auth.Guardian.Plug.current_resource(conn)
    Absinthe.Plug.put_options(conn, context: %{current_user: user})
  end

  pipeline :graphql do
    plug(SmileDB.Auth.Guardian.Pipeline.GraphqlPipeline)
    plug(:authorization)
  end

  # Forward in sub project Messenger
  scope "/" do
    pipe_through([:graphql])
    forward("/messenger", MessengerWeb.Router)
    forward("/admin", KnowledgeAdminWeb.Router)
  end

  scope "/" do
    pipe_through([:graphql])
    if Mix.env() == :dev, do: forward("/sent_emails", Bamboo.SentEmailViewerPlug)
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: KnowledgePhoenixWeb.Schema)

    forward("/", Absinthe.Plug, schema: KnowledgePhoenixWeb.Schema)
  end
end
