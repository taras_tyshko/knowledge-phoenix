defmodule KnowledgePhoenixWeb.Endpoint do
  @moduledoc """
  An endpoint is simply a module defined with the help of `Phoenix.Endpoint`.

  Overall, an endpoint has three responsibilities:

    - to provide a wrapper for starting and stopping the endpoint as part of a supervision tree;
    - to define an initial plug pipeline for requests to pass through;
    - to host web specific configuration for your application.
  """
  use Phoenix.Endpoint, otp_app: :knowledge_phoenix
  use Absinthe.Phoenix.Endpoint

  socket("/socket", KnowledgePhoenixWeb.UserSocket)
  socket("/messenger/socket", MessengerWeb.UserSocket)
  socket("/admin/socket", KnowledgeAdminWeb.UserSocket)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug(Phoenix.CodeReloader)
  end

  if Mix.env() == :dev do
    plug(Plug.Logger)
  else
    plug(LoggerJSON.Plug)
  end

  plug(
    Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Jason
  )

  plug(Plug.MethodOverride)
  plug(Plug.Head)

  plug(CORSPlug)

  plug(
    Plug.Static,
    at: "/uploads",
    from: Application.get_env(:files_uploader, :path),
    gzip: false
  )

  plug(KnowledgePhoenixWeb.Router)

  @doc """
  Callback invoked for dynamically configuring the endpoint.

  It receives the endpoint configuration and checks if
  configuration should be loaded from the system environment.
  """
  @spec init(tuple(), tuple()) :: tuple()
  def init(_key, config) do
    if config[:load_from_system_env] do
      port = System.get_env("PORT") || raise "expected the PORT environment variable to be set"
      {:ok, Keyword.put(config, :http, [:inet6, port: port])}
    else
      {:ok, config}
    end
  end
end
