defmodule KnowledgePhoenixWeb.UserSocket do
  @moduledoc """
  Defines a socket and its state.

  See the [`Phoenix.Socket`](https://hexdocs.pm/phoenix/Phoenix.Socket.html)
  docs for more details.
  """

  use Phoenix.Socket

  use Absinthe.Phoenix.Socket, schema: KnowledgePhoenixWeb.Schema

  ## Channels
  channel("room:*", KnowledgePhoenixWeb.RoomChannel)

  ## Transports
  transport(:websocket, Phoenix.Transports.WebSocket)
  # transport :longpoll, Phoenix.Transports.LongPoll

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.

  @spec connect(map(), map()) :: {:ok, map()} | {:error, atom}
  def connect(params, socket) do
    if params["Authorization"] do
      "Bearer " <> token = params["Authorization"]

      case Guardian.Phoenix.Socket.authenticate(socket, SmileDB.Auth.Guardian, token) do
        {:ok, authed_socket} ->
          socket =
            Absinthe.Phoenix.Socket.put_options(
              socket,
              context: %{
                current_user: Guardian.Phoenix.Socket.current_resource(authed_socket)
              }
            )

          {:ok, assign(socket, :authenticated, true)}

        {:error, _reason} ->
          :error
      end
    else
      {:ok, assign(socket, :authenticated, false)}
    end
  end

  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     KnowledgePhoenixWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  @spec id(map()) :: nil
  def id(_socket), do: nil
end
