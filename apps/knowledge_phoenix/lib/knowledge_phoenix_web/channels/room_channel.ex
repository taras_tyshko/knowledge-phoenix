defmodule KnowledgePhoenixWeb.RoomChannel do
  @moduledoc """
  Channels handle events from clients, so they are similar to Controllers, but there are two key differences.
  Channel events can go both directions - incoming and outgoing. Channel connections also persist beyond a single request/response cycle.
  Channels are the highest level abstraction for realtime communication components in Phoenix.
  """

  use KnowledgePhoenixWeb, :channel

  alias KnowledgePhoenixWeb.Presence

  @doc """
  Joining a channel and sending confirmation.

  ## Parameters

    - room:lobby: The name of the `channel` to which the contact customer.

  ## Examples

      def join("room:lobby", _params, socket) do
        send(self(), :after_join)
        {:ok, socket}
      end
  """
  @spec join(String.t(), map(), map()) :: {:ok, map()}
  def join("room:lobby", _params, socket) do
    send(self(), :after_join)
    {:ok, socket}
  end

  @doc """
  Feature sends event `presence_state` about status users `online/offline`.

  ## Parameters

    - presence_state: Event tracking connection of client to server, using the module `Presence`.

  ## Examples

      def handle_info(:after_join, socket) do
        push socket, "presence_state", Presence.list(socket)
        {:ok, _} =
          Presence.track(socket, socket.assigns.token, %{
            online_at: inspect(System.system_time(:seconds))
          })
        {:noreply, socket}
      end
  """
  @spec handle_info(atom, map()) :: {:ok, map()} | {:noreply, map()}
  def handle_info(:after_join, socket) do
    push(socket, "presence_state", Presence.list(socket))

    if socket.assigns.authenticated do
      user =
        socket.assigns.absinthe.opts
        |> Keyword.fetch!(:context)
        |> Map.fetch!(:current_user)

      {:ok, _} =
        Presence.track(socket, user.id, %{
          online_at: inspect(System.system_time(:seconds)),
          nickname: user.nickname,
          avatar: user.avatar
        })
    end

    {:noreply, socket}
  end
end
