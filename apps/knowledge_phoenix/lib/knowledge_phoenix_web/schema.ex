defmodule KnowledgePhoenixWeb.Schema do
  @moduledoc false

  use SmileDB.Graphql

  import_types(KnowledgePhoenixWeb.Schema.Enum)
  import_types(KnowledgePhoenixWeb.Schema.Mutation)
  import_types(KnowledgePhoenixWeb.Schema.TagTypes)
  import_types(KnowledgePhoenixWeb.Schema.UserTypes)
  import_types(KnowledgePhoenixWeb.Schema.FeedTypes)
  import_types(KnowledgePhoenixWeb.Schema.ScopeTypes)
  import_types(KnowledgePhoenixWeb.Schema.Subscription)
  import_types(KnowledgePhoenixWeb.Schema.ContentTypes)
  import_types(KnowledgePhoenixWeb.Schema.ProfileTypes)
  import_types(KnowledgePhoenixWeb.Schema.AccountsTypes)
  import_types(KnowledgePhoenixWeb.Schema.PaginatorsTypes)
  import_types(KnowledgePhoenixWeb.Schema.NotificationTypes)

  @desc "In this Root Query we display all API routes with which you can work. To work with the API, the router must be required to transmit the 'Token' to the user.
    The work with the router is sure to see what parameters you need to transmit and what type of data it takes, because the types of data are strictly asked."
  query do
    import_fields(:notification_queries)

    import_fields(:account_queries)

    import_fields(:content_queries)

    import_fields(:scoped_queries)

    import_fields(:user_queries)

    import_fields(:feed_queries)

    import_fields(:tag_queries)
  end

  mutation do
    import_fields(:mutations)
  end

  subscription do
    import_fields(:subscriptions)
  end

  def middleware(middleware, field, object) do
    super(middleware, field, object)
  end
end
