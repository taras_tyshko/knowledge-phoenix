defmodule KnowledgePhoenixWeb.Schema.TagTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers

  object :tag_queries do
    @desc "Retrieve a list of tags with cursor pagination and search tags."
    field :tags, :tag_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:input, :string, description: "Search Tag by word.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Tag.list_tags/3)
    end

    @desc "Implements search tags according to specific arguments."
    field :tags_autocomplete, list_of(:string) do
      arg(:input, :string, description: "Search Tag by word.")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.Tag.tags_autocomplete/3)
    end
  end
end
