defmodule KnowledgePhoenixWeb.Schema.AccountsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.{Accounts, Achievements, Notifications, Permissions}
  alias KnowledgePhoenixWeb.Resolvers
  alias KnowledgePhoenixWeb.Middlewares

  object :account_queries do
    @desc "User authorization."
    field :auth, :string do
      arg(:code, non_null(:string), description: "Access code.")
      arg(:provider, non_null(:string), description: "Authorization provider.")

      resolve(&Resolvers.Accounts.callback/3)
    end

    @desc "Connect accounts"
    field :connect, :string do
      middleware(Middlewares.Authentication)
      arg(:code, non_null(:string), description: "Access code.")
      arg(:provider, non_null(:string), description: "Authorization provider.")

      resolve(&Resolvers.Accounts.connect/3)
    end

    @desc "Disconnect accounts"
    field :disconnect, :string do
      middleware(Middlewares.Authentication)
      arg(:provider, non_null(:string), description: "Authorization provider.")

      resolve(&Resolvers.Accounts.disconnect/3)
    end

    @desc "Getting user accounts data. You need to transmit a 'Token' for data retrieval."
    field :account, :account do
      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "For user profile data. Only token is required to retrieve the authorized user's data file. Only a nickname is required to retrieve another user's data."
    field :profile, :profile do
      middleware(Middlewares.Profile)

      arg(:nickname, :string,
        description: "Current nickname on which to obtain the data user profile."
      )

      resolve(&Resolvers.Accounts.find_profile_account/3)
    end

    @desc "Get a list of users according to specific nickname."
    field :user_by_nicknames, list_of(:user) do
      arg(:nicknames, list_of(:string), description: "List of nickname for which the user is.")

      resolve(&Resolvers.Accounts.find_users_by_nicknames/3)
    end

    ## TODO Delete if added in role object :list_permissions from role!
    @desc "Gets a list permission in user by role."
    field(:list_permissions, list_of(:string)) do
      middleware(Middlewares.Authentication)

      resolve(fn _, _, %{context: %{current_user: user}} ->
        Permissions.list_permissions(user.role)
      end)
    end
  end

  @desc "The given object implements the receipt of data pertaining to the user's account."
  object :account do
    @desc "Get user unique identifier ID."
    field :id, :integer do
      resolve(fn %{id: id}, _, _ ->
        {:ok, id}
      end)
    end

    @desc "Get the mail which user has to confirm."
    field :email_verification, :string do
      resolve(&Resolvers.Accounts.email_verification/3)
    end

    @desc "Get user Data."
    field :user, :user do
      resolve(&Resolvers.Accounts.find_user/3)
    end

    @desc "List user of Achieves."
    field(:achieves, list_of(:achieve)) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok, Achievements.list_achieves(id)}
      end)
    end

    @desc "Getting settings for user."
    field :setting, :setting do
      resolve(&Resolvers.Accounts.get_setting/3)
    end

    @desc "Get the list ID of the contents on which user subscribed."
    field :subscribed_contents, list_of(:integer) do
      resolve(&Resolvers.Accounts.get_subscribed_contents/3)
    end

    @desc "Get the list ID of the offices on which user subscribed."
    field :subscribed_offices, list_of(:string) do
      resolve(&Resolvers.Accounts.get_subscribed_offices/3)
    end

    @desc "Get the list of the users on which user subscribed."
    field :subscribed_users, list_of(:user) do
      resolve(&Resolvers.Accounts.get_subscribed_users/3)
    end

    @desc "Get the list of the tags on which user subscribed."
    field :subscribed_tags, list_of(:string) do
      resolve(&Resolvers.Accounts.get_subscribed_tags/3)
    end

    @desc "Get the list ID of the followers on which user subscribed."
    field :followers, list_of(:integer) do
      resolve(&Resolvers.Accounts.get_followers/3)
    end

    @desc "Get the list ID of the user`s notifications."
    field :notifications, list_of(:string) do
      resolve(&Resolvers.Accounts.get_notifications/3)
    end

    @desc "Get the list ID of the user`s messenger notifications."
    field :messenger_notifications, list_of(:string) do
      resolve(fn %{id: id}, _args, _resolution ->
        {:ok,
         id
         |> Notifications.get_message_notification()
         |> Enum.map(& &1.id)}
      end)
    end

    @desc "Generation of the user default avatar."
    field :default_avatar, :string do
      resolve(fn %{email: email}, _args, _resolution ->
        {:ok, Accounts.default_avatar(email)}
      end)
    end

    @desc "To get a photo you need to transfer user Token."
    field :import_avatar, :provider_avatar do
      resolve(fn parent, _, _resolution ->
        {:ok, Accounts.import_from(parent)}
      end)
    end
  end
end
