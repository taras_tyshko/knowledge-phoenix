defmodule KnowledgePhoenixWeb.Schema.ProfileTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers

  @desc "This resolver returns data for a user profile. Only token is required to retrieve the authorized user's data file. Only a nickname is required to retrieve another user's data."
  object :profile do
    @desc "Get account data about user."
    field :account_info, :account do
      resolve(&Resolvers.Accounts.find_user/3)
    end

    @desc "Get a list of contents for the user."
    field :contents, :content_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:tags, list_of(:string), description: "List of content tags.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:type, non_null(:contents_types), description: "Content type to return.")

      arg(:order_by, non_null(:contents_order_by),
        description: "This option sets the content to sort."
      )

      resolve(&Resolvers.Profile.list_contents/3)
    end

    @desc "Get a list of answers for the user."
    field :answers, :answer_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:tags, list_of(:string), description: "List of answer tags.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      arg(:order_by, non_null(:answers_order_by),
        description: "This option sets the answer to sort."
      )

      resolve(&Resolvers.Profile.list_answers/3)
    end

    @desc "Get a list of comments for the user."
    field :comments, :comment_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:tags, list_of(:string), description: "List of comment tags.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      arg(:order_by, non_null(:comments_order_by),
        description: "This option sets the comment to sort."
      )

      resolve(&Resolvers.Profile.list_comments/3)
    end
  end
end
