defmodule KnowledgePhoenixWeb.Schema.Subscription do
  @moduledoc false

  use Absinthe.Schema.Notation

  object :subscriptions do
    @desc "Update user settings, listen to mutation: [:account_setting, :subscribe_content, :subscribe_tag, :create_expert_request, :content_add, :subscribe_office]."
    field :account_info, :account_interface do
      config(fn
        _args, %{context: %{current_user: user}} ->
          if user, do: {:ok, topic: "account:#{user.id}"}
      end)

      trigger(
        [
          :account_setting,
          :subscribe_content,
          :subscribe_tag,
          :subscribe_office,
          :delete_account_notifications
        ],
        topic: fn
          %{user_id: user_id} ->
            "account:#{user_id}"

          %SmileDB.Accounts.User{} = user ->
            "account:#{user.id}"
        end
      )
    end

    field :account_notifications, :notification do
      config(fn
        _args, %{context: %{current_user: user}} ->
          if user, do: {:ok, topic: "account:#{user.id}"}
      end)
    end

    field :feed, :feed_union do
      config(fn _args, %{context: %{current_user: user}} ->
        {:ok, topic: user.id}
      end)
    end

    @desc "Update user accounts listen to mutation: :account_user, :subscribe_office, subscribe_user."
    field :user_account, :account_interface do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        [
          :account_user,
          :subscribe_user
        ],
        topic: fn _ ->
          "room:lobby"
        end
      )
    end

    @desc "Add Content, listen to mutation: :content_add."
    field :add_content, :contents_union do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :content_add,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Edit Content, listen to mutation: :content_edit."
    field :edit_content, :contents_union do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :content_edit,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Delete Content, listen to mutation: :content_delete."
    field :delete_content, :contents_union do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :content_delete,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Add Answer, listen to mutation: :answer_add."
    field :add_answer, :answer do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :answer_add,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Edit Answer, listen to mutation: :answer_edit."
    field :edit_answer, :answer do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :answer_edit,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Delete Answer, listen to mutation: :answer_delete."
    field :delete_answer, :answer do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :answer_delete,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Add Comment, listen to mutation: :comment_add."
    field :add_comment, :comment do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :comment_add,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Edit Comment, listen to mutation: :comment_edit."
    field :edit_comment, :comment do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :comment_edit,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Delete Comment, listen to mutation: :comment_delete."
    field :delete_comment, :comment do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :comment_delete,
        topic: fn
          %{shadow: false} ->
            "room:lobby"

          _ ->
            nil
        end
      )
    end

    @desc "Add reviews for Content, listen to mutation: :reviews_add."
    field :reviews_content, :contents_union do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :add_reviews,
        topic: fn _ ->
          "room:lobby"
        end
      )
    end

    @desc "Add likes for Answer, listen to mutation: :add_answer_likes."
    field :answer_likes, :answer do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :answer_rating,
        topic: fn _ ->
          "room:lobby"
        end
      )
    end

    @desc "Add likes for Comment, listen to mutation: :add_comment_likes."
    field :comment_likes, :comment do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :comment_rating,
        topic: fn _ ->
          "room:lobby"
        end
      )
    end

    @desc "Add MeTo for Content, listen to mutation: :me_to"
    field :me_to, :contents_union do
      config(fn _args, _info ->
        {:ok, topic: "room:lobby"}
      end)

      trigger(
        :me_to,
        topic: fn _ ->
          "room:lobby"
        end
      )
    end
  end
end
