defmodule KnowledgePhoenixWeb.Schema.PaginatorsTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :user_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:user), description: "List of User.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :content_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:contents_union), description: "List of Content.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :answer_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:answer), description: "List of Answer.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :comment_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:comment), description: "List of Comment.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :tag_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:tag), description: "List of Tag.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :feed_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:feed_union), description: "List of Feed.")
  end

  @desc "Implement the structure to retrieve data with the cursor of a pagination."
  object :notification_paginator do
    field(:metadata, :metadata, description: "Additional data for pagination.")
    field(:entries, list_of(:notification), description: "List of Notifications.")
  end

  @desc "Implement the structure to obtain additional data for the cursor pagination such as."
  object :metadata do
    field(:after, :string, description: "Fetch the records after this cursor.")
    field(:limit, :integer, description: "Number of results to return.")
    field(:total_count, :integer, description: "Number of all results.")
  end
end
