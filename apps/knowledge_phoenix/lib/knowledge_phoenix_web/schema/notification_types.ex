defmodule KnowledgePhoenixWeb.Schema.NotificationTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.Notifications
  alias KnowledgePhoenixWeb.Resolvers
  alias KnowledgePhoenixWeb.Middlewares

  object :notification_queries do
    @desc "Get notification data."
    field :notifications, :notification_paginator do
      middleware(Middlewares.Authentication)
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Notification.list_notifications/3)
    end

    @desc "Get the list ID of the user`s messenger notifications."
    field :messenger_notifications, list_of(:messages_notifications) do
      middleware(Middlewares.Authentication)

      resolve(fn _parent, _args, %{context: %{current_user: %{id: id}}} ->
        {:ok, Notifications.get_message_notification(id)}
      end)
    end
  end
end
