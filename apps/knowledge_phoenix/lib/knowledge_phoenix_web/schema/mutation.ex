defmodule KnowledgePhoenixWeb.Schema.Mutation do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias SmileDB.{Repo, Accounts, Ratings, Source, Subscriptions}
  alias KnowledgePhoenixWeb.{Middlewares, Resolvers}

  object :mutations do
    @desc "Restore user account"
    field :restore_account, :account do
      middleware(Middlewares.Authentication)

      resolve(&Resolvers.Accounts.restore/3)
    end

    @desc "Set change user email."
    field :set_change_user_email, :account do
      middleware(Middlewares.Authentication)
      arg(:email, non_null(:string), description: "Email address to which you need to change.")

      resolve(&Resolvers.Accounts.set_change_user_email/3)
    end

    @desc "Confirm change user email."
    field :confirm_email, :account do
      arg(:hash, non_null(:string), description: "Hash to confirm the change of mail.")

      resolve(&Resolvers.Accounts.update_user_email/3)
    end

    @desc "Cancel confirm user email."
    field :cancel_confirm_email, :account do
      arg(:hash, :string, description: "Hash to confirm the change of mail.")

      resolve(&Resolvers.Accounts.cancel_confirm_user_email/3)
    end

    @desc "Subscribe user"
    field :subscribe_user, :account do
      middleware(Middlewares.Authentication)

      arg(
        :status,
        non_null(:subscription_status),
        description: "User subscription status for users."
      )

      arg(
        :subscribe_id,
        non_null(:integer),
        description: "The user ID you want to subscribe to or unsubscribe."
      )

      resolve(
        fn _parent,
           %{subscribe_id: follower_id, status: status} = args,
           %{context: %{current_user: %{id: user_id} = user}} = resolution ->
          Subscriptions.UserSubscription
          |> Repo.get_by(%{follower_id: follower_id, user_id: user_id})
          |> (fn
                %{status: ^status} ->
                  {:middleware, Middlewares.Skip, [new_value: user]}

                result ->
                  result
                  |> case do
                    nil ->
                      %Subscriptions.UserSubscription{follower_id: follower_id, user_id: user_id}

                    user_subscription ->
                      Subscriptions.delete_user_subscription(user_subscription)
                  end
                  |> Resolvers.Subscribe.subscribe_user(args, resolution)
              end).()
        end
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(
        Notifications.Absinthe.Middleware,
        source: & &1.value,
        push_to: [:account, :notifications],
        list_to_publish: fn %{value: %Subscriptions.UserSubscription{} = user_subscription} ->
          [user_subscription.follower_id]
        end
      )

      middleware(Middlewares.Subscribe)
      middleware(Middlewares.SetAccountInterface, :subscribed_users)
    end

    @desc "Subscribe content"
    field :subscribe_content, :account do
      middleware(Middlewares.Authentication)

      arg(
        :status,
        non_null(:subscription_status),
        description: "Content subscription status for users."
      )

      arg(
        :content_id,
        non_null(:integer),
        description: "The content ID you want to subscribe to or unsubscribe."
      )

      resolve(
        (&Resolvers.Subscribe.subscribe_content/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.SetAccountInterface, :subscribed_contents)
    end

    @desc "Subscribe office"
    field :subscribe_office, :account do
      middleware(Middlewares.Authentication)

      arg(
        :status,
        non_null(:subscription_status),
        description: "Office subscription status for users."
      )

      arg(
        :subscribe_id,
        non_null(:string),
        description: "The office ID you want to subscribe to or unsubscribe."
      )

      resolve(
        (&Resolvers.Subscribe.subscribe_office/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.SetAccountInterface, :subscribed_offices)
    end

    @desc "Subscribe tag"
    field :subscribe_tag, :account do
      middleware(Middlewares.Authentication)

      arg(
        :status,
        non_null(:subscription_status),
        description: "Tag subscription status for users."
      )

      arg(
        :tag,
        non_null(:string),
        description: "The tag title you want to subscribe to or unsubscribe."
      )

      resolve(
        (&Resolvers.Subscribe.subscribe_tag/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.SetAccountInterface, :subscribed_tags)
    end

    @desc "Update user settings"
    field :account_setting, :setting do
      middleware(Middlewares.Authentication)
      arg(:by_answers, :boolean)
      arg(:by_comments, :boolean)
      arg(:email_enable, :boolean)
      arg(:email_on_reply, :boolean)
      arg(:web_notif_mentions, :boolean)
      arg(:web_notif_recommend, :boolean)
      arg(:web_notif_upvote, :boolean)

      resolve(&Resolvers.Accounts.update_user_setting/3)
    end

    @desc "Update user data"
    field :account_user, :account do
      middleware(Middlewares.Authentication)
      arg(:auth_provider, :string)
      arg(:avatar, :string)
      arg(:biography, :string)
      arg(:birthday, :string)
      arg(:first_name, :string)
      arg(:gender, :string)
      arg(:last_name, :string)
      arg(:location, :string)
      arg(:nickname, :string)
      arg(:status, :string)
      arg(:website, :string)

      resolve(fn _parent, args, %{context: %{current_user: user}} ->
        Accounts.update_user(user, args)
      end)
    end

    @desc "Delete user answer and comment notifications"
    field :delete_account_notifications, :account do
      middleware(Middlewares.Authentication)
      arg(:id, list_of(:string), description: "List notifications ID`s.")

      resolve(&Resolvers.Accounts.delete_account_notifications/3)
    end

    @desc "Delete User"
    field :delete_user, :user do
      middleware(Middlewares.Authentication)

      resolve(fn _parent, _args, %{context: %{current_user: user}} ->
        Accounts.delete_account(user)
      end)
    end

    @desc "Add Content"
    field :content_add, :contents_union do
      middleware(Middlewares.Authentication)
      arg(:question, :question_input, description: "Question input object.")
      arg(:complaint, :complaint_input, description: "Complaint input object.")

      resolve(
        (&Resolvers.Mutation.add_content/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Rating)
      middleware(Middlewares.Subscribe)

      middleware(
        Notifications.Absinthe.Middleware,
        source: & &1.value,
        push_to: [:account, :notifications],
        list_to_publish: fn %{
                              context: %{current_user: %{id: user_id}},
                              value: %Source.Content{anonymous: false, id: content_id}
                            } ->
          content_id
          |> Subscriptions.list_users_id_from_contents_subscription()
          |> Enum.uniq()
          |> List.delete(user_id)
        end
      )

      middleware(Middlewares.Feed)
    end

    @desc "Edit Content"
    field :content_edit, :contents_union do
      middleware(Middlewares.Authentication)
      arg(:id, non_null(:integer), description: "Unique identifier of the Content.")
      arg(:question, :question_input, description: "Question input object.")
      arg(:complaint, :complaint_input, description: "Complaint input object.")

      resolve(
        (&Resolvers.Mutation.edit_content/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Rating)
      middleware(Middlewares.Feed)
    end

    @desc "Delete Content"
    field :content_delete, :contents_union do
      middleware(Middlewares.Authentication)
      arg(:id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(
        (&Resolvers.Mutation.delete_content/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end

    @desc "Add Answer"
    field :answer_add, :answer do
      middleware(Middlewares.Authentication)
      arg(:tags, list_of(:string), description: "List answer tags.")

      arg(
        :anonymous,
        non_null(:boolean),
        description: "anonymous -> true, not anonymous -> false."
      )

      arg(:description, non_null(:string), description: "Content description.")
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(
        (&Resolvers.Mutation.add_answer/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(
        Notifications.Absinthe.Middleware,
        source: & &1.value,
        push_to: [:account, :notifications],
        list_to_publish: fn %{
                              context: %{current_user: %{id: user_id}},
                              value: %{content_id: content_id}
                            } ->
          content_id
          |> Subscriptions.list_users_id_from_contents_subscription()
          |> Enum.uniq()
          |> List.delete(user_id)
        end
      )

      middleware(Middlewares.Feed)
    end

    @desc "Edit Answer"
    field :answer_edit, :answer do
      middleware(Middlewares.Authentication)
      arg(:description, :string, description: "Content description.")
      arg(:id, non_null(:integer), description: "Unique identifier of the Answer.")
      arg(:tags, list_of(:string), description: "List answer tags.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")

      resolve(
        (&Resolvers.Mutation.edit_answer/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Feed)
    end

    @desc "Delete Answer"
    field :answer_delete, :answer do
      middleware(Middlewares.Authentication)
      arg(:id, non_null(:integer), description: "Unique identifier of the Answer.")

      resolve(
        (&Resolvers.Mutation.delete_answer/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end

    @desc "Add Comment"
    field :comment_add, :comment do
      middleware(Middlewares.Authentication)
      arg(:path, :string, description: "Previous commentary path")
      arg(:answer_id, :integer, description: "Unique identifier of the Answer.")

      arg(
        :anonymous,
        non_null(:boolean),
        description: "anonymous -> true, not anonymous -> false."
      )

      arg(:description, non_null(:string), description: "Content description.")
      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(
        (&Resolvers.Mutation.add_comment/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(
        Notifications.Absinthe.Middleware,
        source: & &1.value,
        push_to: [:account, :notifications],
        list_to_publish: fn %{
                              context: %{current_user: %{id: user_id}},
                              value: %{content_id: content_id}
                            } ->
          content_id
          |> Subscriptions.list_users_id_from_contents_subscription()
          |> Enum.uniq()
          |> List.delete(user_id)
        end
      )

      middleware(Middlewares.Feed)
    end

    @desc "Edit Comment"
    field :comment_edit, :comment do
      middleware(Middlewares.Authentication)
      arg(:description, :string, description: "Content description.")
      arg(:id, non_null(:integer), description: "Unique identifier of the Comment.")
      arg(:anonymous, :boolean, description: "anonymous -> true, not anonymous -> false.")

      resolve(
        (&Resolvers.Mutation.edit_comment/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Feed)
    end

    @desc "Delete Comment"
    field :comment_delete, :comment do
      middleware(Middlewares.Authentication)
      arg(:id, non_null(:integer), description: "Unique identifier of the Comment.")

      resolve(
        (&Resolvers.Mutation.delete_comment/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end

    @desc "Add reviews in Content"
    field :add_reviews, :contents_union do
      middleware(Middlewares.Authentication)
      arg(:id, non_null(:integer), description: "Unique identifier of the Content.")

      resolve(
        (&Resolvers.Mutation.add_reviews/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_authenticated/3
        ])
      )
    end

    @desc "Change rating for Answer"
    field :answer_rating, :answer do
      middleware(Middlewares.Authentication)
      arg(:status, non_null(:rating_status), description: "like, dislike, discard")
      arg(:id, non_null(:integer), description: "Unique identifier of the Answer.")

      resolve(
        fn _parent,
           %{id: answer_id} = args,
           %{context: %{current_user: %{id: user_id}}} = resolution ->
          status = Map.get(args, :status)

          Ratings.AnswerRating
          |> Repo.get_by(%{answer_id: answer_id, user_id: user_id})
          |> (fn
                %{status: ^status} ->
                  {:middleware, Middlewares.Skip, [new_value: Source.get_answer!(answer_id)]}

                result ->
                  result
                  |> case do
                    nil -> %Ratings.AnswerRating{answer_id: answer_id, user_id: user_id}
                    answer_rating -> Ratings.delete_answer_rating(answer_rating)
                  end
                  |> Resolvers.Mutation.answer_rating(args, resolution)
              end).()
        end
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Rating)

      middleware(
        Notifications.Absinthe.Middleware,
        source: fn %{context: %{notification_source: %{__meta__: %{state: :loaded}} = source}} ->
          source
        end,
        push_to: [:account, :notifications],
        list_to_publish: fn %{
                              value: %Source.Answer{} = answer,
                              context: %{current_user: %{id: user_id}}
                            } ->
          List.delete([answer.user_id], user_id)
        end
      )
    end

    @desc "Change rating for Comment"
    field :comment_rating, :comment do
      middleware(Middlewares.Authentication)
      arg(:status, non_null(:rating_status), description: "like, dislike, discard")
      arg(:id, non_null(:integer), description: "Unique identifier of the Comment.")

      resolve(
        fn _parent,
           %{id: comment_id} = args,
           %{context: %{current_user: %{id: user_id}}} = resolution ->
          status = Map.get(args, :status)

          Ratings.CommentRating
          |> Repo.get_by(%{comment_id: comment_id, user_id: user_id})
          |> (fn
                %{status: ^status} ->
                  {:middleware, Middlewares.Skip, [new_value: Source.get_comment!(comment_id)]}

                result ->
                  result
                  |> case do
                    nil -> %Ratings.CommentRating{comment_id: comment_id, user_id: user_id}
                    comment_rating -> Ratings.delete_comment_rating(comment_rating)
                  end
                  |> Resolvers.Mutation.comment_rating(args, resolution)
              end).()
        end
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )

      middleware(Middlewares.Rating)

      middleware(
        Notifications.Absinthe.Middleware,
        source: fn %{context: %{notification_source: %{__meta__: %{state: :loaded}} = source}} ->
          source
        end,
        push_to: [:account, :notifications],
        list_to_publish: fn %{
                              value: %Source.Comment{} = comment,
                              context: %{current_user: %{id: user_id}}
                            } ->
          List.delete([comment.user_id], user_id)
        end
      )
    end

    @desc "Add subscribe user in MeTo for Content, (user token is required)"
    field :me_to, :contents_union do
      middleware(Middlewares.Authentication)

      arg(
        :status,
        non_null(:boolean),
        description: "True -> record exists, False -> record does not exist"
      )

      arg(:content_id, non_null(:integer), description: "Unique identifier of the Content")

      resolve(
        (&Resolvers.Mutation.me_to/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end

    @desc "Create exspert request."
    field :create_expert_request, :user do
      middleware(Middlewares.Authentication)
      arg(:tags, list_of(:string), description: "Tag which is submitted for confirmation.")

      resolve(
        (&Resolvers.Mutation.create_expert_request/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end

    @desc "Add Office"
    field :office_add, :office do
      middleware(Middlewares.Authentication)
      arg(:title, non_null(:string), description: "Title office/location.")
      arg(:location, non_null(:string), description: "Location office.")
      arg(:email, :string, description: "Email office/location.")
      arg(:country, :string, description: "Country office/location.")
      arg(:avatar, :string, description: "Avatar office/location.")

      resolve(
        (&Resolvers.Mutation.add_office/3)
        |> KnowledgePhoenix.Resolver.permission([
          &KnowledgePhoenix.Permission.is_active_account/3
        ])
      )
    end
  end
end
