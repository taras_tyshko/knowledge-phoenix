defmodule KnowledgePhoenixWeb.Schema.UserTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers
  alias KnowledgePhoenixWeb.Middlewares

  object :user_queries do
    @desc "Get a list of experts on defined or search experts, with the cursor of a pagination."
    field :experts, :user_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:input, :string, description: "Search user by word. Where word is user nickname.")
      arg(:tags, list_of(:string), description: "List of preset tags to get experts.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.User.find_experts/3)
    end

    @desc "Get a list of experts on defined or search tags, with the cursor of a pagination."
    field :tag_experts, :tag_experts do
      arg(:input, :string, description: "Search Tag by word.")
      arg(:tags, list_of(:string), description: "List of preset tags to get experts.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.User.search_experts_nickname/3)
    end

    @desc "Get a list of users on search user, with the cursor of a pagination"
    field :users, list_of(:user) do
      arg(:input, :string, description: "Search user by word. Where word is user nickname")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.User.find_users/3)
    end

    @desc "Implements search users according to specific arguments."
    field :users_autocomplete, list_of(:user) do
      arg(:input, :string, description: "Search user by word. Where word is user nickname")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.User.users_autocomplete/3)
    end

    @desc "Implements search experts according to specific arguments."
    field :experts_autocomplete, list_of(:user) do
      arg(:input, :string, description: "Search experts by word. Where word is user nickname")
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(&Resolvers.User.experts_autocomplete/3)
    end

    @desc "Check nickname for availability in the database. If existent -> True or not existent -> False."
    field :check_nickname, :boolean do
      middleware(Middlewares.Authentication)
      arg(:nickname, :string, description: "The specified nickname for verification.")

      resolve(&Resolvers.User.check_nickname/3)
    end
  end

  @desc "Implements the field structure for expert data expansion."
  object :tag_experts do
    field(:exists_experts, list_of(:user),
      description: "The list of users in compliance with the specified parameters."
    )

    field(:anothers_experts, list_of(:user),
      description:
        "The list of all users who do not match the field tags and match the field input."
    )
  end
end
