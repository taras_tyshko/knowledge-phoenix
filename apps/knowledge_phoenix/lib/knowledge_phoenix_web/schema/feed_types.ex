defmodule KnowledgePhoenixWeb.Schema.FeedTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers
  alias KnowledgePhoenixWeb.Middlewares

  object :feed_queries do
    @desc "Get feed data (list of contents ans answers)."
    field :feed, :feed_paginator do
      middleware(Middlewares.Authentication)
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:nicknames, list_of(:string), description: "List of user nicknames.")
      arg(:tags, list_of(:string), description: "List of content or answer tags.")

      resolve(&Resolvers.Feed.list_feed/3)
    end
  end
end
