defmodule KnowledgePhoenixWeb.Schema.Enum do
  @moduledoc false

  use Absinthe.Schema.Notation

  enum :contents_types do
    description "Content types parameter"

    value :question, as: "question", description: "Content type: :question"
    value :complaint, as: "complaint", description: "Content type: :complaint"
  end

  enum :contents_order_by do
    description "Contents `order by` parameter"

    value :newest, as: :inserted_at, description: "Order by inserted at"
    value :trending, as: :answers_count, description: "Order by answers count"
    value :most_popular, as: :reviews_count, description: "Order by reviews count"
  end

  enum :answers_order_by do
    description "Answers `order by` parameter"

    value :newest, as: :inserted_at, description: "Order by inserted at"
    value :trending, as: :comments_count, description: "Order by comments_count"
    value :most_popular, as: :ratings_count, description: "Order by answer rating"
  end

  enum :comments_order_by do
    description "Comments `order by` parameter"

    value :newest, as: :inserted_at, description: "Order by inserted at"
    value :most_popular, as: :ratings_count, description: "Order by comment rating"
  end

  enum :rating_status do
    description "Ratings `status` parameter"

    value :like, as: true, description: "Status true - like"
    value :dislike, as: false, description: "Status false - dislike"
    value :discard, as: nil, description: "Status nil - discard any rating"
  end

  enum :subscription_status do
    description "Subscription `status` parameter"

    value :subscribe, as: true, description: "Status true - subscribe"
    value :unsubscribe, as: false, description: "Status false - unsubscribe"
  end
end
