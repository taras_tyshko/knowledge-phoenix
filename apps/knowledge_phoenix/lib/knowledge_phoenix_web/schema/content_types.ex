defmodule KnowledgePhoenixWeb.Schema.ContentTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers

  @desc "Realizes the receipt of content of data such as: Contents, Idea, Complain of Answers and Comments."
  object :content_queries do
    @desc "Getting a content through a unique identifier ID"
    field :content, :contents_union do
      arg(:id, non_null(:integer), description: "Unique identifier content ID.")

      resolve(&Resolvers.Content.get_content/3)
    end

    @desc "Get list of contents"
    field :contents, :content_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:input, :string, description: "Search contents by field title.")
      arg(:tags, list_of(:string), description: "Submit tags to sort contents on them.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:type, :contents_types, description: "Content type to return.")

      arg(:nicknames, list_of(:string),
        description: "Submit user nicknames to sort contents on them."
      )

      arg(:order_by, non_null(:contents_order_by),
        description: "This option sets the contents to sort."
      )

      resolve(&Resolvers.Content.list_contents/3)
    end

    @desc "Get list of contents"
    field :office_contents, :content_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:office, non_null(:string), description: "Unique identifier Office ID.")
      arg(:type, :contents_types, description: "Content type to return.")

      arg(:order_by, non_null(:contents_order_by),
        description: "This option sets the contents to sort."
      )

      resolve(&Resolvers.Content.office_contents/3)
    end

    @desc "Get list of related contents"
    field :related_contents, :content_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:content_id, non_null(:integer), description: "Content ID to search related contents.")

      resolve(&Resolvers.Content.list_related_contents/3)
    end

    @desc "Get list of answers"
    field :answers, :answer_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")

      arg(:answer_id, :integer,
        description: "To get the desired answer first you need to pass its ID."
      )

      arg(:tags, list_of(:string), description: "Submit tags to sort answer on them.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:content_id, non_null(:integer), description: "Content ID to the current answer.")

      arg(:nicknames, list_of(:string),
        description: "Submit user nicknames to sort answer on them."
      )

      arg(:order_by, non_null(:answers_order_by),
        description: "This option sets the answer to sort."
      )

      resolve(&Resolvers.Content.list_answers/3)
    end

    @desc "Get list of comments"
    field :comments, :comment_paginator do
      arg(:after, :string, description: "Fetch the records after this cursor.")
      arg(:comment_id, :integer, description: "Comment ID to the current comment.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")
      arg(:answer_id, :integer, description: "Answer ID to the current comment.")
      arg(:content_id, :integer, description: "Content ID to the current comment.")

      arg(:order_by, non_null(:comments_order_by),
        description: "This option sets the comment to sort."
      )

      resolve(&Resolvers.Content.list_comments/3)
    end

    @desc "Implements search contents by name according to specific arguments."
    field :contents_autocomplete, list_of(:contents_union) do
      arg(:input, :string, description: "Search contents by field title.")
      arg(:limit, :integer, description: "Number of results to return.")
      arg(:type, non_null(:contents_types), description: "Content type to return.")

      resolve(&Resolvers.Content.contents_autocomplete/3)
    end

    @desc "Getting a office through a unique identifier ID"
    field :office, :office do
      arg(:id, non_null(:string), description: "Unique identifier office ID.")

      resolve(&Resolvers.Content.get_office/3)
    end

    @desc "Check a office through a unique identifier ID"
    field :check_office, :boolean do
      arg(:title, non_null(:string), description: "Title identifier office.")

      resolve(&Resolvers.Content.check_office/3)
    end

    @desc "Implements search offices by name according to specific arguments."
    field :offices, list_of(:office) do
      arg(:input, :string, description: "Search offices by field title.")
      arg(:limit, non_null(:integer), description: "Number of results to return.")

      resolve(&Resolvers.Content.offices/3)
    end
  end
end
