defmodule KnowledgePhoenixWeb.Schema.ScopeTypes do
  @moduledoc false

  use Absinthe.Schema.Notation

  alias KnowledgePhoenixWeb.Resolvers

  object :scoped_queries do
    field :contents_scoped_data, :scoped_data do
      arg(:input, :string, description: "Search contents by field title.")
      arg(:tags, list_of(:string), description: "List of contents tags.")

      resolve(&Resolvers.Content.scoped_data/3)
    end

    field :feed_scoped_data, :scoped_data do
      arg(:tags, list_of(:string), description: "List of feed tags.")

      resolve(&Resolvers.Feed.scoped_data/3)
    end
  end

  object :scoped_data do
    @desc "Get a list of tags."
    field :tags, list_of(:string) do
      arg(:limit, :integer, description: "Number of results to return.")

      resolve(fn
        parent, %{limit: limit}, _ ->
          {:ok, Enum.slice(parent.tags, 1..limit)}
        parent, _, _ ->
          {:ok, parent.tags}
      end)
    end
  end
end
