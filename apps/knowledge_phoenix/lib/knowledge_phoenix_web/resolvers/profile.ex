defmodule KnowledgePhoenixWeb.Resolvers.Profile do
  @moduledoc """
    A module for the release of resolvers Profile which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.{Accounts, Source}
  alias ElasticSearch.Paginator.Metadata

  @doc """
    Functions to process query get list contents for user profile with the pagination.

  ## Examples

      def list_contents(parent, args, %{context: %{total_count: total_count}}) do
        %{entries: entries, metadata: metadata} =
          Source.Content
          |> where([c], c.nickname == ^parent.nickname)
          |> order_by(desc: ^order_by, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [order_by, :id],
            limit: args.limit,
            include_total_count: total_count
          )
      end
  """
  @spec list_contents(
          Accounts.User.t(),
          %{limit: Integer.t(), after: String.t(), order_by: String.t(), tags: list(String.t())},
          map()
        ) :: {:ok, %{entries: list(Source.Content.t()), metadata: Metadata.t()}}
  def list_contents(
        parent,
        %{limit: limit, after: after_cursor, order_by: order_by, tags: tags, type: type},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                filter do
                  term("type", type)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        parent,
        %{limit: limit, after: after_cursor, order_by: order_by, type: type},
        %{
          context: %{current_user: user}
        }
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                filter do
                  term("type", type)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get list answers for user profile with the pagination.

  ## Examples

      def list_answers(parent, args, %{context: %{total_count: total_count}}) do
        %{entries: entries, metadata: metadata} =
          Source.Answer
          |> where([a], a.nickname == ^parent.nickname)
          |> order_by(desc: ^order_by, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [order_by, :id],
            limit: args.limit,
            include_total_count: total_count
          )
      end
  """
  @spec list_answers(
          Accounts.User.t(),
          %{limit: Integer.t(), after: String.t(), order_by: String.t(), tags: list(String.t())},
          map()
        ) :: {:ok, %{entries: list(Source.Answer.t()), metadata: Metadata.t()}}
  def list_answers(
        parent,
        %{limit: limit, after: after_cursor, order_by: order_by, tags: tags},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_answers(parent, %{limit: limit, after: after_cursor, order_by: order_by}, %{
        context: %{current_user: user}
      }) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get list comments for user profile with the pagination.

  ## Examples

      def list_comments(parent, args, %{context: %{total_count: total_count}}) do
        %{entries: entries, metadata: metadata} =
          Source.Comment
          |> where([c], c.nickname == ^parent.nickname)
          |> order_by([c], desc: :inserted_at, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [:inserted_at, :id],
            limit: args.limit,
            include_total_count: total_count
          )
      end
  """
  @spec list_comments(
          Accounts.User.t(),
          %{limit: Integer.t(), after: String.t(), order_by: String.t(), tags: list(String.t())},
          map()
        ) :: {:ok, %{entries: list(Source.Comment.t()), metadata: Metadata.t()}}
  def list_comments(
        parent,
        %{limit: limit, after: after_cursor, tags: tags, order_by: order_by},
        %{
          context: %{current_user: user}
        }
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_comments(parent, %{limit: limit, after: after_cursor, order_by: order_by}, %{
        context: %{current_user: user}
      }) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("author.keyword", parent.nickname)
                end

                must_not do
                  term("shadow", true)
                end
              end
            end

            must_not do
              term("anonymous", true)
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user do
          pop_in(query[:search][:query][:bool][:must_not])
          |> elem(1)
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end
end
