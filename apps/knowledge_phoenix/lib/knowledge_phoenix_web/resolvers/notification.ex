defmodule KnowledgePhoenixWeb.Resolvers.Notification do
  @moduledoc """
    A module for the release of resolvers Notification which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias SmileDB.Notifications.Notification

  def list_notifications(_parent, %{after: after_cursor, limit: limit}, %{context: %{current_user: user}}) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Notification).index do
        query do
          bool do
            must do
              term("user_id", user.id)
            end

            filter do
              term("anonymous", false)
            end

            must_not do
              term("shadow", true)
            end
          end
        end

        sort do
          [inserted_at: :desc, _id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
         limit: limit,
         cursor_fields: [:inserted_at, :_id],
         after: after_cursor
      )

    {:ok,
      %{
        entries: entries,
        metadata: metadata
      }}
  end
end
