defmodule KnowledgePhoenixWeb.Resolvers.User do
  @moduledoc """
    The module allows to work with users entity. Provides renders user by id,
    returns a list of experts, them filtering by tags, search and autocomplete.

    In addition, the module provides api for the user profile, such as:
    updates information about user, uploads and imports user avatar.

    Search and autocomplete implemented by using `ElasticSearch`. We use `Tirexs`
    module that provides many features for work with `ElasticSearch`. See the
    [`Tirexs`](https://hexdocs.pm/tirexs/readme.html) docs for more details.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]

  alias SmileDB.Accounts
  alias SmileDB.Subscriptions

  @doc """
  Returns a list of users.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def find_users(_parent, %{input: input, after: after_cursor, limit: limit}, _resolution) do
        query =
          search index: "users" do
            query do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            sort do
              [contents_count: :desc, id: :desc]
            end
          end

        %{entries: entries, metadata: metadata} =
          ElasticSearch.Repo.paginate(
            query,
            limit: limit,
            cursor_fields: [:contents_count, :id],
            after: after_cursor
          )

        users = Enum.map(entries, &Accounts.get_user!(&1.id))

        {:ok, %{
          entries: users,
          metadata: metadata
        }}
      end
  """
  @spec find_users(map(), %{input: String.t(), limit: Integer.t()}, map()) ::
          {:ok, list(Accounts.User.t())}
  def find_users(_parent, %{input: input, limit: limit}, %{context: %{current_user: user}}) when not is_nil(user) do
    followers_id = Subscriptions.list_users_subscription(user.id)

    sort_script = [
        type: "number",
        order: "desc",
        script: [
          lang: "painless",
          source: """
            def array = [#{Enum.join(followers_id, ", ")}];
            int sort_rating = 0;

            if (array.contains((int)doc['id'].value)) {
              sort_rating += 999999 + doc['contents_count'].value;
            } else {
              sort_rating += doc['contents_count'].value;
            }

            return sort_rating;
          """
        ]
      ]

    followers =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              terms("id", followers_id)
            end
          end
        end

        sort do
        end
      end
      |> put_in([:search, :sort], [_script: sort_script])
      |> ElasticSearch.Repo.all(limit)

    data =
      with limit <- limit - Enum.count(followers),
           true <- limit > 0 do
        experts =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2",
                    ],
                    type: "phrase_prefix"
                  )
                end

                must_not do
                  terms("id", [user.id | Enum.map(followers, & &1.id)])
                end

                filter do
                  exists("expert")
                end
              end
            end

            sort do
              [contents_count: :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        data =
          with limit <- limit - Enum.count(experts),
               true <- limit > 0 do
            data =
              search index: ElasticSearch.get_meta!(Accounts.User).index do
                query do
                  bool do
                    must do
                      bool do
                        must do
                          multi_match(
                            input,
                            [
                              "name_search^3",
                              "nickname_search^2",
                            ],
                            type: "phrase_prefix"
                          )
                        end

                        must_not do
                          terms("id", Enum.concat(
                            [user.id | Enum.map(experts, & &1.id)],
                            Enum.map(followers, & &1.id)
                          ))
                        end
                      end
                    end

                    must_not do
                      exists("expert")
                    end
                  end
                end

                sort do
                  [contents_count: :desc]
                end
              end
              |> ElasticSearch.Repo.all(limit)

            List.flatten(experts, data)
          else
            false -> experts
          end

        List.flatten(followers, data)
      else
        false -> followers
      end

    {:ok, data}
  end

  def find_users(_parent, %{input: input, limit: limit}, _resolution) do
    experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2",
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    data =
      with limit <- limit - Enum.count(experts),
           true <- limit > 0 do
        data =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  bool do
                    must do
                      multi_match(
                        input,
                        [
                          "name_search^3",
                          "nickname_search^2",
                        ],
                        type: "phrase_prefix"
                      )
                    end

                    must_not do
                      terms("id", Enum.map(experts, & &1.id))
                    end
                  end
                end

                must_not do
                  exists("expert")
                end
              end
            end

            sort do
              [contents_count: :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        List.flatten(experts, data)
      else
        false -> experts
      end

    {:ok, data}
  end

  def find_users(_parent, %{limit: limit}, _resolution) do
    experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          ["name.keyword": :asc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    data =
      with limit <- limit - Enum.count(experts),
           true <- limit > 0 do
        data =
          search index: ElasticSearch.get_meta!(Accounts.User).index do
            query do
              bool do
                must do
                  bool do
                    must do
                      match_all(boost: 1.0)
                    end

                    must_not do
                      terms("id", Enum.map(experts, & &1.id))
                    end
                  end
                end

                must_not do
                  exists("expert")
                end
              end
            end

            sort do
              ["name.keyword": :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        List.flatten(experts, data)
      else
        false -> experts
      end

    {:ok, data}
  end

  @doc """
  Returns a list of experts.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def find_experts(_parent, %{after: after_cursor, limit: limit}, _resolution) do
        %{entries: experts, metadata: metadata} =
          Accounts.User
          |> where([u], fragment("array_length(?, 1) > 0", u.expert))
          |> order_by(desc: :contents_count)
          |> Repo.paginate(
            after: after_cursor,
            cursor_fields: [:contents_count, :id],
            limit: limit
          )
      end
  """
  @spec find_experts(
          map(),
          %{input: String.t(), after: String.t(), limit: Integer.t(), tags: list(String.t())},
          map()
        ) ::
          {:ok,
           %{
             entries: list(Accounts.User.t()),
             metadata: ElasticSearch.Paginator.Metadata.t()
           }}
  def find_experts(
        _parent,
        %{input: input, after: after_cursor, limit: limit, tags: tags},
        _resolution
      ) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2",
                      "tags^1"
                    ],
                    type: "phrase_prefix"
                  )
                end

                filter do
                  terms("expert", tags)
                end
              end
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def find_experts(_parent, %{input: input, after: after_cursor, limit: limit}, _resolution) do
    query =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2",
                  "tags^1"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        query,
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def find_experts(_parent, %{after: after_cursor, limit: limit}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:contents_count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def find_experts(_parent, %{tags: tags}, _resolution) do
    experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              terms("expert", tags)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.all()

    {:ok,
     %{
       entries: experts,
       metadata: %{}
     }}
  end

  def find_experts(_parent, %{limit: limit}, _resolution) do
    experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok,
     %{
       entries: experts,
       metadata: %{}
     }}
  end

  def find_experts(_parent, _args, _resolution) do
    experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end

        sort do
          [contents_count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.all()

    {:ok,
     %{
       entries: experts,
       metadata: %{}
     }}
  end

  @doc """
  Returns a list of experts through autocomplete.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def experts_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        query =
          search index: "users" do
            size(limit)

            query do
              bool do
                must do
                  match(
                    "title",
                    input,
                    fuzziness: "AUTO",
                    operator: "and"
                  )
                end

                filter do
                  term("expert", true)
                end
              end
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            titles =
              Enum.map(result.hits.hits, fn source ->
                source.title
              end)

            {:ok, Enum.uniq(titles)}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """
  @spec experts_autocomplete(map(), %{input: String.t(), limit: Integer.t()}, map()) ::
          {:ok, list(Accounts.User.t())}
  def experts_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        suggest do
          user_suggest do
            prefix(input, [])

            completion do
              field("name_autocomplete", [])
              fuzzy(:fuzziness, "AUTO")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)
      |> Keyword.get(:user_suggest) || []
      |> Enum.filter(&(!Enum.empty?(&1.expert)))

    {:ok, suggest}
  end

  @doc """
  Returns a list of users through autocomplete.

  Provides search the structure of users from `ElasticSearch`.

  ## Examples

      def users_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        query =
          search [index: "users"] do
            size(limit)

            query do
              bool do
                must do
                  match(
                    "title",
                    input,
                    fuzziness: "AUTO",
                    operator: "and"
                  )
                end
              end
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            titles =
              Enum.map(result.hits.hits, fn source ->
                %{
                  id: source.id,
                  name: source.title,
                  avatar: source.avatar,
                  nickname: source.nickname
                }
              end)

            {:ok, Enum.uniq(titles)}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """

  @spec users_autocomplete(map(), %{input: String.t(), limit: Integer.t()}, map()) ::
          {:ok, list(Accounts.User.t())}
  def users_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        suggest do
          user_suggest do
            prefix(input, [])

            completion do
              field("name_autocomplete", [])
              fuzzy(:fuzziness, "AUTO")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)
      |> Keyword.get(:user_suggest) || []

    {:ok, suggest}
  end

  @doc """
  Checking the imposing nickname in database .

  ## Examples

      def check_nickname(_parent, %{nickname: nickname}, _resolution) do
        try do
          Accounts.get_user_by_nickname!(nickname)
        rescue
          Ecto.NoResultsError ->
            {:ok, false}
        else
          _ ->
            {:ok, true}
        end
      end
  """
  @spec check_nickname(map(), %{nickname: String.t()}, map()) :: {:ok, boolean}
  def check_nickname(_parent, %{nickname: nickname}, _resolution) do
    {:ok, if(Accounts.get_user_by_nickname(nickname), do: true, else: false)}
  end

  @doc """
  Checking the anonumous of user .

  ## Examples

      def check_anonymous(parent, args, resolution) do
        parent =
          parent.anonymous
          |> if(do: Map.put(parent, :user_id, nil), else: parent)

        dataloader(Accounts).(parent, args, resolution)
      end
  """
  @spec check_anonymous(map(), map(), map()) :: {:ok, Accounts.User.t()}
  def check_anonymous(parent, args, resolution) do
    parent = get_parent(parent, resolution.context)
    dataloader(Accounts).(parent, args, resolution)
  end

  defp get_parent(parent, %{current_user: user}) do
    (!parent.anonymous || (parent.anonymous && user && (user.id == parent.user_id)))
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end

  defp get_parent(parent, %{}) do
    (!parent.anonymous || parent.anonymous)
    |> if(do: parent, else: Map.put(parent, :user_id, nil))
  end

  @doc """
  Returns a list of experts through autocomplete.

  Provides search the structure of users is nickname from `ElasticSearch`.

  ## Examples

      def search_experts_nickname(_parent, %{input: input}, _resolution) do
        query =
          Tirexs.Search.search [index: "users"] do
            size(10000)

            query do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2"
                    ],
                    type: "phrase_prefix"
                  )
                end
              end
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            all_experts =
              Enum.map(result.hits.hits, fn source ->
                %{
                  nickname: source.nickname_search,
                  name: source.name_search,
                  avatar: source.avatar
                }
              end)

            {:ok,
            %{
              exists_experts: [],
              anothers_experts: all_experts
            }}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """
  @spec search_experts_nickname(
          map(),
          %{tags: list(String.t()), input: String.t(), limit: Integer.t()},
          map()
        ) ::
          {:ok,
           %{
             exists_experts: list(Accounts.User.t()),
             anothers_experts: list(Accounts.User.t())
           }}
  def search_experts_nickname(_parent, %{tags: tags, input: input, limit: limit}, _resolution) do
    exists_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              bool do
                must do
                  multi_match(
                    input,
                    [
                      "name_search^3",
                      "nickname_search^2"
                    ],
                    type: "phrase_prefix"
                  )
                end

                filter do
                  terms("expert", tags)
                end
              end
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    all_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            must_not do
              terms("id", Enum.map(exists_experts, & &1.id))
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok,
     %{
       exists_experts: exists_experts,
       anothers_experts: all_experts
     }}
  end

  def search_experts_nickname(_parent, %{input: input, limit: limit}, _resolution) do
    all_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              multi_match(
                input,
                [
                  "name_search^3",
                  "nickname_search^2"
                ],
                type: "phrase_prefix"
              )
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok,
     %{
       exists_experts: [],
       anothers_experts: all_experts
     }}
  end

  def search_experts_nickname(_parent, %{tags: tags, limit: limit}, _resolution) do
    exists_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              bool do
                must do
                  match_all(boost: 1.0)
                end

                filter do
                  terms("expert", tags)
                end
              end
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    all_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            must_not do
              terms("id", Enum.map(exists_experts, & &1.id))
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok,
     %{
       exists_experts: exists_experts,
       anothers_experts: all_experts
     }}
  end

  def search_experts_nickname(_parent, %{limit: limit}, _resolution) do
    exists_experts =
      search index: ElasticSearch.get_meta!(Accounts.User).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              exists("expert")
            end
          end
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok,
     %{
       exists_experts: [],
       anothers_experts: exists_experts
     }}
  end
end
