defmodule KnowledgePhoenixWeb.Resolvers.Tag do
  @moduledoc """
    The module allows to work with tags model.
    Provides renders, searching and autocomplete for contents tags.

    Search and autocomplete implemented by using `ElasticSearch`. We use `Tirexs`
    module that provides many features for work with `ElasticSearch`. See the
    [`Tirexs`](https://hexdocs.pm/tirexs/readme.html) docs for more details.
  """

  import Ecto.Query, warn: false
  import Tirexs.Search

  alias SmileDB.Tags.Tag
  alias ElasticSearch.Paginator.Metadata

  @doc """
  Returns a list of tags and their count.

  Provides search and receives tags from `ElasticSearch`.

  By default, this method sorts by the total number of tags.
  It also supports the paging of the sample result.

  ## URL

    /api-v1/tags

  ## Parameters

    - after: HASH paginations
    - limit: Number of contents to offsets the number of rows selected from the result
    - input: Entered data for search

  ## Examples

      def list_tags(_parent, args, %{context: %{total_count: total_count}}) do
        %{entries: entries, metadata: metadata} =
          Tag
          |> where([t], t.description == "content")
          |> where([t], t.count > 0)
          |> order_by([t], desc: t.count, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [:count, :id],
            limit: args.limit,
            include_total_count: total_count
          )
      end

  ## Examples (search)

      def list_tags(_parent, %{limit: limit, after: after_cursor, input: input}, _resolution) do
        query =
          search [index: "tags"] do
            query do
              prefix("title_search", input)
            end

            sort do
              [count: :desc, id: :desc]
            end
          end

        %{entries: entries, metadata: metadata} =
          ElasticSearch.Repo.paginate(query, limit: limit, cursor_fields: [:count, :id], after: after_cursor)

        tags =
          entries
          |> Enum.map(fn tag ->
            %{
              title: tag.title,
              count: tag.count
            }
          end)

        {:ok, %{
          entries: tags,
          metadata: metadata
        }}
      end
  """
  @spec list_tags(
          map(),
          %{limit: Integer.t(), after: String.t(), input: String.t()},
          map()
        ) :: {:ok, %{entries: list(Tag.t()), metadata: Metadata.t()}}
  def list_tags(_parent, %{limit: limit, after: after_cursor, input: input}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Tag).index do
        query do
          bool do
            must do
              prefix("title_search", input)
            end

            filter do
              range("count", gt: 0)
            end
          end
        end

        sort do
          [count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_tags(_parent, %{limit: limit, after: after_cursor}, _resolution) do
    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Tag).index do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            filter do
              range("count", gt: 0)
            end
          end
        end

        sort do
          [count: :desc, id: :desc]
        end
      end
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:count, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
  Returns a list of tags title for autocomplete.

  ## Examples

      def tags_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        query =
          Tirexs.Search.search [index: "tags"] do
            size(limit)

            query do
              match(
                "title",
                input,
                fuzziness: "AUTO",
                operator: "and"
              )
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            titles = Enum.map(result.hits.hits, & &1.title)

            {:ok, Enum.uniq(titles)}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """
  @spec tags_autocomplete(map(), %{limit: Integer.t(), input: String.t()}, map()) ::
          {:ok, list(String.t())}
  def tags_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Tag).index do
        suggest do
          tag_suggest do
            prefix(input, [])

            completion do
              field("title_autocomplete", [])
              fuzzy(:fuzziness, "AUTO")
            end
          end
        end

        sort do
          [count: :desc]
        end
      end
      |> put_in(
        [:search, :suggest, :tag_suggest, :completion, :fuzzy, :prefix_length],
        String.length(input)
      )
      |> ElasticSearch.Repo.all(limit)
      |> Keyword.get(:tag_suggest, [])
      |> Enum.filter(&(&1.count > 0)) || []

    {:ok,
     suggest
     |> Enum.map(& &1.title)
     |> Enum.uniq()}
  end
end
