defmodule KnowledgePhoenixWeb.Resolvers.Feed do
  @moduledoc """
    A module for the release of resolvers Feed which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias ElasticSearch.Paginator.Metadata
  alias SmileDB.{Source, Contents, Subscriptions}

  @doc """
    Functions to process query get list contents and answers with the pagination.

  ## Examples

      def list_feed(_parent, args, %{context: %{total_count: total_count, current_user: user}}) do
        %{entries: entries, metadata: metadata} =
          Source.Feed
          |> where([f], f.nickname != ^user.nickname)
          |> where([f], f.anonymous == false)
          |> order_by([f], desc: :inserted_at, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [:inserted_at, :id],
            limit: args.limit,
            include_total_count: total_count
          )
      end
  """
  @spec list_feed(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            tags: list(String.t()),
            nicknames: list(String.t())
          },
          map()
        ) :: {:ok, %{entries: list(Source.Feed.t()), metadata: Metadata.t()}}
  def list_feed(
        _parent,
        %{limit: limit, after: after_cursor, tags: tags_filter, nicknames: _nicknames_filter},
        %{context: %{current_user: user}}
      ) do
    content_ids =
      List.flatten(
        Subscriptions.list_contents_subscription(user.id),
        Contents.list_contents_id_from_me_to(user)
      )

    user_ids = Subscriptions.list_users_subscription(user.id)
    tags = Subscriptions.list_tags_subscription(user.id)

    %{entries: entries, metadata: metadata} =
      search index: ElasticSearch.get_meta!(Source.Feed).index do
        query do
          bool do
            must do
              bool do
                should do
                  bool do
                    must do
                      terms("user_id", user_ids)
                    end

                    filter do
                      term("anonymous", false)
                    end
                  end
                end

                must_not do
                  term("author", user.nickname)
                end
              end
            end

            must_not do
              term("shadow", true)
            end

            filter do
            end
          end
        end

        sort do
          [inserted_at: :desc, _id: :desc]
        end
      end
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [tags: tags]] | &1]}
      )
      |> elem(1)
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [content_id: content_ids]] | &1]}
      )
      |> elem(1)
      |> put_in(
        [:search, :query, :bool, :filter],
        Enum.map(tags_filter, &[term: [tags: &1]])
      )
      |> ElasticSearch.Repo.paginate(
        limit: limit,
        cursor_fields: [:inserted_at, :_id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get scoped data (users and tags).

  ## Examples

      def scoped_data(_parent, args, _resolution) do
        contents =
          Source.Content
          |> where([q], fragment("? @> ?", q.tags, ^args.tags))
          |> limit(20)
          |> select([q], %{nickname: q.nickname, tags: q.tags})
          |> Repo.all()

        users =
          contents
          |> Enum.map(& &1.nickname)
          |> Enum.uniq()
          |> Enum.map(&Accounts.get_user_by_nickname!(&1))

        tags =
          contents
          |> Enum.map(& &1.tags)
          |> List.flatten()
          |> Enum.filter(&(not Enum.member?(tags, &1)))
          |> Enum.uniq()

        {:ok, %{
          users: users,
          tags: tags
        }}
      end
  """
  @spec scoped_data(map(), %{tags: list(String.t())}, map()) ::
          {:ok,
           %{
             tags: list(String.t())
           }}
  def scoped_data(_parent, %{tags: tags_filter}, %{context: %{current_user: user}})
      when length(tags_filter) > 0 do
    content_ids =
      List.flatten(
        Subscriptions.list_contents_subscription(user.id),
        Contents.list_contents_id_from_me_to(user)
      )

    user_ids = Subscriptions.list_users_subscription(user.id)
    tags = Subscriptions.list_tags_subscription(user.id)

    tags =
      search index: ElasticSearch.get_meta!(Source.Feed).index do
        query do
          bool do
            must do
              bool do
                should do
                  bool do
                    must do
                      terms("user_id", user_ids)
                    end

                    filter do
                      term("anonymous", false)
                    end
                  end
                end

                must_not do
                  term("author", user.nickname)
                end
              end
            end

            must_not do
              term("shadow", true)
            end

            filter do
            end
          end
        end

        sort do
          [inserted_at: :desc, _id: :desc]
        end
      end
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [tags: tags]] | &1]}
      )
      |> elem(1)
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [content_id: content_ids]] | &1]}
      )
      |> elem(1)
      |> put_in(
        [:search, :query, :bool, :filter],
        Enum.map(tags_filter, &[term: [tags: &1]])
      )
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.tags)
      |> List.flatten()
      |> Enum.filter(&(not Enum.member?(tags_filter, &1)))
      |> Enum.uniq()

    {:ok, %{tags: tags}}
  end

  def scoped_data(_parent, _args, %{context: %{current_user: user}}) do
    content_ids =
      List.flatten(
        Subscriptions.list_contents_subscription(user.id),
        Contents.list_contents_id_from_me_to(user)
      )

    user_ids = Subscriptions.list_users_subscription(user.id)
    tags = Subscriptions.list_tags_subscription(user.id)

    tags =
      search index: ElasticSearch.get_meta!(Source.Feed).index do
        query do
          bool do
            must do
              bool do
                should do
                  bool do
                    must do
                      terms("user_id", user_ids)
                    end

                    filter do
                      term("anonymous", false)
                    end
                  end
                end

                must_not do
                  term("author", user.nickname)
                end
              end
            end

            must_not do
              term("shadow", true)
            end

            filter do
            end
          end
        end

        sort do
          [inserted_at: :desc, _id: :desc]
        end
      end
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [tags: tags]] | &1]}
      )
      |> elem(1)
      |> get_and_update_in(
        [:search, :query, :bool, :must, Access.at(0), :bool, :should],
        &{&1, [[terms: [content_id: content_ids]] | &1]}
      )
      |> elem(1)
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.tags)
      |> List.flatten()
      |> Enum.uniq()

    {:ok, %{tags: tags}}
  end
end
