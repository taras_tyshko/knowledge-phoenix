defmodule KnowledgePhoenixWeb.Resolvers.Mutation do
  @moduledoc """
    A module for the release of resolvers Socket which are washed out in the schema.
  """

  import Ecto.Query, warn: false

  alias SmileDB.{
    Repo,
    Tags,
    Users,
    Source,
    Ratings,
    Contents,
    Category
  }

  @doc """
    Functions to process mutation `content_add` and data transfer on a websocket using the `GraphQl` subscription.

  ## Examples

      iex> add_content(_parent, args, resolution)
      {:ok, %Content{}}

      iex> add_content(_parent, args, resolution)
      {:ok, %Ecto.ChangeError{}}
  """
  @spec add_content(map(), map(), %{context: map()}) ::
          {:ok, Source.Content.t()} | {:error, Ecto.ChangeError.t()}
  def add_content(_parent, %{question: args}, %{context: %{current_user: user}}) do
    args
    |> Map.merge(%{
      author: user.nickname,
      reviews: [user.id],
      reviews_count: 1,
      shadow: user.status == "hidden",
      type: "question",
      user_id: user.id
    })
    |> Source.create_content()
  end

  def add_content(_parent, %{complaint: args}, %{context: %{current_user: user}}) do
    args
    |> Map.merge(%{
      author: user.nickname,
      reviews: [user.id],
      reviews_count: 1,
      office_id: args.office,
      shadow: user.status == "hidden",
      type: "complaint",
      tags: [args.office | args.tags],
      permission_tags: [args.office],
      user_id: user.id
    })
    |> Source.create_content()
  end

  @doc """
    Functions to process mutation ` content_edit` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      iex> edit_content(_parent, args, resolution)
      {:ok, %Content{}}

      iex> edit_content(_parent, args, resolution)
      {:ok, %Ecto.ChangeError{}}
  """
  @spec edit_content(map(), map(), %{context: map()}) ::
          {:ok, Source.Content.t()} | {:error, Ecto.ChangeError.t()}
  def edit_content(_parent, %{id: id, complaint: args}, _resolution) do
    id
    |> Source.get_content!()
    |> Source.update_content(args)
  end

  def edit_content(_parent, %{id: id, question: args}, _resolution) do
    id
    |> Source.get_content!()
    |> Source.update_content(args)
  end

  @doc """
    Functions to process mutation `content_delete` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_content(_parent, args, _resolution) do
        args.id
        |> Source.get_content!()
        |> Source.delete_content()
      end
  """
  @spec delete_content(map(), map(), map()) ::
          {:ok, Source.Content.t()} | {:error, Ecto.ChangeError}
  def delete_content(_parent, args, _resolution) do
    args.id
    |> Source.get_content!()
    |> Source.delete_content()
  end

  @doc """
    Functions to process mutation `answer_add` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_answer(_parent, args, _resolution) do
        Source.create_answer(args)
      end
  """
  @spec add_answer(map(), map(), %{context: map()}) ::
          {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError}
  def add_answer(_parent, args, %{context: %{current_user: user}}) do
    args
    |> Map.put(:shadow, user.status == "hidden")
    |> Map.put(:user_id, user.id)
    |> Map.put(:author, user.nickname)
    |> Source.create_answer()
  end

  @doc """
    Functions to process mutation `answer_edit` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def edit_answer(_parent, args, _resolution) do
        args.id
        |> Source.get_answer!()
        |> Source.update_answer(args)
      end
  """
  @spec edit_answer(map(), map(), %{context: map()}) ::
          {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError}
  def edit_answer(_parent, args, %{context: %{current_user: _user}}) do
    args.id
    |> Source.get_answer!()
    |> Source.update_answer(args)
  end

  @doc """
    Functions to process mutation `answer_delete` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_answer(_parent, args, _resolution) do
        args.id
        |> Source.get_answer!()
        |> Source.delete_answer()
      end
  """
  @spec delete_answer(map(), map(), %{context: map()}) ::
          {:ok, Source.Answer.t()} | {:error, Ecto.ChangeError}
  def delete_answer(_parent, args, %{context: %{current_user: _user}}) do
    args.id
    |> Source.get_answer!()
    |> Source.delete_answer()
  end

  @doc """
    Functions to process mutation `comment_add` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_comment(_parent, args, _resolution) do
        Source.create_comment(args)
      end
  """
  @spec add_comment(map(), map(), %{context: map()}) ::
          {:ok, Source.Comment.t()} | {:error, Ecto.ChangeError}
  def add_comment(_parent, args, %{context: %{current_user: user}}) do
    args
    |> Map.put(:shadow, user.status == "hidden")
    |> Map.put(:user_id, user.id)
    |> Map.put(:author, user.nickname)
    |> Source.create_comment()
  end

  @doc """
    Functions to process mutation `comment_edit` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def edit_comment(_parent, args, _resolution) do
        args.id
        |> Source.get_comment!()
        |> Source.update_comment(args)
      end
  """
  @spec edit_comment(map(), map(), %{context: map()}) ::
          {:ok, Source.Comment.t()} | {:error, Ecto.ChangeError}
  def edit_comment(_parent, args, %{context: %{current_user: _user}}) do
    args.id
    |> Source.get_comment!()
    |> Source.update_comment(args)
  end

  @doc """
    Functions to process mutation `comment_delete` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def delete_comment(_parent, args, _resolution) do
        args.id
        |> Source.get_comment!()
        |> Source.delete_comment()
      end
  """
  @spec delete_comment(map(), map(), %{context: map()}) ::
          {:ok, Source.Comment.t()} | {:error, Ecto.ChangeError}
  def delete_comment(_parent, args, %{context: %{current_user: _user}}) do
    args.id
    |> Source.get_comment!()
    |> Source.delete_comment()
  end

  @doc """
    Functions to process mutation `add_reviews` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_reviews(_parent, args, %{context: %{current_user: user}}) do
        content = Source.get_content!(args.id)

        changset =
          if Enum.member?(content.reviews, user.id) do
            %{reviews: Enum.uniq(content.reviews ++ [user.id])}
          else
            %{
              reviews: Enum.uniq(content.reviews ++ [user.id]),
              reviews_count: content.reviews_count + 1
            }
          end

        case Source.update_content(content, changset) do
          {:ok, content} ->
            {:ok, content}

          {:error, changeset} ->
            {:error, changeset}
        end
      end
  """
  @spec add_reviews(map(), map(), %{context: map()}) ::
          {:ok, Source.Content.t()} | {:error, Ecto.ChangeError}
  def add_reviews(_parent, args, %{context: %{current_user: user}}) do
    content = Source.get_content!(args.id)
    changeset = %{reviews: Enum.uniq([user.id | content.reviews])}

    Source.update_content(
      content,
      Map.put(changeset, :reviews_count, Enum.count(changeset.reviews))
    )
  end

  @doc """
    Functions to process mutation `add_answer_likes` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_answer_likes(_parent, args, %{context: %{current_user: user}}) do
        if not is_nil(args.status) do
          result =
            case Repo.get_by(Ratings.AnswerRating, %{answer_id: args.id, user_id: user.id}) do
              nil ->
                %Ratings.AnswerRating{answer_id: args.id, user_id: user.id}

              answer_rating ->
                answer_rating
            end
            |> Ratings.AnswerRating.changeset(%{status: args.status})
            |> Repo.insert_or_update()
        else
          Ratings.AnswerRating
          |> where([answer_rating], answer_rating.answer_id == ^args.id)
          |> where([answer_rating], answer_rating.user_id == ^user.id)
          |> Repo.delete_all()
        end
      end
  """
  @spec answer_rating(map() | tuple(), map(), %{context: map()}) :: {:ok, Source.Answer.t()}
  def answer_rating(
        {:ok, %Ratings.AnswerRating{} = answer_rating},
        %{status: _} = args,
        resolution
      ) do
    answer_rating
    |> Ecto.put_meta(state: :built)
    |> answer_rating(args, resolution)
  end

  def answer_rating(%Ratings.AnswerRating{} = answer_rating, %{status: status}, _resolution) do
    Ratings.create_answer_rating(answer_rating, %{status: status})
  end

  def answer_rating(%{id: nil}, _args, _resolution) do
    {:error, "Like/dislike does not exist for this answer and user."}
  end

  def answer_rating(answer_rating, _args, _resolution), do: answer_rating

  @doc """
    Functions to process mutation `add_comment_likes` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_comment_likes(_parent, args, %{context: %{current_user: user}}) do
        if not is_nil(args.status) do
          result =
            case Repo.get_by(Ratings.CommentRating, %{comment_id: args.id, user_id: user.id}) do
              nil ->
                %Ratings.CommentRating{comment_id: args.id, user_id: user.id}

              comments_rating ->
                comments_rating
            end
            |> Ratings.CommentRating.changeset(%{status: args.status})
            |> Repo.insert_or_update()
        else
          Ratings.CommentRating
          |> where([comment_rating], comment_rating.comment_id == ^args.id)
          |> where([comment_rating], comment_rating.user_id == ^user.id)
          |> Repo.delete_all()
        end
      end
  """
  @spec comment_rating(map() | tuple(), map(), %{context: map()}) :: {:ok, Source.Comment.t()}
  def comment_rating(
        {:ok, %Ratings.CommentRating{} = comment_rating},
        %{status: _} = args,
        resolution
      ) do
    comment_rating
    |> Ecto.put_meta(state: :built)
    |> comment_rating(args, resolution)
  end

  def comment_rating(%Ratings.CommentRating{} = comment_rating, %{status: status}, _resolution) do
    Ratings.create_comment_rating(comment_rating, %{status: status})
  end

  def comment_rating(%{id: nil}, _args, _resolution) do
    {:error, "Like/dislike does not exist for this comment and user."}
  end

  def comment_rating(comment_rating, _args, _resolution), do: comment_rating

  @doc """
    Functions to process mutation `me_to` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def subscribe_me_to(parent, args, _resolution) do
        Contents.create_user_subscription(%{
          content_id: args.content_id,
          user_id: parent.id
        })
      end
  """
  @spec me_to(map(), %{content_id: Integer.t(), status: boolean}, %{context: map()}) ::
          {:ok, Source.Content.t()}
  def me_to(_parent, %{content_id: content_id, status: status}, %{
        context: %{current_user: user}
      }) do
    if status do
      Contents.Meto
      |> where([m], m.user_id == ^user.id)
      |> where([m], m.content_id == ^content_id)
      |> Repo.one()
      |> Contents.delete_me_to()
    else
      Contents.create_me_to(%{content_id: content_id, user_id: user.id})
    end

    {:ok, Source.get_content!(content_id)}
  end

  @doc """
    Created  [`ExpertRequest`](SmileDB.Users.Expert.html).

  ## Example

      iex> Mutation.create_expert_request(_parent, %{tags: ["test"]}, _resolution)
      {:ok, list(%ExpertRequest{}}

  """
  @spec create_expert_request(map(), %{tags: list(String.t())}, Absinthe.Resolution.t()) ::
          {:ok, Users.Expert.t()} | {:error, Ecto.ChangeError.t()}
  def create_expert_request(_parent, %{tags: tags}, %{context: %{current_user: user}}) do
    tags
    |> Enum.filter(&(!Enum.member?(user.expert, &1)))
    |> Enum.uniq()
    |> Enum.map(fn tag ->
      {:ok, new_tag} = Tags.get_or_insert_tag(Macro.underscore(tag))
      Users.create_expert(%{user_id: user.id, tag: new_tag.title})
    end)

    {:ok, Map.put(user, :d, nil)}
  end

  @doc """
    Functions to process mutation `add_office` and data transfer on a websocket using the `GraphQl` subscription

  ## Examples

      def add_office(_parent, args, _resolution}) do
        Category.create_office(args)
      end
  """
  @spec add_office(map(), map(), %{context: map()}) ::
          {:ok, Category.Office.t()} | {:error, Ecto.ChangeError}
  def add_office(_parent, args, _resolution) do
    args
    |> Map.put(
      :id,
      args.title
      |> String.split()
      |> Enum.map(&Macro.underscore(&1))
      |> Enum.join("_")
    )
    |> Category.create_office()
  end
end
