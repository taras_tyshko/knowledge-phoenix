defmodule KnowledgePhoenixWeb.Resolvers.Accounts do
  @moduledoc """
    A module for the release of resolvers Accounts which are washed out in the schema.
  """

  import Ecto.Query, warn: false

  alias SmileDB.Repo
  alias SmileDB.Auth
  alias SmileDB.Emails
  alias SmileDB.Accounts
  alias SmileDB.Subscriptions
  alias SmileDB.Notifications, as: NotificationContext

  @doc """
    Functions to process query get user data through the nickname .

  ## Examples

      def find_profile_account(_parent, args, _resolution) do
        Accounts.get_user_by_nickname!(args.nickname)
      end
  """
  @spec find_profile_account(map(), %{nickname: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()}
  def find_profile_account(_parent, %{nickname: nickname}, _resolution) do
    {:ok, Accounts.get_user_by_nickname!(nickname)}
  end

  @doc """
    Functions to process query get user data through the token.

  ## Examples

      def find_profile_account(_parent, _args, %{context: %{current_user: user}}) do
        user
      end
  """
  @spec find_profile_account(map(), map(), %{context: %{current_user: map()}}) ::
          {:ok, Accounts.User.t()}
  def find_profile_account(_parent, _args, %{context: %{current_user: user}}) do
    {:ok, user}
  end

  @doc """
    Functions to process query get user data through the parent.

  ## Examples

      def find_user(parent, _args, _resolution) do
        parent
      end
  """
  @spec find_user(Accounts.User.t(), map(), Absinthe.Resolution.t()) :: {:ok, Accounts.User.t()}
  def find_user(parent, _args, _resolution) do
    {:ok, parent}
  end

  @doc """
    Functions to process query get user profile setting through the parent.

  ## Examples

      def get_setting(parent, _args, _resolution) do
        Accounts.get_setting!(parent)
      end
  """
  @spec get_setting(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, Accounts.Setting.t()}
  def get_setting(parent, _args, _resolution) do
    {:ok, Accounts.get_setting!(parent)}
  end

  @doc """
    Functions to process query get user subscribed contents through the parent.

  ## Examples

      def get_subscribed_contents(parent, _args, _resolution) do
        Subscriptions.list_contents_subscription(parent.id)
      end
  """
  @spec get_subscribed_contents(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(Integer.t())}
  def get_subscribed_contents(parent, _args, _resolution) do
    {:ok, Subscriptions.list_contents_subscription(parent.id)}
  end

  @doc """
    Functions to process query get user subscribed offices through the parent.

  ## Examples

      def get_subscribed_offices(parent, _args, _resolution) do
        Subscriptions.list_offices_subscription(parent.id)
      end
  """
  @spec get_subscribed_offices(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(Integer.t())}
  def get_subscribed_offices(parent, _args, _resolution) do
    {:ok, Subscriptions.list_offices_subscription(parent.id)}
  end

  @doc """
    Functions to process query get user subscribed users through the parent.

  ## Examples

      def get_subscribed_users(parent, _args, _resolution) do
        Subscriptions.list_users_subscription(parent.id)
      end
  """
  @spec get_subscribed_users(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(Accounts.User.t())}
  def get_subscribed_users(parent, _args, _resolution) do
    {:ok, Enum.map(Subscriptions.list_users_subscription(parent.id), &Accounts.get_user!(&1))}
  end

  @doc """
    Functions to process query get user subscribed tags through the parent.

  ## Examples

      def get_subscribed_tags(parent, _args, _resolution) do
        Subscriptions.list_tags_subscription(parent.id)
      end
  """
  @spec get_subscribed_tags(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(String.t())}
  def get_subscribed_tags(parent, _args, _resolution) do
    {:ok, Subscriptions.list_tags_subscription(parent.id)}
  end

  @doc """
    Functions to process query get user followers through the parent.

  ## Examples

      def get_followers(parent, _args, _resolution) do
        Subscriptions.list_user_id_users_subscription(parent.id)
      end
  """
  @spec get_followers(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(Integer.t())}
  def get_followers(parent, _args, _resolution) do
    {:ok, Subscriptions.list_user_id_users_subscription(parent.id)}
  end

  @doc """
    Functions to process query get user notifications through the parent.

  ## Examples

      def get_notifications(parent, _args, _resolution) do
        Notifications.list_notifications_by_user_id(parent.id)
      end
  """
  @spec get_notifications(Accounts.User.t(), map(), Absinthe.Resolution.t()) ::
          {:ok, list(Integer.t())}
  def get_notifications(parent, _args, _resolution) do
    {:ok, NotificationContext.list_notifications_by_user_id(parent.id)}
  end

  @doc """
    Functions to process query find users by nicknames.

  ## Examples

      def find_users_by_nicknames(_parent, %{nicknames: nicknames}, _resolution) do
        Accounts.User
        |> where([u], u.nickname in ^nicknames)
        |> Repo.all
      end
  """
  @spec find_users_by_nicknames(map(), %{nicknames: list(String.t())}, Absinthe.Resolution.t()) ::
          {:ok, list(Accounts.User.t())}
  def find_users_by_nicknames(_parent, %{nicknames: nicknames}, _resolution) do
    {:ok,
     Accounts.User
     |> where([u], u.nickname in ^nicknames)
     |> Repo.all()}
  end

  @doc """
    Functions to process mutation update user setting through the parent.

  ## Examples

      def update_user_setting(parent, args, _resolution) do
        Accounts.Setting
        |> Repo.get_by!(user_id: parent.id)
        |> Accounts.update_setting(args)
      end
  """
  @spec update_user_setting(map(), map(), %{context: map()}) :: {:ok, Accounts.Setting.t()}
  def update_user_setting(_parent, args, %{context: %{current_user: user}}) do
    Accounts.Setting
    |> Repo.get_by!(user_id: user.id)
    |> Accounts.update_setting(args)
  end

  @doc """
    Functions to process mutation update user email.

  ## Examples

      def update_user_email(parent, %{email: email, hash: hash}, _resolution) do
        result =
          Emails.ChangeEmail
          |> where([e], e.hash == ^hash)
          |> where([e], e.active == true)
          |> Repo.one()

        if result do
          Emails.update_change_email(result, %{active: false})

          Accounts.get_user!(result.user_id)
          |> Accounts.User.changeset(%{
            email: email
          })
          |> Accounts.update_user()
        else
          {:error, "Change email link is not valid"}
        end
      end
  """
  @spec update_user_email(map(), %{email: String.t(), hash: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, String.t()}
  def update_user_email(_parent, %{hash: hash}, _resolution) do
    with result <- Emails.get_change_email_by_hash!(hash) do
      {:ok, user} =
        result.user_id
        |> Accounts.get_user!()
        |> Accounts.update_user(%{email: result.email})

      Emails.get_change_email_by_user_id(result.user_id)
      |> Enum.map(&Emails.delete_change_email(&1))

      {:ok, user}
    end
  end

  @doc """
    Functions to process mutation cancel confirm user email.

  ## Examples

      def cancel_confirm_user_email(parent, %{hash: hash}, _resolution) do
        result =
          Emails.ChangeEmail
          |> where([e], e.hash == ^hash)
          |> where([e], e.active == true)
          |> Repo.one()

        if result do
          Emails.update_change_email(result, %{active: false})
        end

        Accounts.get_user!(result.user_id)
      end
  """
  @spec cancel_confirm_user_email(map(), %{hash: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, String.t()}
  def cancel_confirm_user_email(_parent, %{hash: hash}, _resolution) when not is_nil(hash) do
    with result <- Emails.get_change_email_by_hash!(hash) do
      Emails.get_change_email_by_user_id(result.user_id)
      |> Enum.map(&Emails.delete_change_email(&1))

      {:ok, Accounts.get_user!(result.user_id)}
    end
  end

  def cancel_confirm_user_email(_parent, _args, %{context: %{current_user: user}}) do
    Emails.get_change_email_by_user_id(user.id)
    |> Enum.map(&Emails.delete_change_email(&1))

    {:ok, user}
  end

  @doc """
    Functions to process query delete user notifications by id.

  ## Examples

      def delete_account_notifications(_parent, %{id: ids}, _resolution) do
        Enum.map(ids, &Notifications.delete_notification/1)
      end
  """
  @spec delete_account_notifications(
          map(),
          %{id: list(String.t())},
          Absinthe.Resolution.t()
        ) :: {:ok, Accounts.User.t()}
  def delete_account_notifications(_parent, %{id: ids}, %{
        context: %{current_user: user}
      }) do
    Notifications.delete_notifications(ids)
    {:ok, Map.put(user, :notifications, nil)}
  end

  @doc """
    Functions to process query connect User in social media.

  ## Examples

      def connect(parent, args, _resolution) do
          %{client | token: args.token}
          |> Accounts.get_info!(args.provider)
          |> Accounts.update_account(args.provider, parent)
      end
  """
  @spec connect(map(), %{code: String.t(), provider: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError}
  def connect(_parent, %{code: access_token, provider: provider}, %{
        context: %{current_user: user}
      }) do
    info = Auth.get_user_info(provider, access_token)
    Accounts.update_account(user, %{"#{provider}_uid": Map.fetch!(info, :"#{provider}_uid")})
  end

  @doc """
    Functions to process query disconnect User with social media.

  ## Examples

      def disconnect(parent, args, _resolution) do
        auth =
          Accounts.update_user(parent, %{"\#{args.provider}_uid" => nil})
      end
  """
  @spec disconnect(map(), %{provider: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError}
  def disconnect(_parent, %{provider: provider}, %{context: %{current_user: user}}) do
    Accounts.update_account(user, %{"#{provider}_uid" => nil})
  end

  @doc """
    Functions to process query callback token.

  ## Examples

      def callback(_parent, %{code: code, provider: provider}, _resolution) do
        provider
        |> Auth.get_user_info(access_token)
        |> Accounts.create_account()
      end
  """
  @spec callback(map(), %{code: String.t(), provider: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError}
  def callback(_parent, %{code: access_token, provider: provider}, _resolution) do
    provider
    |> Auth.get_user_info(access_token)
    |> Accounts.create_account()
  end

  @doc """
    Functions to process changes user `:status` and callback [`User`](SmileDB.Accounts.User.html).

  ## Examples

      alias SmileDB.Accounts

      def restore(_parent, _args, %{context: %{current_user: user}}) do
        [
          SmileDB.Source.Feed,
          SmileDB.Source.Content,
          SmileDB.Source.Comment,
          SmileDB.Source.Answer,
          SmileDB.Contents.Meto,
          SmileDB.Users.Expert
        ]
        |> Enum.map(&ElasticSearch.Ecto.restore(Repo, user, &1))

        user
        |> Accounts.update_user(%{
          status: "active"
        })
      end
  """
  @spec restore(map(), map(), %{context: %{current_user: Accounts.User.t()}}) ::
          {:ok, Accounts.User.t()} | {:error, Ecto.ChangeError.t()}
  def restore(_parent, _args, %{context: %{current_user: user}}) do
    [
      SmileDB.Source.Feed,
      SmileDB.Source.Content,
      SmileDB.Source.Comment,
      SmileDB.Source.Answer,
      SmileDB.Contents.Meto,
      SmileDB.Users.Expert
    ]
    |> Enum.map(&ElasticSearch.Ecto.restore(Repo, user, &1))

    user
    |> Accounts.update_user(%{
      status: "active"
    })
  end

  @doc """
    This feature implements the write-in of the edit table to confirm the change of mail in the user and confirm via mail.

  ## Examples

      alias SmileDB.Emails

      def set_change_user_email(_parent, %{email: email}, %{context: %{current_user: user}}) do
        case Emails.create_change_email(%{
              user_id: user.id,
              email: email,
              hash: Phoenix.Token.sign(KnowledgePhoenixWeb.Endpoint, "\#{email}", user.id)
            }) do
          {:ok, change_email} ->
            EmailWorker.email_verification(user.email, %{hash: change_email.hash})
            {:ok, true}

          {:error, changeset} ->
            {:error, changeset}
        end
      end
  """
  @spec set_change_user_email(map(), %{email: String.t()}, Absinthe.Resolution.t()) ::
          {:ok, boolean} | {:error, Ecto.ChangeError.t()}
  def set_change_user_email(_parent, %{email: email}, %{context: %{current_user: user}}) do
    Emails.get_change_email_by_user_id(user.id)
    |> Enum.map(&Emails.delete_change_email(&1))

    case Emails.create_change_email(%{
           user_id: user.id,
           email: email,
           hash: Phoenix.Token.sign(KnowledgePhoenixWeb.Endpoint, "#{email}", user.id)
         }) do
      {:ok, change_email} ->
        EmailWorker.email_verification(change_email.email, %{hash: change_email.hash})
        {:ok, user}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
    This feature checks or user has an email to confirm.

    ## Examples

      alias SmileDB.Emails

      def email_verification(parent, _args, _resolution) do
        case Emails.get_change_email_by_user_id(parent.id) do
          [change_email] -> Map.fetch(change_email, :email)
          [] -> {:ok, nil}
        end
      end
  """
  @spec email_verification(Accounts.User.t(), map(), map()) :: {:ok, String.t()} | {:ok, nil}
  def email_verification(parent, _args, _resolution) do
    case Emails.get_change_email_by_user_id(parent.id) do
      [change_email] -> Map.fetch(change_email, :email)
      [] -> {:ok, nil}
    end
  end
end
