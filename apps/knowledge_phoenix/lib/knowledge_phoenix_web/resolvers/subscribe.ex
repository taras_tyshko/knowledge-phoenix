defmodule KnowledgePhoenixWeb.Resolvers.Subscribe do
  @moduledoc """
    A module for the release of resolvers Subscribe which are washed out in the schema.
  """

  alias SmileDB.{Repo, Subscriptions}

  @doc """
    Functions to process mutation subscribe user through the parent .

  ## Examples

      def subscribe_user(parent, args, _resolution) do
        Subscriptions.create_user_subscription(%{
          follower_id: args.follower_id,
          user_id: parent.id
        })
      end
  """
  @spec subscribe_user(map() | tuple(), map(), Absinthe.Resolution.t()) ::
          {:ok, Subscriptions.UserSubscription.t()} | {:error, any()}
  def subscribe_user(
        {:ok, %Subscriptions.UserSubscription{} = user_subscription},
        %{status: _} = args,
        resolution
      ) do
    user_subscription
    |> Ecto.put_meta(state: :built)
    |> subscribe_user(args, resolution)
  end

  def subscribe_user(
        %Subscriptions.UserSubscription{} = user_subscription,
        %{status: status},
        _resolution
      ) do
    Subscriptions.create_user_subscription(user_subscription, %{status: status})
  end

  def subscribe_user(%{id: nil}, _args, _resolution) do
    {:error, "User subscription does not exist for this user."}
  end

  def subscribe_user(user_subscription, _args, _resolution), do: user_subscription

  @doc """
    Functions to process mutation subscribe content through the parent .

  ## Examples

      def subscribe_content(parent, args, _resolution) do
          Subscriptions.create_content_subscription(%{
            content_id: args.content_id,
            user_id: parent.id
          })
      end
  """
  @spec subscribe_content(map(), map(), Absinthe.Resolution.t()) ::
          {:ok, Subscriptions.ContentSubscription.t()} | {:error, any()}
  def subscribe_content(_parent, %{status: false, content_id: content_id}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.ContentSubscription
    |> Repo.get_by(%{content_id: content_id, user_id: user_id})
    |> case do
      nil -> {:error, "Content subscription does not exist for this user."}
      content_subscription -> Subscriptions.delete_content_subscription(content_subscription)
    end
  end

  def subscribe_content(_parent, %{status: true, content_id: content_id}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.create_content_subscription(%{
      content_id: content_id,
      user_id: user_id
    })
  end

  @doc """
    Functions to process mutation subscribe office through the parent .

  ## Examples

      def subscribe_office(parent, args, _resolution) do
          Subscriptions.create_office_subscription(%{
            office_id: args.office_id,
            user_id: parent.id
          })
      end
  """
  @spec subscribe_office(map(), map(), Absinthe.Resolution.t()) ::
          {:ok, Subscriptions.OfficeSubscription.t()} | {:error, any()}
  def subscribe_office(_parent, %{status: false, subscribe_id: office_id}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.OfficeSubscription
    |> Repo.get_by(%{office_id: office_id, user_id: user_id})
    |> case do
      nil -> {:error, "Office subscription does not exist for this user."}
      office_subscription -> Subscriptions.delete_office_subscription(office_subscription)
    end
  end

  def subscribe_office(_parent, %{status: true, subscribe_id: office_id}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.create_office_subscription(%{
      office_id: office_id,
      user_id: user_id
    })
  end

  @doc """
    Functions to process mutation subscribe tag through the parent .

  ## Examples

      def subscribe_tag(parent, args, _resolution) do
        Subscriptions.create_tag_subscription(%{
            title: args.tag,
            user_id: parent.id
          })
      end
  """
  @spec subscribe_tag(map(), map(), Absinthe.Resolution.t()) ::
          {:ok, Subscriptions.TagSubscription.t()} | {:error, any()}
  def subscribe_tag(_parent, %{status: false, tag: tag}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.TagSubscription
    |> Repo.get_by(%{title: tag, user_id: user_id})
    |> case do
      nil -> {:error, "Tag subscription does not exist for this user."}
      tag_subscription -> Subscriptions.delete_tag_subscription(tag_subscription)
    end
  end

  def subscribe_tag(_parent, %{status: true, tag: tag}, %{
        context: %{current_user: %{id: user_id}}
      }) do
    Subscriptions.create_tag_subscription(%{
      title: tag,
      user_id: user_id
    })
  end
end
