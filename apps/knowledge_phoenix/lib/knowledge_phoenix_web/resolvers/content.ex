defmodule KnowledgePhoenixWeb.Resolvers.Content do
  @moduledoc """
    A module for the release of resolvers Content which are washed out in the schema.
  """

  import Tirexs.Search
  import Ecto.Query, warn: false

  alias ElasticSearch.Paginator.Metadata
  alias SmileDB.{Repo, Source, Contents, Category}

  @doc """
    Functions to process query get content data through the `id`.

  ## Examples

      def get_content(_parent, args, _resolution) do
        Source.get_content!(args.id)
      end
  """
  @spec get_content(map(), %{id: Integer.t()}, map()) :: {:ok, Source.Content.t()}
  def get_content(_parent, %{id: id}, %{context: %{current_user: %{id: user_id}}}) do
    case ElasticSearch.Repo.get(Source.Content, id) do
      %Source.Content{user_id: ^user_id} = content ->
        {:ok, content}

      %Source.Content{shadow: false} = content ->
        {:ok, content}

      _ ->
        {:error, "Content not found!"}
    end
  end

  def get_content(_parent, %{id: id}, _resolution) do
    case ElasticSearch.Repo.get(Source.Content, id) do
      %Source.Content{shadow: false} = content ->
        {:ok, content}

      _ ->
        {:error, "Content not found!"}
    end
  end

  @doc """
    Functions to process query get list contents with the pagination.

  ## Examples

      def list_contents(_parent, args, _resolution) do
        %{entries: entries, metadata: metadata} =
          Source.Content
          |> order_by(desc: ^args.order_by, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [args.order_by, :id],
            limit: args.limit,
            include_total_count: args.total_count
          )
      end
  """
  @spec list_contents(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            order_by: String.t(),
            tags: list(String.t()),
            nicknames: list(String.t())
          },
          map()
        ) ::
          {:ok,
           %{
             entries: list(Source.Content.t()),
             metadata: Metadata.t()
           }}
  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          tags: tags,
          nicknames: nicknames,
          type: type
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("type", type)
                end

                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          tags: tags,
          nicknames: nicknames
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          nicknames: nicknames,
          type: type
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("type", type)
                end

                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, nicknames: nicknames},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          input: input,
          tags: tags,
          type: type
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            should do
              term("type", type)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, tags: tags, type: type},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              term("type", type)
            end

            filter do
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          input: input,
          tags: tags
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          tags: tags
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            filter do
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, input: input, type: type},
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            filter do
              term("type", type)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, input: input},
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, type: type},
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              term("type", type)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by},
        %{context: %{current_user: user}}
      ) do
    query =
      search(index: ElasticSearch.get_meta!(Source.Content).index) do
        query do
          bool do
            must do
              match_all(boost: 1.0)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get list related contents with the pagination.

  ## Examples

      def list_related_contents(_parent, args, _resolution) do
        %{entries: entries, metadata: metadata} =
          Contents.RelatedContent
          |> where([r], r.content_id == ^args.content_id)
          |> order_by(desc: :inserted_at, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [:inserted_at, :id],
            limit: args.limit,
            include_total_count: args.total_count
          )

        contents =
          Enum.map(entries, &Source.get_content!(&1.related_content_id))

        contents = %{
          entries: contents,
          metadata: metadata
        }
      end

  """
  @spec list_related_contents(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            content_id: Integer.t(),
            type: String.t()
          },
          map()
        ) ::
          {:ok,
           %{
             entries: list(Source.Content.t()),
             metadata: Metadata.t()
           }}
  def list_related_contents(
        _parent,
        %{limit: limit, after: after_cursor, content_id: content_id},
        _resolution
      ) do
    %{entries: entries, metadata: metadata} =
      Contents.RelatedContent
      |> where([r], r.content_id == ^content_id)
      |> order_by(desc: :inserted_at, desc: :id)
      |> Repo.paginate(
        after: after_cursor,
        cursor_fields: [:inserted_at, :id],
        limit: limit
      )

    {:ok,
     %{
       entries:
         Enum.map(
           entries,
           &ElasticSearch.Repo.get!(Source.Content, &1.related_content_id)
         ),
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get list answers with the pagination.

  ## Examples

      def list_answers(_parent, args, _resolution) do
        %{entries: entries, metadata: metadata} =
          Source.Answer
          |> where([a], a.content_id == ^args.content_id)
          |> order_by(desc: ^args.order_by, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [args.order_by, :id],
            limit: args.limit,
            include_total_count: args.total_count
          )
      end
  """

  @spec list_answers(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            order_by: String.t(),
            content_id: Integer.t(),
            tags: list(String.t()),
            nicknames: list(String.t())
          },
          map()
        ) ::
          {:ok,
           %{
             entries: list(Source.Answer.t()),
             metadata: Metadata.t()
           }}
  def list_answers(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          content_id: content_id,
          nicknames: nicknames,
          tags: tags
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              bool do
                filter do
                  term("content_id", content_id)
                end

                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_answers(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          content_id: content_id,
          answer_id: answer_id
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            filter do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    answers =
      if not is_nil(answer_id) do
        Source.Answer
        |> ElasticSearch.Repo.get!(answer_id)
        |> List.wrap()
        |> List.flatten(Enum.filter(entries, &(not (&1.id == answer_id))))
        |> Enum.filter(&(not (&1.shadow == true)))
      else
        entries
      end

    {:ok,
     %{
       entries: answers,
       metadata: metadata
     }}
  end

  def list_answers(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          content_id: content_id,
          nicknames: nicknames
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("content_id", content_id)
                end

                should do
                  terms("author", nicknames)
                end

                must_not do
                  term("anonymous", true)
                end
              end
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_answers(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          order_by: order_by,
          content_id: content_id,
          tags: tags
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter],
          Enum.map(tags, fn tag -> [term: [tags: tag]] end)
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  def list_answers(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, content_id: content_id},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Answer).index do
        query do
          bool do
            must do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get list comments with the pagination.

  ## Examples

      def list_answers(_parent, args, _resolution) do
        %{entries: entries, metadata: metadata} =
          Source.Answer
          |> where([a], a.content_id == ^args.content_id)
          |> order_by(desc: ^args.order_by, desc: :id)
          |> Repo.paginate(
            after: args.after_cursor,
            cursor_fields: [args.order_by, :id],
            limit: args.limit,
            include_total_count: args.total_count
          )
      end
  """
  @spec list_comments(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            order_by: String.t(),
            answer_id: Integer.t(),
            comment_id: Integer.t()
          },
          map()
        ) ::
          {:ok,
           %{
             entries: list(Source.Comment.t()),
             metadata: Metadata.t()
           }}
  def list_comments(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          content_id: content_id,
          comment_id: comment_id,
          order_by: order_by
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                  exists("answer_id")
                end
              end
            end

            filter do
              script do
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter, :script, :script],
          "doc['path'].values.size() == 1"
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    ids = Enum.map(entries, & &1.id)

    comments =
      with true <- not is_nil(comment_id) do
        id =
          Source.Comment
          |> ElasticSearch.Repo.get!(comment_id)
          |> Map.fetch!(:path)
          |> Map.fetch!(:labels)
          |> List.first()
          |> String.to_integer()

        ids
        |> List.insert_at(0, id)
        |> Enum.uniq()
      else
        false -> ids
      end
      |> Enum.map(fn comment_id ->
        Source.Comment
        |> struct(%{id: comment_id})
        |> Source.list_comments()
        |> Enum.sort_by(&Enum.count(Map.from_struct(&1.path).labels))
      end)
      |> List.flatten()

    {:ok,
     %{
       entries: comments,
       metadata: metadata
     }}
  end

  def list_comments(
        _parent,
        %{
          limit: limit,
          after: after_cursor,
          answer_id: answer_id,
          comment_id: comment_id,
          order_by: order_by
        },
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              term("answer_id", answer_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
              script do
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end
        |> put_in(
          [:search, :query, :bool, :filter, :script, :script],
          "doc['path'].values.size() == 1"
        ),
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    ids = Enum.map(entries, & &1.id)

    comments =
      with true <- not is_nil(comment_id) do
        id =
          Source.Comment
          |> ElasticSearch.Repo.get!(comment_id)
          |> Map.fetch!(:path)
          |> Map.fetch!(:labels)
          |> List.first()
          |> String.to_integer()

        ids
        |> List.insert_at(0, id)
        |> Enum.uniq()
      else
        false -> ids
      end
      |> Enum.map(fn comment_id ->
        Source.Comment
        |> struct(%{id: comment_id})
        |> Source.list_comments()
        |> Enum.sort_by(&Enum.count(Map.from_struct(&1.path).labels))
      end)
      |> List.flatten()

    {:ok,
     %{
       entries: comments,
       metadata: metadata
     }}
  end

  def list_comments(
        _parent,
        %{limit: limit, after: after_cursor, content_id: content_id, order_by: order_by},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              term("content_id", content_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                  exists("answer_id")
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end
      |> put_in(
        [:search, :query, :bool, :filter, :script],
        script: [source: "doc['path'].values.size() == 1"]
      )

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    comments =
      entries
      |> Enum.map(fn comment ->
        comment
        |> Source.list_comments()
        |> Enum.sort_by(&Enum.count(Map.from_struct(&1.path).labels))
      end)
      |> List.flatten()

    {:ok,
     %{
       entries: comments,
       metadata: metadata
     }}
  end

  def list_comments(
        _parent,
        %{limit: limit, after: after_cursor, answer_id: answer_id, order_by: order_by},
        %{context: %{current_user: user}}
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Comment).index do
        query do
          bool do
            must do
              term("answer_id", answer_id)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end
      |> put_in(
        [:search, :query, :bool, :filter, :script],
        script: [source: "doc['path'].values.size() == 1"]
      )

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        if user && user.status == "hidden" do
          put_in(
            query,
            [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
            term: [user_id: user.id]
          )
        else
          query
        end,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    comments =
      entries
      |> Enum.map(fn comment ->
        comment
        |> Source.list_comments()
        |> Enum.sort_by(&Enum.count(Map.from_struct(&1.path).labels))
      end)
      |> List.flatten()

    {:ok,
     %{
       entries: comments,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query autocomplete contents.

  ## Examples

      def contents_autocomplete(_parent, %{limit: limit, input: input}, _resolution) do
        query =
          search [index: "contents"] do
            size(limit)

            suggest do
              content_suggest do
                prefix(input, [])

                completion do
                  field("title", [])
                  fuzzy(:fuzziness, "AUTO")
                end
              end
            end
          end

        case Tirexs.Query.create_resource(query) do
          {:ok, _code, result} ->
            titles =
              Enum.map(List.first(result.suggest.content_suggest).options, fn source ->
                source.title
                |> String.downcase()
                |> String.trim()
              end)

            {:ok, Enum.uniq(titles)}

          {:error, _code, status} ->
            {:error, status}
        end
      end
  """
  @spec contents_autocomplete(
          map(),
          %{limit: Integer.t(), input: String.t(), type: String.t()},
          map()
        ) :: {:ok, list(Source.Content.t())}
  def contents_autocomplete(_parent, %{limit: limit, input: input, type: type}, %{
        context: %{current_user: user}
      }) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              match(
                "title_new_autocomplete",
                input,
                fuzziness: "AUTO",
                operator: "and"
              )
            end

            filter do
              term("type", type)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end
      end

    suggest =
      if user && user.status == "hidden" do
        put_in(
          query,
          [:search, :query, :bool, :must_not, Access.all(), :bool, :must_not],
          term: [user_id: user.id]
        )
      else
        query
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, suggest}
  end

  @doc """
    Functions to process query get scoped data (users and tags).

  ## Examples

      def scoped_data(_parent, args, _resolution) do
        contents =
          Source.Content
          |> where([q], fragment("? @> ?", q.tags, ^args.tags))
          |> limit(20)
          |> select([q], %{nickname: q.nickname, tags: q.tags})
          |> Repo.all()

        users =
          contents
          |> Enum.map(& &1.nickname)
          |> Enum.uniq()
          |> Enum.map(&Accounts.get_user_by_nickname!(&1))

        tags =
          contents
          |> Enum.map(& &1.tags)
          |> List.flatten()
          |> Enum.filter(&(not Enum.member?(tags, &1)))
          |> Enum.uniq()

        {:ok, %{
          users: users,
          tags: tags
        }}
      end
  """
  @spec scoped_data(map(), %{tags: list(String.t())}, map()) ::
          {:ok,
           %{
             tags: list(String.t())
           }}
  def scoped_data(_parent, %{input: input, tags: tags}, %{context: %{current_user: user}}) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end
      end

    tags =
      if user && user.status == "hidden" do
        pop_in(query[:search][:query][:bool][:must_not])
        |> elem(1)
      else
        query
      end
      |> put_in(
        [:search, :query, :bool, :filter],
        Enum.map(tags, fn tag -> [term: [tags: tag]] end)
      )
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.tags)
      |> List.flatten()
      |> Enum.filter(&(not Enum.member?(tags, &1)))
      |> Enum.uniq()

    {:ok, %{tags: tags}}
  end

  def scoped_data(_parent, %{input: input}, _resolution) do
    tags =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              match_phrase_prefix("title_search", input)
            end

            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end
          end
        end
      end
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.tags)
      |> List.flatten()
      |> Enum.uniq()

    {:ok, %{tags: tags}}
  end

  def scoped_data(_parent, %{tags: tags}, _resolution) do
    tags =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must_not do
              bool do
                must do
                  term("shadow", true)
                end
              end
            end

            filter do
            end
          end
        end
      end
      |> put_in(
        [:search, :query, :bool, :filter],
        Enum.map(tags, fn tag -> [term: [tags: tag]] end)
      )
      |> ElasticSearch.Repo.all()
      |> Enum.map(& &1.tags)
      |> List.flatten()
      |> Enum.filter(&(not Enum.member?(tags, &1)))
      |> Enum.uniq()

    {:ok, %{tags: tags}}
  end

  @spec office_contents(
          map(),
          %{
            limit: Integer.t(),
            after: String.t(),
            order_by: String.t(),
            office: String.t(),
            type: String.t()
          },
          map()
        ) ::
          {:ok,
           %{
             entries: list(Source.Content.t()),
             metadata: Metadata.t()
           }}
  def office_contents(
        _parent,
        %{limit: limit, after: after_cursor, order_by: order_by, type: type, office: office},
        _resolution
      ) do
    query =
      search index: ElasticSearch.get_meta!(Source.Content).index do
        query do
          bool do
            must do
              bool do
                must do
                  term("office_id", office)
                end

                filter do
                  term("type", type)
                end

                must_not do
                  bool do
                    must do
                      term("shadow", true)
                    end
                  end
                end
              end
            end
          end
        end

        sort do
          ["#{order_by}": :desc, id: :desc]
        end
      end

    %{entries: entries, metadata: metadata} =
      ElasticSearch.Repo.paginate(
        query,
        limit: limit,
        cursor_fields: [order_by, :id],
        after: after_cursor
      )

    {:ok,
     %{
       entries: entries,
       metadata: metadata
     }}
  end

  @doc """
    Functions to process query get office data through the `id`.

  ## Examples

      iex> Mutation.get_office(_parent, %{id: 123}, _resolution)
      {:ok, %SmileDB.Category.Office{}}

      iex> Mutation.get_office(_parent, %{id: 456}, _resolution)
      {:error, "Office not found!"}
  """
  @spec get_office(map(), %{id: Integer.t()}, map()) :: {:ok, Category.Office.t()}
  def get_office(_parent, %{id: id}, _resolution) do
    case Category.get_office!(id) do
      %Category.Office{} = office ->
        {:ok, office}

      _ ->
        {:error, "Office not found!"}
    end
  end

  @doc """
    Functions to process query check office data through the `title`.

  ## Examples

      iex> Mutation.check_office(_parent, %{title: "test"}, _resolution)
      {:ok, true}

      iex> Mutation.check_office(_parent, %{title: "test"}, _resolution)
      {:ok, false}
  """
  @spec check_office(map(), map(), map()) :: {:ok, boolean()}
  def check_office(_parent, %{title: title}, _resolution) do
    title =
      title
      |> String.split()
      |> Enum.map(&Macro.underscore(&1))
      |> Enum.join("_")

    {:ok, !is_nil(ElasticSearch.Repo.get_by(Category.Office, title: title))}
  end

  @doc """
    Returns a list of offices.

  ## Examples

      def offices(_parent, %{limit: limit, input: input}, _resolution) do
        suggest =
          search index: ElasticSearch.get_meta!(Category.Office).index do
            query do
              match_phrase_prefix("title_search", input)
            end

            sort do
              [inserted_at: :desc]
            end
          end
          |> ElasticSearch.Repo.all(limit)

        {:ok, suggest}
      end
  """
  @spec offices(map(), %{limit: Integer.t(), input: String.t()}, map()) ::
          list(String.t()) | list()
  def offices(_parent, %{limit: limit, input: input}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Category.Office).index do
        query do
          match_phrase_prefix("title_search", input)
        end

        sort do
          [inserted_at: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, suggest}
  end

  def offices(_parent, %{limit: limit}, _resolution) do
    suggest =
      search index: ElasticSearch.get_meta!(Category.Office).index do
        query do
          match_all(boost: 1.0)
        end

        sort do
          [inserted_at: :desc]
        end
      end
      |> ElasticSearch.Repo.all(limit)

    {:ok, suggest}
  end
end
