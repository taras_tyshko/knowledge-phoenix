defmodule KnowledgePhoenix do
  @moduledoc """
    KnowledgePhoenix is a Rest Full API using the methodology of [`GraphQl`](https://graphql.org). The purpose of this project is to demonstrate the flexibility and speed of the [`Phoenix`](http://phoenixframework.org) Web framework.

    KnowledgePhoenix is built on the basis of [`Phoenix`](http://phoenixframework.org) productive web framework. 
  Phoenix is a web development framework written in Elixir which implements the server-side MVC pattern. 
  Phoenix provides the best of both worlds - high developer productivity and high application performance. 
  It also has some interesting new twists like channels for implementing realtime features and pre-compiled templates for blazing speed.

  ## Umbrella projects

  The project is built on the architecture [`Umbrella projects`](https://elixir-lang.org/getting-started/mix-otp/dependencies-and-umbrella-projects.html#umbrella-projects),
  which allows to develop the micro service structure of the application in one project and if necessary, and independently divide the subprojects into separate appliquones.
    
  The application has subprojections:
      [`KnowledgePhoenix`](KnowledgePhoenix.html).
      [`Messenger`](Messenger.html).

  ## GraphQl

  KnowledgePhoenix is built as an `API`. The API implementation tool is selected methodology [`GraphQl`](http://absinthe-graphql.org).
  That allows you to implement the `API` and use only the data you invite. The module [`Absinthe`](https://hexdocs.pm/absinthe/overview.html) was chosen to implement the [`GraphQl`](http://absinthe-graphql.org) methodology in the language [`Elixir`](https://elixir-lang.org).

  KnowledgePhoenix keeps the contexts that define your domain
  and business logic.

  ## ElasticSearch

  To perform queries that are associated with search and autocompliments, you have chosen [`ElasticSearch`](https://www.elastic.co).
  To implement this tool in the language [`Elixir`](https://elixir-lang.org). was chosen module [`Tirexs`](https://hexdocs.pm/tirexs/api-reference.html).


  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.


  ## Installation

  ## Testing

  ## Thanks!
    Thank `@smile` for the unbelievable work.
  """
end
