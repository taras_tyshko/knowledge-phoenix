defmodule KnowledgePhoenix.PlugAttack do
  @moduledoc """
  A plug building toolkit for blocking and throttling abusive requests
  """

  import Plug.Conn
  use PlugAttack

  rule "allow local", conn do
    allow(conn.remote_ip == {127, 0, 0, 1})
  end

  rule "throttle by ip", conn do
    throttle(
      conn.remote_ip,
      period:
        Application.get_env(:knowledge_phoenix, PlugAttack)
        |> Keyword.fetch!(:period),
      limit:
        Application.get_env(:knowledge_phoenix, PlugAttack)
        |> Keyword.fetch!(:limit),
      storage: {PlugAttack.Storage.Ets, KnowledgePhoenix.PlugAttack.Storage}
    )
  end

  @doc """
  Rules for the applications.

  ## Examples (Allow local connection)

  Allow local connection from application

      rule "allow local", conn do
        allow conn.remote_ip == {127, 0, 0, 1}
      end

  ## Examples 2 (Limits the connection IP or DNS-name)

  A rule that limits the connection IP or DNS-name to the application for the given parameters: period and limit.
    
      rule "throttle by ip", conn do
        throttle conn.remote_ip,
          period: 4_000, limit: 25,
          storage: {PlugAttack.Storage.Ets, KnowledgePhoenix.PlugAttack.Storage}
      end
  """
  def allow_action(conn, {:throttle, data}, opts) do
    conn
    |> add_throttling_headers(data)
    |> allow_action(true, opts)
  end

  def allow_action(conn, _data, _opts) do
    conn
  end

  @doc """
  Blocks actions for rules and forbid action when exceeded the limit of connections.

  ## Examples 1 (block_action)

      def block_action(conn, {:throttle, data}, opts) do
        conn
        |> add_throttling_headers(data)
        |> block_action(false, opts)
      end

  ## Examples (Forbidden action)

      def block_action(conn, _data, _opts) do
        conn
        |> send_resp(:forbidden, "Forbidden")
        |> halt # It's important to halt connection once we send a response early
      end   
  """
  def block_action(conn, {:throttle, data}, opts) do
    conn
    |> add_throttling_headers(data)
    |> block_action(false, opts)
  end

  def block_action(conn, _data, _opts) do
    # It's important to halt connection once we send a response early
    conn
    |> send_resp(:forbidden, "Forbidden\n")
    |> halt()
  end

  defp add_throttling_headers(conn, data) do
    # The expires_at value is a unix time in milliseconds, we want to return one
    # in seconds
    reset = div(data[:expires_at], 1_000)

    conn
    |> put_resp_header("x-ratelimit-limit", to_string(data[:limit]))
    |> put_resp_header("x-ratelimit-remaining", to_string(data[:remaining]))
    |> put_resp_header("x-ratelimit-reset", to_string(reset))
  end
end
