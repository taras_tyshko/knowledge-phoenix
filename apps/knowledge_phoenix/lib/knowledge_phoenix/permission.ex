defmodule KnowledgePhoenix.Permission do
  @moduledoc false

  @type result :: :ok | :error | {:error, String.t()}
  @type t :: (any, map, map -> result) | (map, map -> result)

  def is_active_account(_, _, %{context: %{current_user: user}}) when not is_nil(user) do
    case user.status do
      "active" -> ok()
      "hidden" -> ok()
      _ -> error_message(nil)
    end
  end

  def is_active_account(_, _, _), do: error_message(nil)

  def is_authenticated(_, _, %{context: %{current_user: user}}) when not is_nil(user), do: ok()
  def is_authenticated(_, _, _), do: error_message(nil)

  defp ok, do: :ok
  defp error_message(nil), do: :error
  defp error_message(message), do: {:error, message}
end
