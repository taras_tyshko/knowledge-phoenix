defmodule KnowledgePhoenix.Application do
  use Application

  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @spec start(Elixir.Application.app(), Elixir.Application.start_type()) :: :ok | {:error, term()}
  def start(_type, _args) do
    import Supervisor.Spec

    # Define workers and child supervisors to be supervised
    children = [
      # Start the endpoint when the application starts
      supervisor(KnowledgePhoenixWeb.Endpoint, []),
      supervisor(Absinthe.Subscription, [KnowledgePhoenixWeb.Endpoint]),
      # Start your own worker by calling: KnowledgePhoenix.Worker.start_link(arg1, arg2, arg3)
      # worker(KnowledgePhoenix.Worker, [arg1, arg2, arg3]),

      # Storage to our supervision tree that will be cleared through the specified time (default -> 2 min).
      worker(PlugAttack.Storage.Ets, [
        KnowledgePhoenix.PlugAttack.Storage,
        [
          clean_period: Application.get_env(:knowledge_phoenix, PlugAttack)[:clean_period]
        ]
      ]),

      # This is the supervisor sets the module presence tracking to determine the presence of the user.
      supervisor(KnowledgePhoenixWeb.Presence, [])
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: KnowledgePhoenix.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @doc """
  Reloads the configuration given the application environment changes.

  Tell Phoenix to update the endpoint configuration whenever the application is updated.
  """
  @spec config_change(term(), term(), term()) :: :ok | {:error, term()}
  def config_change(changed, _new, removed) do
    KnowledgePhoenixWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
