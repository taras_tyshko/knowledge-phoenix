defmodule KnowledgePhoenix.Mixfile do
  use Mix.Project

  def project do
    [
      app: :knowledge_phoenix,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps(),

      # Docs
      name: "KnowledgePhoenix",
      # The main page in the docs
      docs: [
        main: "KnowledgePhoenix",
        extras: ["README.md"],
        markdown_processor: ExDoc.Markdown.Cmark
      ]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {KnowledgePhoenix.Application, []},
      extra_applications: [
        :logger,
        :jason,
        :logger_json,
        :runtime_tools,
        :faker,
        :plug_attack,
        :guardian,
        :tirexs
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:knowledge_admin, in_umbrella: true},
      {:notifications, in_umbrella: true},
      {:email_worker, in_umbrella: true},
      {:messenger, in_umbrella: true},
      {:smile_db, in_umbrella: true},
      {:phoenix, "~> 1.3.4"},
      {:phoenix_pubsub, "~> 1.1"},
      {:phoenix_html, "~> 2.12", override: true},
      {:logger_logstash_backend, "~> 5.0.0"},
      {:timex, "~> 3.4", override: true},
      {:plug_attack, "~> 0.4.1"},
      {:logger_json, "~> 1.2.1"},
      {:plug_cowboy, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:jason, "~> 1.1"},
      {:cors_plug, "~> 2.0.0"},
      {:absinthe, "~> 1.4.13"},
      {:absinthe_plug, "~> 1.4.5"},
      {:absinthe_phoenix, "~> 1.4.3"},
      {:dataloader, "~> 1.0.4"},
      {:tirexs, "~> 0.8.15"},
      {:recase, "~> 0.3.0"},
      {:toml, "~> 0.3.0"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
