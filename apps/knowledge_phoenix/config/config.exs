# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :knowledge_phoenix, KnowledgePhoenixWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Kz46muBJNwAgmEhhfFAGDPPzuodSm+dv55flblKyRcxDEse04LPCMgMCDLf3jE4W",
  render_errors: [view: KnowledgePhoenixWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: KnowledgePhoenix.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: []

config :knowledge_phoenix, PlugAttack,
  clean_period: 120_000,
  period: 4_000,
  limit: 25

config :notifications,
  providers: [Notifications.Providers.Absinthe]

config :email_worker,
  front_host: "https://vuejs-dev.smile-magento.com",
  front_admin_host: "https://vuejs-admin-dev.smile-magento.com"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
