defmodule KnowledgePhoenix.QueryHelpers do
  @moduledoc """
      In this module, the modules describe the functions of objects for unification of samples of data from `GraphQl`.

      What would use these functions for samples should be set `alias` or `import` [`KnowledgePhoenix.QueryHelpers]`(KnowledgePhoenix.QueryHelpers.html) of this module and insert the required function through the interpolation as data as the returned function is a string.
  """

  alias KnowledgePhoenix.QueryHelpers

  @spec user() :: String.t()
  def user do
    "
                id
                answersCount
                authProvider
                avatar
                biography
                birthday
                commentsCount
                email
                expert
                status
                website
                lastName
                firstName
                insertedAt
                likesCount
                contentsCount
                gender
                googleUid
                facebookUid
                nickname
                updatedAt
            "
  end

  @spec description() :: String.t()
  def description do
    "
                desc
                users {
                    #{QueryHelpers.user()}
                    }   
            "
  end

  @spec ratings() :: String.t()
  def ratings do
    "
                dislikes
                likes
            "
  end

  @spec content() :: String.t()
  def content do
    "
                id
                anonymous
                description {
                    #{QueryHelpers.description()}
                }
                answersCount
                answersId
                insertedAt
                keywords
                meTo {
                    #{QueryHelpers.user()}
                }
                reviews
                requests
                reviewsCount
                shadow
                tags 
                user {
                    #{QueryHelpers.user()}
                }
                title 
                updatedAt
            "
  end

  @spec answer() :: String.t()
  def answer do
    "
                id
                anonymous
                commentsCount
                description {
                    #{QueryHelpers.description()}
                }
                insertedAt
                keywords
                content {
                    #{QueryHelpers.content()}
                }
                ratings {
                    #{QueryHelpers.ratings()}
                }
                tags
                user {
                    #{QueryHelpers.user()}
                }
                ratingsCount
                updatedAt
            "
  end

  @spec comment() :: String.t()
  def comment do
    "
                id
                anonymous
                answer {
                    #{QueryHelpers.answer()}
                }
                children
                description {
                    #{QueryHelpers.description()}
                }
                insertedAt
                user {
                    #{QueryHelpers.user()}
                }
                updatedAt
                ratingsCount
                ratings {
                    #{QueryHelpers.ratings()}
                }
                contentId
                path
                parent {
                    id
                }
                keywords
            "
  end

  @spec tag() :: String.t()
  def tag do
            "
                id
                count
                title
                insertedAt
                updatedAt
            "
  end

  @spec setting() :: String.t()
  def setting do
    "
                id
                byAnswers
                byComments
                emailEnable
                emailOnReply
                insertedAt
                updatedAt
                webNotifMentions
                webNotifRecommend
                webNotifUpvote
            "
  end

  @spec metadata() :: String.t()
  def metadata do
    "
                after
                limit
                totalCount
            "
  end
end
