defmodule KnowledgePhoenixWeb.SubscriptionCase do
  @moduledoc """
  This module defines the test case to be used by
  subscription tests
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with channels
      use KnowledgePhoenixWeb.ChannelCase
      import SmileDB.ContentHelpers

      use Absinthe.Phoenix.SubscriptionTest,
        schema: KnowledgePhoenixWeb.Schema

      setup do
        {:ok,
         %{
           comment: comment,
           answer: answer,
           content: content,
           user: user,
           user_one: user_one,
           setting: setting,
          #  feed_answer: feed_answer,
          #  feed_content: feed_content
         }} = SmileDB.ContentHelpers.create_content([])

        {:ok, jwt, _claims} = SmileDB.Auth.Guardian.encode_and_sign(user)

        {:ok, socket} =
          Phoenix.ChannelTest.connect(KnowledgePhoenixWeb.UserSocket, %{
            "Authorization" => "Bearer " <> jwt
          })

        {:ok, socket} = Absinthe.Phoenix.SubscriptionTest.join_absinthe(socket)

        ## Added to Conn  auth token 
        {:ok,
         %{
           comment: comment,
           answer: answer,
           content: content,
           user: user,
           user_one: user_one,
           setting: setting,
           socket: socket,
          #  feed_answer: feed_answer,
          #  feed_content: feed_content
         }}
      end
    end
  end
end
