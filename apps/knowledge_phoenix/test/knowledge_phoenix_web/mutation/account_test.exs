defmodule KnowledgePhoenix.Mutation.AccountTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  alias SmileDB.ContentHelpers
  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "user" do
    test "account_setting/1 returns the updated settings for user", context do
      variables = %{
        byAnswers: false,
        byComments: false,
        emailEnable: false,
        emailOnReply: false,
        webNotifMentions: false,
        webNotifRecommend: false,
        webNotifUpvote: false
      }

      query = """
        mutation {
          accountSetting(byAnswers: #{variables.byAnswers}, byComments: #{variables.byComments}, emailEnable: #{
        variables.emailEnable
      }, emailOnReply: #{variables.emailOnReply}, webNotifMentions: #{variables.webNotifMentions}, webNotifRecommend: #{
        variables.webNotifRecommend
      }, webNotifUpvote: #{variables.webNotifUpvote}) {
              #{QueryHelpers.setting()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      account_setting = json_response(res, 200)["data"]["accountSetting"]

      assert variables.byAnswers == account_setting["byAnswers"]
      assert variables.byComments == account_setting["byComments"]
      assert variables.emailEnable == account_setting["emailEnable"]
      assert variables.emailOnReply == account_setting["emailOnReply"]
      assert variables.webNotifUpvote == account_setting["webNotifUpvote"]
      assert variables.webNotifMentions == account_setting["webNotifMentions"]
      assert variables.webNotifRecommend == account_setting["webNotifRecommend"]
    end

    test "account_user/1 returns the updated settings for user", context do
      variables = %{
        authProvider: "google",
        biography: "some updated biography",
        birthday: "some updated birthday",
        email: "some email", # TODO chack test when end send confirm email with front.
        firstName: "some updated firstName",
        gender: "some updated gender",
        lastName: "some updated lastName",
        location: "some updated location",
        nickname: "some_updated_nickname",
        status: "some updated status",
        website: "some updated website"
      }

      query = """
        mutation {
          accountUser(authProvider: "#{variables.authProvider}",
            biography: "#{variables.biography}", birthday: "#{variables.birthday}",
            email: "#{variables.email}", firstName: "#{variables.firstName}",
            gender: "#{variables.gender}", lastName: "#{variables.lastName}",
            location: "#{variables.location}", nickname: "#{variables.nickname}",
            status: "#{variables.status}", website: "#{variables.website}") {
                id
                user {
                  #{QueryHelpers.user()}
                }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      account_setting = json_response(res, 200)["data"]["accountUser"]["user"] 

      assert variables.email == account_setting["email"]
      assert variables.status == account_setting["status"]
      assert variables.gender == account_setting["gender"]
      assert variables.website == account_setting["website"]
      assert variables.birthday == account_setting["birthday"]
      assert variables.lastName == account_setting["lastName"]
      assert variables.nickname == account_setting["nickname"]
      assert variables.biography == account_setting["biography"]
      assert variables.firstName == account_setting["firstName"]
      assert variables.authProvider == account_setting["authProvider"]
      assert Integer.to_string(context.user.id) == account_setting["id"]
      assert context.user.id == json_response(res, 200)["data"]["accountUser"]["id"]
    end

    test "delete_user/1 returns the deleted user", context do
      query = """
        mutation {
          deleteUser {
            #{QueryHelpers.user()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteUser"]

      assert data ==
               context.user
               |> ContentHelpers.fetch_data_with_model(Map.keys(data))
               |> ContentHelpers.conver_datatimes_and_id_to_iso8601()
    end

    test "delete_account_notifications/1 delete answer notifications and return the accounts", context do
      query = """
        mutation {
          deleteAccountNotifications (answers: [#{context.answer_notification.answer_id}]) {
            answersNotifications
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteAccountNotifications"]["answersNotifications"]

      assert data == []
    end

    test "delete_account_notifications/1 delete comment notifications and return the accounts", context do
      query = """
        mutation {
          deleteAccountNotifications (comments: [#{context.comment_notification.comment_id}]) {
            commentsNotifications
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteAccountNotifications"]["commentsNotifications"]

      assert data == []
    end
    
    test "delete_account__comment_notifications/1 delete comment notifications => return the accounts", context do
      query = """
        mutation {
          deleteAccountNotifications (comments: [#{context.comment.id}]) {
            commentsNotifications
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteAccountNotifications"]

      assert data["commentsNotifications"] == []
    end

    test "delete_account_answer_notifications/1 delete answer_notification => return the accounts", context do
      query = """
        mutation {
          deleteAccountNotifications (answers: [#{context.answer.id}]) {
            answersNotifications
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = json_response(res, 200)["data"]["deleteAccountNotifications"]

      assert data["answersNotifications"] == []
    end
  end
end