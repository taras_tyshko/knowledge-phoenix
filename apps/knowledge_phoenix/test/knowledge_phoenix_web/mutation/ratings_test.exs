defmodule KnowledgePhoenix.Mutation.RatingsTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "ratings" do
    test "answer_likes/1 returns the updated answer with new ratings, add likes in answer",
         context do
      query = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: true) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_likes = json_response(res, 200)["data"]["addAnswerLikes"]

      assert Integer.to_string(context.answer.id) == answer_likes["id"]
      assert context.answer.ratings_count + 1 == answer_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == answer_likes["user"]["id"]
      assert context.user.likes_count + 1 == answer_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => [context.user.id]} == answer_likes["ratings"]
    end

    test "answer_likes/1 returns the updated answer with new ratings, delete likes in answer",
         context do
      query = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: false) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_likes = json_response(res, 200)["data"]["addAnswerLikes"]

      assert Integer.to_string(context.answer.id) == answer_likes["id"]
      assert context.answer.ratings_count == answer_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == answer_likes["user"]["id"]
      assert context.user.likes_count == answer_likes["user"]["likes_count"]
      assert %{"dislikes" => [context.user.id], "likes" => []} == answer_likes["ratings"]
    end

    test "answer_likes/1 returns the updated answer with new ratings (LIKES), delete likes record in answer",
         context do
      query_one = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: false) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: null) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_likes = json_response(res, 200)["data"]["addAnswerLikes"]

      assert Integer.to_string(context.answer.id) == answer_likes["id"]
      assert context.answer.ratings_count == answer_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == answer_likes["user"]["id"]
      assert context.user.likes_count == answer_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => []} == answer_likes["ratings"]
    end

    test "answer_likes/1 returns the updated answer with new ratings (DISLIKES), delete likes record in answer",
         context do
      query_one = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: true) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          addAnswerLikes(id: #{context.answer.id}, status: null) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_likes = json_response(res, 200)["data"]["addAnswerLikes"]

      assert Integer.to_string(context.answer.id) == answer_likes["id"]
      assert context.answer.ratings_count == answer_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == answer_likes["user"]["id"]
      assert context.user.likes_count == answer_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => []} == answer_likes["ratings"]
    end

    test "comment_likes/1 returns the updated comment with new ratings, add likes in comment",
         context do
      query = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: true) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_likes = json_response(res, 200)["data"]["addCommentLikes"]

      assert Integer.to_string(context.comment.id) == comment_likes["id"]
      assert context.comment.ratings_count + 1 == comment_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == comment_likes["user"]["id"]
      assert context.user.likes_count + 1 == comment_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => [context.user.id]} == comment_likes["ratings"]
    end

    test "comment_likes/1 returns the updated comment with new ratings, delete likes in comment",
         context do
      query = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: false) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_likes = json_response(res, 200)["data"]["addCommentLikes"]

      assert Integer.to_string(context.comment.id) == comment_likes["id"]
      assert context.comment.ratings_count == comment_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == comment_likes["user"]["id"]
      assert context.user.likes_count == comment_likes["user"]["likes_count"]
      assert %{"dislikes" => [context.user.id], "likes" => []} == comment_likes["ratings"]
    end

    test "comment_likes/1 returns the updated comment with new ratings (LIKES), delete likes record in comment",
         context do
      query_one = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: true) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: null) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_likes = json_response(res, 200)["data"]["addCommentLikes"]

      assert Integer.to_string(context.comment.id) == comment_likes["id"]
      assert context.comment.ratings_count == comment_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == comment_likes["user"]["id"]
      assert context.user.likes_count == comment_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => []} == comment_likes["ratings"]
    end

    test "comment_likes/1 returns the updated comment with new ratings (DISLIKES), delete likes record in comment",
         context do
      query_one = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: true) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          addCommentLikes(id: #{context.comment.id}, status: null) {
            id
            ratingsCount
            ratings {
              #{QueryHelpers.ratings()}
            }
            user {
              id
              likes_count
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_likes = json_response(res, 200)["data"]["addCommentLikes"]

      assert Integer.to_string(context.comment.id) == comment_likes["id"]
      assert context.comment.ratings_count == comment_likes["ratingsCount"]
      assert Integer.to_string(context.user.id) == comment_likes["user"]["id"]
      assert context.user.likes_count == comment_likes["user"]["likes_count"]
      assert %{"dislikes" => [], "likes" => []} == comment_likes["ratings"]
    end
  end
end
