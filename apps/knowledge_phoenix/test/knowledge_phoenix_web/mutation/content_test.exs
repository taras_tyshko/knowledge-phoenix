defmodule KnowledgePhoenix.Mutation.ContentTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  import SmileDB.ContentHelpers
  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "content" do
    test "content_add/1 returns the content", context do
      variables = %{
        anonymous: false,
        description: "@sosom some, description",
        tags: ["some_content_tag"],
        title: "some title"
      }

      query = """
        mutation {
          contentAdd(anonymous: #{variables.anonymous}, description: "#{variables.description}", tags: ["#{
        Enum.join(variables.tags, ", ")
      }"], title: "#{variables.title}") {
              #{QueryHelpers.content()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      content_data = json_response(res, 200)["data"]["contentAdd"]

      user_fetch_data =
        context.user
        |> Map.put(:contents_count, context.user.contents_count + 1)
        |> fetch_data_with_model(Map.keys(content_data["user"]))
        |> conver_datatimes_and_id_to_iso8601()
        |> Map.put("updatedAt", content_data["user"]["updatedAt"])

      assert variables.tags == content_data["tags"]
      assert user_fetch_data == content_data["user"]
      assert variables.title == content_data["title"]
      assert variables.anonymous == content_data["anonymous"]

      assert %{"desc" => variables.description, "users" => [user_fetch_data]} ==
               content_data["description"]
    end

    test "content_edit/1 returns the updated content for", context do
      variables = %{
        anonymous: false,
        description: "@sosom some, updated description",
        tags: ["some_content_tag_updated"],
        title: "some updated title"
      }

      query = """
        mutation {
          contentEdit(id: #{context.content.id}, anonymous: #{variables.anonymous}, description: "#{
        variables.description
      }", tags: ["#{Enum.join(variables.tags, ", ")}"], title: "#{variables.title}") {
              #{QueryHelpers.content()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      content_data = json_response(res, 200)["data"]["contentEdit"]

      user_fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(content_data["user"]))
        |> conver_datatimes_and_id_to_iso8601()

      assert variables.tags == content_data["tags"]
      assert user_fetch_data == content_data["user"]
      assert variables.title == content_data["title"]
      assert variables.anonymous == content_data["anonymous"]

      assert %{"desc" => variables.description, "users" => [user_fetch_data]} ==
               content_data["description"]
    end

    test "content_delete/1 returns the deleted content", context do
      query = """
        mutation {
          contentDelete(id: #{context.content.id}) {
            id
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      content_data = json_response(res, 200)["data"]["contentDelete"]

      assert Integer.to_string(context.content.id) == content_data["id"]
    end

    test "answer_add/1 returns the answer", context do
      variables = %{
        anonymous: false,
        description: "@sosom some, description",
        tags: ["some_answer_tag"],
        content_id: context.content.id
      }

      query = """
        mutation {
          answerAdd(anonymous: #{variables.anonymous}, description: "#{variables.description}", tags: ["#{
        Enum.join(variables.tags, ", ")
      }"], contentId: #{variables.content_id}) {
              #{QueryHelpers.answer()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_data = json_response(res, 200)["data"]["answerAdd"]

      user_fetch_data =
        context.user
        |> Map.put(:answers_count, context.user.answers_count + 1)
        |> fetch_data_with_model(Map.keys(answer_data["user"]))
        |> conver_datatimes_and_id_to_iso8601()
        |> Map.put("updatedAt", answer_data["user"]["updatedAt"])

      assert variables.tags == answer_data["tags"]
      assert user_fetch_data == answer_data["user"]
      assert Integer.to_string(variables.content_id) == answer_data["content"]["id"]
      assert variables.anonymous == answer_data["anonymous"]

      assert %{"desc" => variables.description, "users" => [user_fetch_data]} ==
               answer_data["description"]
    end

    test "answer_edit/1 returns the updated answer for", context do
      variables = %{
        anonymous: false,
        description: "@sosom some, updated description",
        tags: ["some_answer_tag_updated"],
        content_id: context.content.id
      }

      query = """
        mutation {
          answerEdit(id: #{context.answer.id}, anonymous: #{variables.anonymous}, description: "#{
        variables.description
      }", tags: ["#{Enum.join(variables.tags, ", ")}"]) {
              #{QueryHelpers.answer()}
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_data = json_response(res, 200)["data"]["answerEdit"]

      user_fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(answer_data["user"]))
        |> conver_datatimes_and_id_to_iso8601()
        |> Map.put("updatedAt", answer_data["user"]["updatedAt"])

      assert variables.tags == answer_data["tags"]
      assert Integer.to_string(variables.content_id) == answer_data["content"]["id"]
      assert user_fetch_data == answer_data["user"]
      assert variables.anonymous == answer_data["anonymous"]

      assert %{"desc" => variables.description, "users" => [user_fetch_data]} ==
               answer_data["description"]
    end

    test "answer_delete/1 returns the deleted answer", context do
      query = """
        mutation {
          answerDelete(id: #{context.answer.id}) {
            id
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      answer_data = json_response(res, 200)["data"]["answerDelete"]

      assert Integer.to_string(context.answer.id) == answer_data["id"]
    end

    test "comment_add/1 returns the new comment", context do
      variables = %{
        anonymous: false,
        answer_id: context.answer.id,
        content_id: context.content.id,
        description: "@sosom some, description"
      }

      query = """
        mutation {
          commentAdd(
            anonymous: #{variables.anonymous},
            description: "#{variables.description}", 
            contentId: #{variables.content_id},
            answerId: #{variables.answer_id}) {
                id
                anonymous
                answer {
                    id
                    commentsCount
                }
                description {
                    #{QueryHelpers.description()}
                }
                user {
                    #{QueryHelpers.user()}
                }
                contentId
                path
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comments_graphql_data = json_response(res, 200)["data"]["commentAdd"]

      user_data =
        context.user
        |> Map.put(:comments_count, context.user.comments_count + 1)
        |> model_fetch_user_data(comments_graphql_data)

      assert user_data == comments_graphql_data["user"]
      assert comments_graphql_data["id"] == comments_graphql_data["path"]
      assert variables.anonymous == comments_graphql_data["anonymous"]
      assert variables.content_id == comments_graphql_data["contentId"]
      assert Integer.to_string(variables.answer_id) == comments_graphql_data["answer"]["id"]
      assert context.answer.comments_count + 1 == comments_graphql_data["answer"]["commentsCount"]

      assert %{"desc" => variables.description, "users" => [user_data]} ==
               comments_graphql_data["description"]
    end

    test "comment_edit/1 returns the updated comment", context do
      variables = %{
        anonymous: true,
        comment_id: context.comment.id,
        content_id: context.content.id,
        description: "@sosom some, description"
      }

      query = """
        mutation {
          commentEdit(
            anonymous: #{variables.anonymous},
            description: "#{variables.description}", 
            contentId: #{variables.content_id},
            id: #{variables.comment_id}) {
              id
              anonymous
              description {
                  #{QueryHelpers.description()}
              }
              user {
                  #{QueryHelpers.user()}
              }
              contentId
              path
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comments_graphql_data = json_response(res, 200)["data"]["commentEdit"]

      user_data = model_fetch_user_data(context.user, comments_graphql_data)

      assert user_data == comments_graphql_data["user"]
      assert Integer.to_string(context.comment.id) == comments_graphql_data["id"]
      assert comments_graphql_data["id"] == comments_graphql_data["path"]
      assert variables.anonymous == comments_graphql_data["anonymous"]
      assert variables.content_id == comments_graphql_data["contentId"]

      assert %{"desc" => variables.description, "users" => [user_data]} ==
               comments_graphql_data["description"]
    end

    test "comment_delete/1 returns the deleted comment", context do
      query = """
        mutation {
          commentDelete(id: #{context.comment.id}) {
            id
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_data = json_response(res, 200)["data"]["commentDelete"]

      assert Integer.to_string(context.comment.id) == comment_data["id"]
    end

    test "add_reviews/1, add new reviews in content", context do
      query = """
        mutation {
          addReviews(id: #{context.content.id}) {
            reviews
            reviewsCount
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      comment_data = json_response(res, 200)["data"]["addReviews"]

      assert [context.user.id] == comment_data["reviews"]
      assert 1 == comment_data["reviewsCount"]
    end

    test "me_to/1 returns the list meTo, when follow", context do
      query = """
        mutation {
          meTo(contentId: #{context.content.id}, status: false) {
            meTo {
              #{QueryHelpers.user()}
              }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      data = List.first(json_response(res, 200)["data"]["meTo"]["meTo"])

      user_fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(data))
        |> conver_datatimes_and_id_to_iso8601()

      assert user_fetch_data == data
    end

    test "me_to/1 returns the list meTo, when unfollow", context do
      query_one = """
        mutation {
          meTo(contentId: #{context.content.id}, status: false) {
            meTo {
              #{QueryHelpers.user()}
              }
          }
        }
      """

      res = 
        context.conn 
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query_one))


      assert not Enum.empty?(json_response(res, 200)["data"]["meTo"]["meTo"])

      query = """
        mutation {
          meTo(contentId: #{context.content.id}, status: true) {
            meTo {
              #{QueryHelpers.user()}
              }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      assert [] == json_response(res, 200)["data"]["meTo"]["meTo"]
    end
  end
end
