defmodule KnowledgePhoenix.Mutation.SubscribeTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  import SmileDB.ContentHelpers

  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "subscribe" do
    test "subscribe_content/1 returns the list subscribedContents, when follow", context do
      query = """
        mutation {
          subscribeContent(contentId: #{context.content.id}, follow: false) {
              id 
              subscribedContents
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data = json_response(res, 200)["data"]["subscribeContent"]

      assert context.user.id == subscribe_data["id"]
      assert [context.content.id] == subscribe_data["subscribedContents"]
    end

    test "subscribe_content/1 returns the list subscribedContents when unfollow", context do
      query_one = """
        mutation {
          subscribeContent(contentId: #{context.content.id}, follow: false) {
              id 
              subscribedContents
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          subscribeContent(contentId: #{context.content.id}, follow: true) {
              id 
              subscribedContents
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data = json_response(res, 200)["data"]["subscribeContent"]

      assert context.user.id == subscribe_data["id"]
      assert [] == subscribe_data["subscribedContents"]
    end

    test "subscribe_tag/1 returns the list subscribeTags, when follow", context do
      query = """
        mutation {
          subscribeTag(tag: "#{List.first(context.content.tags)}", follow: false) {
              id 
              subscribedTags
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data = json_response(res, 200)["data"]["subscribeTag"]

      assert context.user.id == subscribe_data["id"]
      assert [List.first(context.content.tags)] == subscribe_data["subscribedTags"]
    end

    test "subscribe_tag/1 returns the list subscribeTags when unfollow", context do
      query_one = """
        mutation {
          subscribeTag(tag: "#{List.first(context.content.tags)}", follow: false) {
              id 
              subscribedTags
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          subscribeTag(tag: "#{List.first(context.content.tags)}", follow: true) {
              id 
              subscribedTags
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data = json_response(res, 200)["data"]["subscribeTag"]

      assert context.user.id == subscribe_data["id"]
      assert [] == subscribe_data["subscribedTags"]
    end

    test "subscribe_user/1 returns the list subscribeUsers, when follow", context do
      query = """
        mutation {
          subscribeUser(followerId: #{context.content.user_id}, follow: false) {
              subscribedUsers {
                #{QueryHelpers.user()}
              }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data =
        List.first(json_response(res, 200)["data"]["subscribeUser"]["subscribedUsers"])

      user_fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(subscribe_data))
        |> conver_datatimes_and_id_to_iso8601()

      assert user_fetch_data == subscribe_data
    end

    test "subscribe_user/1 returns the list subscribeUsers, when unfollow", context do
      query_one = """
        mutation {
          subscribeUser(followerId: #{context.content.user_id}, follow: false) {
              subscribedUsers {
                #{QueryHelpers.user()}
              }
          }
        }
      """

      post(context.conn, "/graphiql", AbsintheHelpers.mutation_skeleton(query_one))

      query = """
        mutation {
          subscribeUser(followerId: #{context.content.user_id}, follow: true) {
              subscribedUsers {
                #{QueryHelpers.user()}
              }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.mutation_skeleton(query))

      subscribe_data = json_response(res, 200)["data"]["subscribeUser"]

      assert [] == subscribe_data["subscribedUsers"]
    end
  end
end
