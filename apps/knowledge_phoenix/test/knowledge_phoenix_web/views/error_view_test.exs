defmodule KnowledgePhoenixWeb.ErrorViewTest do
  use KnowledgePhoenixWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "renders 404.json" do
    assert render(KnowledgePhoenixWeb.ErrorView, "404.json", []) ==
             %{errors: %{detail: "Page not found"}}
  end

  test "render 500.json" do
    assert render(KnowledgePhoenixWeb.ErrorView, "500.json", []) ==
             %{errors: %{detail: "Internal server error"}}
  end

  test "render 401.json" do
    assert render(KnowledgePhoenixWeb.ErrorView, "401.html", []) ==
             %{errors: %{detail: "Unauthorized"}}
  end

  test "render 422.json" do
    assert render(KnowledgePhoenixWeb.ErrorView, "422.html", []) ==
             %{errors: %{detail: "Unprocessable Entity"}}
  end
end
