defmodule KnowledgePhoenix.Query.ProfileTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  import SmileDB.ContentHelpers
  alias KnowledgePhoenix.AbsintheHelpers
  alias KnowledgePhoenix.QueryHelpers

  describe "profiles" do
    @tag auth: false
    test "get_profile_account_nfo/1 returns the user profile for NOT auth user ", context do
      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          accountInfo {
            id
            user {
              #{QueryHelpers.user()}
            }
            followers
            subscribedUsers {
              #{QueryHelpers.user()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      profile_data = json_response(res, 200)["data"]["profile"]["accountInfo"]

      user_fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(profile_data["user"]))
        |> conver_datatimes_and_id_to_iso8601()

      assert user_fetch_data == profile_data["user"]
      assert user_fetch_data["id"] == Integer.to_string(profile_data["id"])
      assert [] == profile_data["followers"]
      assert [] == profile_data["subscribedUsers"]
    end

    @tag auth: false
    test "get_profile_contents/1 returns the list user profile contents for NOT auth user ",
         context do
      current_content = Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          contents(after: null, limit: 1, orderBy: "newest") {
            entries {
              #{QueryHelpers.content()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      contents_graphql_data =
        json_response(res, 200)["data"]["profile"]["contents"]

      content_user_data =
        current_content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_count, 43)
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_user_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => content_user_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    test "get_profile_contents/1 returns the list user profile contents for auth user ",
         context do
      current_content = Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        profile {
          contents(after: null, limit: 1, orderBy: "newest") {
            entries {
              #{QueryHelpers.content()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      contents_graphql_data = json_response(res, 200)["data"]["profile"]["contents"]

      content_user_data =
        current_content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_count, 43)
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_user_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => content_user_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_profile_contents/1 returns the list user profile contents with sort by tags for NOT auth user ",
         context do
      current_content = Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          contents(after: null, limit: 1, orderBy: "newest", tags: ["#{
        List.first(context.content.tags)
      }"]) {
            entries {
              #{QueryHelpers.content()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      contents_graphql_data = json_response(res, 200)["data"]["profile"]["contents"]

      content_user_data =
        current_content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_count, 43)
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_user_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => content_user_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    test "get_profile_contents/1 returns the list user profile contents with sort by tags for auth user ",
         context do
      current_content = Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        profile {
          contents(after: null, limit: 1, orderBy: "newest", tags: ["#{
        List.first(context.content.tags)
      }"]) {
            entries {
              #{QueryHelpers.content()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      contents_graphql_data = json_response(res, 200)["data"]["profile"]["contents"]

      content_user_data =
        current_content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_count, 43)
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_user_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => content_user_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_profile_answers/1 returns the list user profile answers for a NOT auth user ",
         context do
      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          answers(after: null, limit: 1, order_by: "newest") {
            entries {
              #{QueryHelpers.answer()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      answers_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["answers"]["entries"])

      answer_data =
        context.answer
        |> Map.put(:comments_count, 44)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          answers_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          answers_graphql_data["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, answers_graphql_data["content"]))
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, answers_graphql_data))
        })

      assert graph_answers == answers_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["after"]
    end

    @tag auth: false
    test "get_profile_answers/1 returns the list user profile answers with sort by tags for a NOT auth user ",
         context do
      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          answers(after: null, limit: 1, order_by: "newest", tags: ["#{
        List.first(context.answer.tags)
      }"]) {
            entries {
              #{QueryHelpers.answer()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      answers_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["answers"]["entries"])

      answer_data =
        context.answer
        |> Map.put(:comments_count, 44)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          answers_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          answers_graphql_data["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, answers_graphql_data["content"]))
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, answers_graphql_data))
        })

      assert graph_answers == answers_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["after"]
    end

    test "get_profile_answers/1 returns the list user profile answers with sort by tags for a auth user ",
         context do
      query = """
      {
        profile {
          answers(after: null, limit: 1, order_by: "newest", tags: ["#{
        List.first(context.answer.tags)
      }"]) {
            entries {
              #{QueryHelpers.answer()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      answers_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["answers"]["entries"])

      answer_data =
        context.answer
        |> Map.put(:comments_count, 44)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          answers_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          answers_graphql_data["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, answers_graphql_data["content"]))
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, answers_graphql_data))
        })

      assert graph_answers == answers_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["after"]
    end

    test "get_profile_answers/1 returns the list user profile answers with sort by tags for auth user ",
         context do
      query = """
      {
        profile {
          answers(after: null, limit: 1, order_by: "newest") {
            entries {
              #{QueryHelpers.answer()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      answers_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["answers"]["entries"])

      answer_data =
        context.answer
        |> Map.put(:comments_count, 44)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          answers_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          answers_graphql_data["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, answers_graphql_data["content"]))
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, answers_graphql_data))
        })

      assert graph_answers == answers_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["answers"]["metadata"]["after"]
    end

    @tag auth: false
    test "get_profile_comments/1 returns the list user profile comments for NOT auth user ",
         context do
      query = """
      {
        profile(nickname: "#{context.user.nickname}") {
          comments(after: null, limit: 1, order_by: "newest") {
            entries {
              #{QueryHelpers.comment()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      comments_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["comments"]["entries"])

      comment_data =
        context.comment
        |> Map.put(:path, List.first(context.comment.path.labels))
        |> Map.put(:parent, nil)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      answer_data =
        context.answer
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data["answer"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          comments_graphql_data["answer"]["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
        )
        |> Map.put("description", %{
          "desc" => content_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
            )
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        })

      graph_comments =
        comment_data
        |> Map.put("answer", graph_answers)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, comments_graphql_data))
        })

      assert graph_comments == comments_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["after"]
    end

    test "get_profile_comments/1 returns the list user profile comments for auth user ",
         context do
      query = """
      {
        profile {
          comments(after: null, limit: 1, order_by: "newest") {
            entries {
              #{QueryHelpers.comment()}
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "profile"))

      comments_graphql_data =
        List.first(json_response(res, 200)["data"]["profile"]["comments"]["entries"])

      comment_data =
        context.comment
        |> Map.put(:path, List.first(context.comment.path.labels))
        |> Map.put(:parent, nil)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      answer_data =
        context.answer
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data["answer"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          comments_graphql_data["answer"]["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
        )
        |> Map.put("description", %{
          "desc" => content_data["description"],
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
            )
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" =>
            List.wrap(model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        })

      graph_comments =
        comment_data
        |> Map.put("answer", graph_answers)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => answer_data["description"],
          "users" => List.wrap(model_fetch_user_data(context.user, comments_graphql_data))
        })

      assert graph_comments == comments_graphql_data
      assert 1 == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["profile"]["comments"]["metadata"]["after"]
    end
  end
end
