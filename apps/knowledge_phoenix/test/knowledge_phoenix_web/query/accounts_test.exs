defmodule KnowledgePhoenix.Query.AccountsTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  import SmileDB.ContentHelpers
  alias SmileDB.Accounts.User
  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers
  alias KnowledgePhoenixWeb.Resolvers.Accounts

  describe "accounts" do
    test "find_profile_account/1 returns the user with given user.nickname", context do
      query = """
      {
        account {
          user {
            #{QueryHelpers.user()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user = json_response(res, 200)["data"]["account"]["user"]

      fetch_data = fetch_data_with_model(context.user, Map.keys(user))

      user_data =
        fetch_data
        |> Map.put("id", Integer.to_string(fetch_data["id"]))
        |> Map.put("insertedAt", DateTime.to_iso8601(fetch_data["insertedAt"]))
        |> Map.put("updatedAt", DateTime.to_iso8601(fetch_data["updatedAt"]))

      assert user_data == user
    end

    test "check_nickname/1 returns the user (True) and check where user.nickname Exists",
         context do
      query = """
      {
        checkNickname(nickname: "#{context.user.nickname}")
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "query"))
        |> json_response(200)

      assert true == res["data"]["checkNickname"]
    end

    test "check_nickname/1 returns the user (False) and check where user.nickname NOT Exists",
         context do
      query = """
      {
        checkNickname(nickname: "test")
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "query"))
        |> json_response(200)

      assert false == res["data"]["checkNickname"]
    end

    test "find_user/1 returns the user with given user by context", context do
      assert {:ok, %User{} = user} = Accounts.find_user(context.user, [], [])
      assert context.user == user
    end

    test "get_setting/1 returns the user with given user.nickname", context do
      query = """
      {
        account {
          setting {
            #{QueryHelpers.setting()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      setting = json_response(res, 200)["data"]["account"]["setting"]

      fetch_data = fetch_data_with_model(context.setting, Map.keys(setting))

      setting_data =
        fetch_data
        |> Map.put("id", Integer.to_string(fetch_data["id"]))
        |> Map.put("insertedAt", setting["insertedAt"])
        |> Map.put("updatedAt", setting["updatedAt"])

      assert setting_data == setting
    end

    test "get_subscribed_contents/1 returns the user with given user data", context do
      query = """
      {
        account {
          subscribedContents
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user_subscribed_contents =
        json_response(res, 200)["data"]["account"]["subscribedContents"]

      assert {:ok, subscribed_contents} = Accounts.get_subscribed_contents(context.user, [], [])
      assert user_subscribed_contents == subscribed_contents
    end

    test "get_subscribed_users/1 returns the user with given user data", context do
      query = """
      {
        account {
          subscribedUsers {
            #{QueryHelpers.user()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user_subscribed_users = json_response(res, 200)["data"]["account"]["subscribedUsers"]

      assert {:ok, subscribed_users} = Accounts.get_subscribed_users(context.user, [], [])
      assert user_subscribed_users == subscribed_users
    end

    test "get_subscribed_tags/1 returns the user with given user data", context do
      query = """
      {
        account {
          subscribedTags
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user_subscribed_tags = json_response(res, 200)["data"]["account"]["subscribedTags"]

      assert {:ok, subscribed_tags} = Accounts.get_subscribed_tags(context.user, [], [])
      assert user_subscribed_tags == subscribed_tags
    end

    test "get_followers/1 returns the user with given user data", context do
      query = """
      {
        account {
          followers
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      user_followers = json_response(res, 200)["data"]["account"]["followers"]

      assert {:ok, followers} = Accounts.get_followers(context.user, [], [])
      assert user_followers == followers
    end

    @tag auth: false
    test "find_users_by_nicknames/1 returns the user with given user data", context do
      query = """
      {
        userByNicknames(nicknames: ["#{context.user.nickname}"]) {
          #{QueryHelpers.user()}
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      users = List.first(json_response(res, 200)["data"]["userByNicknames"])

      fetch_data =
        context.user
        |> fetch_data_with_model(Map.keys(users))
        |> conver_datatimes_and_id_to_iso8601()

      assert fetch_data == users
    end
  end
end
