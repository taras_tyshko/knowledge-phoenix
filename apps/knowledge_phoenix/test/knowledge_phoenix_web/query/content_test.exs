defmodule KnowledgePhoenix.Query.ContentTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  import SmileDB.ContentHelpers
  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "contents" do
    @tag auth: false
    test "get_content/1 returns the content for not auth user ", context do
      current_content =
        Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        content(id: #{current_content.id}) {
          #{QueryHelpers.content()}
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      content_graphql_data = json_response(res, 200)["data"]

      content_data =
        current_content
        |> fetch_data_with_model(Map.keys(content_graphql_data["content"]))
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_data
        |> Map.put("meTo", [])
        |> Map.put("answersCount", 43)
        |> Map.put("user", model_fetch_user_data(context.user, content_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(model_fetch_user_data(context.user, content_graphql_data["content"]))
        })

      assert graph_content == content_graphql_data["content"]
    end

    @tag auth: false
    test "get_contents_pagination/1 returns the list contents with parametrs: (order_by: newest) ",
         context do
      current_content =
        Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        contents(after: null, limit: 1, order_by: NEWEST) {
          entries {
            #{QueryHelpers.content()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "contents"))

      contents_graphql_data = json_response(res, 200)["data"]["contents"]

      content_data =
        current_content
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_data
        |> Map.put("meTo", [])
        |> Map.put("answersCount", 43)
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_contents_pagination_tags/1 returns the list contents with parametrs: (tags: []]) ",
         context do
      current_content =
        Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        contents(after: null, limit: 1, order_by: NEWEST, tags: ["#{
        List.first(context.content.tags)
      }"]) {
          entries {
            #{QueryHelpers.content()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "contents"))

      contents_graphql_data = json_response(res, 200)["data"]["contents"]

      content_data =
        current_content
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_data
        |> Map.put("meTo", [])
        |> Map.put("answersCount", 43)
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_contents_pagination_nickname/1 returns the list contents with parametrs: (nicknames: []) ",
         context do
      current_content =
        Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        contents(after: null, limit: 1, order_by: NEWEST, nicknames: ["#{context.user.nickname}"]) {
          entries {
            #{QueryHelpers.content()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "contents"))

      contents_graphql_data = json_response(res, 200)["data"]["contents"]

      content_data =
        current_content
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_data
        |> Map.put("meTo", [])
        |> Map.put("answersCount", 43)
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_contents_pagination_nickname_and_tags/1 returns the list contents with parametrs: (nicknames: [], tags: []) ",
         context do
      current_content =
        Map.put(context.content, :answersId, [context.answer.id])

      query = """
      {
        contents(after: null, limit: 1, order_by: NEWEST, nicknames: ["#{context.user.nickname}"], tags: ["#{
        List.first(context.content.tags)
      }"]) {
          entries {
            #{QueryHelpers.content()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "contents"))

      contents_graphql_data = json_response(res, 200)["data"]["contents"]

      content_data =
        current_content
        |> fetch_data_with_model(
          contents_graphql_data["entries"]
          |> List.first()
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_content =
        content_data
        |> Map.put("meTo", [])
        |> Map.put("answersCount", 43)
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, List.first(contents_graphql_data["entries"]))
            )
        })
        |> List.wrap()

      assert graph_content == contents_graphql_data["entries"]
      assert 1 == contents_graphql_data["metadata"]["totalCount"]
      assert 1 == contents_graphql_data["metadata"]["limit"]
      assert nil == contents_graphql_data["metadata"]["after"]
    end

    @tag auth: false
    test "get_answers_pagination/1 returns the list answers", context do
      query = """
      {
        answers(after: null, limit: 1, order_by: NEWEST, content_id: #{
        context.answer.content_id
      }, ) {
          entries {
            #{QueryHelpers.answer()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "answers"))

      answers_graphql_data = List.first(json_response(res, 200)["data"]["answers"]["entries"])

      answer_data =
        context.answer
        |> Map.put(:ratings, [])
        |> fetch_data_with_model(
          answers_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          answers_graphql_data["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data["content"]))
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(model_fetch_user_data(context.user, answers_graphql_data["content"]))
        })

      graph_answers =
        answer_data
        |> Map.put("commentsCount", 44)
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, answers_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => convert_description(answer_data["description"]),
          "users" => List.wrap(model_fetch_user_data(context.user, answers_graphql_data))
        })

      assert graph_answers == answers_graphql_data

      assert 1 == json_response(res, 200)["data"]["answers"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["answers"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["answers"]["metadata"]["after"]
    end

    @tag auth: false
    test "get_comments_pagination/1 returns the list comments", context do
      query = """
      {
        comments(after: null, limit: 1, order_by: NEWEST, answerId: #{context.answer.id}, ) {
          entries {
            #{QueryHelpers.comment()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "comments"))

      comments_graphql_data = List.first(json_response(res, 200)["data"]["comments"]["entries"])

      comment_data =
        context.comment
        |> Map.put(:path, List.first(context.comment.path.labels))
        |> Map.put(:parent, nil)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      answer_data =
        context.answer
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data["answer"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          comments_graphql_data["answer"]["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
            )
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => convert_description(answer_data["description"]),
          "users" =>
            List.wrap(model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        })

      graph_comments =
        comment_data
        |> Map.put("answer", graph_answers)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => convert_description(answer_data["description"]),
          "users" => List.wrap(model_fetch_user_data(context.user, comments_graphql_data))
        })

      assert graph_comments == comments_graphql_data

      assert 1 == json_response(res, 200)["data"]["comments"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["comments"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["comments"]["metadata"]["after"]
    end

    @tag auth: false
    test "get_comments_pagination/1 returns the list comments when 'commentId' its", context do
      query = """
      {
        comments(after: null, limit: 1, order_by: NEWEST, answerId: #{context.answer.id}, commentId: #{
        context.comment.id
      }) {
          entries {
            #{QueryHelpers.comment()}
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "comments"))

      comments_graphql_data = json_response(res, 200)["data"]["comments"]["entries"] |> List.first()

      comment_data =
        context.comment
        |> Map.put(:path, List.first(context.comment.path.labels))
        |> Map.put(:parent, nil)
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      answer_data =
        context.answer
        |> Map.put(:ratings, %{dislikes: [], likes: []})
        |> fetch_data_with_model(
          comments_graphql_data["answer"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      content_data =
        context.content
        |> Map.put(:me_to, [])
        |> Map.put(:answers_id, [context.answer.id])
        |> fetch_data_with_model(
          comments_graphql_data["answer"]["content"]
          |> Map.keys()
        )
        |> conver_datatimes_and_id_to_iso8601()

      graph_contents =
        content_data
        |> Map.put(
          "user",
          model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
        )
        |> Map.put("description", %{
          "desc" => convert_description(content_data["description"]),
          "users" =>
            List.wrap(
              model_fetch_user_data(context.user, comments_graphql_data["answer"]["content"])
            )
        })

      graph_answers =
        answer_data
        |> Map.put("content", graph_contents)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => convert_description(answer_data["description"]),
          "users" =>
            List.wrap(model_fetch_user_data(context.user, comments_graphql_data["answer"]))
        })

      graph_comments =
        comment_data
        |> Map.put("answer", graph_answers)
        |> Map.put("user", model_fetch_user_data(context.user, comments_graphql_data))
        |> Map.put("ratings", %{"dislikes" => [], "likes" => []})
        |> Map.put("description", %{
          "desc" => convert_description(comment_data["description"]),
          "users" => List.wrap(model_fetch_user_data(context.user, comments_graphql_data))
        })

      assert graph_comments == comments_graphql_data

      assert 1 == json_response(res, 200)["data"]["comments"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["comments"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["comments"]["metadata"]["after"]
    end
  end

  describe "experts" do
    @tag auth: false
    test "get_experts/1 returns the list users who are experts for NOT auth user ", context do
      query = """
        {
          experts(after: null, limit: 1, tags: ["#{List.first(context.user.expert)}"]) {
            entries {
              id
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "experts"))

      data = List.first(json_response(res, 200)["data"]["experts"]["entries"])

      assert Integer.to_string(context.user.id) == data["id"]

      assert 2 == json_response(res, 200)["data"]["experts"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["experts"]["metadata"]["limit"]
      assert nil != json_response(res, 200)["data"]["experts"]["metadata"]["after"]
    end

    @tag auth: false
    test "get_experts/1 returns the list users who are experts, wher params (input) for NOT auth user ",
         context do
      query = """
        {
          experts(after: null, limit: 1, input: "#{context.user.first_name}") {
            entries {
              id
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "experts"))

      data = json_response(res, 200)["data"]["experts"]["entries"] |> List.first()

      assert Integer.to_string(context.user.id) == data["id"]
      assert 1 == json_response(res, 200)["data"]["experts"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["experts"]["metadata"]["limit"]
      assert true == is_binary(json_response(res, 200)["data"]["experts"]["metadata"]["after"])
    end

    @tag auth: false
    test "experts_autocomplete/1 returns the list users who are experts", context do
      query = """
        {
          expertsAutocomplete(limit: 2, input: "#{context.user.first_name}") {
            id
            nickname
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "experts"))

      data = json_response(res, 200)["data"]["expertsAutocomplete"]

      assert [%{"id" => Integer.to_string(context.user.id), "nickname" => context.user.nickname}] == data
    end
  end

  # todo Check Feed!!!
  describe "feed" do
    test "feed/1 returns the content for auth user ", context do
      query = """
      {
        feed(after: null, limit: 1, tags: ["#{List.first(context.content.tags)}"], nickname: []) {
          entries {
            content {
              id
              tags
            }
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = List.first(json_response(res, 200)["data"]["feed"]["entries"])

      assert Integer.to_string(context.content.id) == data["content"]["id"]
      assert context.content.tags == data["content"]["tags"]

      assert 1 == json_response(res, 200)["data"]["feed"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["feed"]["metadata"]["after"]
      assert 1 == json_response(res, 200)["data"]["feed"]["metadata"]["totalCount"]
    end

    test "feed/1 returns the answer for auth user ", context do
      query = """
      {
        feed(after: null, limit: 1, tags: ["#{List.first(context.answer.tags)}"], nickname: []) {
          entries {
            answer {
              id
              tags
            }
          }
          metadata {
            #{QueryHelpers.metadata()}
          }
        }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = List.first(json_response(res, 200)["data"]["feed"]["entries"])

      assert Integer.to_string(context.answer.id) == data["answer"]["id"]
      assert context.answer.tags == data["answer"]["tags"]

      assert 1 == json_response(res, 200)["data"]["feed"]["metadata"]["limit"]
      assert nil == json_response(res, 200)["data"]["feed"]["metadata"]["after"]
      assert 1 == json_response(res, 200)["data"]["feed"]["metadata"]["totalCount"]
    end
  end

  describe "scopedData" do
    test "content_scoped_data/1 returns the tags and users", context do
      query = """
        {
          contentScopedData(contentId: #{context.content.id}) {
            tags(limit: 2) 
            users(limit: 1) {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["contentScopedData"]

      user_data =
        context.user_one
        |> fetch_data_with_model(Map.keys(List.first(data["users"])))
        |> conver_datatimes_and_id_to_iso8601()

      assert context.content.tags == data["tags"]
      assert [user_data] == data["users"]
    end

    test "scoped_data/1 returns the tags and list users", context do
      query = """
        {
          scopedData(tags: ["#{List.first(context.content.tags)}"]) {
            tags(limit: 2) 
            users(limit: 1) {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["scopedData"]

      user_data =
        context.user
        |> fetch_data_with_model(Map.keys(List.first(data["users"])))
        |> conver_datatimes_and_id_to_iso8601()

      assert [] == data["tags"]
      assert [user_data] == data["users"]
    end

    test "scoped_data/1 returns the list tags and list users", context do
      query = """
        {
          scopedData(nicknames: ["#{context.user.nickname}"]) {
            tags(limit: 2) 
            users(limit: 1) {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["scopedData"]

      assert [List.last(context.content.tags)] == data["tags"]
      assert [] == data["users"]
    end

    test "scoped_data/1 returns the list tags and list users when twice params (tags, nicknames)",
         context do
      query = """
        {
          scopedData(nicknames: ["#{context.user.nickname}"], tags: ["#{
        List.first(context.content.tags)
      }"]) {
            tags(limit: 2) 
            users(limit: 1) {
              #{QueryHelpers.user()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["scopedData"]

      user_data =
        context.user
        |> fetch_data_with_model(Map.keys(List.first(data["users"])))
        |> conver_datatimes_and_id_to_iso8601()

      assert [] == data["tags"]
      assert [user_data] == data["users"]
    end
  end

  describe "users" do
    test "list_users/1 returns the list users with pagination", context do
      query = """
        {
          users(after: null, limit: 1, input: "#{context.user.first_name}") {
            entries {
              id
              nickname
            }
            metadata {
              #{QueryHelpers.metadata()}
            }
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["users"]["entries"] |> List.first()

      assert Integer.to_string(context.user.id) == data["id"]
      assert context.user.nickname == data["nickname"]
      assert 1 == json_response(res, 200)["data"]["users"]["metadata"]["limit"]
      assert 1 == json_response(res, 200)["data"]["users"]["metadata"]["totalCount"]
      assert nil == json_response(res, 200)["data"]["users"]["metadata"]["after"]
    end

    test "users_autocomplete/1 returns the list users about usersAutocomplete", context do
      query = """
        {
          usersAutocomplete(limit: 1, input: "#{context.user.first_name}") {
            id
            nickname
          }
        }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "content"))

      data = json_response(res, 200)["data"]["usersAutocomplete"]

      assert [%{"id" => Integer.to_string(context.user.id), "nickname" => context.user.nickname}] == data
    end
  end
end
