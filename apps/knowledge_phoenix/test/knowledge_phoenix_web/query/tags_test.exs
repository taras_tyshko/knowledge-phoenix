defmodule KnowledgePhoenix.Query.TagsTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true

  alias KnowledgePhoenix.QueryHelpers
  alias KnowledgePhoenix.AbsintheHelpers

  describe "accounts" do
    @tag auth: false
    test "tags/1 returns the tags with paginations with params (input)", context do
      query = """
      {
          tags(after: null, limit: 1, input: "#{List.last(context.content.tags)}") {
              entries {
                  #{QueryHelpers.tag()}
              }
              metadata {
                  #{QueryHelpers.metadata()}
              }
          }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      data = json_response(res, 200)["data"]["tags"]["entries"] |> List.first()

      assert List.last(context.content.tags) == data["title"]
      assert nil == json_response(res, 200)["data"]["tags"]["metadata"]["after"]
      assert 1 == json_response(res, 200)["data"]["tags"]["metadata"]["totalCount"]
      assert 1 == json_response(res, 200)["data"]["tags"]["metadata"]["limit"]
    end

    @tag auth: false
    test "tags/1 returns the tags with paginations", context do
      query = """
      {
          tags(after: null, limit: 2) {
              entries {
                  #{QueryHelpers.tag()}
              }
              metadata {
                  #{QueryHelpers.metadata()}
              }
          }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      data = json_response(res, 200)["data"]["tags"]["entries"] |> List.first()

      assert List.last(context.content.tags) == data["title"]
      assert nil == json_response(res, 200)["data"]["tags"]["metadata"]["after"]
      assert 2 == json_response(res, 200)["data"]["tags"]["metadata"]["totalCount"]
      assert 2 == json_response(res, 200)["data"]["tags"]["metadata"]["limit"]
    end

    @tag auth: false
    test "tags_autocomplete/1 returns the list tags", context do
      query = """
      {
          tagsAutocomplete(limit: 2, input: "#{List.first(context.content.tags)}")
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      assert [List.first(context.content.tags)] ==
               json_response(res, 200)["data"]["tagsAutocomplete"]
    end

    test "tag_experts/1 returns the list anothersExperts = [] and existsExperts = data",
         context do
      query = """
      {
          tagExperts(limit: 2, input: "#{context.user.nickname}", tags: ["#{
        List.first(context.content.tags)
      }"]) {
              anothersExperts {
                  avatar
                  name
                  nickname
              }
              existsExperts {
                  avatar
                  name
                  nickname
              }
          }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      data = json_response(res, 200)["data"]["tagExperts"]["existsExperts"] |> List.first()
      assert [] == json_response(res, 200)["data"]["tagExperts"]["anothersExperts"]
      assert context.user.nickname == data["nickname"]
      assert "#{context.user.first_name} #{context.user.last_name}" == data["name"]
      assert context.user.avatar == data["avatar"]
    end

    test "tag_experts/1 returns the list anothersExperts = data and existsExperts = []",
         context do
      query = """
      {
          tagExperts(limit: 2, input: "#{context.user.nickname}") {
              anothersExperts {
                  avatar
                  name
                  nickname
              }
              existsExperts {
                  avatar
                  name
                  nickname
              }
          }
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      data = json_response(res, 200)["data"]["tagExperts"]["anothersExperts"] |> List.first()
      assert [] == json_response(res, 200)["data"]["tagExperts"]["existsExperts"]
      assert context.user.nickname == data["nickname"]
      assert "#{context.user.first_name} #{context.user.last_name}" == data["name"]
      assert context.user.avatar == data["avatar"]
    end

    test "tags_autocomplete/1 returns the list tags for tagsAutocomplete", context do
      query = """
      {
          tagsAutocomplete(limit: 1, input: "#{List.first(context.content.tags)}") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "tags"))

      data = json_response(res, 200)["data"]["tagsAutocomplete"]
      assert [List.first(context.content.tags)] == data
    end
  end
end
