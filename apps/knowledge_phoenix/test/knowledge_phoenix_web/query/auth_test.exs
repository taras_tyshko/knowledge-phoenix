defmodule KnowledgePhoenix.Query.AuthTest do
  use KnowledgePhoenixWeb.ConnCase
  use ExUnit.Case, async: true
  use Plug.Test

  alias KnowledgePhoenix.AbsintheHelpers

  @facebook_code "EAAH9aVyjs7ABANGiZCZC7zGjs71VY5LZCZCAcIMIhK4wvG0um5p8rMfqIIUu7v0W9ElJidsSgiKkDbK4wEQSEW4BL6buBJXXN5fANUd8H6dVkHPa2rO6zLLTC511j5fNHvyNRDyF7EdpRiexeonWYvRnCwRh8MjAlHRquQRyXv7D30NU5ZBfCvxIzFR7b8l5lA64nXk6bDJqWUw18dwjsc3vglcLxSxBcGKXZA32jWYwZDZD"
  @google_code "ya29.GltOBoXRZVVd-lR1g0i4xCugj1qBB_2byHtli0cZszHkGBBuWwsi0xIPgHCm4eWQRk2lK3uvgIETV19l_zA-RMkjqU4ASKu2YLfAQN9W2m6Y4s-QqhV8fQO1_KgP"
  describe "auth" do
    @tag auth: false
    test "auth/1 returns the user token for auth when provider 'Facebook'.", context do
      query = """
      {
        auth(code: "#{@facebook_code}", provider: "facebook") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      assert nil != json_response(res, 200)["data"]
    end

    # @tag skip: true
    @tag auth: false
    test "auth/1 returns the user token for auth when provider 'Google'.", context do
      # To successfully pass the test you need to check the urgency of the access code from the 'Google' side. 
      #   OAuth 2.0 Playground https://developers.google.com/oauthplayground
      #
      # Test account with the following approaches was created for this test:
      #   Credentials: [
      #     login: knowledge.phoenix.smile@gmail.com
      #     password: JoE-A2V-JDM-pHN
      #   ]
      query = """
      {
        auth(code: "#{@google_code}", provider: "google") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      assert nil != json_response(res, 200)["data"]["auth"]
    end

    @tag auth: false
    test "connect/1 returns the user for connect provider 'Facebook' and 'Google'.", context do
      # Create user!
      query = """
      {
        auth(code: "#{@google_code}", provider: "google") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["auth"]

      # Put Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Connect other provider to current user.
      query = """
      {
        connect(code: "#{@facebook_code}", provider: "facebook") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["connect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          id
        } 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      json_response(res, 200)["data"]["account"]
    end

    @tag auth: false
    test "connect/1 returns the user for connect provider 'Google' and 'Facebook'.", context do
      # Create user!
      query = """
      {
        auth(code: "#{@facebook_code}", provider: "facebook") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["auth"]

      # Put Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Connect other provider to current user.
      query = """
      {
        connect(code: "#{@google_code}", provider: "google") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["connect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          id
        } 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      json_response(res, 200)["data"]["account"]
    end

    @tag auth: false
    test "disconnect/1 returns the user for disconnect provider 'Google' and 'Facebook'.",
         context do
      # Create user!
      query = """
      {
        auth(code: "#{@facebook_code}", provider: "facebook") 
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["auth"]

      # Put Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Connect other provider to current user.
      query = """
      {
        connect(code: "#{@google_code}", provider: "google") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["connect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          id
        } 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      json_response(res, 200)["data"]["account"]
      # Disconnect other provider to current user.
      query = """
      {
        disconnect(provider: "google") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["disconnect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          user {
            google_uid
          }
        }
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      assert nil == json_response(res, 200)["data"]["account"]["user"]["google_uid"]
    end

    @tag auth: false
    test "disconnect/1 returns the user for disconnect provider 'Facebook' and 'Google'.",
         context do
      # Create user!
      query = """
      {
        auth(code: "#{@google_code}", provider: "google")
      }
      """

      res =
        context.conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["auth"]

      # Put Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Connect other provider to current user.
      query = """
      {
        connect(code: "#{@facebook_code}", provider: "facebook") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["connect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          id
        } 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      json_response(res, 200)["data"]["account"]
      # Disconnect other provider to current user.
      query = """
      {
        disconnect(provider: "facebook") 
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "auth"))

      jwt = json_response(res, 200)["data"]["disconnect"]

      # Change new Token in conn.
      conn =
        Phoenix.ConnTest.build_conn()
        |> Plug.Conn.put_req_header("authorization", "Bearer #{jwt}")
        |> Plug.Conn.put_req_header("content-type", "application/json")

      # Get user data.
      query = """
      {
        account {
          user {
            facebook_uid
          }
        }
      }
      """

      res =
        conn
        |> post("/graphiql", AbsintheHelpers.query_skeleton(query, "account"))

      assert nil == json_response(res, 200)["data"]["account"]["user"]["google_uid"]
    end
  end
end
