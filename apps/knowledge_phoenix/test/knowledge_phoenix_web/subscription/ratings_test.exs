defmodule KnowledgePhoenix.Subscription.RatingTest do
  use KnowledgePhoenixWeb.SubscriptionCase
  alias KnowledgePhoenix.QueryHelpers

  describe "subscribe" do
    test "answerLikes/1 add answerLikes and check subscription with parametrs (follow: false)", %{
      socket: socket,
      answer: answer
    } do
      subscription = """
          subscription ($topic: String!) {
              answerLikes(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean!) {
              addAnswerLikes(id: $id, status: $status) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => false})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "answerLikes/1 add answerLikes and check subscription with parametrs (follow: true)", %{
      socket: socket,
      answer: answer
    } do
      subscription = """
          subscription ($topic: String!) {
              answerLikes(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean!) {
              addAnswerLikes(id: $id, status: $status) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => true})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "answerLikes/1 add answerLikes and check subscription with parametrs (follow: true and null)",
         %{socket: socket, answer: answer} do
      subscription = """
          subscription ($topic: String!) {
              answerLikes(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean) {
              addAnswerLikes(id: $id, status: $status) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => true})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push

      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => nil})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "answerLikes/1 add answerLikes and check subscription with parametrs (follow: false and null)",
         %{socket: socket, answer: answer} do
      subscription = """
          subscription ($topic: String!) {
              answerLikes(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean) {
              addAnswerLikes(id: $id, status: $status) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => false})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push

      ref = push_doc(socket, mutation, variables: %{"id" => answer.id, "status" => nil})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addAnswerLikes" => addAnswerLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"answerLikes" => addAnswerLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "commentLikes/1 add commentLikes and check subscription with parametrs (follow: false)",
         %{socket: socket, comment: comment} do
      subscription = """
          subscription ($topic: String!) {
              commentLikes(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean!) {
              addCommentLikes(id: $id, status: $status) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => false})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "commentLikes/1 add commentLikes and check subscription with parametrs (follow: true)",
         %{socket: socket, comment: comment} do
      subscription = """
          subscription ($topic: String!) {
              commentLikes(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean!) {
              addCommentLikes(id: $id, status: $status) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => true})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "commentLikes/1 add commentLikes and check subscription with parametrs (follow: true and null)",
         %{socket: socket, comment: comment} do
      subscription = """
          subscription ($topic: String!) {
              commentLikes(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean) {
              addCommentLikes(id: $id, status: $status) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => true})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push

      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => nil})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "commentLikes/1 add commentLikes and check subscription with parametrs (follow: false and null)",
         %{socket: socket, comment: comment} do
      subscription = """
          subscription ($topic: String!) {
              commentLikes(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $status: Boolean) {
              addCommentLikes(id: $id, status: $status) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => false})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push

      ref = push_doc(socket, mutation, variables: %{"id" => comment.id, "status" => nil})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addCommentLikes" => addCommentLikes}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"commentLikes" => addCommentLikes}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end
end
