defmodule KnowledgePhoenix.Subscription.SubscribeTest do
  use KnowledgePhoenixWeb.SubscriptionCase

  alias KnowledgePhoenix.QueryHelpers

  describe "subscribe" do
    test "contentSubscribe/1 add contentSubscribe and check subscription with parametrs (follow: false)",
         %{socket: socket, content: content} do
      subscription = """
          subscription ($topic: String!) {
              contentSubscribe(topic: $topic) {
                  subscribedContents
              }
          }
      """

      mutation = """
          mutation ($contentId: Int!, $follow: Boolean!) {
              subscribeContent(contentId: $contentId, follow: $follow) {
                  subscribedContents
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"contentId" => content.id, "follow" => false})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeContent" => subscribeContent}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"contentSubscribe" => subscribeContent}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "contentSubscribe/1 add contentSubscribe and check subscription with parametrs (follow: true)",
         %{socket: socket, content: content} do
      mutation_create = """
          mutation ($contentId: Int!, $follow: Boolean!) {
              subscribeContent(contentId: $contentId, follow: $follow) {
                  subscribedContents
              }
          }
      """

      # run a mutation to trigger the subscription
      push_doc(
        socket,
        mutation_create,
        variables: %{"contentId" => content.id, "follow" => false}
      )

      subscription = """
          subscription ($topic: String!) {
              contentSubscribe(topic: $topic) {
                  subscribedContents
              }
          }
      """

      mutation = """
          mutation ($contentId: Int!, $follow: Boolean!) {
              subscribeContent(contentId: $contentId, follow: $follow) {
                  subscribedContents
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"contentId" => content.id, "follow" => true})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeContent" => subscribeContent}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"contentSubscribe" => subscribeContent}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "tagSubscribe/1 add tagSubscribe and check subscription with parametrs (follow: false)",
         %{socket: socket, content: content} do
      subscription = """
          subscription ($topic: String!) {
              tagSubscribe(topic: $topic) {
                  subscribedTags
              }
          }
      """

      mutation = """
          mutation ($tag: String!, $follow: Boolean!) {
              subscribeTag(tag: $tag, follow: $follow) {
                  subscribedTags
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{"tag" => List.first(content.tags), "follow" => false}
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeTag" => subscribeTag}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"tagSubscribe" => subscribeTag}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "tagSubscribe/1 add tagSubscribe and check subscription with parametrs (follow: true)",
         %{socket: socket, content: content} do
      mutation_one = """
          mutation ($tag: String!, $follow: Boolean!) {
              subscribeTag(tag: $tag, follow: $follow) {
                  subscribedTags
              }
          }
      """

      # run a mutation to trigger the subscription
      push_doc(
        socket,
        mutation_one,
        variables: %{"tag" => List.first(content.tags), "follow" => false}
      )

      subscription = """
          subscription ($topic: String!) {
              tagSubscribe(topic: $topic) {
                  subscribedTags
              }
          }
      """

      mutation = """
          mutation ($tag: String!, $follow: Boolean!) {
              subscribeTag(tag: $tag, follow: $follow) {
                  subscribedTags
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{"tag" => List.first(content.tags), "follow" => true}
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeTag" => subscribeTag}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"tagSubscribe" => subscribeTag}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "userSubscribe/1 add userSubscribe and check subscription with parametrs (follow: false)",
         %{socket: socket, user: user} do
      subscription = """
          subscription ($topic: String!) {
              userSubscribe(topic: $topic) {
                  subscribedUsers {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($followerId: Int!, $follow: Boolean!) {
              subscribeUser(followerId: $followerId, follow: $follow) {
                  subscribedUsers {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"followerId" => user.id, "follow" => false})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeUser" => subscribeUser}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"userSubscribe" => subscribeUser}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "userSubscribe/1 add userSubscribe and check subscription with parametrs (follow: true)",
         %{socket: socket, user: user} do
      mutation_one = """
          mutation ($followerId: Int!, $follow: Boolean!) {
              subscribeUser(followerId: $followerId, follow: $follow) {
                  subscribedUsers {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      # run a mutation to trigger the subscription
      push_doc(socket, mutation_one, variables: %{"followerId" => user.id, "follow" => false})

      subscription = """
          subscription ($topic: String!) {
              userSubscribe(topic: $topic) {
                  subscribedUsers {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($followerId: Int!, $follow: Boolean!) {
              subscribeUser(followerId: $followerId, follow: $follow) {
                  subscribedUsers {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"followerId" => user.id, "follow" => true})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"subscribeUser" => subscribeUser}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"userSubscribe" => subscribeUser}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end
end
