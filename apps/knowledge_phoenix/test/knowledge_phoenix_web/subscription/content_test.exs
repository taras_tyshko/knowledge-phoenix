defmodule KnowledgePhoenix.Subscription.ContentTest do
  use KnowledgePhoenixWeb.SubscriptionCase

  alias KnowledgePhoenix.QueryHelpers

  describe "content" do
    test "addContent/1 add new content and check subscription", %{socket: socket} do
      subscription = """
          subscription ($topic: String!) {
              addContent(topic: $topic) {
                  #{QueryHelpers.content()}
              }
          }
      """

      mutation = """
          mutation ($anonymous: Boolean!, $description: String, $tags: [String], $title: String!) {
              contentAdd(anonymous: $anonymous, description: $description, tags: $tags, title: $title) {
                  #{QueryHelpers.content()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "anonymous" => false,
            "description" => "some description",
            "tags" => ["some_tag"],
            "title" => "some title"
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"contentAdd" => contentAdd}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"addContent" => contentAdd}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "editContent/1 add update content and check subscription", %{
      socket: socket,
      content: content
    } do
      subscription = """
          subscription ($topic: String!) {
              editContent(topic: $topic) {
                  #{QueryHelpers.content()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $anonymous: Boolean!, $description: String, $tags: [String], $title: String!) {
              contentEdit(id: $id, anonymous: $anonymous, description: $description, tags: $tags, title: $title) {
                  #{QueryHelpers.content()} 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "id" => content.id,
            "anonymous" => true,
            "description" => "some updated description",
            "tags" => ["some_updated_tag"],
            "title" => "some updated title"
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"contentEdit" => contentEdit}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"editContent" => contentEdit}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "deleteContent/1 delete content and check subscription", %{
      socket: socket,
      content: content
    } do
      subscription = """
          subscription ($topic: String!) {
              deleteContent(topic: $topic) {
                  id
              }
          }
      """

      mutation = """
          mutation ($id: Int!) {
              contentDelete(id: $id) {
                  id 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => content.id})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"contentDelete" => contentDelete}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"deleteContent" => contentDelete}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "addAnswer/1 add new answer and check subscription", %{
      socket: socket,
      content: content
    } do
      subscription = """
          subscription ($topic: String!) {
              addAnswer(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($contentId: Int!, $anonymous: Boolean!, $description: String, $tags: [String]) {
              answerAdd(contentId: $contentId, anonymous: $anonymous, description: $description, tags: $tags) {
                  #{QueryHelpers.answer()}    
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "contentId" => content.id,
            "anonymous" => false,
            "description" => "some description",
            "tags" => ["some_tag"]
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"answerAdd" => answerAdd}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"addAnswer" => answerAdd}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "answerEdit/1 add update answer and check subscription", %{
      socket: socket,
      answer: answer
    } do
      subscription = """
          subscription ($topic: String!) {
              editAnswer(topic: $topic) {
                  #{QueryHelpers.answer()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $anonymous: Boolean!, $description: String, $tags: [String]) {
              answerEdit(id: $id, anonymous: $anonymous, description: $description, tags: $tags) {
                  #{QueryHelpers.answer()} 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "id" => answer.id,
            "anonymous" => true,
            "description" => "some updated description",
            "tags" => ["some_updated_tag"]
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"answerEdit" => answerEdit}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"editAnswer" => answerEdit}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "deleteAnswer/1 delete content and check subscription", %{
      socket: socket,
      answer: answer
    } do
      subscription = """
          subscription ($topic: String!) {
              deleteAnswer(topic: $topic) {
                  id
              }
          }
      """

      mutation = """
          mutation ($id: Int!) {
              answerDelete(id: $id) {
                  id 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => answer.id})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"answerDelete" => answerDelete}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"deleteAnswer" => answerDelete}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "addComment/1 add new comment and check subscription", %{
      socket: socket,
      content: content,
      answer: answer
    } do
      subscription = """
          subscription ($topic: String!) {
              addComment(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($answerId: Int!, $contentId: Int!, $anonymous: Boolean!, $description: String) {
              commentAdd(answerId: $answerId, contentId: $contentId, anonymous: $anonymous, description: $description) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "answerId" => answer.id,
            "contentId" => content.id,
            "anonymous" => false,
            "description" => "some description",
            "tags" => ["some_tag"]
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"commentAdd" => commentAdd}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"addComment" => commentAdd}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "commentEdit/1 add update comment and check subscription", %{
      socket: socket,
      content: content,
      comment: comment
    } do
      subscription = """
          subscription ($topic: String!) {
              editComment(topic: $topic) {
                  #{QueryHelpers.comment()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!, $contentId: Int!, $anonymous: Boolean!, $description: String) {
              commentEdit(id: $id, contentId: $contentId, anonymous: $anonymous, description: $description) {
                  #{QueryHelpers.comment()} 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "id" => comment.id,
            "contentId" => content.id,
            "anonymous" => true,
            "description" => "some updated description"
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"commentEdit" => commentEdit}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"editComment" => commentEdit}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "deleteComment/1 delete comment and check subscription", %{
      socket: socket,
      comment: comment
    } do
      subscription = """
          subscription ($topic: String!) {
              deleteComment(topic: $topic) {
                  id
              }
          }
      """

      mutation = """
          mutation ($id: Int!) {
              commentDelete(id: $id) {
                  id 
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => comment.id})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"commentDelete" => commentDelete}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"deleteComment" => commentDelete}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "addReviews/1 add Reviews and check subscription", %{socket: socket, content: content} do
      subscription = """
          subscription ($topic: String!) {
              reviewsContent(topic: $topic) {
                  #{QueryHelpers.content()}
              }
          }
      """

      mutation = """
          mutation ($id: Int!) {
              addReviews(id: $id) {
                  #{QueryHelpers.content()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: %{"id" => content.id})
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"addReviews" => addReviews}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"reviewsContent" => addReviews}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "meTo/1 add meTo and check subscription with parametrs (status: false)", %{
      socket: socket,
      content: content
    } do
      subscription = """
          subscription ($topic: String!) {
              meTo(topic: $topic) {
                  #{QueryHelpers.content()}
              }
          }
      """

      mutation = """
          mutation ($contentId: Int!, $status: Boolean!) {
              meTo(contentId: $contentId, status: $status) {
                  #{QueryHelpers.content()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"contentId" => content.id, "status" => false})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"meTo" => meTo}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"meTo" => meTo}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "meTo/1 add meTo and check subscription with parametrs (status: false and true)", %{
      socket: socket,
      content: content
    } do
      subscription = """
          subscription ($topic: String!) {
              meTo(topic: $topic) {
                  #{QueryHelpers.content()}
              }
          }
      """

      mutation = """
          mutation ($contentId: Int!, $status: Boolean!) {
              meTo(contentId: $contentId, status: $status) {
                  #{QueryHelpers.content()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(socket, mutation, variables: %{"contentId" => content.id, "status" => false})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"meTo" => meTo}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"meTo" => meTo}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push

      ref =
        push_doc(socket, mutation, variables: %{"contentId" => content.id, "status" => true})

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"meTo" => meTo}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"meTo" => meTo}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end
end
