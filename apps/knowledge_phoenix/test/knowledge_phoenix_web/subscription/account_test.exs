defmodule KnowledgePhoenix.Subscription.AccountTest do
  use KnowledgePhoenixWeb.SubscriptionCase

  alias KnowledgePhoenix.QueryHelpers

  describe "account" do
    test "account_setting/1 test subscription on updated account Setting", %{socket: socket} do
      subscription = """
          subscription ($topic: String!) {
              userSettings(topic: $topic) {
                  #{QueryHelpers.setting()}
              }
          }
      """

      mutation = """
          mutation ($byAnswers: Boolean, $byComments: Boolean, $emailEnable: Boolean, $emailOnReply: Boolean, $webNotifMentions: Boolean, $webNotifRecommend: Boolean, $webNotifUpvote: Boolean) {
              accountSetting(byAnswers: $byAnswers, byComments: $byComments, emailEnable: $emailEnable, emailOnReply: $emailOnReply, webNotifMentions: $webNotifMentions, webNotifRecommend: $webNotifRecommend, webNotifUpvote: $webNotifUpvote) {  
                  #{QueryHelpers.setting()}
              }
          }
      """

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref =
        push_doc(
          socket,
          mutation,
          variables: %{
            "byAnswers" => false,
            "byComments" => false,
            "emailEnable" => false,
            "emailOnReply" => false,
            "webNotifMentions" => false,
            "webNotifRecommend" => false,
            "webNotifUpvote" => false
          }
        )

      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"accountSetting" => accountSetting}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"userSettings" => accountSetting}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end

    test "user_accounts/1 test subscription on updated account User", %{socket: socket} do
      subscription = """
          subscription ($topic: String!) {
              userAccounts(topic: $topic) {
                  id
                  user {
                      #{QueryHelpers.user()}
                  }
              }
          }
      """

      mutation = """
          mutation ($authProvider: String, $biography: String, $birthday: String, $email: String, $firstName: String, $gender: String, $lastName: String, $location: String, $nickname: String, $status: String, $website: String) {
              accountUser(authProvider: $authProvider, biography: $biography, birthday: $birthday, email: $email, firstName: $firstName, gender: $gender, lastName: $lastName, location: $location, nickname: $nickname, status: $status, website: $website) {  
                  id
                  user {
                      #{QueryHelpers.user()}
                  }                    
              }
          }
      """

      variables = %{
        "authProvider" => "google",
        "biography" => "some updated biography",
        "birthday" => "some updated birthday",
        "email" => "some email",
        "firstName" => "some updated firstName",
        "gender" => "some updated gender",
        "lastName" => "some updated lastName",
        "location" => "some updated location",
        "nickname" => "some_updated_nickname",
        "status" => "some updated status",
        "website" => "some updated website"
      }

      # setup a subscription
      ref = push_doc(socket, subscription, variables: %{"topic" => "room:lobby"})
      assert_reply(ref, :ok, %{subscriptionId: subscription_id}, 1_000)
      # Rest of test case

      # run a mutation to trigger the subscription
      ref = push_doc(socket, mutation, variables: variables)
      assert_reply(ref, :ok, reply, 1_000)
      assert %{data: %{"accountUser" => accountUser}} = reply

      # check to see if we got subscription data
      expected = %{
        result: %{data: %{"userAccounts" => accountUser}},
        subscriptionId: subscription_id
      }

      assert_push("subscription:data", push)
      assert expected == push
    end
  end
end
